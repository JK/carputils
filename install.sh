#!/bin/bash

### PACKAGE DEPENDENCIES TO CARPUTILS ###
#########################################
# Listed below, you will find the minimum Python package 
# requirements for running carputils. 
#
# Please do not add other dependencies which may arise
# from your local system setup!!!
 

### INSTALLATION ###
####################
# Subsequent command installs the stated Python packages into
# user space!
#
# Consider the possibility to lock a package version. New package
# versions may break with their previous behavior. In such case, add
# the latest functional version!
#
# Examples:
#   numpy==v1.14.5
#   matplotlib==2.1.0


#PIP=pip2
PIP=pip3

cmd="$PIP install --user -r requirements.txt"

echo $cmd
eval $cmd
