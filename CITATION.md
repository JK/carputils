## Attribution

! If you use this software, please cite it as follows:
! 
! *Vigmond, E.J., Hughes, M., Plank, G., Leon L.J. (2003). Computational tools for modeling electrical activity in cardiac tissue. J Electrocardiol. 2003;36 Suppl:69-74.*

```bibtex
@article{carp,
author    = {Vigmond, E. J. and Hughes, M. and Plank, G. and Leon, L. J.},
title     = {Computational tools for modeling electrical activity in cardiac tissue},
journal   = {J Electrocardiol},
year      = {2003},
volume    = {36 Suppl},
pages     = {69--74},
doi       = {10.1016/j.jelectrocard.2003.09.017},
}
```

Please check this information regularly, the relevant citation will be updated soon with an openCARP specific publication.

