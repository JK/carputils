## Installing carputils

### Linux / macOS
To locally run carputils, first install [Python3](https://www.python.org/downloads/), [pip](https://pip.pypa.io/en/stable/installing/), and [openCARP](https://opencarp.org/download/installation#building-from-source).

carputils has already been included to `openCARP/external/carputils` if you [installed openCARP with additional tools](https://opencarp.org/download/installation#building-using-cmake).
Otherwise, clone the carputils repository and enter the codebase folder `carputils`.
```
git clone https://git.opencarp.org/openCARP/carputils.git
cd carputils
```

#### Installation
This section explains how to build carputils and install it using `pip`.
This is recommended if you do not expect to develop carputils.

In the carputils directory, to install carputils in the Python user-space, run
```
python3 -m pip install . --user
```

If the Python user-space bin folder is not in your `PATH`, you should add this line to your `.bashrc`.
```
export PATH="$PATH:`python3 -m site --user-base`/bin"
```

You can then generate the carputils settings file `settings.yaml` if it does not exist:
```
python3 ./bin/cusettings $HOME/.config/carputils/settings.yaml
```
This file contains the carputils settings, including the paths to openCARP binary files. It contains default values adapted to your local setup, but can be modified manually.

> **Note**: if the directory containing the openCARP binary files is not in your PATH (most likely if you compiled openCARP from sources), you will have to change the default value of `CARP_EXE_DIR` in the settings file.
> For example, if your `openCARP` executables are located at `/home/openCARP/_build/bin`, you will have to edit the file and set `CARP_EXE_DIR` to
> ```
> CARP_EXE_DIR:
>    CPU: /home/openCARP/_build/bin
> ```

On the other hand, if you plan to develop carputils, manually configuring it as follows is recommended.

#### Development Installation

Enter the `carputils` directory and follow the instructions to do a development installation of carputils.

Install the dependencies using `pip` - preferably into user-space:
```
python3 -m pip install --user -r requirements.txt
```

Add `carputils/bin` to your `PATH`, and `carputils` to your `PYTHONPATH`.
For example, if carputils is located at `$HOME/openCARP/external`, add the following lines in your .bashrc:
```
export PATH=$PATH:$HOME/openCARP/external/carputils/bin
export PYTHONPATH=$PYTHONPATH:$HOME/openCARP/external/carputils
```

Create a `settings.yaml` file, which is a summary of paths and (default) 
parameters associated with your installation. To generate a default file, run the 
following command in the root folder of your carputils installation:
```
python3 ./bin/cusettings $HOME/.config/carputils/settings.yaml
```

Open the file and specify the proper directories for openCARP binary files. 
Notice using the *2 or 4 characters indentation* before the keyword `CPU`.
```
CARP_EXE_DIR:
    CPU: $HOME/install/openCARP/bin
```

Set directories for other openCARP tools if you use them. For example,
```
MESHALYZER_DIR:     $HOME/install/meshalyzer
```

#### Compiling C extensions and auto-generate code
To compile the C extensions for reading and writing meshes, just call
```
make

```
in the root folder of your carputils installation. 
If carputils is not installed under openCARP/external/carputils, you will also have to provide the
path to the openCARP sources root folder:
```
make OPENCARP_PATH=/path/to/openCARPsources
```
If you need to use a non-default compiler (e.g. under macOS for OpenMP support), provide it as a variable
```
CC=clang make
```

A plain `make` will also auto-generate the model.ionic, model.activetension and model.mechanics
python modules. They can be re-generated manually by calling
```
make models
```

## Troubleshooting

The installation of some dependencies of carputils can fail if `pip` and `setuptools` are not up-to-date.
They can be upgraded with the following commands:
```
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade setuptools
```

