#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Query functions
"""

import sys

def query_yes_no(question, default="yes"):
    """
    Ask a yes/no question via raw_input() and return the answer.

    Parameters
    ----------
    question : str
        Question to be presented to the user
    default : str or None
        The default answer if the user just hits <Enter>. It must be "yes" (the
        default), "no" or None (meaning an answer is required from the user).

    Returns
    -------
    bool
        The answer to the query
    """

    # Get the prompt string
    try:
        prompt = {None:  " [y/n] ",
                  "yes": " [Y/n] ",
                  "no":  " [y/N] "}[default]
    except KeyError:
        raise ValueError("invalid default answer: '{0}'".format(default))

    # Define valid answers
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}

    # Keep trying until a valid answer given
    while True:

        # Write the question
        sys.stdout.write("\n")
        sys.stdout.write(question + prompt)

        # Get the answer, strip whitespace and make lower case
        if sys.version_info.major < 3:
            choice = raw_input().strip().lower()
        else:
            choice = input().strip().lower()

        try:
            return valid[choice]

        except KeyError:

            # No valid answer given

            if default is not None and choice == '':
                # Enter pressed, return default answer
                return valid[default]

            else:
                # No default or invalid string, try again
                sys.stdout.write("Please respond with 'yes' or 'no' "\
                                 "(or 'y' or 'n').\n")
                continue

def query_folder(question, default="append"):
    """
    Process the output of asking a questions

    Parameters
    ----------
    question : str
        Question to be presented to the user
    default : str or None
        The default answer if the user just hits <Enter>. It must be "yes" (the
        default), "no" or None (meaning an answer is required from the user).

    Returns
    -------
    bool
        The answer to the query
    """

    # Get the prompt string
    try:
        prompt = {None:  " [y/n/a] ",
                  "yes": " [Y/n/a] ",
                  "no":  " [y/N/a] ",
                  "append": "[y/n/A] "}[default]
    except KeyError:
        raise ValueError("invalid default answer: '{0}'".format(default))

    # Define valid answers
    valid = {"yes": 0, "y": 0, "ye": 0,
             "no": 1, "n": 1,
             "a": 2, "app": 2, "append": 2}

    # Keep trying until a valid answer given
    while True:

        # Write the question
        sys.stdout.write("\n")
        sys.stdout.write(question + prompt)

        # Get the answer, strip whitespace and make lower case
        if sys.version_info.major < 3:
            choice = raw_input().strip().lower()
        else:
            choice = input().strip().lower()

        try:
            return valid[choice]

        except KeyError:

            # No valid answer given

            if default is not None and choice == '':
                # Enter pressed, return default answer
                return valid[default]

            else:
                # No default or invalid string, try again
                sys.stdout.write("Please respond with 'yes', 'no', or 'append' "\
                                 "('y', 'n' or 'a').\n")
                continue
