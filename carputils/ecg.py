#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some tools to assist in post-processing recovered ecg signals using openCARP.

input         ... Recovered extracellular potentials output file 
                  derived from an openCARP simulation. Can be compressed. 
freq_lowpass  ... Cutoff-frequency for lowpass filter (Bessel).
                  Recommended values: 40, 100, [150Hz].
freq_highpass ... Cutoff-frequency for highpass filter (Bessel).
                  Recommended values: [0.05], 0.5, 1.0Hz'.
filter_order  ... Order of lowpass, highpass or bandstop filter (Bessel).
                  Default: [4]
"""

import sys
import os
import json
import copy
import numpy
import math
import re
import time
from   datetime         import datetime
from   scipy            import signal, io, fftpack, fft, log10

from   carputils.carpio import igb, limpetguihdf5, txt
from   carputils        import dataproc
import matplotlib
from   matplotlib       import pyplot as plt
from   matplotlib       import rc
from   mpl_toolkits.mplot3d import Axes3D           # needed for subplot projection 3D!
from   copy             import deepcopy
from   collections      import OrderedDict
import numpy as np

if sys.version_info.major > 2:
    isPy2 = False
else:
    isPy2 = True

# Save original file open
fopen = open

# matplotlib global style preferences
plt.style.use('ggplot')     # a popular plotting package for R
#print(plt.style.available)

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
rc('axes', edgecolor='black') # black
rc('grid', color='black', linestyle=':')
rc('savefig', dpi=100, transparent=True)
rc('path', simplify=True)

def magnitude(v):
    return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))


def calc_ecg_features(_t, outdata_filt, Vm, params=None,silent=False):

    # reduce dataset
    pltinds = get_plot_range(_t, params)


    # derive from filtered data
    vl3Dx = outdata_filt['vl3Dx'][pltinds]
    vl3Dy = outdata_filt['vl3Dy'][pltinds]
    vl3Dz = outdata_filt['vl3Dz'][pltinds]
    I     = outdata_filt['I'][pltinds]
    II    = outdata_filt['II'][pltinds]


    #
    t     = _t[pltinds]


    # main electrical vector
    absEVector = numpy.square(vl3Dx) + numpy.square(vl3Dy) + numpy.square(vl3Dz)
    absEVector = numpy.sqrt(absEVector)
    mxIndx     = numpy.argmax(absEVector) # derived from 3D
    EVector    = [vl3Dx[mxIndx], vl3Dy[mxIndx], vl3Dz[mxIndx]]


    # Q, R and S indices based on frontal projection (2D, within plot range)
    Qindx, Rindx, Sindx = get_QRS_inds(t, I)

    if abs(Rindx-mxIndx) > Rindx/10.:
        if not silent:
            print ('Note: Considerable mismatch between R direction (2D, frontal axis)\n'
                   '      and the electrical heart axis (3D) detected!')
    del mxIndx  # this index could serve as R index as well (derived from 3D)


    # angle I & R peak
    alpha_r = numpy.arctan2(vl3Dz[Rindx], vl3Dy[Rindx]) * 180. / numpy.pi  # in degrees
    ## alpha_r  = numpy.arctan2(2*_II - _I, numpy.sqrt(3) * _I) * 180. / numpy.pi  # in degrees
    ## alpha_r = numpy.arctan2(vl3Dz, vl3Dy) * 180. / numpy.pi  # in degrees


    # determine maximum values of vector loop
    bbox = get_equal_bbox(vl3Dx, vl3Dy, vl3Dz)


    # compute delta QRS based on available lead signals (V1-V6, I, II, III)
    if Vm is not None:
        if not silent:
            print ('Note: Calculating delta QRS based on Vm signals!')
        dQRS = get_dQRS(_t, Vm)
    else:
        if not silent:
            print ('Note: No delta QRS calculation possible!')
        dQRS = None

    features = OrderedDict()
    features['pltinds']    = pltinds
    features['bbox']       = bbox
    features['EVector']    = EVector
    features['absEVector'] = absEVector
    #features['alpha_q']    = alpha_q
    features['alpha_r']    = alpha_r
    #features['alpha_s']    = alpha_s
    features['Qindx']      = Qindx
    features['Rindx']      = Rindx
    features['Sindx']      = Sindx
    features['dQRS']       = dQRS

    return features

def calc_std_ecg(data, vl3D_method='lead'):

    assert vl3D_method in ['lead','kors']

    # ---UNZIP FIELD VALUES-----------------------------------------------------
    fnyq  = data['fnyq']

    if 'RA' in data: RA = data['RA']
    if 'LA' in data: LA = data['LA']
    if 'LL' in data: LL = data['LL']
    V1 = data['V1']
    V2 = data['V2']
    V3 = data['V3']
    V4 = data['V4']
    V5 = data['V5']
    V6 = data['V6']

    # compute Einthoven
    if 'I' in data:
        I   = data['I']
        II  = data['II']
        III = data['III']
    else:
        I   = LA - RA
        II  = LL - RA
        III = LL - LA

    # compute Goldberg's ECG traces
    if 'RA' in data:
        aVF = LL - .5*(RA + LA)
        aVL = LA - .5*(RA + LL)
        aVR = RA - .5*(LA + LL)
    elif 'aVF' in data:
        aVF = data['aVF']
        aVL = data['aVL']
        aVR = data['aVR']
    else:
        aVF = None
        aVL = None
        aVR = None

    # compute Wilson's ECG traces

    if data['wCTcorr'] == False:
        wCT = (RA + LA + LL)/3     # Wilson's central terminal
        V1  = V1 - wCT
        V2  = V2 - wCT
        V3  = V3 - wCT
        V4  = V4 - wCT
        V5  = V5 - wCT
        V6  = V6 - wCT
        data['wCTcorr'] = True

    # compute vector loop
    x_vloop =    I
    y_vloop = -III*math.cos(30*math.pi/180)
    # y_vloop = -(II-I*cos(60*moth.pi/180))/sin(60*math.pi/180)
    #x_vloop =   LA - RA
    #y_vloop = -(LL - LA)*math.cos(30*math.pi/180)


    if vl3D_method == 'lead':

        # 3D vector loop
        vl3Dx =  V2
        vl3Dy =  I
        if aVF is not None:
            vl3Dz = -aVF
        else:
            vl3Dz    = numpy.copy(vl3Dx)
            vl3Dz[:] = 0.

    elif vl3D_method == 'kors':

        kors_transformation = [[0.38, -0.07, 0.11],
                               [-0.07, 0.93, -0.23],
                               [-0.13, 0.06, -0.43],
                               [0.05, -0.02, -0.06],
                               [-0.01, -0.05, -0.14],
                               [0.14, 0.06, -0.20],
                               [0.06, -0.17, -0.11],
                               [0.54, 0.13, 0.31]]

        trim_elec = [I, II, V1, V2, V3, V4, V5, V6]

        vec_loop = numpy.dot(numpy.transpose(kors_transformation), trim_elec)

        vl3Dx = vec_loop[0,:]
        vl3Dy = vec_loop[1,:]
        vl3Dz = vec_loop[2,:]

    outdata            = OrderedDict()
    outdata['fnyq']    = fnyq
    outdata['I'  ]     = I
    outdata['II' ]     = II
    outdata['III']     = III
    outdata['aVF']     = aVF
    outdata['aVL']     = aVL
    outdata['aVR']     = aVR
    outdata['V1']      = V1
    outdata['V2']      = V2
    outdata['V3']      = V3
    outdata['V4']      = V4
    outdata['V5']      = V5
    outdata['V6']      = V6
    outdata['x vloop'] = x_vloop
    outdata['y vloop'] = y_vloop
    outdata['vl3Dx']   = vl3Dx
    outdata['vl3Dy']   = vl3Dy
    outdata['vl3Dz']   = vl3Dz

    return outdata


def filter_std_ecg(ecg_data,filter_order=2, freq_lowpass=150.0,
                   freq_highpass=0.5, freq_bandstop=(48., 52.)):

    fnyq  = ecg_data['fnyq']

    # ---filter Einthoven's ECG traces
    if freq_lowpass is not None:
        LPFILTER = dataproc.LowPassBesselFilter( fnyq, filter_order, freq_lowpass)
    if freq_highpass is not None:
        HPFILTER = dataproc.HighPassBesselFilter(fnyq, filter_order, freq_highpass)
    if freq_bandstop is not None:
        BSFILTER = dataproc.BandStopBesselFilter(fnyq, filter_order, freq_bandstop[0], freq_bandstop[1])


    # ---FILTER EINTHOVEN LEADS------------------------------------------------
    I_filt   = deepcopy(ecg_data['I'])
    II_filt  = deepcopy(ecg_data['II'])
    III_filt = deepcopy(ecg_data['III'])

    # APPLY LOWPASS FILTER
    if freq_lowpass is not None:
        I_filt   = LPFILTER.apply(I_filt)
        II_filt  = LPFILTER.apply(II_filt)
        III_filt = LPFILTER.apply(III_filt)
    # APPLY HIGHPASS FILTER
    if freq_highpass is not None:
        I_filt   = HPFILTER.apply(I_filt)
        II_filt  = HPFILTER.apply(II_filt)
        III_filt = HPFILTER.apply(III_filt)
    # APPLY BANDSTOP FILTER
    if freq_bandstop is not None:
        I_filt   = BSFILTER.apply(I_filt)
        II_filt  = BSFILTER.apply(II_filt)
        III_filt = BSFILTER.apply(III_filt)


    # ---FILTER GOLDBERGER LEADS-----------------------------------------------
    aVF_filt = deepcopy(ecg_data['aVF'])
    aVL_filt = deepcopy(ecg_data['aVL'])
    aVR_filt = deepcopy(ecg_data['aVR'])

    # APPLY LOWPASS FILTER
    if freq_lowpass is not None:
        aVF_filt = LPFILTER.apply(aVF_filt)
        aVL_filt = LPFILTER.apply(aVL_filt)
        aVR_filt = LPFILTER.apply(aVR_filt)
    # APPLY HIGHPASS FILTER
    if freq_highpass is not None:
        aVF_filt = HPFILTER.apply(aVF_filt)
        aVL_filt = HPFILTER.apply(aVL_filt)
        aVR_filt = HPFILTER.apply(aVR_filt)
    # APPLY BANDSTOP FILTER
    if freq_bandstop is not None:
        aVF_filt = BSFILTER.apply(aVF_filt)
        aVL_filt = BSFILTER.apply(aVL_filt)
        aVR_filt = BSFILTER.apply(aVR_filt)


    # ---FILTER WILSON LEADS---------------------------------------------------
    V1_filt = deepcopy(ecg_data['V1'])
    V2_filt = deepcopy(ecg_data['V2'])
    V3_filt = deepcopy(ecg_data['V3'])
    V4_filt = deepcopy(ecg_data['V4'])
    V5_filt = deepcopy(ecg_data['V5'])
    V6_filt = deepcopy(ecg_data['V6'])

    if freq_lowpass is not None:
        V1_filt = LPFILTER.apply(V1_filt)
        V2_filt = LPFILTER.apply(V2_filt)
        V3_filt = LPFILTER.apply(V3_filt)
        V4_filt = LPFILTER.apply(V4_filt)
        V5_filt = LPFILTER.apply(V5_filt)
        V6_filt = LPFILTER.apply(V6_filt)
    if freq_highpass is not None:
        V1_filt = HPFILTER.apply(V1_filt)
        V2_filt = HPFILTER.apply(V2_filt)
        V3_filt = HPFILTER.apply(V3_filt)
        V4_filt = HPFILTER.apply(V4_filt)
        V5_filt = HPFILTER.apply(V5_filt)
        V6_filt = HPFILTER.apply(V6_filt)
    if freq_bandstop is not None:
        V1_filt = BSFILTER.apply(V1_filt)
        V2_filt = BSFILTER.apply(V2_filt)
        V3_filt = BSFILTER.apply(V3_filt)
        V4_filt = BSFILTER.apply(V4_filt)
        V5_filt = BSFILTER.apply(V5_filt)
        V6_filt = BSFILTER.apply(V6_filt)


    # ---FILTER 2D VECTOR LOOP-------------------------------------------------
    x_vloop_filt = deepcopy(ecg_data['x vloop'])
    y_vloop_filt = deepcopy(ecg_data['y vloop'])

    if freq_lowpass is not None:
        x_vloop_filt = LPFILTER.apply(x_vloop_filt)
        y_vloop_filt = LPFILTER.apply(y_vloop_filt)
    if freq_highpass is not None:
        x_vloop_filt = HPFILTER.apply(x_vloop_filt)
        y_vloop_filt = HPFILTER.apply(y_vloop_filt)
    if freq_bandstop is not None:
        x_vloop_filt = BSFILTER.apply(x_vloop_filt)
        y_vloop_filt = BSFILTER.apply(y_vloop_filt)


    # ---FILTER 3D VECTOR LOOP---------------------------------------------------
    vl3Dx_filt = deepcopy(ecg_data['vl3Dx'])
    vl3Dy_filt = deepcopy(ecg_data['vl3Dy'])
    vl3Dz_filt = deepcopy(ecg_data['vl3Dz'])

    if freq_lowpass is not None:
        vl3Dx_filt = LPFILTER.apply(vl3Dx_filt)
        vl3Dy_filt = LPFILTER.apply(vl3Dy_filt)
        vl3Dz_filt = LPFILTER.apply(vl3Dz_filt)
    if freq_highpass is not None:
        vl3Dx_filt = HPFILTER.apply(vl3Dx_filt)
        vl3Dy_filt = HPFILTER.apply(vl3Dy_filt)
        vl3Dz_filt = HPFILTER.apply(vl3Dz_filt)
    if freq_bandstop is not None:
        vl3Dx_filt = BSFILTER.apply(vl3Dx_filt)
        vl3Dy_filt = BSFILTER.apply(vl3Dy_filt)
        vl3Dz_filt = BSFILTER.apply(vl3Dz_filt)

    #if False:
    #    FFT = abs(fft(I))
    #    freqs = fftpack.fftfreq(I.size, (t[1]-t[0])/1000)
    #    FFT_filt = abs(fft(I_filt))
    #    freqs_filt = fftpack.fftfreq(I_filt.size, (t[1]-t[0])/1000)

     #   plt.subplot(411)
     #   plt.plot(t, I)
     #   plt.subplot(412)
     #   plt.plot(t, I_filt)
     #   plt.subplot(413)
     #   plt.plot(freqs,20*log10(FFT),'x')
     #   plt.subplot(414)
     #   plt.plot(freqs_filt,20*log10(FFT_filt),'x')
     #   plt.show()

    # ---make data ready for hd5 output
    outdata_filt            = OrderedDict()
    outdata_filt['fnyq']    = fnyq
    outdata_filt['I'  ]     = I_filt
    outdata_filt['II' ]     = II_filt
    outdata_filt['III']     = III_filt
    outdata_filt['aVF']     = aVF_filt
    outdata_filt['aVL']     = aVL_filt
    outdata_filt['aVR']     = aVR_filt
    outdata_filt['V1']      = V1_filt
    outdata_filt['V2']      = V2_filt
    outdata_filt['V3']      = V3_filt
    outdata_filt['V4']      = V4_filt
    outdata_filt['V5']      = V5_filt
    outdata_filt['V6']      = V6_filt
    outdata_filt['x vloop'] = x_vloop_filt
    outdata_filt['y vloop'] = y_vloop_filt
    outdata_filt['vl3Dx']   = vl3Dx_filt
    outdata_filt['vl3Dy']   = vl3Dy_filt
    outdata_filt['vl3Dz']   = vl3Dz_filt

    return outdata_filt

def save_std_ecg(t, data, data_filt, filepath='.'):
    
    # ---REMOVE VECTOR LOOP INFORMATION FROM OUTPUT DATA------------------------
    # It would not make sense to export it to HDF5.
    outdata      = deepcopy(data)
    outdata_filt = deepcopy(data_filt)
    keys2del     = []

    for key in outdata:
        if 'loop' in key:
            keys2del.append(key)
        elif 'fnyq' in key:
            keys2del.append(key)
        elif 'vl3D' in key:
            keys2del.append(key)
        elif outdata[key] is None:
            keys2del.append(key)

    for key in keys2del:
        del outdata[key]
        del outdata_filt[key]
            
    # ---OUTPUT STANDARD ECG AS HDF5 FILE---------------------------------------    
    with limpetguihdf5.open(os.path.join(filepath, 'ecg.h5'), 'w', title='Computed ECG Traces') as fp:
        fp.write(t, named_traces=outdata)
    with limpetguihdf5.open(os.path.join(filepath, 'ecg_filt.h5'), 'w', title='Computed ECG Traces') as fp:
        fp.write(t, named_traces=outdata_filt)
    
    # ---OUTPUT STANDARD ECG AS TEXT FILES--------------------------------------
    I        = outdata['I']
    II       = outdata['II']
    III      = outdata['III']
    I_filt   = outdata_filt['I']
    II_filt  = outdata_filt['II']
    III_filt = outdata_filt['III']
    if 'aVF' in outdata:
        aVF      = outdata['aVF']
        aVL      = outdata['aVL']
        aVR      = outdata['aVR']
        aVF_filt = outdata_filt['aVF']
        aVL_filt = outdata_filt['aVL']
        aVR_filt = outdata_filt['aVR']
    V1       = outdata['V1']
    V2       = outdata['V2']
    V3       = outdata['V3']
    V4       = outdata['V4']
    V5       = outdata['V5']
    V6       = outdata['V6']
    V1_filt  = outdata_filt['V1']
    V2_filt  = outdata_filt['V2']
    V3_filt  = outdata_filt['V3']
    V4_filt  = outdata_filt['V4']
    V5_filt  = outdata_filt['V5']
    V6_filt  = outdata_filt['V6']
    
    
    #print 'Saving Einthoven data...'
    with open(os.path.join(filepath, "Einthoven.trc.dat"), 'w') as file:
        for index in range(len(t)):
            file.write(str(       t[index]) + " " + 
                       str(       I[index]) + " " +
                       str(      II[index]) + " " + 
                       str(     III[index]) + " " + 
                       str(  I_filt[index]) + " " +
                       str( II_filt[index]) + " " +
                       str(III_filt[index]) + "\n")
                        
    
    #print 'Saving Goldberger data...'
    if 'aVF' in outdata:
        with open(os.path.join(filepath, "Goldberger.trc.dat"), 'w') as file:
            for index in range(len(t)):
                file.write(str(       t[index]) + " " +
                           str(     aVR[index]) + " " +
                           str(     aVL[index]) + " " +
                           str(     aVF[index]) + " " +
                           str(aVR_filt[index]) + " " +
                           str(aVL_filt[index]) + " " +
                           str(aVF_filt[index]) + "\n")
                        
    
    #print 'Saving Wilson data...'
    with open(os.path.join(filepath, "Wilson.trc.dat"), 'w') as file:
        for index in range(len(t)):
            file.write(str(      t[index]) + " " + 
                       str(     V1[index]) + " " +
                       str(     V2[index]) + " " +
                       str(     V3[index]) + " " +
                       str(     V4[index]) + " " +
                       str(     V5[index]) + " " +
                       str(     V6[index]) + " " +
                       str(V1_filt[index]) + " " +
                       str(V2_filt[index]) + " " +
                       str(V3_filt[index]) + " " +
                       str(V4_filt[index]) + " " +
                       str(V5_filt[index]) + " " +
                       str(V6_filt[index]) + "\n")
        
    return


def save_features(f_list, filepath='.'):
    # ---OUTPUT STANDARD ECG AS TEXT FILES--------------------------------------
    alpha_r = f_list['alpha_r']
    dqrs    = f_list['dQRS']

    # ---OUTPUT STANDARD ECG AS HDF5 FILE---------------------------------------
    with open(os.path.join(filepath, 'Features.dat'), 'w') as fp:
        fp.write("alpha_r (degree) " + "{0:5.2f}\n".format(alpha_r))
        if dqrs is not None:
            fp.write("dqrs    (ms)     " + "{0:5.2f}\n".format(dqrs))
        else:
            fp.write("dqrs    (ms)     " + "None\n".format(dqrs))
    return


def savefig_einthoven(t, outdata, outdata_filt, params=None, filepath='.', compress=None, silent=False):

    I        = outdata['I']
    II       = outdata['II']
    III      = outdata['III']
    I_filt   = outdata_filt['I']
    II_filt  = outdata_filt['II']
    III_filt = outdata_filt['III']

    fs       = 10  # font size for text labels
    pltinds  = range(0,len(t))

    tmpArray = numpy.array([])
    for arr in [I_filt[pltinds], II_filt[pltinds], III_filt[pltinds]]:
        tmpArray = numpy.append(tmpArray, arr)
    bbox = get_equal_bbox(numpy.zeros(2), tmpArray, numpy.zeros(2))
    del tmpArray
    bbox['xlim'] = [t[0], t[-1]]
    
    meanY = numpy.mean([I,II,II])

    #if params['ylim'] is not None:  bbox['ylim'] = params['ylim']
    if params['tlim'] is not None:  bbox['xlim'] = params['tlim']

    fig = plt.figure()
    fig.suptitle(r'Einthoven', fontsize=2*fs)
    fig.subplots_adjust(wspace=.3,hspace=.3)

    # ---I----------------------------------------------------------------------
    plt.subplot(131)
    #plt.plot(t, I[pltinds] - meanY, 'k-', label='$I$')
    plt.plot(t, I     [pltinds], 'k-', label='$I$')
    plt.plot(t, I_filt[pltinds], 'r-', label='$I_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')

    # ---II----------------------------------------------------------------------
    plt.subplot(132)
    #plt.plot(t, II[pltinds] - meanY, 'k-', label=r'$II$')
    plt.plot(t, II     [pltinds], 'k-', label=r'$II$')
    plt.plot(t, II_filt[pltinds], 'g-', label=r'$II_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off')
    plt.legend(loc='best')

    # ---III----------------------------------------------------------------------
    plt.subplot(133)
    #plt.plot(t, III[pltinds] - meanY, 'k-', label='$III$')
    plt.plot(t, III     [pltinds], 'k-', label='$III$')
    plt.plot(t, III_filt[pltinds],       'b-', label='$III_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off')
    plt.legend(loc='best')

    fig.savefig(os.path.join(filepath, 'Einthoven.pdf'), format='pdf', dpi=300, transparent=True)
    #fig.savefig(os.path.join(filepath, 'Einthoven.eps'), format='eps', dpi=300, transparent=True)
    if silent:
        plt.close()
    return


def savefig_goldberger(t, outdata, outdata_filt, params=None, filepath='.', compress=None, silent=False):

    aVF      = outdata['aVF']
    aVL      = outdata['aVL']
    aVR      = outdata['aVR']
    aVF_filt = outdata_filt['aVF']
    aVL_filt = outdata_filt['aVL']
    aVR_filt = outdata_filt['aVR']

    if aVF is None:
        if not silent:
            print('No Goldberger traces available, skipping image output!')
        return
    
    fs       = 10  # font size for text labels
    pltinds  = range(0,len(t))

    tmpArray = numpy.array([])
    for arr in [aVF_filt[pltinds], aVL_filt[pltinds], aVF_filt[pltinds]]:
        tmpArray = numpy.append(tmpArray, arr)
    bbox = get_equal_bbox(numpy.zeros(2), tmpArray, numpy.zeros(2))
    del tmpArray
    bbox['xlim'] = [t[0], t[-1]]

    meanY = numpy.mean([aVF, aVR, aVL])

    #if params['ylim'] is not None:  bbox['ylim'] = params['ylim']
    if params['tlim'] is not None:  bbox['xlim'] = params['tlim']

    fig = plt.figure()
    fig.suptitle(r'Goldberger', fontsize=2*fs)
    fig.subplots_adjust(wspace=.3,hspace=.3)

    # ---aVR----------------------------------------------------------------------
    plt.subplot(131)
    #plt.plot(t, aVR[pltinds] - meanY, 'k-', label='$aVR$')
    plt.plot(t, aVR     [pltinds], 'k-', label='$aVR$')
    plt.plot(t, aVR_filt[pltinds], 'r-', label='$aVR_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')

    # ---aVL----------------------------------------------------------------------
    plt.subplot(132)
    #plt.plot(t, aVL[pltinds] - meanY, 'k-', label=r'$aVL$')
    plt.plot(t, aVL     [pltinds], 'k-', label=r'$aVL$')
    plt.plot(t, aVL_filt[pltinds], 'g-', label=r'$aVL_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off')
    plt.legend(loc='best')

    # ---aVF----------------------------------------------------------------------
    plt.subplot(133)
    #plt.plot(t, aVF[pltinds] - meanY, 'k-', label='$aVF$')
    plt.plot(t, aVF     [pltinds], 'k-', label='$aVF$')
    plt.plot(t, aVF_filt[pltinds], 'b-', label='$aVF_{filt}$')
    plt.grid(True, color='k')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off')
    plt.legend(loc='best')

    fig.savefig(os.path.join(filepath, 'Goldberger.pdf'), format='pdf', transparent=True)
    #fig.savefig(os.path.join(filepath, 'Goldberger.eps'), format='eps', transparent=True)
    if silent:
        plt.close()
    return
    
    
def savefig_wilson(t, outdata, outdata_filt, params=None, filepath='.', compress=1.0, silent=False):
    
    V1       = outdata['V1']
    V2       = outdata['V2']
    V3       = outdata['V3']
    V4       = outdata['V4']
    V5       = outdata['V5']
    V6       = outdata['V6']
    V1_filt  = outdata_filt['V1']
    V2_filt  = outdata_filt['V2']
    V3_filt  = outdata_filt['V3']
    V4_filt  = outdata_filt['V4']
    V5_filt  = outdata_filt['V5']
    V6_filt  = outdata_filt['V6']

    fs       = 10  # font size for text labels
    pltinds  = range(0,len(t))
    minY     = min(V1_filt.min(),V2_filt.min(),V3_filt.min(),V4_filt.min(),V5_filt.min(),V6_filt.min())
    maxY     = max(V1_filt.max(),V2_filt.max(),V3_filt.max(),V4_filt.max(),V5_filt.max(),V6_filt.max())
    meanY    = numpy.mean([V1,V2,V3,V4,V5,V6])

    bbox         = {}
    bbox['xlim'] = [t[0], t[-1]]
    bbox['ylim'] = [math.floor(minY), math.ceil(maxY)]

    #if params['ylim'] is not None:  bbox['ylim'] = params['ylim']
    if params['tlim'] is not None:  bbox['xlim'] = params['tlim']

    fig = plt.figure()
    fig.suptitle(r'Wilson', fontsize=2*fs)
    fig.subplots_adjust(wspace=.1,hspace=.1)

    # ---V1----------------------------------------------------------------------
    plt.subplot(321)
    #plt.plot(t, V1[pltinds] - meanY, 'k-', label='$V1$')
    plt.plot(t, V1     [pltinds], 'k-', label='$V1$')
    plt.plot(t, V1_filt[pltinds], 'r-', label='$V1_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    #plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.tick_params(labelbottom='off')
    plt.legend(loc='best')


    # ---V2----------------------------------------------------------------------
    plt.subplot(323)
    #plt.plot(t, V2     [pltinds]-meanY, 'k-', label='$V2$')
    plt.plot(t, V2     [pltinds], 'k-', label='$V2$')
    plt.plot(t, V2_filt[pltinds], 'r-', label='$V2_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    #plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.tick_params(labelbottom='off')
    plt.legend(loc='best')


    # ---V3----------------------------------------------------------------------
    plt.subplot(325)
    #plt.plot(t, V3[pltinds] - meanY, 'k-', label='$V3$')
    plt.plot(t, V3     [pltinds], 'k-', label='$V3$')
    plt.plot(t, V3_filt[pltinds], 'r-', label='$V3_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')


    # ---V4--------------------------------------------------------------------
    plt.subplot(322)
    #plt.plot(t, V4[pltinds] - meanY, 'k-', label='$V4$')
    plt.plot(t, V4     [pltinds], 'k-', label='$V4$')
    plt.plot(t, V4_filt[pltinds], 'r-', label='$V4_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    #plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off', labelbottom='off')
    plt.legend(loc='best')


    # ---V5--------------------------------------------------------------------
    plt.subplot(324)
    plt.plot(t, V5     [pltinds]-meanY, 'k-', label='$V5$')
    plt.plot(t, V5_filt[pltinds],       'r-', label='$V5_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    #plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off', labelbottom='off')
    plt.legend(loc='best')


    # ---V6--------------------------------------------------------------------
    plt.subplot(326)
    #plt.plot(t, V6[pltinds] - meanY, 'k-', label='$V6$')
    plt.plot(t, V6     [pltinds], 'k-', label='$V6$')
    plt.plot(t, V6_filt[pltinds], 'r-', label='$V6_{filt}$')
    plt.grid(True, which='both')
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['ylim'])
    plt.xlabel(r'time (ms)', fontsize=fs)
    #plt.ylabel(r'U (mV)')
    plt.tick_params(labelleft='off')
    plt.legend(loc='best')


    fig.savefig(os.path.join(filepath, 'Wilson.pdf'), format='pdf')
    #fig.savefig(os.path.join(filepath, 'Wilson.eps'), format='eps')
    if silent:
        plt.close()
    return
    
    
def savefig_vectorloop(t, outdata, outdata_filt, params=None, filepath='./', mtime=None, silent=False):
    
    vl3Dx      = outdata['vl3Dx']
    vl3Dy      = outdata['vl3Dy']
    vl3Dz      = outdata['vl3Dz']
    vl3Dx_filt = outdata_filt['vl3Dx'] if 'vl3Dx' in outdata_filt else None
    vl3Dy_filt = outdata_filt['vl3Dy'] if 'vl3Dy' in outdata_filt else None
    vl3Dz_filt = outdata_filt['vl3Dz'] if 'vl3Dz' in outdata_filt else None

    if vl3Dz is None:
        if not silent:
            print('No z-component of loop available, skipping vector plot!')
        return
    if isinstance(t, list):
        t = numpy.asarray(t, dtype=numpy.float)

    datestring = mtime if mtime is not None else mydatestring()
    
    fs       = 10  # font size for text labels
    if 'tlim' in params:
        inds = numpy.intersect1d(numpy.squeeze(numpy.where(t >= params['tlim'][0])),
                                 numpy.squeeze(numpy.where(t <= params['tlim'][1])))
    else:
        inds = range(len(t))
    

    fig = plt.figure()
    fig.suptitle(r'Vectorloop' + '      ' + datestring + '\n' + filepath.replace('_',' '), fontsize=2*fs)

    # -------------------------------------------------------------------------
    #plt.plot(vl3Dy[inds],      vl3Dz[inds], 'k-', label='$raw$', linewidth=0.5)
    plt.plot(vl3Dy_filt[inds], vl3Dz_filt[inds], 'r-', label='$filtered$', linewidth=0.5)
    plt.grid(True, color='r', linestyle='--', linewidth=.1, dashes=(5,20))
    plt.xlabel(r'U (mV)', fontsize=fs)
    plt.ylabel(r'U (mV)', fontsize=fs)
    plt.axes().set_aspect('equal')
    plt.axes().spines['top'].set_color('red')
    plt.axes().spines['bottom'].set_color('red')
    plt.axes().spines['left'].set_color('red')
    plt.axes().spines['right'].set_color('red')
    #plt.axis('equal')
    plt.legend(loc='best')

    fig.savefig(os.path.join(filepath, 'VectorLoop.pdf'), format='pdf', transparent=True)
    #fig.savefig(os.path.join(filepath, 'VectorLoop.pdf'), format='pdf', transparent=True)
    if silent:
        plt.close()
    else:
        fig.show(block=False)
    return
    
    
def show_frequency_response(t, order, freq_lowpass, freq_highpass, freq_bandstop, silent=False):

    ts = (t[1] - t[0]) / 1000  # sampling time in s
    fs = 1. / ts  # sampling frequency in Hz
    fnyq = fs / 2.
    t = numpy.linspace(0, 1000, 1000)
    data = numpy.zeros(len(t), dtype=int)
    ids = numpy.where(t < .01)  # create impuls lasting 10ms
    data[50] = 1

    # investigate impulse response on low pass filter
    if freq_lowpass is not None:
        #b, a = signal.butter(order, freq_lowpass / fnyq, 'lowpass', analog=False)
        b, a = signal.bessel(order, freq_lowpass / fnyq, 'lowpass', analog=False)
        #b, a = signal.bessel(order, freq_lowpass / fnyq, 'lowpass', analog=True)
        #data_f = signal.filtfilt(b, a, data, method='pad', padtype='even', padlen=3*max(len(a), len(b)))
        #data_f = signal.filtfilt(b, a, data, method='pad', padlen=3 * max(len(a), len(b)))
        data_f = signal.filtfilt(b, a, data)
        fig = plt.figure()
        plt.plot(t/1000., data_f, 'r--', linewidth=.5, marker='.', markersize=2)
        plt.title('LP Impulse Response: {0}Hz, order {1}'.format(freq_lowpass, order))
        plt.xlabel('Frequency [s]')
        plt.ylabel('Amplitude [au]')
        plt.xlim(-0.01,0.3)
        #plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        #plt.axvline(freq_lowpass, color='green')  # cutoff frequency
        plt.axhline(0, color='gray', linewidth=0.1)  # zero
        plt.show()

    if freq_lowpass is not None:
        b, a = signal.bessel(order, freq_lowpass/fnyq, 'lowpass', analog=False)
        w, h = signal.freqs(b, a)

        fig = plt.figure()
        plt.semilogx(w, 20 * numpy.log10(abs(h)))
        plt.title('LP Bessel filter: {0}Hz, order {1}'.format(freq_lowpass, order))
        plt.xlabel('Frequency [radians / second]')
        plt.ylabel('Amplitude [dB]')
        #plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        plt.axvline(freq_lowpass, color='green') # cutoff frequency
        #fig.show()
    else:
        if not silent: print ('Note: Not plotting lowpass filter response. No freq specified!')

    if freq_highpass is not None:
        b, a = signal.bessel(order, freq_highpass/fnyq, 'highpass', analog=False)
        w, h = signal.freqs(b, a)

        fig = plt.figure()
        plt.semilogx(w, 20 * numpy.log10(abs(h)))
        plt.title('HP Bessel filter: {0}Hz, order {1}'.format(freq_highpass, order))
        plt.xlabel('Frequency [radians / second]')
        plt.ylabel('Amplitude [dB]')
        #plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        plt.axvline(freq_highpass, color='green') # cutoff frequency
        #fig.show()
    else:
        if not silent: print ('Note: Not plotting highpass filter response. No freq specified!')

    if freq_bandstop is not None:
        b, a = signal.bessel(order, numpy.asarray(freq_bandstop)/fnyq, 'bandstop', analog=False)
        w, h = signal.freqs(b, a)

        fig = plt.figure()
        plt.semilogx(w, 20 * numpy.log10(abs(h)))
        plt.title('Bandstop Bessel filter: {0}Hz, order {1}'.format(freq_bandstop, order))
        plt.xlabel('Frequency [radians / second]')
        plt.ylabel('Amplitude [dB]')
        #plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        plt.axvline(freq_bandstop[0], color='green')  # cutoff frequency
        plt.axvline(freq_bandstop[1], color='green')  # cutoff frequency
        #fig.show()
    else:
        if not silent:
            print ('Note: Not plotting bandstop filter response. No freq specified!')
        else:
            plt.close()

    return


def get_equal_bbox(xdata, ydata, zdata):
    xmin   = xdata.min()
    ymin   = ydata.min()
    zmin   = zdata.min()
    xmax   = xdata.max()
    ymax   = ydata.max()
    zmax   = zdata.max()
    nboxes = 0            # number of plot boxes for the size 'denom'
    denom  = 0.5

    # inquire x bounding box
    if not xmin == 0.0 and not xmax == 0.0:
        nboxes = numpy.floor((xdata.max() - xdata.min() + denom) / denom)

        while nboxes <= 1.0 or nboxes > 10.0:
            if nboxes == 1.0:
                denom  = denom / 10.0
                nboxes = numpy.floor((xdata.max() - xdata.min() + denom) / denom)
                continue

            if nboxes > 10.0:
                denom  = denom * 10.0
                nboxes = numpy.floor((xdata.max() - xdata.min() + denom) / denom)
                continue

        xmin = numpy.floor((xmin - denom)/denom) * denom
        xmax = numpy.floor((xmax + denom)/denom) * denom


    # inquire y bounding box
    if not ymin == 0.0 and not ymax == 0.0:
        nboxes = numpy.floor((ydata.max() - ydata.min() + denom) / denom)

        while nboxes <= 1.0 or nboxes > 10.0:
            if nboxes == 1.0:
                denom  = denom / 10.0
                nboxes = numpy.floor((ydata.max() - ydata.min() + denom) / denom)
                continue

            if nboxes > 10.0:
                denom  = denom * 10.0
                nboxes = numpy.floor((ydata.max() - ydata.min() + denom) / denom)
                continue

        ymin = numpy.floor((ymin - denom)/denom) * denom
        ymax = numpy.floor((ymax + denom)/denom) * denom


    # inquire z bounding box
    if not zmin == 0.0 and not zmax == 0.0:
        nboxes = numpy.floor((zdata.max() - zdata.min() + denom) / denom)

        while nboxes <= 1.0 or nboxes > 10.0:
            if nboxes == 1.0:
                denom  = denom / 10.0
                nboxes = numpy.floor((zdata.max() - zdata.min() + denom) / denom)
                continue

            if nboxes > 10.0:
                denom  = denom * 10.0
                nboxes = numpy.floor((zdata.max() - zdata.min() + denom) / denom)
                continue

        zmin = numpy.floor((zmin - denom)/denom) * denom
        zmax = numpy.floor((zmax + denom)/denom) * denom

    x_width = xmax - xmin
    y_width = ymax - ymin
    z_width = zmax - zmin

    # enforce global bounding box size in all directions
    bbsize  = max(x_width, y_width, z_width)

    while x_width < bbsize:
        # always add at the end
        xmax = xmax + denom

        # recompute bbox size
        x_width = xmax - xmin

    tf = False
    while y_width < bbsize:
        if tf:
            ymax = ymax + denom
            tf = False
        else:
            ymin = ymin - denom
            tf = True

        # recompute bbox size
        y_width = ymax - ymin

    tf = False
    while z_width < bbsize:
        if tf:
            zmax = zmax + denom
            tf = False
        else:
            zmin = zmin - denom
            tf = True

        # recompute bbox size
        z_width = zmax - zmin

    return {'xlim': [xmin, xmax],
            'ylim': [ymin, ymax],
            'zlim': [zmin, zmax]}


def vectorloop_plot3D(_t, outdata, outdata_filt, features, filepath='./', params=None,silent=False):

    pltinds = features['pltinds']

    # display filtered data
    vl3Dx   = outdata_filt['vl3Dx'][pltinds]
    vl3Dy   = outdata_filt['vl3Dy'][pltinds]
    vl3Dz   = outdata_filt['vl3Dz'][pltinds]
    I       = outdata_filt['I'][pltinds]

    #
    t       = _t[pltinds]

    # get calculated features
    alpha_r     = features['alpha_r']
    EVector     = features['EVector']
    absEVector  = features['absEVector']
    bbox        = features['bbox']
    Qindx       = features['Qindx']
    Rindx       = features['Rindx']
    Sindx       = features['Sindx']


    # help identifying the orientation of the loop
    # finding an index based on amplitude is better than bisecting the duration until the R peak
    indxT     = numpy.where(I[0:Rindx] > numpy.max(I[0:Rindx])/2)
    try:
        indxT = numpy.min(indxT)
    except ValueError:
        indxT = 0
        if not silent: print ('Determining orientation of loop not successful!\n')

    vl3Dx_offset = numpy.zeros(len(vl3Dx)) + bbox['xlim'][0]
    vl3Dy_offset = numpy.zeros(len(vl3Dy)) + bbox['ylim'][0]
    vl3Dz_offset = numpy.zeros(len(vl3Dz)) + bbox['zlim'][0]

    fig = plt.figure(dpi=100)
    fig.suptitle('Vectorloop 3D')

    #ax = fig.add_subplot(111, projection='3d', aspect='equal')
    ax = fig.add_subplot(111, projection='3d', aspect='auto')
    # plot plain loops
    ax.plot(vl3Dx,      vl3Dy,      vl3Dz,      'y-', label='vl (raw)')
    #ax.plot(vl3Dx_filt, vl3Dy_filt, vl3Dz_filt, 'k-', label='vl (filtered)')
    ax.plot([0],        [0],        [0],        'ko')

    # add projection of loops
    ax.plot( vl3Dx_offset,     vl3Dy,           vl3Dz,            'r-', label='frontal')
    ax.plot( vl3Dx,            vl3Dy_offset,    vl3Dz,            'g-', label='sagittal')
    ax.plot( vl3Dx,            vl3Dy,           vl3Dz_offset,     'b-', label='transverse')

    # mark starting point
    ax.plot([vl3Dx_offset[0]],[0],              [0],              'ro')
    ax.plot([0],              [vl3Dy_offset[0]],[0],              'go')
    ax.plot([0],              [0],              [vl3Dz_offset[0]],'bo')

    # mark first 75ms of loop
    index = numpy.where(t<=75.)
    ax.plot([vl3Dx[       numpy.max(indxT)]], [vl3Dy[       numpy.max(indxT)]], [vl3Dz[       numpy.max(indxT)]], 'y*')
    ax.plot([vl3Dx_offset[numpy.max(indxT)]], [vl3Dy[       numpy.max(indxT)]], [vl3Dz[       numpy.max(indxT)]], 'y*')
    ax.plot([vl3Dx[       numpy.max(indxT)]], [vl3Dy_offset[numpy.max(indxT)]], [vl3Dz[       numpy.max(indxT)]], 'y*')
    ax.plot([vl3Dx[       numpy.max(indxT)]], [vl3Dy[       numpy.max(indxT)]], [vl3Dz_offset[numpy.max(indxT)]], 'y*')

    # plot main electrical axis vector
    # ax.plot([0, EVector[0]], [0, EVector[1]], [0, EVector[2]], 'c-')
    # plot main electrical axis vector based on lead I
    ax.plot([vl3Dx_offset[0], vl3Dx_offset[0]], [vl3Dy[0], vl3Dy[Rindx]], [vl3Dz[0], vl3Dz[Rindx]], 'c-')
    ax.grid(True)
    #ax.axis('equal')
    ax.legend(frameon=True)

    ax.set_xlabel('V2 (mV)')    # V2 x axis
    ax.set_ylabel('I (mV)')     # I y axis
    ax.set_zlabel('-aVF (mV)')  # -aVF z axis

    ax.set_xlim3d(bbox['xlim'])
    ax.set_ylim3d(bbox['ylim'])
    ax.set_zlim3d(bbox['zlim'])
    #ax.set_aspect('equal')

    fig.savefig(os.path.join(filepath, 'VectorLoop3D.pdf'), format='pdf')
    #fig.savefig(os.path.join(filepath, 'VectorLoop3D.eps'), format='eps')
    if silent:
        plt.close()
    else:
        fig.show()
    return


def vectorloop_plot2D(_t, outdata, outdata_filt, features, filepath='./', params=None,silent=False):

    # 'V2'      -- x axis
    # 'I'       -- y axis
    # '-aVF'    -- z axis
    pltinds = features['pltinds']

    # get reduced filtered data
    vl3Dx = outdata_filt['vl3Dx'][pltinds]
    vl3Dy = outdata_filt['vl3Dy'][pltinds]
    vl3Dz = outdata_filt['vl3Dz'][pltinds]
    I     = outdata_filt['I'][pltinds]

    #
    t     = _t[pltinds]

    # get calculated features
    alpha_r     = features['alpha_r']
    EVector     = features['EVector']
    absEVector  = features['absEVector']
    bbox        = features['bbox']
    Qindx       = features['Qindx']
    Rindx       = features['Rindx']
    Sindx       = features['Sindx']
    dQRS        = features['dQRS']

    mnIndx_aVF  = numpy.argmin(vl3Dz)
    mxIndx_aVF  = numpy.argmax(vl3Dz)

    annotate      = r"$\alpha\ {:.1f}^\circ$".format(alpha_r)
    if dQRS is not None:
        annotate += "\n"
        annotate += r"$\Delta QRS\ {:.1f}ms$".format(dQRS)

    # help identifying the orientation of the loop
    # finding an index based on amplitude is better than bisecting the duration until the R peak
    indxT       = numpy.where(I[0:Rindx] > numpy.max(I[0:Rindx])/2)
    try:
        indxT   = numpy.min(indxT)
    except ValueError:
        indxT   = 0
        if not silent: print ('Determining orientation of loop not successful!\n')


    fig = plt.figure()
    fig.subplots_adjust(wspace=.3,hspace=.3)
    fig.suptitle('Vectorloop 2D')

    plt.subplot(221)
    plt.title('saggital')
    # ---
    plt.plot( vl3Dx,                                vl3Dz,                                 'g-', label='raw')
    #plt.plot( vl3Dx_filt,                           vl3Dz_filt,                            'g-', label='filtered')
    plt.plot( 0,                                    0,                                     'go')   # start point
    plt.plot( vl3Dx[indxT],                         vl3Dz[indxT],                          'g*')   # mark loop @ 75ms
    # which one to choose ???
    #plt.plot([0, EVector[0]],                      [0, EVector[2]],                        'c-')   # main electrical vector
    plt.plot([0, vl3Dx[Rindx]],                    [0, vl3Dz[Rindx]],                      'c-')   # main electrical vector
    # ---
    plt.plot([0, bbox['xlim'][0]],                 [0, 0],                                 'k--')  # horizontal line @ origin
    plt.plot([vl3Dx[mnIndx_aVF], bbox['xlim'][0]], [vl3Dz[mnIndx_aVF], vl3Dz[mnIndx_aVF]], 'k--')  # horizontal line @ min "y"-axis
    plt.plot([vl3Dx[mxIndx_aVF], bbox['xlim'][0]], [vl3Dz[mxIndx_aVF], vl3Dz[mxIndx_aVF]], 'k--')  # horizontal line @ max "y"-axis
    # ---
    plt.xlim(bbox['xlim'])
    plt.ylim(bbox['zlim'])
    plt.xlabel(r'V2 (mV)')
    plt.ylabel(r'-aVF (mV)')
    plt.gca().invert_xaxis()
    #plt.legend(frameon=True)
    plt.grid(True)


    plt.subplot(222)
    plt.title('frontal')
    # ---
    plt.plot( vl3Dy,                                vl3Dz,                                 'y-')#, label='raw')
    #plt.plot( vl3Dy_filt,                           vl3Dz_filt,                            'r-', label='filtered')
    plt.plot( 0,                                    0,                                     'ro')   # start point
    plt.plot( vl3Dy[indxT],                         vl3Dz[indxT],                          'r*')   # mark loop @ 75ms
    # which one to choose ???
    # plt.plot([0, EVector[1]],                      [0, EVector[2]],                        'c-', label=annotate)   # main electrical vector
    plt.plot([0, vl3Dy[Rindx]],                    [0, vl3Dz[Rindx]],                       'c-', label=annotate)  # main electrical vector
    # ---
    plt.plot([0, 0],                               [bbox['zlim'][0], 0],                   'k--')  # vertical line @ origin
    plt.plot([bbox['ylim'][0], 0],                 [0, 0],                                 'k--')  # horizontal line @ origin
    plt.plot([vl3Dy[Qindx], vl3Dy[Qindx]],         [bbox['zlim'][0], vl3Dz[Qindx]],        'k--')  # vertical line @ Q
    plt.text( vl3Dy[Qindx],                         bbox['zlim'][0]+0.005, 'Q', color='r')          # textlabel Q
    plt.plot([vl3Dy[Rindx], vl3Dy[Rindx]],         [bbox['zlim'][0], vl3Dz[Rindx]],        'k--')  # vertical line @ R
    plt.text( vl3Dy[Rindx],                         bbox['zlim'][0]+0.005, 'R', color='g')          # textlabel R
    plt.plot([vl3Dy[Sindx], vl3Dy[Sindx]],         [bbox['zlim'][0], vl3Dz[Sindx]],        'k--')  # vertical line @ S
    plt.text( vl3Dy[Sindx],                         bbox['zlim'][0]+0.005, 'S', color='b')                 # textlabel S
    plt.plot([vl3Dy[mnIndx_aVF], bbox['ylim'][0]], [vl3Dz[mnIndx_aVF], vl3Dz[mnIndx_aVF]], 'k--')  # horizontal line @ min "y"-axis
    plt.plot([vl3Dy[mxIndx_aVF], bbox['ylim'][0]], [vl3Dz[mxIndx_aVF], vl3Dz[mxIndx_aVF]], 'k--')  # horizontal line @ max "y"-axis
    #  ---
    plt.xlim(bbox['ylim'])
    plt.ylim(bbox['zlim'])
    #plt.xlabel('I')
    #plt.ylabel('-aVF')
    plt.grid(True)
    plt.legend(loc='best')


    plt.subplot(224)
    plt.title('transverse')
    # ---
    plt.plot(vl3Dy,                         vl3Dx,                          'y-', label='raw')
    #plt.plot(vl3Dy_filt,                    vl3Dx_filt,                     'b-', label='filtered')
    plt.plot( 0,                            0,                              'bo')   # start point
    plt.plot( vl3Dy[indxT],                 vl3Dx[indxT],                   'b*')   # mark loop @ 75ms
    # which one to choose ???
    # plt.plot([0, EVector[1]],              [0, EVector[0]],                 'c-')   # main electrical vector
    plt.plot([0, vl3Dy[Rindx]],            [0, vl3Dx[Rindx]],               'c-')  # main electrical vector
    # ---
    plt.plot([0, 0],                       [bbox['xlim'][0], 0],            'k--')  # vertical line @ origin
    plt.plot([vl3Dy[Qindx], vl3Dy[Qindx]], [bbox['xlim'][0], vl3Dx[Qindx]], 'k--')  # vertical line @ Q
    plt.plot([vl3Dy[Rindx], vl3Dy[Rindx]], [bbox['xlim'][0], vl3Dx[Rindx]], 'k--')  # vertical line @ R
    plt.plot([vl3Dy[Sindx], vl3Dy[Sindx]], [bbox['xlim'][0], vl3Dx[Sindx]], 'k--')  # vertical line @ S
    # ---
    plt.xlim(bbox['ylim'])
    plt.ylim(bbox['xlim'])
    plt.xlabel('I (mV)')
    plt.ylabel('V2 (mV)')
    plt.gca().invert_yaxis()
    plt.grid(True)
    # plt.legend()


    plt.subplot(223)
    plt.title('filtered ecg traces')
    # ---
    plt.plot(t, vl3Dz,     'r-', label='-aVF')
    plt.plot(t, I,         'g-', label='I')
    plt.plot(t, vl3Dx,     'b-', label='V2')
    plt.plot(t, absEVector,'k-', label='$||E||$')
    # ---
    plt.xlabel('t (ms)')
    plt.ylabel('U (mV)')
    plt.grid(True)
    plt.legend(loc='best')


    fig.savefig(os.path.join(filepath, 'VectorLoop2D.pdf'), format='pdf')
    #fig.savefig(os.path.join(filepath, 'VectorLoop2D.eps'), format='eps')
    if silent:
        plt.close()
    else:
        fig.show()
    return

def mydatestring():
    format = "%d-%b-%Y %H:%M:%S"
    today = datetime.today()
    return today.strftime(format)

def randomID():
    s = ''
    for i in range(10):
        s = s + str(numpy.random.randint(0,9))
    return s

def set_subfig_props(ax, bbox=None, text=None, fontsize=None, useGrid=True, nx_ticks=None, ny_ticks=None,
                         figaspect=None, clip_on=False):

    # make copy of the dictionary, don't want to alter the values outside the function
    figbbox = copy.deepcopy(bbox)

    XLIM = None
    YLIM = None
    if figbbox is not None:
        XLIM = figbbox['xlim']
        YLIM = figbbox['ylim']
        ax.set_xlim(XLIM)
        ax.set_ylim(YLIM)

    if clip_on == True:
        # assuming this is the top row of the electrogramm,
        # plotting either I or V1
        figbbox['ylim'][0] = math.floor(figbbox['ylim'][0])
        figbbox['ylim'][1] = math.ceil( figbbox['ylim'][1])
    else:
        # assuming to plot elsewhere
        figbbox['ylim'][0] = math.ceil( figbbox['ylim'][0])
        figbbox['ylim'][1] = math.floor(figbbox['ylim'][1])


    if useGrid:
        ax.grid(b=True, which='major', color='r', linestyle='-',  linewidth=.15)
        ax.grid(b=True, which='minor', color='r', linestyle='--', linewidth=.1, dashes=(5,20))
        ax.minorticks_on()

    if figaspect is not None:
        ax.axes().set_aspect(figaspect)
    #ax.legend(loc=3, frameon=False)

    if text is not None:
        ax.text(0.01, 0.5, text, horizontalalignment='left', verticalalignment='top',
                fontsize=fontsize, transform=ax.transAxes)
        #ax.annotate(text, xy=(XLIM[0], YLIM[0]), xycoords='data', ha='right', va='top', fontsize=fontsize)

    ax.spines['top'  ].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # grid settings
    if nx_ticks is not None:
        if XLIM is None:
            XLIM = ax.get_xlim()
        xticks   = numpy.linspace(XLIM[0], XLIM[1], nx_ticks)
        ax.set_xticks(xticks)

    if ny_ticks is not None:
        yticks   = numpy.linspace(YLIM[0], YLIM[1], ny_ticks)
        ax.set_yticks(yticks)

    ax.tick_params(axis='both', which='both', bottom='off', top='off',
                   left='off', right='off', labelbottom='off', labelleft='off')

    ax.set_facecolor('w')  # make background white
    ax.set_axisbelow(True) # send grid lines to background
    return

def electrogram(_t, outdata, outdata_filt, features, filepath='./',
                params=None, freq_lowpass=None, freq_highpass=None,
                nx_ticks=None, ny_ticks=None, mtime=None, silent=False):

    pltinds    = features['pltinds']
    grid       = True
    kwargs     = {'color': 'k', 'linewidth': .5}
    datestring = mtime if mtime is not None else mydatestring()
    if isinstance(_t, list):
        _t = numpy.asarray(_t, dtype=numpy.float)

    # ---UNZIP FIELD VALUES-----------------------------------------------------
    I   = outdata_filt['I']
    II  = outdata_filt['II']
    III = outdata_filt['III']
    aVF = outdata_filt['aVF']
    aVL = outdata_filt['aVL']
    aVR = outdata_filt['aVR']
    V1  = outdata_filt['V1']
    V2  = outdata_filt['V2']
    V3  = outdata_filt['V3']
    V4  = outdata_filt['V4']
    V5  = outdata_filt['V5']
    V6  = outdata_filt['V6']

    #
    t   = _t[pltinds]

    ## get calculated features
    #alpha_r     = features['alpha_r']
    #EVector     = features['EVector']
    #absEVector  = features['absEVector']
    #bbox        = features['bbox']
    #Qindx       = features['Qindx']
    #Rindx       = features['Rindx']
    #Sindx       = features['Sindx']
    #dQRS        = features['dQRS']

    tmpArray = numpy.array([])
    if aVF is not None:
        for arr in [I [pltinds], II[pltinds], III[pltinds], aVR[pltinds], aVL[pltinds], aVF[pltinds],
                    V1[pltinds], V2[pltinds], V3 [pltinds], V4 [pltinds], V5 [pltinds], V6 [pltinds]]:
            tmpArray = numpy.append(tmpArray, arr)
    else:
        for arr in [I [pltinds], II[pltinds], III[pltinds],
                    V1[pltinds], V2[pltinds], V3 [pltinds], V4 [pltinds], V5 [pltinds], V6 [pltinds]]:
            tmpArray = numpy.append(tmpArray, arr)

    bbox = get_equal_bbox(numpy.zeros(2), tmpArray, numpy.zeros(2))
    del tmpArray
    bbox['xlim'] = [t[0], t[-1]]


    ##aspect = int( (pltinds[1]-pltinds[0])/bbox['ylim'][1] * compress )
    #aspect = int((t[1]-t[0]) / (bbox['ylim'][1]-bbox['ylim'][0]))
    #if aspect < 1:
    #    aspect = None
    aspect = None

    gs  = (6,2) # grid size
    fs  = 8    # font size for text labels
    fig = plt.figure(dpi=300)

    fig.suptitle('ID:' + randomID() + '      ' + datestring, fontsize=fs)
    fig.subplots_adjust(wspace=0., hspace=0.)

    # ---I----------------------------------------------------------------------
    ax00 = plt.subplot2grid(gs, (0, 0))
    ax00.plot(t, I[pltinds], **kwargs)
    set_subfig_props(ax00, bbox=bbox, text='I', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect, clip_on=True)
    # change back a few things as we at the top of the chart
    ax00.spines['top'].set_linewidth(0.01)
    ax00.spines['top'].set_linestyle('--')
    ax00.spines['top'].set_color('r')
    ax00.spines['top'].set_visible(True)
    # ---II---------------------------------------------------------------------
    ax10 = plt.subplot2grid(gs, (1, 0))
    ax10.plot(t, II[pltinds], **kwargs)
    set_subfig_props(ax10, bbox=bbox, text='II', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect)
    # ---III--------------------------------------------------------------------
    ax20 = plt.subplot2grid(gs, (2, 0))
    ax20.plot(t, III[pltinds], **kwargs)
    set_subfig_props(ax20, bbox=bbox, text='III', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect)
    # ---aVR--------------------------------------------------------------------
    ax30 = plt.subplot2grid(gs, (3, 0))
    if aVR is not None: ax30.plot(t, aVR[pltinds], **kwargs)
    set_subfig_props(ax30, bbox=bbox, text='aVR', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect)
    # ---aVL--------------------------------------------------------------------
    ax40 = plt.subplot2grid(gs, (4, 0))
    if aVL is not None: ax40.plot(t, aVL[pltinds], **kwargs)
    set_subfig_props(ax40, bbox=bbox, text='aVL', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect)
    # ---aVF--------------------------------------------------------------------
    ax50 = plt.subplot2grid(gs, (5, 0))
    if aVF is not None: plt.plot(t, aVF[pltinds], **kwargs)
    set_subfig_props(ax50, bbox=bbox, text='aVF', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect, clip_on=True)
    ax_left_footnote = ax50 #plt.gca()
    # ---V1---------------------------------------------------------------------
    ax01 = plt.subplot2grid(gs, (0, 1))
    ax01.plot(t, V1[pltinds], **kwargs)
    set_subfig_props(ax01, bbox=bbox, text='V1', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect, clip_on=False)
    # change back a few things as we at the top of the chart
    ax01.spines['top'].set_linewidth(0.01)
    ax01.spines['top'].set_linestyle('--')
    ax01.spines['top'].set_color('r')
    ax01.spines['top'].set_visible(True)
    # ---V2---------------------------------------------------------------------
    ax11 = plt.subplot2grid(gs, (1, 1))
    ax11.plot(t, V2[pltinds], **kwargs)
    set_subfig_props(ax11, bbox=bbox, text='V2', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect)
    # ---V3---------------------------------------------------------------------
    ax21 = plt.subplot2grid(gs, (2, 1))
    ax21.plot(t, V3[pltinds], **kwargs)
    set_subfig_props(ax21, bbox=bbox, text='V3', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                     figaspect=aspect)
    # ---V4---------------------------------------------------------------------
    ax31 = plt.subplot2grid(gs, (3, 1))
    ax31.plot(t, V4[pltinds], **kwargs)
    set_subfig_props(ax31, bbox=bbox, text='V4', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                     figaspect=aspect)
    # ---V5---------------------------------------------------------------------
    ax41 = plt.subplot2grid(gs, (4, 1))
    ax41.plot(t, V5[pltinds], **kwargs)
    set_subfig_props(ax41, bbox=bbox, text='V5', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                     figaspect=aspect)
    # ---V6---------------------------------------------------------------------
    ax51 = plt.subplot2grid(gs, (5, 1))
    ax51.plot(t, V6[pltinds], **kwargs)
    set_subfig_props(ax51, bbox=bbox, text='V6', fontsize=fs, useGrid=grid, nx_ticks=nx_ticks, ny_ticks=ny_ticks,
                           figaspect=aspect, clip_on=True)
    # --------------------------------------------------------------------------
    # ---FOOTNOTES--------------------------------------------------------------
    xticks       = ax_left_footnote.get_xticks()
    yticks       = ax_left_footnote.get_yticks()
    delta_xticks = xticks[1]-xticks[0]
    delta_yticks = yticks[1]-yticks[0]

    tab  = 0
    txt  = '{}ms/DIV'.format(delta_xticks)
    ax_left_footnote.annotate(txt, xy=(tab, -10), ha='left', va='top',
                              xycoords='axes pixels', fontsize=.8*fs)
    tab += 70
    txt  = '{}mV/DIV'.format(delta_yticks)
    ax_left_footnote.annotate(txt, xy=(tab, -10), ha='left', va='top',
                              xycoords='axes pixels', fontsize=.8*fs)
    if freq_highpass is not None:
        tab += 70
        txt  = 'HP: {}Hz'.format(freq_highpass)
        ax_left_footnote.annotate(txt, xy=(tab, -10), ha='left', va='top',
                              xycoords='axes pixels', fontsize=.8*fs)
    if freq_lowpass is not None:
        tab += 70
        txt  = 'LP: {}Hz'.format(freq_lowpass)
        ax_left_footnote.annotate(txt, xy=(tab, -10), ha='left', va='top', xycoords='axes pixels', fontsize=.8*fs)
    # --------------------------------------------------------------------------
    try:
        os.stat(filepath)
    except:
        os.mkdir(filepath)
    # --------------------------------------------------------------------------
    #fig.tight_layout()
    fig.savefig(os.path.join(filepath, 'ClinicalRecording.pdf'), format='pdf', transparent=True)
    #fig.savefig(os.path.join(filepath, 'ClinicalRecording.eps'), format='eps', transparent=True)

    if silent:
        plt.close()
    else:
        fig.show()
    return


def pcolormap(_t, outdata, outdata_filt, features, filepath='./',
              params=None, silent=False):

    # TODO: add footnotes for timing
    pltinds = features['pltinds']

    # ---UNZIP FIELD VALUES-----------------------------------------------------
    I   = outdata_filt['I']
    II  = outdata_filt['II']
    III = outdata_filt['III']
    aVF = outdata_filt['aVF']
    aVL = outdata_filt['aVL']
    aVR = outdata_filt['aVR']
    V1  = outdata_filt['V1']
    V2  = outdata_filt['V2']
    V3  = outdata_filt['V3']
    V4  = outdata_filt['V4']
    V5  = outdata_filt['V5']
    V6  = outdata_filt['V6']

    #
    t   = _t[pltinds]

    # MatLab export of dataset
    if aVF is not None:
        io.savemat(os.path.join(filepath, 'pcolormap.mat'),
                   dict(t=t,
                        I=I[pltinds], II=II[pltinds], III=III[pltinds],
                        aVR=aVR[pltinds], aVF=aVF[pltinds],aVL=aVL[pltinds],
                        V1=V1[pltinds], V2=V2[pltinds], V3=V3[pltinds],
                        V4=V4[pltinds], V5=V5[pltinds], V6=V6[pltinds]))
    else:
        io.savemat(os.path.join(filepath, 'pcolormap.mat'),
                   dict(t=t,
                        I=I[pltinds], II=II[pltinds], III=III[pltinds],
                        V1=V1[pltinds], V2=V2[pltinds], V3=V3[pltinds],
                        V4=V4[pltinds], V5=V5[pltinds], V6=V6[pltinds]))

    # create pcolor matrix
    map       = numpy.zeros(shape=(12,len(t)))
    map[0, :] =    I[pltinds]
    if aVR is not None:
        map[1, :] = -aVR[pltinds]
    map[2, :] =   II[pltinds]
    if aVF is not None:
        map[3, :] =  aVF[pltinds]
    map[4, :] =  III[pltinds]
    if aVL is not None:
        map[5, :] = -aVL[pltinds]
    map[6, :] =   V1[pltinds]
    map[7, :] =   V2[pltinds]
    map[8, :] =   V3[pltinds]
    map[9, :] =   V4[pltinds]
    map[10,:] =   V5[pltinds]
    map[11,:] =   V6[pltinds]


    fs            = 10    # font size for text labels
    interpolation = 'none'
    figaspect     = (t[-1] - t[0])/12.
    my_cmap       = matplotlib.cm.get_cmap('gray')
    #my_cmap.set_under('w')

    fig           = plt.figure(dpi=100)

    fig.suptitle('ID:' + randomID() + '    ' + mydatestring(), fontsize=10)
    plt.imshow(map, aspect=float(figaspect), #clim(0.0,0.7),
               interpolation=interpolation, cmap=my_cmap)
    plt.colorbar()

    ax = plt.gca()
    plt.setp(ax.get_xticklabels(), visible=False)
    plt.setp(ax.get_yticklabels(), visible=False)
    plt.tick_params(axis='both', which='both', bottom='off', top='off',
                    left='off', right='off', labelbottom='off')

    # ---FOOTNOTES--------------------------------------------------------------
    kwargs = {'ha': 'right', 'va': 'center', 'size': fs, 'clip_on': False,
              'color': 'black', 'transform': ax.transAxes}
    ax.text(0., 11./12.+.5/12., u'I', **kwargs)
    ax.text(0., 10./12.+.5/12., u'-aVR', **kwargs)
    ax.text(0.,  9./12.+.5/12., u'II', **kwargs)
    ax.text(0.,  8./12.+.5/12., u'aVF', **kwargs)
    ax.text(0.,  7./12.+.5/12., u'III', **kwargs)
    ax.text(0.,  6./12.+.5/12., u'-aVL', **kwargs)
    ax.text(0.,  5./12.+.5/12., u'V1', **kwargs)
    ax.text(0.,  4./12.+.5/12., u'V2', **kwargs)
    ax.text(0.,  3./12.+.5/12., u'V3', **kwargs)
    ax.text(0.,  2./12.+.5/12., u'V4', **kwargs)
    ax.text(0.,  1./12.+.5/12., u'V5', **kwargs)
    ax.text(0.,  0./12.+.5/12., u'V6', **kwargs)
    # --------------------------------------------------------------------------

    #fig.tight_layout()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    fig.savefig(os.path.join(filepath, 'PColorRecording.png'), format='png', transparent=False)
    if silent:
        plt.close()
    return


def periodogram_plot(t, outdata, outdata_filt, semilog=True, filepath='./', silent=False):

    I        = outdata['I']
    V3       = outdata['V3']
    aVF      = outdata['aVF']
    I_filt   = outdata_filt['I']
    V3_filt  = outdata_filt['V3']
    aVF_filt = outdata_filt['aVF']

    ts       = (t[1] - t[0])/1000  # sampling time in s
    fs       = 1 / ts              # sampling frequency in Hz


    fig = plt.figure(dpi=100)
    fig.suptitle('Power Spectral Density')

    plt.subplot(231)
    plt.title('Einthoven I')
    f, Pxx_den = signal.periodogram(I, fs)
    ylim       = [0, math.ceil(max(Pxx_den)*10)/10]
    if semilog:
        plt.semilogy(f, Pxx_den)
    else:
        plt.plot(f, Pxx_den)
        plt.ylim(ylim)
    plt.xlim(-20,)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.grid(True)


    plt.subplot(234)
    plt.title('Einthoven I (filtered)')
    f, Pxx_den = signal.periodogram(I_filt, fs)
    if semilog:
        plt.semilogy(f, Pxx_den)
    else:
        plt.plot(f, Pxx_den)
        plt.ylim(ylim)
    plt.xlim(-20,)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.grid(True)


    plt.subplot(232)
    plt.title('aVF')
    if aVF is not None:
        f, Pxx_den = signal.periodogram(aVF, fs)
        ylim       = [0, math.ceil(max(Pxx_den) * 10) / 10]
        if semilog:
            if max(Pxx_den) <= 0.:
                plt.plot(f, Pxx_den)
                plt.ylim(ylim)
            else:
                plt.semilogy(f, Pxx_den)
        else:
            plt.plot(f, Pxx_den)
            plt.ylim(ylim)
    plt.xlim(-20,)
    plt.grid(True)


    plt.subplot(235)
    plt.title('aVF (filtered)')
    if aVF_filt is not None:
        f, Pxx_den = signal.periodogram(aVF_filt, fs)
        ylim       = [0, math.ceil(max(Pxx_den) * 10) / 10]
        if semilog:
            if max(Pxx_den) <= 0.:
                plt.plot(f, Pxx_den)
                plt.ylim(ylim)
            else:
                plt.semilogy(f, Pxx_den)
        else:
            plt.plot(f, Pxx_den)
            plt.ylim(ylim)
    plt.xlim(-20,)
    plt.xlabel('frequency [Hz]')
    plt.grid(True)


    plt.subplot(233)
    plt.title('V3')
    f, Pxx_den = signal.periodogram(V3, fs)
    ylim       = [0, math.ceil(max(Pxx_den) * 10) / 10]
    if semilog:
        plt.semilogy(f, Pxx_den)
    else:
        plt.plot(f, Pxx_den)
        plt.ylim(ylim)
    plt.xlim(-20,)
    plt.grid(True)


    plt.subplot(236)
    plt.title('V3 (filtered)')
    f, Pxx_den = signal.periodogram(V3_filt, fs)
    if semilog:
        plt.semilogy(f, Pxx_den)
    else:
        plt.plot(f, Pxx_den)
        plt.ylim(ylim)
    plt.xlim(-20,)
    plt.xlabel('frequency [Hz]')
    plt.grid(True)

    fig.savefig(os.path.join(filepath, 'Periodogram.pdf'), format='pdf')
    #fig.savefig(os.path.join(filepath, 'Periodogram.eps'), format='eps')
    if silent:
        plt.close()
    else:
        fig.show()
    return


def readfile(input, sample_time=None, silent=False):

    projPath,_ = os.path.split(input)
    filetype   = input.split(".")[-1]
    datadict   = OrderedDict()

    (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(input)

    # ---reading pseudo output of ecg recorder
    if filetype == "xls":
        with open(input, "r") as f:
            delim   = '\t|\r|\n'
            header  = f.readline().rstrip().split('\t')
            datalst = re.split(delim, f.read())  # split tsv
            datalst = [item.replace(',', '.') for item in datalst] # change type of comma
            datalst = [item.strip() for item in datalst] # deblank strings

            datalst = filter(None, datalst) # remove empty strings
            if not isinstance(datalst, list):
                datalst = list(datalst)

        data       = numpy.asarray(datalst,dtype=numpy.float)
        num_traces = len(header)
        num_tsteps = int(len(data) / num_traces)
        data       = data.reshape(num_tsteps, num_traces).T


        # NOTE: ecg recorder tries to enforce the internal common mode reference of 1.3V
        # Einthoven limb electrodes
        if 'RA' in header: datadict['RA'] = data[header.index('RA'),:]
        if 'LA' in header: datadict['LA'] = data[header.index('LA'),:]
        if 'LL' in header: datadict['LL'] = data[header.index('LL'),:]
        # Einthoven lead data
        if 'Lead I'   in header: datadict['I']   = data[header.index('Lead I'),  :]
        if 'Lead II'  in header: datadict['II']  = data[header.index('Lead II'), :]
        if 'Lead III' in header: datadict['III'] = data[header.index('Lead III'),:]

        # ---------------------------------------------------------------------
        # Wilson leads
        # Assuming that V1-V6 for the Wilson's raw recordings, which need a
        # correction by wCT.
        # Assuming V1'-V6' if the correction has been done within the recorder.

        # Wilson leads are corrected?
        if 'V1\'' in header:
            wCTcorr = True
        else:
            wCTcorr = False

        if 'V1' in header: datadict['V1'] = data[header.index('V1'),:]
        if 'V2' in header: datadict['V2'] = data[header.index('V2'),:]
        if 'V3' in header: datadict['V3'] = data[header.index('V3'),:]
        if 'V4' in header: datadict['V4'] = data[header.index('V4'),:]
        if 'V5' in header: datadict['V5'] = data[header.index('V5'),:]
        if 'V6' in header: datadict['V6'] = data[header.index('V6'),:]

        # scale data to change units
        for key in datadict:
            if key is not None:
                datadict[key] = datadict[key] * 1000.  # V to mV

        datadict['wCTcorr'] = wCTcorr
        t                   = data[0, :]*1000     # time vector in ms
        ts                  = (t[1] - t[0])/1000  # sampling time in s
        fs                  = 1. / ts             # sampling frequency in Hz
        fnyq                = fs / 2.             # Nyquist frequency
        #datadict['ts']      = ts
        #datadict['fs']      = fs                 # sampling frequency in Hz
        datadict['fnyq']    = fnyq                # Nyquist frequency
        datadict['mtime']   = time.ctime(mtime)  # file last modified

    # reading openCARP cvs file (most 12 leads ecg plus header)
    elif filetype == "csv":

        cvsdata = np.genfromtxt(input, delimiter=',', skip_header=1)

        if cvsdata.shape[1] != 13:
            print('Assuming time plus 12 leads as input. Aborting...')
            return

        t  = np.asarray(cvsdata[:,0])
        ts = t[1] - t[0]    # sampling time
        fs = 1000. / ts                             # sampling frequency in Hz
        fnyq = fs / 2.                              # Nyquist frequency

        if fs > 1000.:
            print('Assuming time is stored at seconds granularity!')
            t    = t*1000.
            fs   = fs/1000.
            ts   = ts*1000.
            fnyq = fnyq/1000.

        datadict['mtime']= None    # don't know what this is supposed for
        datadict['ts']   = ts
        datadict['fs']   = fs
        datadict['fnyq'] = fnyq
        # Einthoven
        datadict['I']   = np.asarray(cvsdata[:,1])
        datadict['II']  = np.asarray(cvsdata[:,2])
        datadict['III'] = np.asarray(cvsdata[:,3])
        # Goldberger
        datadict['aVR'] = np.asarray(cvsdata[:,4])
        datadict['aVL'] = np.asarray(cvsdata[:,5])
        datadict['aVF'] = np.asarray(cvsdata[:,6])
        # Wilson
        datadict['V1'] = np.asarray(cvsdata[:,7])
        datadict['V2'] = np.asarray(cvsdata[:,8])
        datadict['V3'] = np.asarray(cvsdata[:,9])
        datadict['V4'] = np.asarray(cvsdata[:,10])
        datadict['V5'] = np.asarray(cvsdata[:,11])
        datadict['V6'] = np.asarray(cvsdata[:,12])

        datadict['wCTcorr'] = True

    # reading openCARP phie recovery file
    elif filetype == "igb":
        igbobj = igb.IGBFile(input)
        header = igbobj.header()
        data   = igbobj.data()
        igbobj.close()

        # reshape data
        num_traces = header.get('x')
        num_tsteps = header.get('t')
        inc_t      = header.get('inc_t')
        dim_t      = header.get('dim_t')

        # allow plots from unfinished simulations
        if len(data) / num_traces < num_tsteps:
            num_tsteps = len(data) / num_traces
            dim_t      = (num_tsteps - 1) * inc_t
            data       = numpy.resize(data, num_tsteps * num_traces)

        data             = data.reshape(num_tsteps, num_traces).T
        t                = numpy.linspace(0, dim_t, num_tsteps)
        ts               = inc_t        # sampling time
        fs               = 1000. / ts   # sampling frequency in Hz
        fnyq             = fs / 2.      # Nyquist frequency
        #datadict['ts']   = ts
        #datadict['fs']   = fs
        datadict['fnyq'] = fnyq
        datadict['mtime']= time.ctime(mtime)  # file last modified

        # relate traces to lead names
        datadict['RA']   = data[0, :]
        datadict['LA']   = data[1, :]

        if data.shape[0] == 9:
            if not silent:
                print ('Determined 9 ecg traces!')
            datadict['LL'] = data[2, :]

            datadict['V1']  = data[3, :]
            datadict['V2']  = data[4, :]
            datadict['V3']  = data[5, :]
            datadict['V4']  = data[6, :]
            datadict['V5']  = data[7, :]
            datadict['V6']  = data[8, :]
        elif data.shape[0] == 10:
            if not silent: print ('Determined 10 ecg traces!')
            datadict['LL'] = data[3, :]

            datadict['V1'] = data[4, :]
            datadict['V2'] = data[5, :]
            datadict['V3'] = data[6, :]
            datadict['V4'] = data[7, :]
            datadict['V5'] = data[8, :]
            datadict['V6'] = data[9, :]
        else:
            if not silent: print("Don't know what to do with this ecg signals!")
        datadict['wCTcorr'] = False

    # reading openCARP dat file: most likely a leadfield output without HEADER
    #                            and fixed position of keywords and number of columns
    # Note - studio ecg output is written with a header - see next elif!
    elif filetype == "dat" and not re.search(r"[aVRLFI1-6]", open(input, 'r').readline().strip()):

        with fopen(input, 'r') as fp:
            line  = fp.readline().strip()
            num_traces = numpy.char.split(line)

        data = txt.read(input)
        num_tsteps = data.size//num_traces

        # reshape data
        data = data.reshape(num_tsteps, num_traces)

        data = data.reshape(num_tsteps, num_traces).T
        t = numpy.linspace(0, sample_time*(num_tsteps-1), num_tsteps) # in ms
        ts = sample_time  # sampling time
        fs = 1000. / ts  # sampling frequency in Hz
        fnyq = fs / 2.  # Nyquist frequency
        # datadict['ts']   = ts
        # datadict['fs']   = fs
        datadict['fnyq'] = fnyq
        datadict['mtime'] = time.ctime(mtime)  # file last modified

        # relate traces to lead names
        datadict['RA'] = data[0, :]
        datadict['LA'] = data[1, :]

        if data.shape[0] == 9:
            if not silent:
                print('Determined 9 ecg traces!')
            datadict['LL'] = data[2, :]

            datadict['V1'] = data[3, :]
            datadict['V2'] = data[4, :]
            datadict['V3'] = data[5, :]
            datadict['V4'] = data[6, :]
            datadict['V5'] = data[7, :]
            datadict['V6'] = data[8, :]
        elif data.shape[0] == 10:
            if not silent: print('Determined 10 ecg traces!')
            datadict['LL'] = data[3, :]

            datadict['V1'] = data[4, :]
            datadict['V2'] = data[5, :]
            datadict['V3'] = data[6, :]
            datadict['V4'] = data[7, :]
            datadict['V5'] = data[8, :]
            datadict['V6'] = data[9, :]
        else:
            if not silent: print("Don't know what to do with this ecg signals!")
        datadict['wCTcorr'] = False

    # assuming this data file has a header and was written with studio
    elif filetype == 'dat':
        with fopen(input, 'r') as fp:
            line = fp.readline().strip()
            trace_names = (numpy.char.split(line)).tolist()
            num_traces = len(trace_names)

        data = np.genfromtxt(input, delimiter=' ', skip_header=1, dtype=float)
        num_tsteps = data.size//num_traces

        # reshape data
        data = data.reshape(num_tsteps, num_traces)
        data = data.reshape(num_tsteps, num_traces).T
        datadict = dict()

        for idx, trace in enumerate(trace_names):
            if 'time' == trace.lower():
                t = data[idx,:]
                ts = t[1]-t[0]  # sampling time
                fs = 1000. / ts # sampling frequency
                fnyq = fs /2.   # Nyquist frequency
            elif 'III' == trace.upper():
                datadict['III'] = data[idx,:]
            elif 'II' == trace.upper():
                datadict['II'] = data[idx, :]
            elif 'I' == trace.upper():
                datadict['I'] = data[idx, :]
            elif 'V1' == trace.upper():
                datadict['V1'] = data[idx, :]
            elif 'V2' == trace.upper():
                datadict['V2'] = data[idx, :]
            elif 'V3' == trace.upper():
                datadict['V3'] = data[idx, :]
            elif 'V4' == trace.upper():
                datadict['V4'] = data[idx, :]
            elif 'V5' == trace.upper():
                datadict['V5'] = data[idx, :]
            elif 'V6' == trace.upper():
                datadict['V6'] = data[idx, :]
            elif 'AVR' == trace.upper():
                datadict['aVR'] = data[idx, :]
            elif 'MAVR' == trace.upper():
                datadict['aVR'] = -data[idx, :]
            elif 'AVL' == trace.upper():
                datadict['aVL'] = data[idx, :]
            elif 'AVF' == trace.upper():
                datadict['aVF'] = data[idx, :]
            else:
                if not silent:
                    print("Don't know what to do with {}!".format(trace))

        datadict['fnyq'] = fnyq
        datadict['mtime'] = time.ctime(mtime)  # file last modified
        datadict['wCTcorr'] = True

    # reading openCARP dat file (most likely a leadfield output)
    elif filetype == "json":

        with fopen(input, 'r') as fp:
            jsondata = json.load(fp)

        t  = np.asarray(jsondata['t'])
        ts = t[1] - t[0]    # sampling time
        fs = 1000. / ts                             # sampling frequency in Hz
        fnyq = fs / 2.                              # Nyquist frequency

        if fs > 1000.:
            print('Assuming time is stored at seconds granularity!')
            t    = t*1000.
            fs   = fs/1000.
            ts   = ts*1000.
            fnyq = fnyq/1000.

        datadict['mtime']= None    # don't know what this is supposed for
        datadict['ts']   = ts
        datadict['fs']   = fs
        datadict['fnyq'] = fnyq
        # Einthoven
        datadict['I']   = np.asarray(jsondata['ecg']['I'])
        datadict['II']  = np.asarray(jsondata['ecg']['II'])
        datadict['III'] = np.asarray(jsondata['ecg']['III'])
        # Goldberger
        datadict['aVR'] = np.asarray(jsondata['ecg']['aVR'])
        datadict['aVL'] = np.asarray(jsondata['ecg']['aVL'])
        datadict['aVF'] = np.asarray(jsondata['ecg']['aVF'])
        # Wilson
        datadict['V1'] = np.asarray(jsondata['ecg']['V1'])
        datadict['V2'] = np.asarray(jsondata['ecg']['V2'])
        datadict['V3'] = np.asarray(jsondata['ecg']['V3'])
        datadict['V4'] = np.asarray(jsondata['ecg']['V4'])
        datadict['V5'] = np.asarray(jsondata['ecg']['V5'])
        datadict['V6'] = np.asarray(jsondata['ecg']['V6'])

        datadict['wCTcorr'] = True
    else:
        assert "Data reader for {} not yet implemented".format(input)


    # reading Vm file
    vmdata = None

    if os.path.isfile(os.path.join(projPath, 'vm.igb.gz')):
        vmFile = os.path.join(projPath, 'vm.igb.gz')
        mode   = 'rb'
    elif os.path.isfile(os.path.join(projPath, 'vm.igb')):
        vmFile = os.path.join(projPath, 'vm.igb')
        mode   = 'r'
    else:
        if not silent:
            print ('Did not find any Vm file!')

    if vmdata is not None:
        vmobj = igb.IGBFile(vmFile, mode)
        vmheader = vmobj.header()
        vmdata   = vmobj.data()
        vmdata   = vmdata.reshape(vmheader.get('t'), vmheader.get('x')).T
        vmobj.close()

    datadict['vm'] = vmdata

    verbose = False
    if verbose:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(t, datadict['V1'],'r')
        plt.show()

    return t, datadict


def get_plot_range(t, params=None):
    if isinstance(t, list):
        t = numpy.asarray(t,dtype=numpy.float)

    if isinstance(params, dict):
        if 'tlim' not in params:
            params         = {'tlim': None}
            params['tlim'] = [min(t), max(t)]


    if params is not None:
        a       = numpy.asarray(numpy.where(t <= params['tlim'][1]))
        b       = numpy.asarray(numpy.where(t >= params['tlim'][0]))
        pltinds = numpy.intersect1d(a, b)
    else:
        pltinds = range(len(t))
    return pltinds


def get_QRS_inds(t, I, Rindx=None):
    # Q, R and S amplitudes based on frontal projection

    if Rindx is None:
        mxIndx = numpy.argmax(I)
        if mxIndx.size > 1:
            mxIndx = mxIndx[0]
    else:
        mxIndx = Rindx

    # ---DEBUGGING PURPOSES---
    #fig = plt.figure(dpi=100)
    #plt.title('saggital')
    #plt.plot(t, I, 'g-', label='raw')
    #plt.grid(True)
    #plt.show()
    #plt.close()
    # ---

    searchRange = numpy.arange(mxIndx+1)
    if Rindx is None:
        Rindx   = numpy.argmax(I[searchRange])
    Sindx       = numpy.argmin(I[Rindx:])+Rindx
    Qindx       = numpy.argmin(I[0:Rindx])

    return Qindx.item(0), Rindx.item(0), Sindx.item(0)


def get_dQRS(t, _data, thresh=0.):
    # determine local activation time
    mn_lat = t[-1]
    mx_lat = t[ 0]

    data = deepcopy(_data)
    data[numpy.where(data >= 0.)] = 1
    data[numpy.where(data <  0.)] = 0

    rows, cols = data.shape
    for i in range(rows):
        d_diff = numpy.diff(data[i,:])
        index  = numpy.where(d_diff > 0.)
        index  = numpy.squeeze(index)

        if t[index] < mn_lat:
            mn_lat = t[index]
        if t[index] > mx_lat:
            mx_lat = t[index]

    return (mx_lat-mn_lat)
