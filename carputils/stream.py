#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some tools for handling output streams (stdout, etc.)
"""

import sys
from contextlib import contextmanager

class DummyStream(object):
    """
    A placeholder stream that simply ignores the write instruction.
    """
    def write(self, *args, **kwargs):
        pass

class RegexCallbackStream(object):
    """
    Simple stream-like class that calls a callback when a regex object matches
    a line in the string.

    Args:
        pattern : str
            A regular expression to match
        callback : function
            A python callback to execute when the regex is matched
    """
    
    def __init__(self, regex, callback, max_matches=None):
        assert callable(callback)
        self._regex    = regex
        self._callback = callback
        self._process  = True
        self._matches_remaining = max_matches

    def write(self, line, *args, **kwargs):
        """
        Write a line to the stream.
        """

        # Don't bother with process once close() has been called
        if not self._process:
            return
        
        # Search the line
        result = self._regex.search(line)

        # Call the callback if there was a match
        if result is not None:
            self._callback(result)

            if self._matches_remaining is not None:
                
                # Decrement remaining matches
                self._matches_remaining -= 1
                
                # Close if none remaining
                if self._matches_remaining <= 0:
                    self.close()

    def close(self):
        """
        Do not search the input any more, but allow calling of write()
        """
        self._process = False

class MultiStream(object):
    """
    A stream that forwards on the write call to other streams.

    Args:
        streams : list
            A list of output streams - practically, any object with a write method
    """

    def __init__(self, streams=[]):
        for s in streams:
            assert hasattr(s, 'write'), 'Not a valid stream'
        self._streams = streams

    def write(self, *args, **kwargs):
        for stream in self._streams:
            stream.write(*args, **kwargs)

def merge(streams):
    """
    Merge the provided streams into a single one.
    """
    if streams is None:
        return DummyStream()
    elif hasattr(streams, 'write'):
        # Single stream
        return streams
    elif len(streams) > 0:
        # Sequence of streams
        return MultiStream(streams)

@contextmanager
def divert_std(replacement):
    """
    Context manager to temporarily replace sys.stdout and sys.stderr.

    Either create a file object:
        
    >>> out = open('somefile.txt', 'r')

    or create a string stream:

    >>> from cStringIO import StringIO
    >>> out = StringIO()

    and then use thus context manager to ensure that all stdout and stderr gets
    written to the desired output:

    >>> with divert_stdout(out):
    >>>     print 'Hello, world!'

    Args:
        replacement : string stream
            Replacement string stream object - in practice, anything with a write
            method
    """

    # Swap new target and sys.stdout
    orig_stdout, sys.stdout = sys.stdout, replacement

    # Swap new target and sys.stderr
    orig_stderr, sys.stderr = sys.stderr, replacement

    try:
        yield

    finally:
        # Restore stdout and stderr
        sys.stdout = orig_stdout
        sys.stderr = orig_stderr
