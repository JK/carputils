#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/sh -f
#$ -V
#$ -cwd
#$ -j y
#$ -pe mpi {nnode}
#$ -N {jobID}
#$ -o {jobID}.$JOB_ID

# write ranks and slots
rank=0;
while read line; do
for i in {pinlist}; do
echo "rank" $rank"="$line "slot="$i
let "rank += 1"
done
done < $TMPDIR/machines > $TMPDIR/rankfile
cat $TMPDIR/rankfile
"""

class Mephisto(BatchPlatform):
    """
    Run on the local Mephisto GPU cluster
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpiexec'
    BATCH_EXT = '_Script'

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False,
                     *args, **kwargs):

        #cmd = [cls.LAUNCHER, '-n', nproc,
        #       '--hostfile', '$TMPDIR/machines',
        #       '--rankfile', '$TMPDIR/rankfile']
        cmd = [cls.LAUNCHER, '-n', nproc,
               '--hostfile', '$TMPDIR/machines']
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect',
                   '--cuda' if cuda else '--no-cuda'] + cmd

        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)

        if not cuda:
            assert 0 < nproc <= 72, 'Only 72 cores available on Mephisto'
            nnode = int((nproc + 11) / 12)
            pinlist = range(12)

        else:
            assert 0 < nproc <= 20, 'Only 20 GPUs available on Mephisto'
            nnode = int((nproc + 3) / 4)
            pinlist = range(0, 12, 4)

        # MEPHISTO DOESN'T LIKE COMMAS IN THE JOB-ID
        job_id = 'JB_' + jobID.replace(',', '_')
        return TEMPLATE.format(jobID=job_id, nnode=nnode,
                               pinlist=' '.join([str(i) for i in pinlist]))
