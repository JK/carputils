#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import Platform
from carputils.debug import mpi_attach_debugger

class Desktop(Platform):
    """
    Desktop platform, providing extensive debugging and profiling capabilities
    """

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, nproc_job=0, gdb_procs=None, ddd_procs=None,
                     lldb_procs=None, ddt=False, valgrind=None, map=False,
                     cuda=False):

        nproc = int(nproc)

        if gdb_procs is not None:

            if valgrind is not None:
                # Run with valgrind + gdb
                # gdb procs have --vgdb-error=1 set, others do not
                unsel = ['valgrind'] + ['--' + o for o in valgrind]
                sel = unsel + ['--vgdb-error=1']
                xterm = False

            else:
                # gdb only
                sel   = ['gdb', '--args']
                unsel = []
                xterm = True

            cmd = mpi_attach_debugger(cls.LAUNCHER, carp_cmd, nproc, gdb_procs,
                                      sel, unsel, xterm=xterm)

        elif valgrind is not None:

            # Run valgrind without gdb
            cmd = ['valgrind'] + ['--' + o for o in valgrind]

            if nproc > 1:
                cmd = [cls.LAUNCHER, '-n', nproc] + cmd

            cmd += carp_cmd

        elif ddd_procs is not None:

            # Normal launcher
            cmd = [cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

            # Get PETSc to launch debugger
            cmd += ['+', '-start_in_debugger', 'ddd']

            # Set specific procs if requested
            if len(ddd_procs) != 0:
                cmd += ['-debugger_nodes',
                        ','.join(str(i) for i in ddd_procs)]

        elif lldb_procs is not None:

            # lldb only
            sel   = ['lldb', '--']
            unsel = []
            xterm = True

            cmd = mpi_attach_debugger(cls.LAUNCHER, carp_cmd, nproc, lldb_procs,
                                      sel, unsel, xterm=xterm)

        elif ddt:
            cmd = ['ddt',
                   '--cuda' if cuda else '--no-cuda',
                   cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

        elif map:
            cmd = ['map',
                   cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

        else:
            if nproc == 1:
                cmd = carp_cmd
            elif nproc_job > 0:
                cmd = [cls.LAUNCHER, '-n', nproc_job]
                cmd += carp_cmd
            else:
                cmd = [cls.LAUNCHER, '-n', nproc]
                cmd += carp_cmd

        return cmd
