#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import math
from carputils.machines.general import BatchPlatform
TEMPLATE = """#!/bin/bash
#
#SBATCH --job-name={jobID}
#SBATCH --nodes={nnode}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --cpus-per-task=1
#SBATCH --output={jobID}.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user={email}
#SBATCH --time={walltime}
#SBATCH --account=e348
#SBATCH --partition=standard
#SBATCH --qos=standard

source {config}

# more optimized communication with ucx
# see archer2 wiki for more info
module load craype-network-ucx
module load cray-mpich-ucx

# as per archer2 wiki
export FI_OFI_RXM_SAR_LIMIT=64K
export SRUN_CPUS_PER_TASK=$SLURM_CPUS_PER_TASK

# No Hybrid MPI/OpenMP
export OMP_NUM_THREADS=1

"""

class ARCHER2(BatchPlatform):
    """
    Run on Archer2
    Info: https://www.archer2.ac.uk/
    """

    SUBMIT    = 'sbatch'
    LAUNCHER  = 'srun --distribution=block:block --hint=nomultithread '
    PYTHON    = 'python'
    BATCH_EXT = '.slrm'

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, nproc_job=0, ddt=False, cuda=False,
                     *args, **kwargs):
        cmd = [cls.LAUNCHER]
        if nproc_job > 0:
            cmd += ['-ntasks', nproc_job]
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect'] + cmd

        return cmd

    @classmethod
    def add_python_launcher(cls, exe_cmd):
        cmd = [cls.LAUNCHER, 'python'] + exe_cmd
        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email, config=None):

        nproc = int(nproc)

        # Check sensible number of nodes
        #assert nproc % 128 == 0, 'Use a multiple of 128 processes on Archer2'

        nnode = math.ceil(nproc / 128)
        t_p_n = min(nproc, 128)
        config = config if config else os.path.join(os.environ['HOME'], '.bashrc')
        return TEMPLATE.format(jobID=jobID, nnode=nnode, tpn=t_p_n, walltime=walltime,
                               email=email, config=config)

    @classmethod
    def polling(cls, polling_opts, nproc, nproc_job, script):

        if polling_opts is None:
            return None

        # Check sensible number of nodes per job
        assert nproc_job <= nproc,\
            'Number of Processes per job has to be smaller than the total '\
            'number of processes!'

        # Get maximal number of tasks per node
        max_tasks = int(16 / nproc_job)

        # Add polling file options
        poll_tmp = "runopts=(\n"

        nruns = len(polling_opts)
        for i in range(nruns):
            poll_tmp += '  \"' + \
                        str(polling_opts[i]).replace('\n', '').replace('\r', '') + \
                        '\"\n'

        poll_tmp += ")\n\n"
        poll_tmp += "tasks_to_be_done=${#runopts[@]}  ## total number of tasks\n"
        poll_tmp += "max_tasks={}                     "\
                    "## number of tasks per node\n\n".format(max_tasks)
        poll_tmp += "current_task=0                   ## initialization\n"
        poll_tmp += "running_tasks=0                  ## initialization\n\n"

        poll_tmp += "while (($current_task < $tasks_to_be_done))\n"\
                    "do\n\n"\
                    "   ## count the number of tasks currently running\n"\
                    "   running_tasks=`ps -C test --no-headers | wc -l`\n\n"\
                    "   while (($running_tasks < $max_tasks && "\
                    "${current_task} < ${tasks_to_be_done}))\n"\
                    "   do\n"\
                    "      ((current_task++))\n\n"\
                    "      ## run application\n"
        poll_tmp += "      " + script + " ${runopts[${current_task}]} &\n\n"

        poll_tmp += "      ## count the number of tasks currently running\n"\
                    "      running_tasks=`ps -C test --no-headers | wc -l`\n"\
                    "   done\n"\
                    "done\n"\
                    "wait\n"

        return poll_tmp
