#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import math
from carputils.machines.general import BatchPlatform
TEMPLATE = """#!/usr/bin/bash

#SBATCH --job-name={jobID}
#SBATCH --nodes={nnode}
#SBATCH --ntasks={nproc}
#SBATCH --cpus-per-task=1
#SBATCH --output={jobID}.out
#SBATCH --error={jobID}.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user={email}
#SBATCH --time={walltime}
#SBATCH --partition={partition}

source {config}

# No Hybrid MPI/OpenMP
export OMP_NUM_THREADS=1

"""

class BwUniCluster(BatchPlatform):
    """
    Run on BwUniCluster 2.0
    Info: https://wiki.bwhpc.de/e/Category:BwUniCluster_2.0
    """

    SUBMIT    = 'sbatch'
    LAUNCHER  = 'mpirun --bind-to core --map-by core'
    PYTHON    = 'python'
    BATCH_EXT = '.slrm'

    DEFAULT_QUEUE = 'single'
    AVAILABLE_QUEUES = ['single', 'dev_single', 'dev_multiple', 'multiple', 'dev_multiple_e', 'multiple_e', 'fat']

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False,
                     *args, **kwargs):
        cmd = [cls.LAUNCHER]
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect'] + cmd

        return cmd

    @classmethod
    def add_python_launcher(cls, exe_cmd):
        cmd = [cls.LAUNCHER, 'python'] + exe_cmd
        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email, config=None):

        nproc = int(nproc)

        if queue == 'multiple':
            assert nproc > 40 and nproc <= 128 * 40,\
                'Use a multiple of 40 processes for \'multiple\' partition, 80 minimum, 128 * 40 maximum.'
            nnode = math.ceil(nproc / 40)
        elif queue == 'dev_multiple':
            assert nproc > 40 and nproc <= 8 * 40, \
                'Use a multiple of 40 processes for \'dev_multiple\' partition, 80 minimum, 8 * 40 maximum.'
            nnode = math.ceil(nproc / 40)
        elif queue == 'multiple_e':
            assert nproc > 40 and nproc <= 128 * 40, \
                'Use a multiple of 28 processes for \'multiple\' partition, 56 minimum, 128 * 28 maximum.'
            nnode = math.ceil(nproc / 28)
        elif queue == 'dev_multiple_e':
            assert nproc > 40 and nproc <= 8 * 40, \
                'Use a multiple of 28 processes for \'dev_multiple\' partition, 56 minimum, 8 * 28 maximum.'
            nnode = math.ceil(nproc / 28)
        elif queue == 'fat':
            assert nproc <= 80, 'Use less than 80 processes on \'fat\' partition.'
            nnode = 1
        else:
            assert nproc <= 40, 'Use less than 40 processes on bwUniCluster single partitions.'
            nnode = 1

        config = config if config else os.path.join(os.environ['HOME'], '.bashrc')
        return TEMPLATE.format(jobID=jobID, nnode=nnode, nproc=nproc, walltime=walltime,
                               email=email, partition=cls.determine_queue(queue), config=config)

