#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from os.path import expandvars

class ClassPropertyDescriptor(object):

    def __init__(self, fget):
        self.fget = fget

    def __get__(self, obj, klass=None):
        if klass is None:
            klass = type(obj)
        return self.fget.__get__(obj, klass)()

def classproperty(func):
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)
    return ClassPropertyDescriptor(func)

class Platform(object):
    """
    Generic platform oject outlying the interface
    """

    BATCH = False
    SUBMIT = None

    @classproperty
    def LAUNCHER(cls):
        """
        A little trickery to prevent cyclic import
        """
        from carputils import settings
        return expandvars(settings.config.MPIEXEC)

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, nproc_job=0, gdb_procs=None, ddd_procs=None,
                     ddt=False, valgrind=None, map=False, cuda=False):
        """
        Add MPI launcher to openCARP command

        Parameters
        ----------
        carp_cmd : list
            Python list of openCARP command, with arguments
        nproc : int
            Number of processes to use
        gdb_procs : list or None
            None - no gdb, empty list - gdb on all procs, non-empty list -
            debug specific proc(s)
        ddd_procs : list or None
            None - no ddd, empty list - ddd on all procs, non-empty list -
            debug specific proc(s)
        ddt : bool
            Debug with Allinea DDT
        valgrind : list or None
            None - no valgrind, list - valgrind with given options
        map : bool
            Profile with Allinea MAP
        cuda : bool
            Job is being run with CUDA

        Returns
        -------
        list
            List of command line options with launcher and debug opts added
        """

        raise NotImplementedError()

    @classmethod
    def add_python_launcher(cls, exe_cmd):
        """
        Add python launcher to exe command

        Parameters
        ----------
        exe_cmd : list
            Python list of CARP command, with arguments

        Returns
        -------
        list
            List of command line options with launcher added
        """

        raise NotImplementedError()

class BatchPlatform(Platform):
    """
    Generic class for batch platforms
    """

    BATCH = True
    BATCH_EXT = None

    DEFAULT_QUEUE = None
    AVAILABLE_QUEUES = None

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, nproc_job=0, ddt=False, cuda=False,
                     *args, **kwargs):

        cmd = cls.LAUNCHER.split() + ['-n', nproc] + carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect',
                   '--cuda' if cuda else '--no-cuda'] + cmd

        return cmd

    @classmethod
    def add_python_launcher(cls, exe_cmd):

        cmd = cls.PYTHON.split() + exe_cmd
        return cmd

    @classmethod
    def determine_queue(cls, user_selected=None):
        """
        Determine the correct queue.
        """

        if user_selected is not None:

            if user_selected not in cls.AVAILABLE_QUEUES:

                # Construct error message
                msg = 'invalid queue: '
                msg += repr(user_selected)
                msg += ', must be one of {'
                msg += ','.join([repr(q) for q in cls.AVAILABLE_QUEUES])
                msg += '}'

                raise ValueError(msg)

            return user_selected

        else:
            return cls.DEFAULT_QUEUE

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email, config=None):
        """
        Generate the header for a run script on this platform

        Parameters
        ----------
        jobID : str
            Identifier for this job in batch system
        nproc : int
            Number of processes to use
        walltime : str
            Walltime string of format HH:MM:SS
        queue : str
            Specify a queue to submit to
        cuda : bool
            True if running a CUDA executable
        email : str
            Email address to notify when done

        Returns
        -------
        str
            The generated header
        """

        raise NotImplementedError()

    @classmethod
    def footer(cls):
        """
        Generate the footer for a run script on this platform
        """
        return ""
