#
# This file was auto-generated by generate.py
# do not edit, manual changes might get lost.
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from .general import AbstractPlugin as _AbstractPlugin, NoPlugin
from collections import OrderedDict as _OrderedDict
from carputils.format import stringtable


class Stress_Land12Stress(_AbstractPlugin):
    """
    Describes the Stress_Land12 active stress model

    Parameters
    ----------
    a : float, optional
        Value of a, defaults to 0.35
    A_1 : float, optional
        Value of A_1, defaults to -29.0
    A_2 : float, optional
        Value of A_2, defaults to 116.0
    alpha_1 : float, optional
        Value of alpha_1, defaults to 0.1
    alpha_2 : float, optional
        Value of alpha_2, defaults to 0.5
    beta_0 : float, optional
        Value of beta_0, defaults to 1.65
    beta_1 : float, optional
        Value of beta_1, defaults to -1.5
    Ca_50ref : float, optional
        Value of Ca_50ref, defaults to 0.8
    k_TRPN : float, optional
        Value of k_TRPN, defaults to 0.1
    k_xb : float, optional
        Value of k_xb, defaults to 0.1
    n_TRPN : float, optional
        Value of n_TRPN, defaults to 2.0
    n_xb : float, optional
        Value of n_xb, defaults to 5.0
    T_ref : float, optional
        Value of T_ref, defaults to 120.0
    TRPN_50 : float, optional
        Value of TRPN_50, defaults to 0.35
    """
    SHORTNAME = 'stress_land12'
    PLUGIN = 'Stress_Land12'
    PARAMETERS = {'a': 0.35,  'A_1': -29.0,  'A_2': 116.0,  'alpha_1': 0.1,  
                  'alpha_2': 0.5,  'beta_0': 1.65,  'beta_1': -1.5,  
                  'Ca_50ref': 0.8,  'k_TRPN': 0.1,  'k_xb': 0.1,  
                  'n_TRPN': 2.0,  'n_xb': 5.0,  'T_ref': 120.0,  
                  'TRPN_50': 0.35}


class Stress_Land17Stress(_AbstractPlugin):
    """
    Describes the Stress_Land17 active stress model

    Parameters
    ----------
    beta_0 : float, optional
        Value of beta_0, defaults to 2.3
    beta_1 : float, optional
        Value of beta_1, defaults to -2.4
    dr : float, optional
        Value of dr, defaults to 0.25
    gamma : float, optional
        Value of gamma, defaults to 0.0085
    gamma_wu : float, optional
        Value of gamma_wu, defaults to 0.615
    koff : float, optional
        Value of koff, defaults to 0.1
    ktm_unblock : float, optional
        Value of ktm_unblock, defaults to 1.0
    perm50 : float, optional
        Value of perm50, defaults to 0.35
    phi : float, optional
        Value of phi, defaults to 2.23
    TOT_A : float, optional
        Value of TOT_A, defaults to 25.0
    Tref : float, optional
        Value of Tref, defaults to 120.0
    TRPN_n : float, optional
        Value of TRPN_n, defaults to 2.0
    wfrac : float, optional
        Value of wfrac, defaults to 0.5
    """
    SHORTNAME = 'stress_land17'
    PLUGIN = 'Stress_Land17'
    PARAMETERS = {'beta_0': 2.3,  'beta_1': -2.4,  'dr': 0.25,  
                  'gamma': 0.0085,  'gamma_wu': 0.615,  'koff': 0.1,  
                  'ktm_unblock': 1.0,  'perm50': 0.35,  'phi': 2.23,  
                  'TOT_A': 25.0,  'Tref': 120.0,  'TRPN_n': 2.0,  'wfrac': 0.5}


class Stress_LumensStress(_AbstractPlugin):
    """
    Describes the Stress_Lumens active stress model

    Parameters
    ----------
    VmThresh : float, optional
        Value of VmThresh, defaults to -50.0
    """
    SHORTNAME = 'stress_lumens'
    PLUGIN = 'Stress_Lumens'
    PARAMETERS = {'VmThresh': -50.0}


class Stress_NiedererStress(_AbstractPlugin):
    """
    Describes the Stress_Niederer active stress model

    Parameters
    ----------
    a : float, optional
        Value of a, defaults to 0.35
    A_1 : float, optional
        Value of A_1, defaults to -29.0
    A_2 : float, optional
        Value of A_2, defaults to 138.0
    A_3 : float, optional
        Value of A_3, defaults to 129.0
    alpha_0 : float, optional
        Value of alpha_0, defaults to 0.008
    alpha_1 : float, optional
        Value of alpha_1, defaults to 0.03
    alpha_2 : float, optional
        Value of alpha_2, defaults to 0.13
    alpha_3 : float, optional
        Value of alpha_3, defaults to 0.625
    alpha_r1 : float, optional
        Value of alpha_r1, defaults to 0.002
    alpha_r2 : float, optional
        Value of alpha_r2, defaults to 0.00175
    beta_0 : float, optional
        Value of beta_0, defaults to 4.9
    beta_1 : float, optional
        Value of beta_1, defaults to -4.0
    Ca_50ref : float, optional
        Value of Ca_50ref, defaults to 0.00105
    Ca_TRPN_Max : float, optional
        Value of Ca_TRPN_Max, defaults to 0.07
    gamma_trpn : float, optional
        Value of gamma_trpn, defaults to 2.0
    k_on : float, optional
        Value of k_on, defaults to 100.0
    k_Ref_off : float, optional
        Value of k_Ref_off, defaults to 0.2
    K_z : float, optional
        Value of K_z, defaults to 0.15
    n_Hill : float, optional
        Value of n_Hill, defaults to 3.0
    n_Rel : float, optional
        Value of n_Rel, defaults to 3.0
    T_ref : float, optional
        Value of T_ref, defaults to 56.2
    z_p : float, optional
        Value of z_p, defaults to 0.85
    """
    SHORTNAME = 'stress_niederer'
    PLUGIN = 'Stress_Niederer'
    PARAMETERS = {'a': 0.35,  'A_1': -29.0,  'A_2': 138.0,  'A_3': 129.0,  
                  'alpha_0': 0.008,  'alpha_1': 0.03,  'alpha_2': 0.13,  
                  'alpha_3': 0.625,  'alpha_r1': 0.002,  'alpha_r2': 0.00175,  
                  'beta_0': 4.9,  'beta_1': -4.0,  'Ca_50ref': 0.00105,  
                  'Ca_TRPN_Max': 0.07,  'gamma_trpn': 2.0,  'k_on': 100.0,  
                  'k_Ref_off': 0.2,  'K_z': 0.15,  'n_Hill': 3.0,  
                  'n_Rel': 3.0,  'T_ref': 56.2,  'z_p': 0.85}


class Stress_RiceStress(_AbstractPlugin):
    """
    Describes the Stress_Rice active stress model

    Parameters
    ----------
    failing : float, optional
        Value of failing, defaults to 0.0
    force_coeff : float, optional
        Value of force_coeff
    SLmax : float, optional
        Value of SLmax, defaults to 2.4
    SLmin : float, optional
        Value of SLmin, defaults to 1.4
    """
    SHORTNAME = 'stress_rice'
    PLUGIN = 'Stress_Rice'
    PARAMETERS = {'failing': 0.0,  'force_coeff': None,  'SLmax': 2.4,  
                  'SLmin': 1.4}


MAPPING = _OrderedDict()
MAPPING['stress_land12']   = Stress_Land12Stress
MAPPING['stress_land17']   = Stress_Land17Stress
MAPPING['stress_lumens']   = Stress_LumensStress
MAPPING['stress_niederer'] = Stress_NiedererStress
MAPPING['stress_rice']     = Stress_RiceStress
MAPPING['none']            = NoPlugin


def keys():
    """
    Return a list of string keys to be used with :func:`get`
    """
    return MAPPING.keys()


def get(*args, **kwargs):
    """
    Get a class from its key string from :func:`keys`
    """
    return MAPPING.get(*args, **kwargs)


def summary(indent=2, ncolumns=4, padding=2):
    """
    Return a string summarizing the models and their parameters
    """
    modelattr = ['SHORTNAME', 'PLUGIN']
    maxattrlen = max(map(len, modelattr+['PARAMETERS']))
    summarystr = 'STRESS MODELS\n\n'
    indentstr = ' '*indent
    for name, model in MAPPING.items():
        summarystr += indentstr+'{}\n'.format(name)
        for attr in modelattr:
            obj = getattr(model, attr, None)
            if obj is None:
                continue
            summarystr += indentstr*2+'{}: {}\n'.format(attr.lower().ljust(maxattrlen), str(obj))
        attr = 'PARAMETERS'
        obj = getattr(model, attr, None)
        if obj is not None and len(obj) > 0:
            tablestr = stringtable(obj, ncolumns, padding, rowprefix=' '*(maxattrlen+2*indent+2))
            summarystr += indentstr*2+attr.lower().ljust(maxattrlen)+': '+tablestr[len(attr)+2*indent+2:]
        summarystr += '\n'
    return summarystr
