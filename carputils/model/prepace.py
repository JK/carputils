#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

EXAMPLE_DESCRIPTIVE_NAME = 'Tissue prepacing to reach steady state'
EXAMPLE_AUTHOR = 'Luca Azzolin <luca.azzolin@kit.edu>'

import os
import sys

from datetime import date
import numpy as np

def prepace(args, job, cmd_ref, meshname, sinus, steady_state_dir, tissue_init):
    """
    Tissue prepacing:

    It consists of a series of pulses at a fixed basic cycle length to let the
    tissue reach a stable limit cycle. An activation time map is computed for
    the last beat. This method will generate an intermediate state to be loaded
    in the other protocols. This is not a protocol to induce arrhythmia.

    Input: parser arguments (args), output directory (job.ID), struct containing imp_regions and gregions (cmd_ref),
    meshname, sinus node location (sinus), prepacing directory (steady_state_dir) and tissue initialization 
    
    Args:
    '--M_lump',
        type=int,
        default='1',
        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.'
    '--stim_size',
        type=str,
        default='2000.0',
        help='stimulation edge square size in micrometers'
    '--prepace_bcl',
        type=float, default=500.0,
        help='initial basic cycle lenght in [ms]'
    '--prebeats',
        type = int,
        default = 4, 
        help='Number of beats to prepace the tissue'

    """
    simid = steady_state_dir
    
    try:
        os.makedirs(simid)
    except OSError:
        print ("Creation of the directory %s failed" % simid)
    else:
        print ("Successfully created the directory %s " % simid)

    cmd = list(cmd_ref)
    tsav_state = args.prebeats*args.prepace_bcl
    # Setting the stimulus at the sinus node
    prepace = ['-num_stim',                     1,
            '-num_tsav', 1,
            '-tsav[0]', tsav_state,
            '-stimulus[0].stimtype',         0,
            '-stimulus[0].strength',         args.prepace_strength,
            '-stimulus[0].duration',         2.0,
            '-stimulus[0].npls',             args.prebeats,
            '-stimulus[0].bcl',              args.prepace_bcl]
    cmd += tissue_init + prepace + sinus

    cmd += ['-simID', simid,
            '-dt',  20,
            '-spacedt', 10,
            '-mass_lumping', args.M_lump,
            '-timedt', 100,
            '-tend', tsav_state+0.1,
            '-meshname', meshname]
    #Run simulation
    job.carp(cmd)

    old_file = os.path.join(str(simid), "vm.igb")
    new_file = os.path.join(str(simid), "vm_prepace.igb")
    os.rename(old_file, new_file)

    # Last beat
    cmd = list(cmd_ref)

    startstatef = simid + '/state.' + str(tsav_state)
    tsav_state += args.prepace_bcl 
    last_beat = ['-num_stim',                     1,
            '-start_statef', startstatef,
            '-num_tsav', 1,
            '-tsav[0]', tsav_state,
            '-stimulus[0].start',            args.prebeats*args.prepace_bcl,
            '-stimulus[0].stimtype',         0,
            '-stimulus[0].strength',         args.prepace_strength,
            '-stimulus[0].duration',         2.0,
            '-stimulus[0].npls',             1]
    
    lat = ['-num_LATs',           1,
           '-lats[0].all',        0,
           '-lats[0].measurand',  0,
           '-lats[0].mode',       0,
           '-lats[0].threshold',  -50]

    cmd += last_beat + sinus + lat

    cmd += ['-simID', simid,
            '-dt',  20,
            '-spacedt', 1,
            '-timedt', 100,
            '-mass_lumping', args.M_lump,
            '-tend', tsav_state+0.1,
            '-meshname', meshname]
            
    #Run simulation
    job.carp(cmd)

    old_file = os.path.join(str(simid), "vm.igb")
    new_file = os.path.join(str(simid), "vm_last_beat.igb")
    os.rename(old_file, new_file)

