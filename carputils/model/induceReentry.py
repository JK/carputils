#!/usr/bin/env python
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
# Author: Luca Azzolin


import os
import math

from carputils import settings
from carputils.carpio import igb

import numpy as np
import numpy.matlib
from numpy import linalg as LA
from scipy import spatial
from scipy.sparse import coo_matrix
from scipy.sparse import dia_matrix
from scipy.sparse import csr_matrix
from scipy.sparse import csc_matrix
from scipy.sparse import lil_matrix
from scipy.sparse import identity
from scipy.sparse.linalg import spsolve
from scipy.sparse.linalg import spilu
from scipy.sparse.linalg import bicgstab
from scipy.sparse.linalg import LinearOperator


"""

4 different protocols to induce reentry are implemented:
"RP_E": Rapid Pacing with activity checking at the End of the protocol; \
"RP_B": Rapid Pacing with activity checking after each Beat; \
"PSD": Phase Singularity Distribution method; \
"PEERP": Pacing at the End of the Effective Refractory Period'.

An example of their application is presented in the experiment:
02_tissue/21_induction_protocols

"""

# PEERP

def converge_to_EB(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2, upper_bound, lower_bound):
    iterations = 0

    while upper_bound - lower_bound > args.tol:
        iterations +=1
        print ('iteration: ', iterations)

        stim = ['-num_stim',                     1,
                '-stimulus[0].stimtype',         0,
                '-stimulus[0].strength',         args.strength,
                '-stimulus[0].start',            start_S2,
                '-stimulus[0].duration',         2,
                '-stimulus[0].npls',             1,
                '-stimulus[0].ctr_def',          1,
                '-stimulus[0].x0',               stim_index[0],
                '-stimulus[0].xd',               args.stim_size,
                '-stimulus[0].y0',               stim_index[1],
                '-stimulus[0].yd',               args.stim_size,
                '-stimulus[0].z0',               stim_index[2],
                '-stimulus[0].zd',               args.stim_size]

        cmd = list(cmd_ref) + stim 

        cmd += ['-simID', simid,
                '-dt',  20,
                '-timedt', 30,
                '-spacedt', 1,
                '-start_statef', startstatef,
                '-num_tsav', 2,
                '-tsav[0]', start_S2-1,
                '-tsav[1]', start_S2+60,
                '-mass_lumping', args.M_lump,
                '-tend', start_S2 + 60.1,
                '-meshname', meshname]
                
        #Run simulation
        job.carp(cmd)

        igb_f = igb.IGBFile(simid+'/vm.igb')
        header = igb_f.header()
        cmd = [settings.execs.igbextract,
            '-l', nodes_to_check_str,
            '-t', header['dim_t']-30,
            '-O', simid+'/out',
            simid + '/vm.igb']
        job.bash(cmd)

        vm = np.loadtxt(simid + '/out')
        vm = vm[:,1:]

        M2 = np.amax(vm)

        if M2 < -50:     #REFRACTORY
            print ('REFRACTORY   S2_start = ', start_S2, M2)
            lower_bound = start_S2
            old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2-1))
            new_file = os.path.join(str(simid), 'state_lower_bound.roe')
            os.rename(old_file, new_file)
            old_file = os.path.join(str(simid), 'vm.igb')
            new_file = os.path.join(str(simid), 'vm_lower_bound.igb')
            os.rename(old_file, new_file)
            startstatef = simid + '/state_lower_bound'
            start_S2 = float(math.ceil((upper_bound + lower_bound)/2))
            print (upper_bound, lower_bound)

        else:           #NOT REFRACTORY
            print ('NOT REFRACTORY   S2_start = ', start_S2, M2)
            upper_bound = start_S2
            old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2+60))
            new_file = os.path.join(str(simid), 'state_upper_bound.roe')
            os.rename(old_file, new_file)
            old_file = os.path.join(str(simid), 'vm.igb')
            new_file = os.path.join(str(simid), 'vm_upper_bound.igb')
            os.rename(old_file, new_file)
            start_S2 = float(math.ceil((upper_bound + lower_bound)/2))
            print (upper_bound, lower_bound)

    return upper_bound, lower_bound, start_S2, startstatef

def PEERP(args, job, cmd_ref, meshname, sinus, stim_points, steady_state_dir):
    """
    Pacing at the End of the Effective Refractory Period:

    It triggers ectopic beats at the end of the effective refractory period, 
    computed as the minimum coupling interval at which the action potential 
    could propagate in the tissue.

    Input: parser arguments (args), output directory (job.ID), struct containing imp_regions and gregions (cmd_ref),
    meshname, sinus node location (sinus), stimulation point location (x,y,z) and prepacing directory (steady_state_dir)
    
    Args:
    '--M_lump',
        type=int,
        default='1',
        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.'
    '--stim_size',
        type=str,
        default='2000.0',
        help='stimulation edge square size in micrometers'
    '--max_n_beats_PEERP',
        type=int,
        default=2,
        help='Max number of beats for the PEERP'
    '--APD_percent',
        type=float, default=94.0,
        help='action potential duration percentage to set as first guess to find the end of the effective refractory period'
    '--tol',
        type=float, default=1.0,
        help='tolerance to compute the end of the effective refractory period'

    """

    simid = job.ID  
    try:
        os.makedirs(simid)
    except OSError:
        print ("Creation of the directory %s failed" % simid)
    else:
        print ("Successfully created the directory %s " % simid)

    stim_file = simid + '/stim_point.txt'
    with open(stim_file, 'w') as f:
        f.write("1\n")
        f.write("{} {} {} 500\n".format(stim_points[0],stim_points[1],stim_points[2]))

    # Find vertex id of closest point
    os.system('meshtool query idxlist -msh={} -coord={}'.format(meshname, stim_file))
    index_node = np.loadtxt(simid+'/stim_point.txt.out.txt', dtype=int, skiprows=1, usecols=(0,))
    index_node_str = str(index_node)

    #Now compute the APD
    igbdir = steady_state_dir + '/vm_last_beat.igb'
    igbout = '--output-file=' + job.ID + '/apd_{}.dat'.format(str(args.APD_percent))
    cmd  = [ settings.execs.igbapd,
             '--repol='+str(args.APD_percent),
             igbout,
             igbdir ]

    #Run simulation
    job.bash(cmd)

    apdfile = job.ID + '/apd_{}.dat'.format(str(args.APD_percent))
    actfile = steady_state_dir + '/init_acts_vm_act-thresh.dat'

    stim_index = stim_points
    node_coord_str = ','.join([str(i) for i in stim_index])

    # Generate list of virtual electrodes around the stimulation point to check wave propagation
    os.system("meshtool extract surface -msh={} -surf={}_node_{}.vtx -size={} -lower_size={} -coord={}".format(meshname, meshname, index_node, float(args.stim_size)*3, float(args.stim_size)*2, node_coord_str))
    nodes_to_check = np.loadtxt(meshname +'_node_{}.vtx.surf.vtx'.format(index_node), skiprows=2,dtype=int)
    nodes_to_check_str = ','.join([str(i) for i in nodes_to_check])

    simid = job.ID + '/point_{}'.format(index_node) +'/beat_0'
    try:
        os.makedirs(simid)
    except OSError:
        print ("Creation of the directory %s failed" % simid)
    else:
        print ("Successfully created the directory %s " % simid)
   
    with open(apdfile) as fp:
        for i, line in enumerate(fp):
            if i == index_node:
                APD = float(line)
            elif i > index_node:
                break
    with open(actfile) as fp:
        for i, line in enumerate(fp):
            if i == index_node:
                ACT = float(line)
            elif i > index_node:
                break

    tbeat = []
    lower_bound = float(math.ceil(ACT+APD)-40)
    start_S2 = float(math.ceil(ACT+APD))
    upper_bound = float(math.ceil(start_S2 + APD/4.))

    print("node=", index_node)
    print("ACT=", ACT)
    print("APD=", APD)
    print("pace at=", start_S2)

    cmd = list(cmd_ref) 

    startstatef = steady_state_dir + '/state.{}'.format(args.start)
    
    tsav_state = lower_bound
    # Setting the stimulus at the sinus node + ectopic beat

    stim = ['-num_stim',                     2,
            '-stimulus[0].stimtype',         0,
            '-stimulus[0].strength',         args.prepace_strength,
            '-stimulus[0].start',            args.start,
            '-stimulus[0].duration',         2.0,
            '-stimulus[0].npls',             1,
            '-stimulus[0].bcl',              start_S2-args.start,
            '-stimulus[1].stimtype',         0,
            '-stimulus[1].strength',         args.strength,
            '-stimulus[1].start',            start_S2,
            '-stimulus[1].duration',         2,
            '-stimulus[1].npls',             1,
            '-stimulus[1].ctr_def',          1,
            '-stimulus[1].x0',               stim_index[0],
            '-stimulus[1].xd',               args.stim_size,
            '-stimulus[1].y0',               stim_index[1],
            '-stimulus[1].yd',               args.stim_size,
            '-stimulus[1].z0',               stim_index[2],
            '-stimulus[1].zd',               args.stim_size]

    cmd += stim + sinus

    cmd += ['-simID', simid,
            '-dt',  20,
            '-timedt', 30,
            '-spacedt', 1,
            '-start_statef', startstatef,
            '-num_tsav', 3,
            '-tsav[0]', tsav_state,
            '-tsav[1]', start_S2-1,
            '-tsav[2]', start_S2+60,
            '-mass_lumping', args.M_lump,
            '-tend', start_S2 + 60.1,
            '-meshname', meshname]
            
    #Run simulation
    job.carp(cmd)
    igb_f = igb.IGBFile(simid+'/vm.igb')
    header = igb_f.header()
    cmd = [settings.execs.igbextract,
        '-l', nodes_to_check_str,
        '-t', header['dim_t']-30,
        '-O', simid+'/out',
        simid + '/vm.igb']
    job.bash(cmd)
    vm = np.loadtxt(simid + '/out')
    vm = vm[:,1:]

    M2 = np.amax(vm)
    old_tsav_state = tsav_state
    if M2 < -50:     #REFRACTORY
        print ('REFRACTORY   S2_start = ', start_S2, M2)
        lower_bound = start_S2
        old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2+60))
        new_file = os.path.join(str(simid), 'state_lower_bound.roe')
        os.rename(old_file, new_file)
        old_file = os.path.join(str(simid), 'vm.igb')
        new_file = os.path.join(str(simid), 'vm_lower_bound.igb')
        os.rename(old_file, new_file)
        old_tsav_state = start_S2-1
        start_S2 = float(math.ceil((upper_bound + lower_bound)/2))
        print (upper_bound, lower_bound)

    else:           #NOT REFRACTORY
        print ('NOT REFRACTORY   S2_start = ', start_S2, M2)
        upper_bound = start_S2
        old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2+60))
        new_file = os.path.join(str(simid), 'state_upper_bound.roe')
        os.rename(old_file, new_file)
        old_file = os.path.join(str(simid), 'vm.igb')
        new_file = os.path.join(str(simid), 'vm_upper_bound.igb')
        os.rename(old_file, new_file)
        start_S2 = float(math.ceil((upper_bound + lower_bound)/2))
        print (upper_bound, lower_bound)
    
    n_beat = 0
    while n_beat < args.max_n_beats_PEERP:
        print ('-----------------------Point and number of beats' , index_node, n_beat+1)
        
        simid = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat)
        try:
            os.makedirs(simid)
        except OSError:
            print ("Creation of the directory %s failed" % simid)
        else:
            print ("Successfully created the directory %s " % simid)
        
        if n_beat > 0:
            startstatef = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat-1) +'/state.{}'.format(old_tsav_state)
        else:
            startstatef = job.ID + '/point_{}'.format(index_node) + '/beat_0/state.{}'.format(old_tsav_state)

        new_upper_bound, new_lower_bound, start_S2, startstatef = converge_to_EB(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2, upper_bound, lower_bound)
        while not os.path.isfile(simid + '/state_lower_bound.roe') and new_lower_bound>old_tsav_state:
            print("Lower bound missing, trying with ", new_upper_bound, new_lower_bound-20)
            new_upper_bound, new_lower_bound, start_S2, startstatef = converge_to_EB(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2, new_upper_bound, new_lower_bound-20)
        while not os.path.isfile(simid + '/state_upper_bound.roe'):
            print("Upper bound missing, trying with ", new_upper_bound+20, new_lower_bound)
            new_upper_bound, new_lower_bound, start_S2, startstatef = converge_to_EB(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2, new_upper_bound+20, new_lower_bound)
        if os.path.exists(simid + '/state_upper_bound.roe'):
            startstatef = simid + '/state_upper_bound'
            start_S2 = new_upper_bound
            print("Algorithm converged")
        else:
            print("Algorithm did not converge")
            break
    
        if os.path.exists(job.ID + '/tbeats.txt'):
            append_write = 'a' # append if already exists
        else:
            append_write = 'w' # make a new file if not
        f=open(job.ID + '/tbeats.txt', append_write)
        f.write("{} {}\n".format(str(index_node), str(start_S2)))
        f.close()
        
        cmd = list(cmd_ref)
        stim = ['-num_stim',                     0]
        cmd += stim
        cmd += ['-simID', simid,
                '-dt',  20,
                '-timedt', 100,
                '-spacedt', 1,
                '-start_statef', startstatef,
                '-num_tsav', 3,
                '-tsav[0]', start_S2+100,
                '-tsav[1]', start_S2+200,
                '-tsav[2]', start_S2+300,
                '-mass_lumping', args.M_lump,
                '-tend', start_S2 + 400.1,
                '-meshname', meshname]
                
        #Run simulation
        job.carp(cmd)
    
        igb_f = igb.IGBFile(simid+'/vm.igb')
        header = igb_f.header()
        cmd = [settings.execs.igbextract,
            '-l', nodes_to_check_str,
            '-t', header['dim_t']-100,
            '-O', simid+'/out',
            simid + '/vm.igb']
        job.bash(cmd)
        vm = np.loadtxt(simid + '/out')
        M2 = np.amax(vm[:,1:])

        cmd = [settings.execs.igbextract,
            '-l', index_node,
            '-O', simid+'/out',
            simid + '/vm.igb']
        job.bash(cmd)
        vm = np.loadtxt(simid + '/out')

        for ii in range(len(vm[:,0])-2, 0, -1):
            if vm[ii,1]<=-50 and vm[ii+1,1]>-50:
                break
        last_ACT_i = vm[ii,0]
        print(last_ACT_i)
        tsav_states = [100,200,300]
        for ii in tsav_states:
            if ii-last_ACT_i-60 >= 0:
                break
        print(ii)
        old_tsav_state = start_S2+ii
        if M2 > -50:     #Reentry to save
            if os.path.exists(job.ID + '/saved.txt'):
                append_write = 'a' # append if already exists
            else:
                append_write = 'w' # make a new file if not
            f=open(job.ID + '/saved.txt', append_write)
            f.write("{} {} {}\n".format(str(index_node), str(start_S2+400), str(n_beat)))
            f.close()
            old_file = os.path.join(str(simid), "vm.igb")
            new_file = os.path.join(str(simid), "vm_{}_{}_{}.igb".format(str(index_node), str(start_S2+400), str(n_beat)))
            os.rename(old_file, new_file)

            print ('Arrhythmia initiated at S2_start = ', start_S2, M2)

            cmd = list(cmd_ref)

            startstatef = simid + '/state.{}'.format(start_S2+300)

            cmd += stim
            cmd += ['-simID', simid,
                    '-dt',  20,
                    '-timedt', 100,
                    '-spacedt', 1,
                    '-mass_lumping', args.M_lump,
                    '-start_statef', startstatef,
                    '-tend', start_S2 + 500,
                    '-meshname', meshname]
            #Run simulation
            job.carp(cmd)
            igb_f = igb.IGBFile(simid+'/vm.igb')
            header = igb_f.header()
            cmd = [settings.execs.igbextract,
                '-l', nodes_to_check_str,
                '-t', header['dim_t']-100,
                '-O', simid+'/out',
                simid + '/vm.igb']
            job.bash(cmd)
            vm = np.loadtxt(simid + '/out', usecols=(1,))
    
            M2 = np.amax(vm)

            if M2 > -50:     #Reentry
                if os.path.exists(job.ID + '/reentries.txt'):
                    append_write = 'a' # append if already exists
                else:
                    append_write = 'w' # make a new file if not
                f=open(job.ID + '/reentries.txt', append_write)
                f.write("{} {} {}\n".format(str(index_node), str(start_S2+500), str(n_beat)))
                f.close()
                print("reentry p_{}_at_{}".format(str(index_node), math.ceil(start_S2+500)))
                break
            else:
                print ('No arrhythmia sustained at S2_start = ', start_S2, M2)
                lower_bound = old_tsav_state
                start_S2 = float(math.ceil(lower_bound+80))
                upper_bound = float(math.ceil(start_S2 + 40))
                n_beat += 1
        else:
            print ('No arrhythmia initiated at S2_start = ', start_S2, M2)
            lower_bound = old_tsav_state
            start_S2 = float(math.ceil(lower_bound+80))
            upper_bound = float(math.ceil(start_S2 + 40))
            n_beat += 1

# RP_E

def stimulation(args, start, start_bcl, end_bcl, step, n_beats, stim_index):
    start_beat = start
    bcl_v = np.arange(start_bcl-step, end_bcl-step, -step)
    n_pulses_v = np.ones((len(bcl_v),), dtype=int)*n_beats
    stim = ['-num_stim', len(bcl_v)+1,]
    for i in range(1,len(bcl_v)+1):
        
        stim += ['-stimulus['+str(i)+'].stimtype',         0,
                '-stimulus['+str(i)+'].strength',         args.strength,
                '-stimulus['+str(i)+'].start',            start_beat,
                '-stimulus['+str(i)+'].duration',         2.0,
                '-stimulus['+str(i)+'].npls',             n_pulses_v[i-1],
                '-stimulus['+str(i)+'].bcl',              bcl_v[i-1],
                '-stimulus['+str(i)+'].x0',               stim_index[0],
                '-stimulus['+str(i)+'].xd',               args.stim_size,
                '-stimulus['+str(i)+'].y0',               stim_index[1],
                '-stimulus['+str(i)+'].yd',               args.stim_size,
                '-stimulus['+str(i)+'].z0',               stim_index[2],
                '-stimulus['+str(i)+'].zd',               args.stim_size,
                '-stimulus['+str(i)+'].ctr_def',          1]
        start_beat += bcl_v[i-1]*n_pulses_v[i-1]-step

    return stim, start_beat

def RP_E(args, job, cmd_ref, meshname, stim_points, steady_state_dir):
    """
    Rapid Pacing with reentry check at the End:

    It consists of a train of electrical stimulations with decreasing coupling 
    interval (args.step) between args.start_bcl and args.end_bcl. 
    It can give multiple beats with the same cycle length (args.max_n_beats_RP) 
    and it checks for arrhythmia induction at the end of the protocol

    Input: parser arguments (args), output directory (job.ID), struct containing imp_regions and gregions (cmd_ref),
    meshname, stimulation point location (x,y,z) and prepacing directory (steady_state_dir)
    
    Args:
    '--M_lump',
        type=int,
        default='1',
        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.'
    '--stim_size',
        type=str,
        default='2000.0',
        help='stimulation edge square size in micrometers'
    '--step',
        type=float, 
        default=10,
        help='Step to decrease bcl in ms'
    '--max_n_beats_RP',
        type=int,
        default=1,
        help='Max number of beats for th RP'
    '--start',
        type=float, default=2000.0,
        help='initial basic cycle lenght in [ms]'
    '--start_bcl',
        type=float, default=200.0,
        help='initial basic cycle lenght in [ms]'
    '--end_bcl',
        type=float, default=130.0,
        help='last basic cycle lenght in [ms]'

    """

    simid = job.ID  
    try:
        os.makedirs(simid)
    except OSError:
        print ("Creation of the directory %s failed" % simid)
    else:
        print ("Successfully created the directory %s " % simid)

    stim_file = simid + '/stim_point.txt'
    with open(stim_file, 'w') as f:
        f.write("1\n")
        f.write("{} {} {} 500\n".format(stim_points[0],stim_points[1],stim_points[2]))

    if not os.path.isfile(simid+'/stim_point.txt.out.txt'):
        os.system('meshtool query idxlist -msh={} -coord={}'.format(meshname, stim_file))
    index_node = np.loadtxt(simid+'/stim_point.txt.out.txt', dtype=int, skiprows=1, usecols=(0,))            

    stim_index = stim_points
    node_coord_str = ','.join([str(i) for i in stim_index])

    if not os.path.isfile('{}_node_{}.vtx.surf.vtx'.format(meshname, index_node)):
        os.system("meshtool extract surface -msh={} -surf={}_node_{}.vtx -size={} -lower_size={} -coord={}".format(meshname, meshname, index_node, float(args.stim_size)*3, float(args.stim_size)*2, node_coord_str))
    nodes_to_check = np.loadtxt(meshname +'_node_{}.vtx.surf.vtx'.format(index_node), skiprows=2,dtype=int)
    nodes_to_check_str = ','.join([str(i) for i in nodes_to_check])
    start_S2 = args.start
    reentry = 0
    for n_beat in range(1,args.max_n_beats_RP+1):
        if reentry:
            break
        print ('-----------------------Point and number of beats' , index_node, n_beat)
        
        simid = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat)

        try:
            os.makedirs(simid)
        except OSError:
            print ("Creation of the directory %s failed" % simid)
        else:
            print ("Successfully created the directory %s " % simid)

        if n_beat > 1:
            start_S2 = args.start+args.start_bcl*(n_beat-1)
            if os.path.exists(job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat-1) +'/state.{}.roe'.format(start_S2 - args.step)):
                startstatef = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat-1) +'/state.{}'.format(start_S2 - args.step)
            else:
                break
        else:
            startstatef = steady_state_dir + '/state.{}'.format(args.start)
        
        stim2, t_last_beat = stimulation(args, start_S2+args.start_bcl-args.step, args.start_bcl, args.end_bcl, args.step, n_beat, stim_index)
        if os.path.exists(job.ID + '/tbeats.txt'):
            append_write = 'a' # append if already exists
        else:
            append_write = 'w' # make a new file if not
        f=open(job.ID + '/tbeats.txt', append_write)
        f.write("{} {} {}\n".format(str(index_node), str(t_last_beat), n_beat))
        f.close()

        stim = ['-num_stim',                 1,
            '-stimulus[0].stimtype',         0,
            '-stimulus[0].strength',         args.strength,
            '-stimulus[0].start',            start_S2,
            '-stimulus[0].duration',         2,
            '-stimulus[0].npls',             1,
            '-stimulus[0].ctr_def',          1,
            '-stimulus[0].x0',               stim_index[0],
            '-stimulus[0].xd',               args.stim_size,
            '-stimulus[0].y0',               stim_index[1],
            '-stimulus[0].yd',               args.stim_size,
            '-stimulus[0].z0',               stim_index[2],
            '-stimulus[0].zd',               args.stim_size]

        cmd = cmd_ref + stim + stim2
        t_save = start_S2 + args.start_bcl - args.step
        t_last_save = t_last_beat + 200

        cmd += ['-simID', simid,
            '-dt',  20,
            '-timedt', 100,
            '-spacedt', 1,
            '-mass_lumping', args.M_lump,
            '-start_statef', startstatef,
            '-num_tsav', 2,
            '-tsav[0]', t_save, 
            '-tsav[1]', t_last_save,
            '-tend', t_last_save + 0.1,
            '-meshname', meshname]
            
        #Run simulation
        job.carp(cmd)

        igb_f = igb.IGBFile(simid+'/vm.igb')
        header = igb_f.header()
        cmd = [settings.execs.igbextract,
            '-l', nodes_to_check_str,
            '-t', header['dim_t']-100,
            '-O', simid+'/out',
            simid + '/vm.igb']
        job.bash(cmd)
        vm = np.loadtxt(simid + '/out')

        if np.amax(vm[:,1:]) > -50.0:
            old_file = os.path.join(str(simid), "vm.igb")
            new_file = os.path.join(str(simid), "vm_prepace.igb")
            os.rename(old_file, new_file)
            startstatef = simid + '/state.{}'.format(t_last_save)
            stim = ['-num_stim',                      0]

            cmd = cmd_ref + stim
            cmd += ['-simID', simid,
                    '-dt',  20,
                    '-timedt', 100,
                    '-spacedt', 1,
                    '-mass_lumping', args.M_lump,
                    '-start_statef', startstatef,
                    '-tend', t_last_beat + 500,
                    '-meshname', meshname]
            #Run simulation
            job.carp(cmd)
            
            igb_f = igb.IGBFile(simid+'/vm.igb')
            header = igb_f.header()
            cmd = [settings.execs.igbextract,
                '-l', nodes_to_check_str,
                '-t', header['dim_t']-100,
                '-O', simid+'/out',
                simid + '/vm.igb']
            job.bash(cmd)
            vm = np.loadtxt(simid + '/out') 
            if np.amax(vm[:,1:]) > -50.0:
                if os.path.exists(job.ID + '/reentries.txt'):
                    append_write = 'a' # append if already exists
                else:
                    append_write = 'w' # make a new file if not
                f=open(job.ID + '/reentries.txt', append_write)
                f.write("{} {} {}\n".format(str(index_node), str(start_S2), n_beat))
                f.close()
                reentry = 1
                print("Reentry p_{}_at_{}".format(index_node, t_last_beat + 500))
            else:
                print("No reentry p_{}_at_{}".format(index_node, t_last_beat + 500))
        
        else:
            print("No saved p_{}_at_{}".format(index_node, t_last_save))

    return simid

# RP_B

def check_propagation(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2):
    propagation = 0

    stim = ['-num_stim',                     1,
            '-stimulus[0].stimtype',         0,
            '-stimulus[0].strength',         args.strength,
            '-stimulus[0].start',            start_S2,
            '-stimulus[0].duration',         2,
            '-stimulus[0].npls',             1,
            '-stimulus[0].ctr_def',1,
            '-stimulus[0].x0',               stim_index[0],
            '-stimulus[0].xd',               args.stim_size,
            '-stimulus[0].y0',               stim_index[1],
            '-stimulus[0].yd',               args.stim_size,
            '-stimulus[0].z0',               stim_index[2],
            '-stimulus[0].zd',               args.stim_size]

    cmd = list(cmd_ref) + stim 

    cmd += ['-simID', simid,
            '-dt',  20,
            '-timedt', 30,
            '-spacedt', 1,
            '-start_statef', startstatef,
            '-num_tsav', 1,
            '-tsav[0]', start_S2+60,
            '-mass_lumping', args.M_lump,
            '-tend', start_S2 + 60.1,
            '-meshname', meshname]      
    #Run simulation
    job.carp(cmd)
    igb_f = igb.IGBFile(simid+'/vm.igb')
    header = igb_f.header()
    cmd = [settings.execs.igbextract,
    '-l', nodes_to_check_str,
    '-t', header['dim_t']-30,
    '-O', simid+'/out',
    simid + '/vm.igb']
    job.bash(cmd)

    vm = np.loadtxt(simid + '/out')
    vm = vm[:,1:]

    M = np.amax(vm)

    if M < -50:     #REFRACTORY 
        print ('REFRACTORY   S2_start = ', start_S2, M)
        old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2+60))
        new_file = os.path.join(str(simid), 'state_no_prop.roe')
        os.rename(old_file, new_file)
        old_file = os.path.join(str(simid), 'vm.igb')
        new_file = os.path.join(str(simid), 'vm_no_prop.igb')
        os.rename(old_file, new_file)
        startstatef = simid + '/state_no_prop'

    else:           #NOT REFRACTORY
        print ('NOT REFRACTORY   S2_start = ', start_S2, M)
        propagation = 1
        old_file = os.path.join(str(simid), 'state.{}.roe'.format(start_S2+60))
        new_file = os.path.join(str(simid), 'state_prop.roe')
        os.rename(old_file, new_file)
        old_file = os.path.join(str(simid), 'vm.igb')
        new_file = os.path.join(str(simid), 'vm_prop.igb')
        os.rename(old_file, new_file)
        startstatef = simid + '/state_prop'

    return propagation, startstatef

def RP_B(args, job, cmd_ref, meshname, stim_points, steady_state_dir):
    """
    Rapid Pacing with reentry check after every Beat:

    It consists of a train of electrical stimulations with decreasing coupling 
    interval (args.step) between args.start_bcl and args.end_bcl. 
    It can give multiple beats with the same cycle length (args.max_n_beats_RP) 
    and it checks for arrhythmia induction after every beat.

    Input: parser arguments (args), output directory (job.ID), struct containing imp_regions and gregions (cmd_ref),
    meshname, stimulation point location (x,y,z) and prepacing directory (steady_state_dir)
    
    Args:
    '--M_lump',
        type=int,
        default='1',
        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.'
    '--stim_size',
        type=str,
        default='2000.0',
        help='stimulation edge square size in micrometers'
    '--step',
        type=float, 
        default=10,
        help='Step to decrease bcl in ms'
    '--max_n_beats_RP',
        type=int,
        default=1,
        help='Max number of beats for th RP'
    '--start',
        type=float, default=2000.0,
        help='initial basic cycle lenght in [ms]'
    '--start_bcl',
        type=float, default=200.0,
        help='initial basic cycle lenght in [ms]'
    '--end_bcl',
        type=float, default=130.0,
        help='last basic cycle lenght in [ms]'
       
    """
    simid = job.ID  
    try:
        os.makedirs(simid)
    except OSError:
        print ("Creation of the directory %s failed" % simid)
    else:
        print ("Successfully created the directory %s " % simid)

    stim_file = simid + '/stim_point.txt'
    with open(stim_file, 'w') as f:
        f.write("1\n")
        f.write("{} {} {} 500\n".format(stim_points[0],stim_points[1],stim_points[2]))

    if not os.path.isfile(simid+'/stim_point.txt.out.txt'):
        os.system('meshtool query idxlist -msh={} -coord={}'.format(meshname, stim_file))
    index_node = np.loadtxt(simid+'/stim_point.txt.out.txt', dtype=int, skiprows=1, usecols=(0,))
    index_nodes_str = str(index_node)
               
    stim_index = stim_points
    node_coord_str = ','.join([str(i) for i in stim_index])

    if not os.path.isfile('{}_node_{}.vtx.surf.vtx'.format(meshname, index_node)):
        os.system("meshtool extract surface -msh={} -surf={}_node_{}.vtx -size={} -lower_size={} -coord={}".format(meshname, meshname, index_node, float(args.stim_size)*3, float(args.stim_size)*2, node_coord_str))
    nodes_to_check = np.loadtxt(meshname +'_node_{}.vtx.surf.vtx'.format(index_node), skiprows=2,dtype=int)
    nodes_to_check_str = ','.join([str(i) for i in nodes_to_check])
    start_S2 = args.start
    reentry = 0
    for n_beat in range(1,args.max_n_beats_RP+1):
        if reentry:
            break
        print ('-----------------------Point and number of beats' , index_node, n_beat)
        
        simid = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat)
        try:
            os.makedirs(simid)
        except OSError:
            print ("Creation of the directory %s failed" % simid)
        else:
            print ("Successfully created the directory %s " % simid)

        if n_beat > 1:
            start_S2 = args.start+args.start_bcl*(n_beat-1)
            if os.path.exists(job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat-1) +'/state.{}.roe'.format(start_S2-args.step)):
                startstatef = job.ID + '/point_{}'.format(index_node) +'/beat_{}'.format(n_beat-1) +'/state.{}'.format(start_S2-args.step)
            else:
                break
        else:
            startstatef = steady_state_dir + '/state.{}'.format(args.start)

        bcl = args.start_bcl
        while bcl <= args.start_bcl and bcl >= args.end_bcl and not reentry:
        
            # Setting the stimulus at the pacing point
            propagation, startstatef = check_propagation(args, job, meshname, cmd_ref, stim_index, simid, nodes_to_check_str, startstatef, start_S2)

            if propagation:
                if os.path.exists(job.ID + '/tbeats.txt'):
                    append_write = 'a' # append if already exists
                else:
                    append_write = 'w' # make a new file if not
                f=open(job.ID + '/tbeats.txt', append_write)
                f.write("{} {} {}\n".format(str(index_node), str(start_S2), n_beat))
                f.close()

                stim = ['-num_stim',                 1,
                    '-stimulus[0].stimtype',         0,
                    '-stimulus[0].strength',         args.strength,
                    '-stimulus[0].start',            start_S2,
                    '-stimulus[0].duration',         2,
                    '-stimulus[0].npls',             n_beat,
                    '-stimulus[0].bcl',              bcl,
                    '-stimulus[0].ctr_def', 1,
                    '-stimulus[0].x0',               stim_index[0],
                    '-stimulus[0].xd',               args.stim_size,
                    '-stimulus[0].y0',               stim_index[1],
                    '-stimulus[0].yd',               args.stim_size,
                    '-stimulus[0].z0',               stim_index[2],
                    '-stimulus[0].zd',               args.stim_size]

                t_last_beat = start_S2 + bcl*n_beat-args.step
                if bcl == args.start_bcl:
                    stim = ['-num_stim',                      0]
                    t_last_beat = start_S2 + bcl-args.step
                t_last_save = t_last_beat + 200
                cmd = list(cmd_ref) + stim
                cmd += ['-simID', simid,
                    '-dt',  20,
                    '-timedt', 100,
                    '-spacedt', 1,
                    '-mass_lumping', args.M_lump,
                    '-start_statef', startstatef,
                    '-num_tsav', 2,
                    '-tsav[0]', t_last_beat, 
                    '-tsav[1]', t_last_save,
                    '-tend', t_last_save + 0.1,
                    '-meshname', meshname]
                    
                #Run simulation
                job.carp(cmd)
                igb_f = igb.IGBFile(simid+'/vm.igb')
                header = igb_f.header()
                cmd = [settings.execs.igbextract,
                    '-l', nodes_to_check_str,
                    '-t', header['dim_t']-100,
                    '-O', simid+'/out',
                    simid + '/vm.igb']
                job.bash(cmd)
                vm = np.loadtxt(simid + '/out')

                if np.amax(vm[:,1:]) > -50.0:
                    old_file = os.path.join(str(simid), "vm.igb") 
                    new_file = os.path.join(str(simid), "vm_saved.igb")
                    os.rename(old_file, new_file)
                    startstatef = simid + '/state.{}'.format(t_last_save)
                    stim = ['-num_stim',                      0]

                    cmd = list(cmd_ref) + stim
                    cmd += ['-simID', simid,
                            '-dt',  20,
                            '-timedt', 100,
                            '-spacedt', 1,
                            '-mass_lumping', args.M_lump,
                            '-start_statef', startstatef,
                            '-tend', t_last_beat + 600,
                            '-meshname', meshname]
                    #Run simulation
                    job.carp(cmd)
                    
                    igb_f = igb.IGBFile(simid+'/vm.igb')
                    header = igb_f.header()
                    cmd = [settings.execs.igbextract,
                        '-l', nodes_to_check_str,
                        '-t', header['dim_t']-100,
                        '-O', simid+'/out',
                        simid + '/vm.igb']
                    job.bash(cmd)
                    vm = np.loadtxt(simid + '/out') 
                    if np.amax(vm[:,1:]) > -50.0:
                        print("reentry {} {} {} {}\n".format(str(index_node), str(start_S2), n_beat, bcl))
                        if os.path.exists(job.ID + '/reentries.txt'):
                            append_write = 'a' # append if already exists
                        else:
                            append_write = 'w' # make a new file if not
                        f=open(job.ID + '/reentries.txt', append_write)
                        f.write("{} {} {} {}\n".format(str(index_node), str(start_S2), n_beat, bcl))
                        f.close()
                        print("reentry p_{}_at_{}".format(index_node, t_last_beat + 100))
                        reentry = 1
                    else:
                        start_S2 = t_last_beat
                        startstatef = simid + '/state.{}'.format(start_S2)
                        print("no reentry {} {} {} {}\n".format(str(index_node), str(start_S2), n_beat, bcl))
                        bcl -= args.step
                        old_file = os.path.join(str(simid), "vm.igb")
                        new_file = os.path.join(str(simid), "vm_no_reentry{}.igb".format(bcl))
                        os.rename(old_file, new_file)
                else:
                    start_S2 = t_last_beat
                    startstatef = simid + '/state.{}'.format(start_S2)
                    bcl -= args.step
                    print("not to save {} {} {} {}\n".format(str(index_node), str(start_S2), n_beat, bcl))
            else:
                # No propagation so try adding one beat more
                print("no propagation {} {} {} {}\n".format(str(index_node), str(start_S2), n_beat, bcl))
                break

    return simid

# PSD
def assignRandomCoord(points, sing_points):

    coordinates = np.zeros((sing_points,3))
    coordinates[0] = points[np.random.randint(len(points))]
    directions = np.ones(sing_points, dtype=int)*(-1)
    balls = np.array([], dtype=int)
    for i in range(1,sing_points):
        balls = np.append(balls, spatial.cKDTree(points).query_ball_point(coordinates[i-1], 2*radius))
        p = np.ones(len(points), dtype=float)
        p[balls] = 0.0
        p /= sum(p)
        coordinates[i] = points[np.random.choice(len(points), replace=False, p=p)]
        distance, index = spatial.cKDTree(coordinates[0:i]).query(coordinates[i])
        if distance < 3.5*radius:
            directions[i] = 1

    return coordinates, directions

def assignDirection(coordinates):

    directions = np.ones(len(coordinates), dtype=int)*(-1)
    for i in range(1,len(coordinates)):
        distance, index = spatial.cKDTree(coordinates[0:i]).query(coordinates[i])
        if distance < 3.5*radius:
            directions[i] = 1

    return directions

def makeCircle3D(startCoord, radius, seedNum, normV, point1, point2, nodes):
    
    increment = (2 * np.pi) / seedNum
    seedCount = 0
    t = 0.
    timePoints = np.zeros((1, seedNum), dtype=float)
    vector = point1 - point2
    u = vector / LA.norm(vector)
    
    seeds_list = np.zeros((seedNum, 3))

    while seedCount < seedNum:
        coordinate = ((radius * np.cos(t)) * u) + np.cross((radius * np.sin(t)) * normV, u) + startCoord
        seeds_list[seedCount, :] = coordinate
        distance,index_node = spatial.cKDTree(nodes).query(coordinate)
        timePoints[0, seedCount] = index_node
        seedCount += 1
        t += increment
    
    return timePoints

def assignPhaseValues(seeds, originalCoord, directions, normV, point1, point2, nodes):

    phaseCount = 0
    gammaPhi = np.zeros((2, seeds * len(originalCoord)), dtype=complex)
    while phaseCount < len(originalCoord):
        # assigns the known nodes
        gammaPhi[0, (phaseCount * seeds) : (phaseCount + 1) * seeds ] = makeCircle3D(originalCoord[phaseCount], radius, seeds, normV[phaseCount], point1[phaseCount], point2[phaseCount], nodes)
        if directions[phaseCount] == 1: # clockwise
            seeds_range = np.arange(1., seeds+1.)
        else: # counterclockwise
            seeds_range = np.arange(seeds, 0., -1.)
        seeds_range =  seeds_range/seeds # fractions, period itself doesn't matter
        phase_values = seeds_range*2*np.pi
        gammaPhi[1, (phaseCount * seeds) : (phaseCount + 1) * seeds ] = np.exp(1j*phase_values) #turns to phase values

        phaseCount += 1

    return gammaPhi

def planeInfo(coordinate, nodes, triangles):

    distance,index_node = spatial.cKDTree(nodes).query(coordinate, workers=-1)

    points = [(row) for counter, row in enumerate(triangles) if index_node in row]
    saved = nodes[points[0]]

    plane_info = []
    for i in range(0,len(saved)):
        if points[0][i] != index_node:
            plane_info.append(saved[i])

    vec1 = saved[0] - saved[1]
    vec2 = saved[0] - saved[2]
    preNorm = np.cross(vec1,vec2)
    Norm = preNorm / LA.norm(preNorm)
    plane_info.append(Norm)
    plane_info = np.array(plane_info)

    return plane_info

def deflate(x):
    A_sol = A.dot(x) + np.mean(x)*np.ones((nv,1), dtype=float).reshape((nv,))
    return A_sol

def csr_row_set_nz_to_val(csr, row, value=0):
    """Set all nonzero elements (elements currently in the sparsity pattern)
    to the given value. Useful to set to 0 mostly.
    """
    if not isinstance(csr, csr_matrix):
        raise ValueError('Matrix given must be of CSR format.')
    csr.data[csr.indptr[row]:csr.indptr[row+1]] = value

def eikonal_solver(nodes, triangles, knownNodes, phi0):

    global nv
    nv = len(nodes)
    nt = len(triangles)

    phi = np.ones((nv,1), dtype = complex)
    phi[knownNodes,0] = phi0
    tol = 10**(-10)
    iterationNum = 0
    updateChange = 1.
    maxIterations = 30
    triangles = np.array(triangles)
    

    u = (nodes[triangles[:,1],:]-nodes[triangles[:,0],:])/1000
    v = (nodes[triangles[:,2],:]-nodes[triangles[:,0],:])/1000
    # scalar products
    u2 = np.transpose(np.sum(u**2,axis=1))
    v2 = np.transpose(np.sum(v**2,axis=1))
    uv = np.transpose(np.sum(np.multiply(u,v),axis=1))
    u2.shape = (len(u2),1)
    v2.shape = (len(v2),1)
    uv.shape = (len(uv),1)
    # determinant
    delta = np.multiply(u2,v2) - np.multiply(uv,uv)

    e = np.ones((1,3), dtype=int)
    
    # (us,vs) is the dual basis of (u,v)
    us = np.multiply(np.multiply(np.divide(v2,delta),e), u) - np.multiply(np.multiply(np.divide(uv,delta),e), v)   
    vs = np.multiply(np.multiply(np.divide(u2,delta),e), v) - np.multiply(np.multiply(np.divide(uv,delta),e), u)

    # components of the gradient matrix
    Gx = np.column_stack((-us[:,0]-vs[:,0], us[:,0], vs[:,0]))     
    Gy = np.column_stack((-us[:,1]-vs[:,1], us[:,1], vs[:,1]))
    Gz = np.column_stack((-us[:,2]-vs[:,2], us[:,2], vs[:,2]))

    # indices in the sparse matrices
    col = np.concatenate(triangles, axis=0)
    row = np.zeros(3*nt)
    row[0:len(row)-2:3] = range(0,nt)
    row[1:len(row)-1:3] = range(0,nt)
    row[2:len(row):3] = range(0,nt)

    data = np.concatenate(Gx, axis=0)
    Gradx = coo_matrix((data, (row, col)),shape=(nt,nv)).tocsr()
    data = np.concatenate(Gy, axis=0)
    Grady = coo_matrix((data, (row, col)),shape=(nt,nv)).tocsr()
    data = np.concatenate(Gz, axis=0)
    Gradz = coo_matrix((data, (row, col)),shape=(nt,nv)).tocsr()

    # area of the triangles
    St = np.matlib.repmat(np.sqrt(delta)/2,1,3)

    data = np.concatenate(St, axis=0)
    Tt2v = coo_matrix((data, (col, row)),shape=(nv,nt))

    # area associated with each vertex and its inverse
    Sv = np.sum(Tt2v,axis=1)/3
    data = np.concatenate(np.reciprocal(Sv), axis=1)
    invS = dia_matrix((data,[0]),shape=(nv,nv))

    # interpolation triangle to vertex and vertex to triangle
    Tt2v = (invS/3).dot(Tt2v)
    data = np.ones(3*nt)*1/3
    Tv2t = coo_matrix((data, (row, col)),shape=(nt,nv)).tocsr()

    # create the sparse matrices
    data = np.concatenate(np.multiply(-St,Gx),axis=0)
    Divx = invS.dot(coo_matrix((data, (col, row)),shape=(nv,nt)).tocsr())
    data = np.concatenate(np.multiply(-St,Gy),axis=0)
    Divy = invS.dot(coo_matrix((data, (col, row)),shape=(nv,nt)).tocsr())
    data = np.concatenate(np.multiply(-St,Gz),axis=0)
    Divz = invS.dot(coo_matrix((data, (col, row)),shape=(nv,nt)).tocsr())

    # Calculating speed per triangle

    maxT = 1000.0/(2*np.pi)
    dDivisor = 2083.3

    c = np.ones(nt)*CV*maxT
    D = np.ones(nt)*(CV**2)*maxT/dDivisor

    # creates diagonal version for all of the D and c values
    DDiag = dia_matrix((D, [0]),shape=(nt,nt))
    cDiag = dia_matrix((c, [0]),shape=(nt,nt))

    Diag = identity(nt)
    DGradx = DDiag.dot(Diag.dot(Gradx))
    DGrady = DDiag.dot(Diag.dot(Grady))
    DGradz = DDiag.dot(Diag.dot(Gradz))

    CGradx = cDiag.dot(Diag.dot(Gradx))
    CGrady = cDiag.dot(Diag.dot(Grady))
    CGradz = cDiag.dot(Diag.dot(Gradz))

    print("Solving laplacian interpolation")

    while (updateChange > tol) and (iterationNum < maxIterations):
        iterationNum = iterationNum+1

        phiRefLastIteration = phi
    
        # set value of phi on the triangles
        conjphi = np.conj(phi)
        data = np.concatenate(Tv2t.dot(conjphi),axis=0)
        phit = dia_matrix((data,[0]),shape=(nt,nt))
        # matrix A inf
        Ainf = Divx.dot(phit).dot(DGradx) + Divy.dot(phit).dot(DGrady) + Divz.dot(phit).dot(DGradz)
        # replace row i with row i of the identity matrix
        for row in knownNodes:
            csr_row_set_nz_to_val(Ainf, row, 0)

        # And to remove zeros from the sparsity pattern:
        Ainf.eliminate_zeros()

        Ainf[np.unravel_index(np.dot(knownNodes,nv) + knownNodes, (nv,nv))] = 1 #makes sure these values are fixed
        # right hand side finf, which has length nv with knownNodes
        finf = np.zeros((nv,1), dtype = complex)
        finf[knownNodes] = phi[knownNodes]

        # solve equation
        phiinf = spsolve(Ainf,finf)[np.newaxis].T
        
        # normalize
        phi = np.divide(phiinf,np.abs(phiinf))

        # set measured values
        phi[knownNodes,0] = phi0

        # set difference
        updateChange = LA.norm(phiRefLastIteration-phi)
        print("iterationNum=",iterationNum,"with updateChange=",updateChange)

    phiLaplace = phi

    # Eikonal interpolation

    print("Solving eikonal diffusion equation")

    Diff = Divx.dot(DGradx) + Divy.dot(DGrady) + Divz.dot(DGradz)

    iterationNum = 0
    updateChange = 1.
    
    global A
    while (updateChange > tol) and (iterationNum < maxIterations):
        iterationNum = iterationNum+1
        print("iterationNum=",iterationNum,"with updateChange=",updateChange)
    
        # set value of phi on the triangles
        conjphi = np.conj(phi)
        phit = Tv2t.dot(conjphi)

        # norm of the gradient
        Norm = np.sqrt(np.abs(CGradx.dot(phi))**2 \
             + np.abs(CGrady.dot(phi))**2 \
             + np.abs(CGradz.dot(phi))**2)
        
        # Propagation velocity for every triangle
        cv = np.reciprocal(Norm)
        # matrix B
        B = dia_matrix((np.concatenate(CGradx.dot(conjphi),axis=0),[0]),shape=(nt,nt)).dot(CGradx) \
          + dia_matrix((np.concatenate(CGrady.dot(conjphi),axis=0),[0]),shape=(nt,nt)).dot(CGrady) \
          + dia_matrix((np.concatenate(CGradz.dot(conjphi),axis=0),[0]),shape=(nt,nt)).dot(CGradz) 

        # matrix A
        A = Diff \
          + 3./4*Tt2v.dot(dia_matrix((np.concatenate(cv,axis=0),[0]),shape=(nt,nt)).dot(np.imag(dia_matrix((np.concatenate(Tv2t.dot(phi),axis=0),[0]),shape=(nt,nt)).dot(B)))) \
          + 1./4*np.imag(dia_matrix((np.concatenate(phi,axis=0),[0]),shape=(nv,nv)).dot(Tt2v).dot(dia_matrix((np.concatenate(cv,axis=0),[0]),shape=(nt,nt)).dot(B)))
        
        # right hand side f
        f = Tt2v.dot(Norm-1.) \
          - Divx.dot(np.imag(np.multiply(phit, DGradx.dot(phi)))) \
          - Divy.dot(np.imag(np.multiply(phit, DGrady.dot(phi)))) \
          - Divz.dot(np.imag(np.multiply(phit, DGradz.dot(phi))))

        # solve system using deflation method to obtain correction term theta
        try:
            LU = spilu(A.tocsc(), drop_tol=10**(-3))
        except RuntimeError:
            phiEikonal = phiLaplace
            return phiLaplace, phiEikonal, -1000
        M_x = lambda x: LU.solve(x)
        M = LinearOperator((nv,nv), M_x)
        try:
            A_sol = LinearOperator((nv,nv), matvec = deflate)
            theta, num_iter = bicgstab(A_sol, f, tol=10**(-10), maxiter=100, M=M)
        except RuntimeError:
            phiEikonal = phiLaplace
            return phiLaplace, phiEikonal, -1000
        alpha = np.mean(theta)

        theta -= alpha

        T = 1000.0/(1.0 + alpha)
        print(T)

        # under relaxation step
        thetarelaxed = theta*min(1, 0.1/np.max(np.abs(theta)))
    
        # correct estimate for phi
        phi = np.multiply(phi,np.array(np.exp(1j*thetarelaxed)).reshape(nv,1))
        
        # set difference
        updateChange = LA.norm(thetarelaxed)

    phiEikonal = phi

    return phiLaplace, phiEikonal, T

def assignWallValues(surfacePoints, allPoints, surfaceValues):
    # assignWallValues: transfers phase data from the surface mesh to the
    # volumetric mesh. 
    #
    # Requires: 
    #   -surfacePoints: the coordinates of all of the nodes on the surface
    #   mesh.
    #   -allPoints: the coordinates of all of the nodes on the volumetric mesh.
    #   -surfaceValues: the phase values corresponding with the nodes on the
    #   surface mesh. 
    #
    # Results: returns a phi matrix which contains phase values for every point
    # on the volumetric mesh. 

    phi_vol = np.zeros((len(allPoints), 1), dtype = complex)
    distance, index_node = spatial.cKDTree(surfacePoints).query(allPoints, workers=-1)
    phi_vol = surfaceValues[index_node]

    return phi_vol

def PSD(args, job, cmd, meshname, xyz, triangles, centre):
    """
    Phase Singularity Distribution:

    It consists of manually placing phase singularities on the geometrical model
    and then solving the Eikonal equation to estimate the activation time map. 
    Based on this initial state, you can simulate electrical wave propagation 
    by solving the monodomain equation.

    Input: parser arguments (args), output directory (job.ID), struct containing imp_regions and gregions (cmd_ref),
    meshname, stimulation point location (x,y,z) and prepacing directory (steady_state_dir)
    
    Args:
    '--M_lump',
        type=int,
        default='1',
        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.'
    '--cv',
        type=float, 
        default=0.3,
        help='conduction velocity in m/s'
    '--PSD_bcl',
        type=float,
        default=160,
        help='BCL in ms' 
    '--radius',
        type=float, 
        default=10000.0,
        help='radius of circles in which to set the phase from -pi to +pi'
    '--seeds',
        type=int, 
        default=50,
        help='# of initial seeds in which to set the phase from -pi to +pi'
    '--chirality',
        type=int, 
        default=-1,
        help='Chirality of the rotation: -1 for counterclockwise, 1 for clockwise'
       
    """

    global coordinates, CV, radius
    CV = args.cv*1000 # here in mm/s
    radius = args.radius

    directions = []
    plane_info = []
    coordinates = np.atleast_2d(centre) # we locate the phase singularity in the centre

    plane_info = planeInfo(coordinates, xyz, triangles)
    directions.append(args.chirality)

    print("Coordinates: ",coordinates)
    print("Directions: ",directions)

    initialConditions = assignPhaseValues(args.seeds, coordinates, directions, plane_info[2::3], plane_info[0:len(plane_info)-2:3], plane_info[1:len(plane_info)-1:3], xyz)

    seedIndex = [int(initialConditions[0, i]) for i in range(len(initialConditions[0]))]
    phi0 = initialConditions[1, :]

    laplacian_sol, eikonal_sol, T = eikonal_solver(xyz, triangles, seedIndex, phi0)

    print("Assigning volume values")
    if T > -1000:
        print("Using Eikonal phases")
    else:
        print("T<-1000 - Using laplace phases")
        
    points_vol = np.loadtxt(meshname + '.pts', skiprows = 1)
    phi_Volume = assignWallValues(xyz, points_vol, eikonal_sol)
    phi_Volume_laplace = assignWallValues(xyz, points_vol, laplacian_sol)

    phase = np.angle(phi_Volume)
    phase_laplace = np.angle(phi_Volume_laplace)
    print("Computing LATs on the volume mesh with T=", T)
    factor = args.PSD_bcl*1.05

    LATS = [factor*(i/(2*np.pi)) for i in (phase + np.pi)] #Max LAT = BCL*1.05
    
    PHASE = [ i[0] for i in phase]
    PHASE_laplace = [ i[0] for i in phase_laplace]

    if not os.path.exists(job.ID):

        os.makedirs(job.ID)

    print("Writing LATs")
    writefile = job.ID + '/LATS.dat'
    file = open(writefile, 'w')

    phase_file = open(job.ID + '/phase.dat','w')
    phase_laplace_file = open(job.ID + '/phase_laplace.dat','w')

    for line in range(len(phase)):
        file.write("%f\n" % LATS[line])
        phase_file.write( '{}\n'.format(PHASE[line]))
        phase_laplace_file.write( '{}\n'.format(PHASE_laplace[line]))

    file.close()
    phase_file.close()
    phase_laplace_file.close()

    writestatef = 'state'
    tsav_state = 500

    # Setting the stimulus
    stim = ['-num_stim', 0]
    cmd += stim
    cmd +=['-meshname', meshname,
        '-write_statef', writestatef,
        '-num_tsav', 1,
        '-tsav[0]', tsav_state,
        '-tend',     500.1,
        '-simID',    job.ID,
        '-mass_lumping', args.M_lump,
        '-prepacing_lats', job.ID + '/LATS.dat',
        '-prepacing_beats', 5,
        '-prepacing_bcl' , args.PSD_bcl]


    # Run simulations
    job.carp(cmd)

    # Arrhythmia check
    
    igb_f = igb.IGBFile(job.ID+'/vm.igb')
    header = igb_f.header()
    cmd = [settings.execs.igbextract,
        '-t', header['dim_t']-100,
        '-O', job.ID+'/out',
        job.ID + '/vm.igb']
    job.bash(cmd)
    vm = np.loadtxt(job.ID + '/out', usecols=(1,))

    M2 = np.amax(vm)

    if M2 > -50:     #Reentry
        if os.path.exists(job.ID + '/reentries.txt'):
            append_write = 'a' # append if already exists
        else:
            append_write = 'w' # make a new file if not
        f=open(job.ID + '/reentries.txt', append_write)
        f.write("{}\n".format(str(args.PSD_bcl)))
        f.close()
        print("reentry with_bcl_{}".format(str(args.PSD_bcl)))
    else:
        print ("No sustained reentry with_bcl_{}".format(str(args.PSD_bcl)))
