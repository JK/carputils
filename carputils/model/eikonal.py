#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from .general import AbstractModelComponent, SingleIDRegionMixin

class EikonalRegion(SingleIDRegionMixin, AbstractModelComponent):
    """
    Defines an Eikonal model conduction velocity region.

    Args:
        ID : int
            The ID of the conductivity region (**not** the element tags) this
            Eikonal region is to be built from
        name : str, optional
            A short descriptive name for this region
        vel_f : float, optional
            Conduction velocity in the fibre direction
        vel_s : float, optional
            Conduction velocity in the sheet direction
        vel_n : float, optional
            Conduction velocity in the sheet-normal direction
    """

    PRM_ARRAY  = 'ekregion'
    PRM_LENGTH = 'num_ekregions'

    def __init__(self, ID, name=None, vel_f=0.6, vel_s=0.4, vel_n=0.2):

        super(EikonalRegion, self).__init__(ID=ID, name=name)
        
        self._vel_f = vel_f
        self._vel_s = vel_s
        self._vel_n = vel_n
        self._passive = False

    @classmethod
    def isotropic(cls, ID, name=None, vel=0.4):
        """
        Define an isotropic conduction velocity Eikonal region.

        Args:
            ID : int
                The ID of the conductivity region (**not** the element tags) this
                Eikonal region is to be built from
            name : str, optional
                A short descriptive name for this region
            vel : float, optional
                Conduction velocity
        """
        obj = cls(name, ID, vel, vel, vel)
        return obj

    @classmethod
    def passive(cls, ID, name=None):
        """
        Define a passive (non-conductive) Eikonal region.

        Args:
            ID : int
                The ID of the conductivity region (**not** the element tags) this
                Eikonal region is to be built from
            name : str, optional
                A short descriptive name for this region
        """
        obj = cls(ID, name)
        obj._passive = True
        return obj

    def opts(self):
    
        for opt in super(EikonalRegion, self).opts():
            yield opt
        
        # Add velocities
        yield 'vel_f', self._vel_f
        yield 'vel_s', self._vel_s
        yield 'vel_n', self._vel_n

        # Add ignore flag
        yield 'ignore', int(self._passive)
