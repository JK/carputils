#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
The core infrastructure of the model component classes.

Concepts
--------

model component
    Model components are some aspect of an openCARP simulation setup that can be
    mapped onto a particular part of the command line. These can be things like
    ionic model regions, conductivity regions and stimuli.
param
    The tool used to generate the openCARP command line.
param structure
    A structure in param that forms part of an array within the command line
    options and has attributes that can be set by dot notation e.g.
    `-stimulus[0].name`.
field
    A single attribute of a param structure, e.g. 'name' in the stimulus example
    above.
parameter set
    A set of options that are passed to openCARP as a single field in a single
    string in a p1=0,p2=4 format.

Implementation Information
--------------------------

This module makes use of Python's multiple class inheritance functionality to
combine features in a modular way. An important aspect of this is are the
'Mixin' classes - these are classes which are not intended to be used directly,
but are intended to add additional functionality by being combined with other
classes through multiple inheritance.

Multiple inheritance is used in the classes of this module in their `__init__`
initialiser methods and `opt` methods. Note the use of `super` in these methods
- this allows an instance to call its parent class's methods. In `__init__`,
each class or Mixin processes the arguments it is responsible for, and passes
the remaining arguments to the next class in the resolution order. `opt`
similarly adds its own options on to the end of its parent class's options,
allowing each Mixin to retain its own responsibilities.
"""


from collections import OrderedDict


class Modifier(object):
    """
    Describes a parameter assigment modifier.

    Parameters
    ----------
    value : float
        The modifier value
    """
    def __init__(self, value):
        self._value = value

    def paramstr(self, name):
        """
        Return the parameter modifier string
            `name`=`value`

        Parameters
        ----------
        name : str
            The name of the parameter

        Returns
        -------
        str
            The parameter modifier string
        """
        return '{}={}'.format(name, self._value)

    def __str__(self):
        return '={}'.format(self._value)

    __repr__ = __str__


class AddModifier(Modifier):
    """
    Describes a parameter add modifier.

    Parameters
    ----------
    value : float
        The modifier value
    """
    def __init__(self, value):
        super(AddModifier, self).__init__(value)

    def paramstr(self, name):
        """
        Return the parameter modifier string
            `name`+`value`

        Parameters
        ----------
        name : str
            The name of the parameter

        Returns
        -------
        str
            The parameter modifier string
        """
        return '{}+{}'.format(name, self._value)

    def __str__(self):
        return '+{}'.format(self._value)

    __repr__ = __str__


class MultModifier(Modifier):
    """
    Describes a parameter multiply modifier.

    Parameters
    ----------
    value : float
        The modifier value
    """
    def __init__(self, value):
        super(MultModifier, self).__init__(value)

    def paramstr(self, name):
        """
        Return the parameter modifier string
            `name`*`value`

        Parameters
        ----------
        name : str
            The name of the parameter

        Returns
        -------
        str
            The parameter modifier string
        """
        return '{}*{}'.format(name, self._value)

    def __str__(self):
        return '*{}'.format(self._value)

    __repr__ = __str__


def ensure_set(obj, attributes):
    """
    Check that the named attributes are not None on an object.
    """

    for attr in attributes:
        if getattr(obj, attr) is None:
            tpl = 'must set attribute {} in subclass {}'
            raise NotImplementedError(tpl.format(attr, obj.__class__.__name__))


class AbstractModelComponent(object):
    """
    Describes an abstract model component.

    The described model component should maps on to a structure of the param
    command line parser. Non-abstract subclasses should override several class
    variables, in particular PRM_ARRAY, the name of the array in the .prm file
    (e.g. 'stimulus' when the structure is like '-stimulus[0].name'), and
    PRM_LENGTH, the variable specifiying the length of the structure array (e.g.
    'num_stim' for the simuli.)

    As the intended root base class for all model components, this class is
    responsible for checking that no unrecognised fields were passed (done in
    `__init__`) and generating the actual option list (in `opts_formatted`).

    Additional types of options can be added in base classes by extending the
    output of the `opts` method, which is used by `opts_formatted` when
    generating the option list. This is readily achieved in subclasses and
    mixins (in the case of multiple inheritance) by first looping over the super
    class's `opts` output and then appending on to it the additional opts to be
    returned.
    """

    PRM_ARRAY  = None # Name of the array of this structure in the .prm file
    PRM_LENGTH = None # Variable setting the length of the array in .prm file
    FIELDS     = []   # The names of fields allowed in this model components

    def __init__(self, **fields):

        # Check that needed fields have been overridden
        ensure_set(self, ['PRM_ARRAY', 'PRM_LENGTH'])

        self._fields = {}

        for name, value in fields.items():

            # Since this base class is last in the method resolution order,
            # ensure that no invalid fields remain to avoid accidental errors
            # related to misspelled option names
            if name not in self.FIELDS:
                tpl = 'an invalid option "{}" was passed'
                raise ValueError(tpl.format(name))

            self._fields[name] = value

    def opts(self):
        """
        Generator that yields field name - value pairs for the command line.
        """
        for name, value in self._fields.items():
            yield name, value

    def opts_formatted(self, index):
        """
        Return the command line options for this model component.

        Parameters
        ----------
        index : int
            The array index to use in generating the option list
        """

        # Create a template for the command line options
        prefix = '-{}[{}]'.format(self.PRM_ARRAY, index)
        tpl = prefix + '.{}'

        # Loop over the name - value pairs and assemble the option list
        opts = []
        for name, value in self.opts():
            if value is None:
                opts += [tpl.format(name) ]
            else :
                opts += [tpl.format(name), value]

        return opts


class ParameterSet(object):
    """
    Describes a set of parameters that can be generated into a single string.

    This class is essentially a wrapper for a dictionary, with the following
    features:

    * The class is instanstiated with a set of allowed parameter names
    * These names are used later to ensure correctness of assigned parameters
    * Several methods exist for assigning parameter values in different use
      cases
    * Calling str() on an instance of the class provides a param-friendly
      parameter string

    Parameters
    ----------
    parameters : list
        A list of allowed parameter names for this parameter set
    """

    def __init__(self, parameters):
        self._parameters = list(parameters)
        self._values = {}

    def assign(self, values):
        """
        Assign the provided parameter values.

        This method will attempt to assign all provided values, and will cause
        an error if an invalid parameter name is passed.

        Parameters
        ----------
        values : dict
            The parameter values to assign
        """

        for name, val in values.items():

            # Check this is a valid parameter name
            if name not in self._parameters:
                raise ValueError('"{}" is not a permitted parameter'.format(name))

            # Assign the value
            self._values[name] = val

    def pop_and_assign(self, values):
        """
        Remove and assign any matching parameters from a provided dict.

        This method is more error-tolerant than `assign`. Instead of trying to
        assign all parameters, any non-matching ones are simply ignored (and are
        left in the dict for further processing by the caller).

        Parameters
        ----------
        values : dict
            The parameter values to assign
        """
        for name in self._parameters:
            if name in values:
                self._values[name] = values.pop(name)

    def __str__(self):
        """
        Generate the material parameter string for this model.

        Returns
        -------
        str
            The parameter string
        """

        tpl = '{}={}'

        parts = []

        for name in self._parameters: # Maintain consistent order
            if name in self._values:
                value = self._values[name]
                if str(value).startswith(('*','-','/','+')):
                    parts.append(f"{name}{value}")
                else:
                    parts.append(f"{name}={value}")

        return ','.join(parts)


class ParameterSetMixin(object):
    """
    A mixin to add a parameter set field to a model component class.

    Two class attributes must be set in subclasses - `PARAMETERS`, the list of
    allowed parameters, and `PARAMSET_FIELD`, the name of the field to which the
    parameter set's values should be assigned.
    """

    PARAMETERS     = None # The parameter names to allow
    PARAMSET_FIELD = None # The field of the parameter set

    def __init__(self, *args, **kwargs):

        # Check that required values have been set
        ensure_set(self, ['PARAMETERS', 'PARAMSET_FIELD'])

        # Initialise and store the parameter set
        self._parameter_set = ParameterSet(self.PARAMETERS)

        # Take any matching values from the keyword arguments
        self._parameter_set.pop_and_assign(kwargs)

        # Pass remaining arguments to next __init__
        super(ParameterSetMixin, self).__init__(*args, **kwargs)

    def opts(self):
        """
        Generator that yields field name - value pairs for the command line.
        """

        # First yield any parents' options
        for opt in super(ParameterSetMixin, self).opts():
            yield opt

        # Yield the parameter string if not empty
        pstr = str(self._parameter_set)
        if len(pstr) > 0:
            yield self.PARAMSET_FIELD, pstr


class RegionMixin(object):
    """
    A mixin to add region fields to a model component class.

    When this mixin is used, the first two arguments may be positional and will
    specify a list of region IDs and a name for the model component.
    """

    def __init__(self, IDs=[], name=None, *args, **kwargs):

        self._name = name

        # Allow the IDs to be None
        if IDs is None:
            self._IDs = None
        else:
            # If IDs are not none, the list must have length >=1
            if len(IDs) == 0:
                raise ValueError('no IDs provided')
            self._IDs = [int(i) for i in IDs]

        super(RegionMixin, self).__init__(*args, **kwargs)

    def opts(self):
        """
        Generator that yields field name - value pairs for the command line.
        """

        # First yield any parents' options
        for opt in super(RegionMixin, self).opts():
            yield opt

        # Add the name, if set
        if self._name is not None:
            yield 'name', '"{}"'.format(self._name)

        # Add the IDs, if set
        if self._IDs is not None:
            yield 'num_IDs', len(self._IDs)
            for i, ID in enumerate(self._IDs):
                yield 'ID[{}]'.format(i), ID


class SingleIDRegionMixin(object):
    """
    A mixin to add fields for a single-ID region to a model component class.

    When this mixin is used, the first two arguments may be positional and will
    specify a single region ID and a name for the model component.
    """

    def __init__(self, ID=None, name=None, *args, **kwargs):

        self._ID = None if ID is None else int(ID)
        self._name = name

        super(SingleIDRegionMixin, self).__init__(*args, **kwargs)

    def opts(self):
        """
        Generator that yields field name - value pairs for the command line.
        """

        # First yield any parents' options
        for opt in super(SingleIDRegionMixin, self).opts():
            yield opt

        # Add the name, if set
        if self._name is not None:
            yield 'name', '"{}"'.format(self._name)

        # Add the ID, if set
        if self._ID is not None:
            yield 'ID', self._ID


class ModelMixin(ParameterSetMixin):
    """
    More concrete version of the ParameterSetMixin for ep/mechanics models.
    """

    MODELFIELD = None # Name of the param field to specify the model type
    MODELID    = None # The value to assign to MODELFIELD for the model type
    SHORTNAME  = None # A shorter name for this model for slug generation

    def __init__(self, *args, **kwargs):
        ensure_set(self, ['MODELFIELD', 'MODELID'])
        super(ModelMixin, self).__init__(*args, **kwargs)

    def opts(self):
        """
        Generator that yields field name - value pairs for the command line.
        """

        # First yield any parents' options
        for opt in super(ModelMixin, self).opts():
            yield opt

        # Add the model type
        yield self.MODELFIELD, self.MODELID

    def slug(self):
        """
        Generate a descriptive short name.
        """

        if self.SHORTNAME is None:
            slug = self.__class__.__name__.lower()
            if slug.endswith('region'):
                slug = slug[:-6]
            if slug.endswith('material'):
                slug = slug[:-8]
        else:
            slug = self.SHORTNAME

        return slug + str(self._parameter_set)
        #return slug + self._params.paramstr('', '')


class AbstractIonicModel(RegionMixin, ModelMixin, AbstractModelComponent):

    PRM_ARRAY      = 'imp_region'
    PRM_LENGTH     = 'num_imp_regions'
    MODELFIELD     = 'im'
    PARAMSET_FIELD = 'im_param'

    FIELDS = ['cellSurfVolRatio',
              'volFrac',
              'im_sv_dumps',
              'im_sv_init']

    def __init__(self, *args, **kwargs):
        super(AbstractIonicModel, self).__init__(*args, **kwargs)
        self._stress = None

    def set_stress(self, stress):
        assert isinstance(stress, AbstractPlugin)
        self._stress = stress

    def opts(self):

        # First yield any parents' options
        for opt in super(AbstractIonicModel, self).opts():
            yield opt

        # Yield the stress options
        if self._stress is not None:
            for opt in self._stress.opts():
                yield opt

class ExternalIonicModel(AbstractIonicModel):
    """
    Describes the passive ionic model configuration

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    """
    SHORTNAME = 'external'
    MODELID = 'EXTERNAL'
    PARAMETERS = {}

    def set_name(self, name):
        self.MODELID = name

class AbstractMechanicsMaterial(ModelMixin, RegionMixin,
                                AbstractModelComponent):

    PRM_ARRAY  = 'mregion'
    PRM_LENGTH = 'num_mregions'
    MODELFIELD = 'type'
    PARAMSET_FIELD   = 'params'


class AbstractPlugin(object):

    NOPLUGIN = False
    PLUGIN = None

    def __init__(self, **params):

        if not self.NOPLUGIN:
            ensure_set(self, ['PLUGIN', 'PARAMETERS'])

        self._paramset = ParameterSet(self.PARAMETERS)
        self._paramset.assign(params)

    def opts(self):
        if not self.NOPLUGIN:
            yield 'plugins', self.PLUGIN
        pstr = str(self._paramset)
        if len(pstr) > 0:
            yield 'plug_param', pstr


class NoPlugin(AbstractPlugin):
    """
    Placeholder representing no plugin to be used.

    This class does not accept any parameters.
    """
    NOPLUGIN = True


def optionlist(components):
    """
    Generate a list of command line options from the specified components.
    """

    # Sort into component types
    component_groups = OrderedDict()
    for comp in components:
        try:
            component_groups[comp.PRM_LENGTH].append(comp)
        except KeyError:
            component_groups[comp.PRM_LENGTH] = [comp]

    opts = []

    for prm_len_variable, group in component_groups.items():

        # number of materials option
        opts += ['-{}'.format(prm_len_variable), len(group)]

        # append the options for each model/region
        for i, comp in enumerate(group):
            opts += comp.opts_formatted(i)

    return opts


def convert_param_dict(params):
    """ convert parameter dict to comma separated string list """
    param_list=""
    for key, value in params.items():
        if str(value).startswith(('*','-','/','+')):
            param_list += "{}{},".format(key, value)
        else:
            param_list += "{}={},".format(key, value)
    return param_list[:-1]
