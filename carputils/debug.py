#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some tools for running jobs in the debugger
"""

import sys
import re

from carputils import divertoutput
from carputils import stream

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range

def run_valgrind_interactive(command, executable, *args, **kwargs):
    """
    Run an MPI command and attach GDB instances with valgrind.

    Diverts the output of the MPI job to a stream which filters for valgrind's
    prompt to attach GDB instances to specific process indicies. When the
    regular expression searching for this is matched, a callback is run which
    automatically attaches the GDB instances.

    Parameters
    ----------
    command : str
        The MPI command to execute
    """

    # Import at runtime to prevent circular imports
    from carputils import settings

    # Sanity check
    assert settings.cli.gdb is not None

    # Pattern to get the process ID from the help message printed from valgrind
    regex = re.compile('target remote \| .* --pid=(\d+)')

    # Set up a callback that attaches a valgrind-gdb instance to the process
    def callback(match):
        # Get the process ID
        pid = int(match.group(1))
        gdb_cmd = ['xterm', '-e', 'gdb', executable,
                   '-ex', 'target remote | vgdb --pid={}'.format(pid),
                   '-ex', 'continue']
        divertoutput.call(gdb_cmd, detached=True)

    # Determine the total number of processes to look for
    n_gdb = len(settings.cli.gdb)
    if n_gdb == 0:
        # None specified, run on all
        n_gdb = settings.cli.np

    # Set up steam with callback to connect gdb procs
    strm = stream.RegexCallbackStream(regex, callback, n_gdb)
    
    # Set up a merged stream to write to stdout and the regex stream
    merged = stream.merge([sys.stdout, strm])

    # Run openCARP command with stdout diverted to merged stream
    with stream.divert_std(merged):
        divertoutput.call(command, *args, **kwargs)

def mpi_attach_debugger(launcher, command, nproc, attach_procs,
                        attach_prefix, detach_prefix=[], xterm=True):
    """
    Convenience method to launch MPI with a specific prefix added to the
    command on specific processes

    Parameters
    ----------
    launcher : str
        The MPI launcher to use
    command : str
        The un-prefixed command
    nproc : int
        The total number of processes to use
    attach_procs : list
        The processes to attach the debugger to (empty list - select all)
    attach_prefix : list
        List of commands to use as prefix on attached processes
    detach_prefix : list, optional
        List of commands to use as prefix on non-attached processes
    xterm : bool, optional
        Launch selected processes in xterm (default: True)

    Returns
    -------
    str
        The compiled command
    """ 

    # If selected_procs is an empty list, select all
    if len(attach_procs) == 0:
        attach_procs = range(nproc)

    # Only use xterm if >1 proc attached
    if len(attach_procs) < 2:
        xterm = False
    if xterm:
        attach_prefix = ['xterm', '-e'] + attach_prefix

    # Single proc case, no need for MPI
    if nproc == 1:
        if 0 in attach_procs:
            return attach_prefix + command 
        else:
            return detach_prefix + command

    # Assemble the command
    assembled = [launcher]
    group_attached = False
    group_size = 0

    for iproc in xrange(nproc):
        
        # Is this proc to be attached?
        attached = iproc in attach_procs

        # Does this end a group?
        if attached != group_attached:
            
            # Only add group if >0
            if group_size > 0:

                # Add the previous group to the assembled command
                assembled += ['-n', group_size]
                if group_attached:
                    assembled += attach_prefix
                else:
                    assembled += detach_prefix
                assembled += command + [':']

            # Initialise new group
            group_attached = attached
            group_size = 0

        # Accumulate group
        group_size += 1

    # Add final group
    assembled += ['-n', group_size]
    if group_attached:
        assembled += attach_prefix
    else:
        assembled += detach_prefix
    assembled += command 

    return assembled
