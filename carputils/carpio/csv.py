#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Read csv files
"""

import gzip
import numpy as np
from carputils.carpio.filelike import FileLikeMixin

# Save original file open
fopen = open

class CSVFile(FileLikeMixin):
    """
    Read from a csv file.

    The file may be gzipped - the code automatically determines if this is the
    case from the file extension.
    """

    def __init__(self, filename):
        """
        Args:
            filename: (str) Path of the file to read.
        """
        if filename.endswith('.gz'):
            self._fp = gzip.open(filename, 'rb')
        else:
            self._fp = fopen(filename, 'rb')

        self._hdr = None

    def close(self):
        """
        Close the file.
        """
        self._fp.close()

    def header(self):
        """
        Read the file header.

        Returns:
            header: The file header
        """

        if self._hdr is not None:
            return self._hdr

        bhdr = self._fp.readline()
        self._hdr = bhdr.decode('utf8').strip().replace(' ','').split(',')

        return self._hdr

    def data(self):
        """
        Read the file data as a numpy array.

        Returns:
            data: (numpy.ndarray) The file data contents.
        """

        data = np.genfromtxt(self._fp, delimiter=',', skip_header=1, filling_values=0.0)
        return data

def open(*args, **kwargs):
    """
    Open a csv file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Parameters inherited from `CSVFile.__init__()`
    """
    return CSVFile(*args, **kwargs)
