#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Provides a class to convert physical quantities from a string to numerical values

The PhysicalQuantity class can be used in combination with the ArgumentParser class
whenever physical quantities are required as arguments. First you have to define your
physical quantity:

    # supported units and their scale factors to a reference unit, kPa in this case
    unitdict = {'Pa': 0.001, 'kPa': 1.0, 'mPa': 1000.0, 'mmHg': 0.133322387415}
    # defualt input unit
    iunit = 'kPa'
    # output unit
    ounit = 'mmHg'
    PressureQuantity = PhysicalQuantity(unitdict, iunit, ounit)

Next you can define your argument:

    parser.add_argument('--pressure', type=PressureQuantity, default=10.0, ....)

    args = parser.parse_args()
    # `args.pressure` is of type `float` and already converted to the desired output unit

Now the user can state a pressure with different units:

    ./script.py --pressure 10.0Pa
    ./script.py --pressure 10.0kPa
    ./script.py --pressure 10.0mPa
    ./script.py --pressure 10.0mmHg
    ./script.py --pressure 10.0       # No unit, default input unit is used
"""


class PhysicalQuantity:
    """
    Defines a physical quantity

    Args:
        unitdict : dict
            A dictionary containing the physical unit and the scale
            factor to a reference unit
        iunit : str
            The default input unit if not unit is given
        ounit : str
            The output unit
    """

    def __init__(self, unitdict, iunit, ounit):
        self._udict = dict(unitdict)
        self._iunit = str(iunit)
        assert self._iunit in self._udict
        self._ounit = str(ounit)
        assert self._ounit in self._udict
        self._units = sorted(self._udict.keys(), reverse=True, key=len)

    @property
    def input_unit(self):
        """
        Get the default input unit
        """
        return self._iunit

    @property
    def output_unit(self):
        """
        Get the output unit
        """
        return self._ounit

    @property
    def units(self):
        """
        Get all the supported units
        """
        return self._units

    def from_string(self, valuestr, ounit=None):
        """
        Convert a str to numerical value

        Args:
            valuestr : str
                A string representing the physical quantity
            ounit : str, optional
                The desired output unit, if `None` the default
                output unit is used
        """
        if ounit is None:
            ounit = self._ounit
        elif ounit not in self._udict:
            raise AttributeError('`{}` is not a valid unit, choose from {}!'.format(ounit, self._udict.keys()))
        iunit = None
        strlen = len(valuestr)
        for unit in self._units:
            if valuestr.endswith(unit):
                iunit = unit
                break
        if iunit is None:
            iunit = self._iunit
        else:
            strlen -= len(iunit)
        value = float(valuestr[:strlen])
        return value*self._udict[iunit]/self._udict[ounit]

    def __call__(self, valuestr):
        return self.from_string(valuestr)


