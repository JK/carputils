#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Functionality to read/write state variable (sv) files.
"""

import os
import stat
import numpy as np
from carputils.carpio.filelike import FileLikeMixin

fopen = open

class SVFile(FileLikeMixin):
    """
    Read a single state variable (sv) file.

    It's unlikely that you will want to instantiate this class directly.
    Correctly reading an sv file requires additional information found in the
    header file, which is already done by :class:`SVSeries`.

    Args:
        filename : str
            The filename to be read
        dtype : str
            The sv-header data type
        dsize : int
            The size, in bytes, of the data type
        bigendian : bool
            Whether the file is big endian
        nsamps : int, optional
            The number of values in the sv file
    """

    def __init__(self, filename, dtype, dsize, bigendian, nsamps=None):

        # Check that file size matches expected size
        if nsamps is not None:
            
            # Get file size
            filestats = os.stat(filename)
            filesize  = filestats[stat.ST_SIZE]
            
            # Compare with expected file size
            if nsamps * dsize != filesize:
                tpl = 'File size {} does not match N*dsize'
                raise Exception(tpl.format(filesize))

        self._fp = fopen(filename, 'rb')

        if dtype in ['Gatetype', 'Real', 'GlobalData_t'] and dsize == 4:
            self._dtype = 'float'
        elif dtype in ['Gatetype', 'Real', 'GlobalData_t'] and dsize == 8:
            self._dtype = 'double'
        else:
            self._dtype = dtype

    def close(self):
        """
        Close the file.
        """
        self._fp.close()

    def varname(self):
        """
        Return the variable name, determined from the read file name.

        Returns:
            str
                The variable name
        """
        fname = os.path.basename(self._fp.name)
        return fname.split('.')[1]

    def dtype(self):
        """
        Return the file data type.

        Returns:
            str
                The data type name
        """
        return self._dtype

    def data(self):
        """
        Return the sv file contents as a numpy array.

        Returns:
            numpy.ndarray
                The file data
        """
        dtype = {'float':  'float32',
                 'double': 'float64'}[self._dtype]
        return np.fromfile(self._fp, dtype=dtype)

class SVSeries(FileLikeMixin):
    """
    Read a set of state variable (sv) files.
    """

    def __init__(self):
        self._series = []

    @classmethod
    def from_header(cls, filename):
        """
        Read the series of sv files described by a sv header file.
        
        Args:
            filename : str
                The path of the sv header file

        Returns:
            SVSeries
                The series of sv files
        """
        obj = cls()
        
        dirname = os.path.dirname(filename)

        with fopen(filename) as hdr:

            line = hdr.readline()
            bigendian = int(line.split()[0])

            for line in hdr:
                
                fields = line.split()
                
                fname = os.path.join(dirname, fields[0])
                dtype = fields[1]
                size  = int(fields[2])
                nsamps = int(fields[3])

                sv = SVFile(fname, dtype, size, bigendian, nsamps)
                obj.append(sv)

        return obj

    def append(self, svfile):
        """
        Add a new sv file to the series.

        Args:
            svfile : SVFile
                The sv file to add 
        """
        assert isinstance(svfile, SVFile)
        self._series.append(svfile)

    def __iter__(self):
        """
        Make series iterable (usable in for loop).
        """
        return iter(self._series)

    def close(self):
        """
        Close all constituent files.
        """
        for svfile in self:
            svfile.close()

def open(*args, **kwargs):
    """
    Read a state variable (sv) file
    """
    return SVFile(*args, **kwargs)

def open_series(*args, **kwargs):
    """
    Read a state variable (sv) file series
    """
    return SVSeries.from_header(*args, **kwargs)
