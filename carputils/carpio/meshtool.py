#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import os, sys

from contextlib import contextmanager

@contextmanager
def suppress_stdout():
    """
    Context manager to redirect stdout.
    """
    with open(os.devnull,'w') as devnull:
        _stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = _stdout
    return


def execute(job, cmd, msg=None, outdir=None, silent=False, stdout=None, stderr=None):
    """
    Execute pre-assembled MeshTool command.

    Args:
        job: `job.job.Job` object.
        cmd: MeshTool command to be executed.
    Kwargs:
        msg:    User defined terminal message.
        outdir: Dedicated data output directory.
        silent: Set true to silence commandline messages.
    """
    if not outdir:
        outdir = os.getcwd()

    print('output directory:{}'.format(outdir))

    if not silent:
        if outdir is not None and not os.path.exists(outdir):
            job.mkdir(outdir, parents=True)
        job.meshtool(cmd, msg, stdout=stdout, stderr=stderr)
    else:
        with suppress_stdout():
            if outdir is not None and not os.path.exists(outdir):
                job.mkdir(outdir, parents=True)
            job.meshtool(cmd, msg, stdout=stdout, stderr=stderr)
    return

def generate_splitsurf(job, msh, surfs, split_ofile, silent=False, stdout=None, stderr=None):

    """
    MESHTOOL    generate a split file from a given surface.

    parameters:
        msh:   (input) path to basename of the input mesh
        surf:   (input) list of surfaces seperated with a comma
        ifmt:   (optional) input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow)
        split:		 (output) path to the split list file

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    cmd = ['generate',' surfsplit',
          '-msh='+msh,
          '-surf='+surfs,
          '-split='+split_ofile]

    execute(job, cmd, outdir=None, silent=silent, stdout=stdout, stderr=stderr)

    # [job.mv(x,split_ofile+'.'+x) for x in ['nod_selection.dat','surface_alignment.vec'] if os.path.isfile(x)]

    return


def generate_split(job, msh, ops, split_ofile, silent=False, stdout=None, stderr=None):
    """
    MESHTOOL    generate a split file from a given surface.

    parameters:
        msh:   (input) path to basename of the input mesh
        op:     (input) sequence of split operations.
        ifmt:   (optional) input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow)
        split:		 (output) path to the split list file

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    cmd = ['generate','split',
          '-msh='+msh,
          '-op='+ops,
           '-split='+split_ofile]

    execute(job, cmd, outdir=None, silent=silent, stdout=stdout, stderr=stderr)

    return


def convert(job, imsh, ifmt, omsh, ofmt, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool 'convert'.

    Converts a mesh between different formats.

    Args:
        job:    job.job.Job object
        imsh:   (input) path to basename of the input mesh
        ifmt:   (input) format of the input mesh
        omsh:   (output) path to basename of the output mesh
        ofmt:   (input) format of the output mesh
    Kwargs:
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats are -- carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar<br>
        The supported output formats are -- carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
    """

    # check if output directory exists
    outdir = os.path.dirname(omsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd = ['convert', '-imsh=' + imsh,
                      '-ifmt=' + ifmt,
                      '-omsh=' + omsh,
                      '-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return


def UVC_localize(job, msh, pts2find, scale_thr=1, ofile=None, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's query idxlist_uvc.

    Args:
        msh:        (input) path to basename of the input mesh. Mesh must contain +'.pts_t' file describing the UVC Combined Coordinates.
        pts2find:   (input) list of uvcpts to localize with included thresholds. Format = {z,rho,phi,ven,thr_rho,thr_ep}
        scale_thr:  (output) scaling the sizing of the stimulus sites.
        ofile:      (optional) format of the output mesh
        silent:     (optional) set True to silence terminal messages
    Returns:
        UVC data.
    """

    if ofile is None:
        ofile=msh+'.MT.pts'

    with open(ofile,'w') as f:
        f.write(str(len(pts2find))+'\n')
        for k,entry in enumerate(pts2find):
            f.write(' '.join((str(entry[0]),str(entry[1]),str(entry[2]),str(entry[3]),
                              str(round(scale_thr*entry[4])),
                              str(entry[5]),'\n')))

    cmd=['query','idxlist_uvc',
          '-msh='+msh,
          '-coord='+ofile]

    execute(job, cmd, outdir=None, silent=silent, stdout=stdout, stderr=stderr)

    f=open(ofile+'.out.txt','r')
    uvc_mt_data=f.readlines()[1::]
    f.close()

    return uvc_mt_data

def insert(job, cmode, submsh=None, msh=None, outmsh=None, ofmt=None, ifmt=None, idat=None, mode=None, silent=False, stdout=None, stderr=None):
    """ MESHTOOL
    # -------------------------------------------------------------------------
    insert submesh: a given submesh is inserted back into a mesh and written to an output mesh
    parameters:
        -submsh=<path>	... (input) path to basename of the submesh to insert from
        -msh=<path>	    ... (input) path to basename of the mesh to insert into
        -ofmt=<format>	... (optional) mesh output format. may be: carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
        -outmsh=<path>	... (output) path to basename of the output mesh

    Note that the files defining the submesh must include a *.eidx and a *.nod file.
    This files define how elements and nodes of the submesh map back into the original mesh.
    The *.eidx and *.nod files are generated when using the "extract mesh" mode.

    # -------------------------------------------------------------------------
    insert meshdata: the fiber and tag data of a mesh is inserted into another mesh
                     based on vertex locations.
    parameters:
        -imsh=<path>		... (input) path to basename of the mesh to insert from
        -msh=<path>		... (input) path to basename of the mesh to insert into
        -op=<path>		... (input) Operation index: 0 = only tags, 1 = only fibers, 2 = both
        -ifmt=<format>		... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -ofmt=<format>		... (optional) mesh output format. may be: carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
        -trsp=<vec-file>	... (optional) element-based transport gradient
        -trsp_dist=<float>	... (optional) transport distance threshold
        -grad_thr=<float>	... (optional) gradient correlation threshold. Must be in (0, 1). Default is 0.99.
        -corr_thr=<float>	... (optional) correspondance linear search fallback threshold. Default is 1000.
        -con_thr=<float>	... (optional) Connectivity threshold. Required number of nodes an element of mesh1
		                                   needs to share with an element of mesh2 in order for the data to be inserted. Default is 1.
        -outmsh=<path>		... (output) path to basename of the output mesh

    # -------------------------------------------------------------------------
    insert data: data defined on a submesh is inserted back into a mesh
    parameters:
        -msh=<path>		... (input) path to basename of the mesh
        -submsh=<path>		... (input) path to basename of the submesh
        -submsh_data=<path>	... (input) path to submesh data
        -msh_data=<path>	... (input) path to mesh data
        -odat=<path>	... (output) path to output data

    Note that the files defining the submesh must include a *.nod file.
    This file defines how the nodes of the submesh map back into the original mesh.
    The *.nod file is generated when using the "extract mesh" mode.

    # -------------------------------------------------------------------------
    insert points: points of a submesh are inserted back into a mesh and written to an output file
    parameters:
        -submsh=<path>	... (input) path to basename of the submesh to insert from
        -msh=<path>	... (input) path to basename of the mesh to insert into
        -outmsh=<path>	... (output) path to basename of the output mesh

    Note that the files defining the submesh must include a *.nod file.
    This file defines how the nodes of the submesh map back into the original mesh.
    The *.nod file is generated when using the "extract mesh" mode.

    # -------------------------------------------------------------------------
    insert tags: insert an element data vector as new mesh element tags.
    parameters:
        -msh=<path>	 (input) path to basename of the mesh.
        -idat=<path>	 (input) .dat file the tags are inserted from.
        -outmsh=<path>	 (output) path to basename of the output mesh
        -ifmt=<format>	 (optional) mesh input format.
        -ofmt=<format>	 (optional) mesh output format.

    # -------------------------------------------------------------------------
    insert fibers: insert element fibers.
    parameters:
        -msh=<path>		(input) path to basename of the mesh.
        -idat=<fiber>,<sheets>	(input) fibers or sheets, or list of fibers followed by sheets. Files are .vec files.
        -mode=<int>	 (optional) insert mode. 0 == fibers and sheets, 1 == only fibers, 2 == only sheets. 0 is default.
        -outmsh=<path>		(output) path to basename of the output mesh
        -ifmt=<format>		(optional) mesh input format.
        -ofmt=<format>		(optional) mesh output format.
    """

    # check if output directory exists
    outdir = os.path.dirname(outmsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd  = ['insert', cmode]
    if cmode == 'submesh':
        cmd += ['-submsh=' + submsh]
        cmd += ['-msh='    + msh   ]
        cmd += ['-outmsh=' + outmsh]
    elif cmode == 'tags':
        cmd += ['-msh='    + msh]
        cmd += ['-idat='   + idat]
        cmd += ['-outmsh=' + outmsh]
    elif cmode == 'fibers':
        cmd += ['-msh='    + msh]
        cmd += ['-idat='   + idat]
        cmd += ['-outmsh=' + outmsh]
        if ofmt is not None:
            cmd += ['-mode='   + mode]
    else:
        print (f'Warning: insert {cmode} not yet implemented!')
        sys.exit(0)


    if ofmt is not None:
        cmd += ['-ofmt=' + ofmt]
    if ifmt is not None:
        cmd += ['-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return

def extract_mesh(job, msh, submsh, tags, tag_file=None, ifmt=None, ofmt=None, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool 'extract mesh'

    A submesh is extracted from a given mesh based on given element tags.

    Args:
        job:    job.job.Job object
        msh:    (input) path to basename of the mesh to extract from
        tags:   (input) ","-seperated list of tags, e.g. tag1,tag2
	tag_file: (optional input) path to an alternative tag file, e.g. *.tags, *.btags
        submsh: (output) path to basename of the submesh to extract to
    Kwargs:
        ifmt:	(optional) mesh input format
        ofmt:   (optional) mesh output format
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats are -- carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow<br>
        The supported input formats are -- carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow
    """

    # check if output directory exists
    if submsh.find(','):
        outdir = os.path.dirname(submsh.split(',')[-1])
    else:
        outdir = os.path.dirname(submsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd  = ['extract', 'mesh'  ]
    cmd += ['-msh='    + msh   ]
    cmd += ['-tags='   + tags  ]
    cmd += ['-submsh=' + submsh]

    if tag_file is not None:
        cmd += ['-tag_file={}'.format(tag_file)]
    if ifmt is not None:
        cmd += ['-ifmt=' + ifmt]
    if ofmt is not None:
        cmd += ['-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return

def interpolate(job, cmode, omsh=None, idat=None, odat=None, imsh=None, dynpts=None, pts=None, mode=None, silent=False,
                stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'interpolate nodedata' 'interpolate elemdata' 'interpolate clouddata' 'interpolate elem2node' 'interpolate node2elem' function.

    Examples:
        #interpolate nodal data from one mesh onto another
        meshtool.interpolate(job, 'nodedata', omsh, imsh, idat, odat, dynpts=)
    Args:
        job:    job.job.Job object
        cmode:  choose 'nodedata'
        omsh:   (input) path to basename of the mesh we interpolate to
        imsh:   (input) path to basename of the mesh we interpolate from
        idat:   (input) path to input data.
        odat:   (output) path to output data
    Kwargs:
        dynpts: (optional) dynpts describing point cloud movement
        silent: (optional) set True to silence terminal messages

    Examples:
        #interpolate element data from one mesh onto another
        meshtool.interpolate(job, 'elemdata', omsh, imsh, idat, odat, dynpts=None, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'elemdata'
        omsh:   (input) path to basename of the mesh we interpolate to
        imsh:   (input) path to basename of the mesh we interpolate from
        idat:   (input) path to input data.
        odat:   (output) path to output data
    Kwargs:
        dynpts: (optional) dynpts describing point cloud movement
        silent: (optional) set True to silence terminal messages

    Examples:
        #interpolate data from a pointcloud onto a mesh using radial basis function interpolation
        meshtool.interpolate(job, 'clouddata', omsh, pts, idat, odat, mode=2, dynpts=None, silent=False)

    Args:
        job:    job.job.Job object
        cmode:  choose 'clouddata'
        omsh:   (input) path to basename of the mesh we interpolate to
        pts:    (input) path to the coordinates of the point cloud
        idat:   (input) path to input data.
        odat:   (output) path to output data.
    Kwargs:
        mode:   (optional) Choose between localized Shepard (=0), global Shepard (=1), and RBF interpolation (=2). Default is 2.
        dynpts: (optional) dynpts describing point cloud movement.

    Examples:
        #interpolate data from one mesh onto another
        meshtool.interpolate(job, 'elemdata', omsh, imsh, idat, odat, dynpts=None, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'elemdata'
        omsh:   (input) path to basename of the mesh we interpolate to
        imsh:   (input) path to basename of the mesh we interpolate from
        idat:   (input) path to input data.
        odat:   (output) path to output data
    Kwargs:
        dynpts: (optional) dynpts describing point cloud movement
        silent: (optional) set True to silence terminal messages

    Examples:
        #interpolate data from elements onto nodes
        meshtool.interpolate(job, 'elem2node', omsh, idat, odat, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'elem2node'
        omsh:   (input) path to basename of the mesh we interpolate to
        idat:   (input) path to input data.
        odat:   (output) path to output data
    Kwargs:
        silent: (optional) set True to silence terminal messages

    Examples:
        #interpolate data from nodes onto elements
        meshtool.interpolate(job, 'node2elem', omsh, idat, odat, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'node2elem'
        omsh:   (input) path to basename of the mesh we interpolate to
        idat:   (input) path to input data.
        odat:   (output) path to output data
    Kwargs:
        silent: (optional) set True to silence terminal messages
    """

    MODES  = ['nodedata', 'elemdata', 'clouddata', 'elem2node', 'node2elem']
    assert cmode in MODES, 'meshtool interpolate detected incorrect mode: {}'.format(cmode)

    # check if output directory exists
    outdir = os.path.dirname(omsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd = ['interpolate', cmode]
    # -------------------------------------------------------------------------
    if 'nodedata' in cmode or 'elemdata' in cmode:
        cmd += ['-imsh={}'.format(imsh)]
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
        if dynpts is not None:
            cmd += ['-dynpts={}'.format(dynpts)]
    # -------------------------------------------------------------------------
    elif 'clouddata' in cmode:
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-pts={}'.format(pts)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
        if mode is not None:
            cmd += ['-mode={}'.format(mode)]
        if dynpts is not None:
            cmd += ['-dynpts={}'.format(dynpts)]
    # -------------------------------------------------------------------------
    elif 'elem2node' in cmode or 'node2elem' in cmode:
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(cmode)

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return


def itk(job, cmode, msh=None, min=None, max=None, size=None, surf=None, tags=None, outmsh=None, silent=False,
        stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'itk sample' 'itk <to be implemented>' function.

    Examples:
        #create an itk image stack from sampling surfaces
        meshtool.itk(job, 'sample', msh, min, max, size, surf, outmsh, tags=,)
    Args:
        job:    job.job.Job object
        cmode:  choose 'sample'
        min:    (input) Path of the image stack to sample on
        max:    (input) path to basename of the mesh we interpolate from
        size:   (input) path to input data.
        surf:   (output) path to output data
        outmsh: (output) path to output data
    Kwargs:
        tags:   (optional) tag indices for inserted regions. Should match order of surfaces.
        silent: (optional) set True to silence terminal messages
    """

    MODES = ['sample']
    assert cmode in MODES, 'meshtool itksample detected incorrect mode: {}'.format(cmode)

    # check if output directory exists
    outdir = os.path.dirname(outmsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd = ['itk', cmode]
    # -------------------------------------------------------------------------
    if 'sample' in cmode:
        if msh is not None:
            cmd += ['-msh={}'.format(msh)]
        else:
            cmd += ['-min={}'.format(min)]
            cmd += ['-max={}'.format(max)]
            cmd += ['-size={}'.format(size)]
        cmd +=  ['-surf={}'.format(surf)]
        cmd += ['-outmsh={}'.format(outmsh)]
        if tags is not None:
            cmd += ['-tags={}'.format(tags)]
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(cmode)

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return

def extract(job, cmode, msh, surf=None, submsh=None, op=None, tags=None, tag_file=None, odat=None, mode=None, ifmt=None, ofmt=None, edge=None, ang_thr=None,
                    coord=None, size=None, lower=None, hybrid=0, silent=False, stdout=None, stderr=None):
    if cmode == "surface":
        return extract_surface(job, msh, surf, op, tag_file, ifmt, ofmt, edge, ang_thr,
                               coord, size, lower, hybrid, silent, stdout, stderr)
    elif cmode == "mesh":
        return extract_mesh(job, msh, submsh, tags, tag_file, ifmt, ofmt, silent, stdout, stderr)
    elif cmode in ["tags", "fibers"]:
        cmd  = ['extract', cmode]
        cmd += [f'-msh={msh}']
        cmd += [f'-odat={odat}']
        if cmode == "fibers" and mode:
            cmd += [f'-mode={mode}']
        outdir = os.path.dirname(msh)
    else:
        print (f'Warning: insert {cmode} not yet implemented!')
        sys.exit(0)

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    print('Done')
    return

def extract_surface(job, msh, surf, op=None, tag_file=None, ifmt=None, ofmt=None, edge=None, ang_thr=None,
                    coord=None, size=None, lower=None, hybrid=0, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'extract surface'.

    Extract a sequence of surfaces defined by set operations on element tags.

    Args:
        job:    job.job.Job object
        msh:    (input) path to basename of the mesh
        surf:	(output) list of names associated to the given operations
    Kwargs:
        op:		(optional) list of operations to perform. By default, the surface of the full mesh is computed.
        tag_file: (optional) path to an alternative tag file {*.tags, *.btags}
        edge:   (optional) <deg. angle><br>
                surface elements connected to sharp edges will be removed. A sharp edge is defined by the nodes
                which connect elements with normal vectors at angles above the given threshold.
        ang_thr: (optional) if set, surface traversal stops when angle between current and starting
                 normal vectors exceeds threshold
        coord:  (optional) <xyz>:<xyz>:..<br> restrict surfaces to those elements reachable by surface edge-traversal
                from the surface vertices closest to the given coordinates. If -edge= is also provided, sharp edges
                will block traversal, thus limit what is reachable.
        size:   (optional) surface edge-traversal is limited to the given radius from the initial index.
        lower:	(optional) surface edge-traversal limitation lower size (for extracting bands).
        hybrid: (optional) Write hybrid quad + tri surfaces. 1 == on, 0 == off. 0 is default.
        ifmt:	(optional) mesh input format
        ofmt:	(optional) mesh output format. If set, the surfaces will also be written as surface meshes.
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats are -- carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow<br>
        The supported output formats are -- carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, neu, obj, stellar, vcflow, ensight_txt
        <br><br>
        The format of the operations is:<br>
        tagA1,tagA2,[surfA1,surfA2..]..[+-:]tagB1,tagB2,[surfB1..]..;tagA1,..[+-:]tagB1..;..<br>
        Tag regions separated by "," will be unified into submeshes and their surface computed.
        Alternatively, surfaces can be provided directly by .surf surface files (only basename, no extension).
        If two surfaces are separated by "-", the rhs surface will be removed from the
        lhs surface (set difference). Similarly, using "+" will compute the surface union.
        If the submeshes are separated by ":", the set intersection of the two submesh surfaces will be computed.
        Individual operations are separated by ";".
        <br><br>
        The number of names provided with "-surf=" must match the number of operations. If no operations are provided,
        the surface of the whole geometry will be extracted. Individual names are separated by ",".
        <br><br>
        Further restrictions can be added to the surface extraction with the -edge= , -coord= , -size= options.
    """

    # check if output directory exists
    if surf.find(',') >= 0:
        outdir = os.path.dirname(surf.split(',')[-1])
    else:
        outdir = os.path.dirname(surf)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    # TODO: this error check may be removed sometime soon
    #       just put it here to more quickly account for meshtool changes
    if isinstance(op, str) and ',' in surf:
        if not ';' in op:
            os.error('Individual operations NOW need to be separated by ";"! You may need to update meshtool.')
    # -------------------------------------------------------------------------

    cmd  = ['extract', 'surface']
    cmd += ['-msh={}'.format(msh)]
    cmd += ['-surf={}'.format(surf)]

    if op is not None:
        if isinstance(op, (list, tuple)):
            # op needs to be a string
            op   = ';'.join(str(x) for x in op)
            cmd += ['-op={}'.format(op)]
        else:
            cmd += ['-op={}'.format(op)]
    if tag_file is not None:
        cmd += ['-tag_file={}'.format(tag_file)]
    if edge is not None:
        cmd += ['-edge={}'.format(edge)]
    if ang_thr is not None:
        cmd += ['-ang_thr={}'.format(ang_thr)]
    if coord is not None:
        cmd += ['-coord={}'.format(coord)]
    if size is not None:
        cmd += ['-size={}'.format(size)]
    if lower is not None:
        cmd += ['-lower_size={}'.format(lower)]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]
    cmd += ['-hybrid={}'.format(hybrid)]

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    print('Done')
    return


def generate_mesh(job, surf, outmsh, ins_tag=None, holes=None, scale=None, bdry_layers=None, bdry_step=None,
                  bdry_inc=None, bdry_symm=None, mode=None, prsv_bdry=None, ifmt=None, ofmt=None,
                  silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'generate mesh'.

    Generate a tetrahedral mesh from a list of nested triangle surfaces.

    Args:
        job:    job.job.Job object
        surf:	(input) list of paths to triangle meshes of closed surfaces
        outmsh: (output) path to the basename of the output mesh
    Kwargs:
        ins_tag:     (optional) tag indices for meshed regions. Should match order of surfaces.
        holes:       (optional) Specify coordinates of hole seed points.
        scale:       (optional)  Global edge length scaling (one value provided) *OR*
                                 regional edge length scaling (one value per surface provided).
        bdry_layers: (optional) Number of boundary element layers to include in mesh.
                     If negative, only the boundary mesh part will be generated.
        bdry_step:   (optional) Size of a boundary layer step, relative to the average edge length. Default is 0.10.
        bdry_inc:    (optional) Relative boundary layer step increment. Default is 1.25
        bdry_symm:   (optional) if 1, the boundary layer will be applied symmetrically (inward and outward) to the input surface. default is 0.
        mode:        (optional) Volumetric element sizing mode. 0 = uniform, 1 = based on surface element size. Default is 1.
        prsv_bdry:   (optional) 0 = remesh surface, 1 = preserve surface. Default is 1.
        ifmt:        (optional) mesh input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, off, gmsh, stellar, vcflow)
        ofmt         (optional) mesh output format. (carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, neu, obj, off, stellar, vcflow, ensight_txt)
        silent:      (optional) set True to silence terminal messages
    """

    # check if output directory exists
    if outmsh.find(',') >= 0:
        outdir = os.path.dirname(outmsh.split(',')[-1])
    else:
        outdir = os.path.dirname(outmsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    ## TODO: this error check may be removed sometime soon
    ##       just put it here to more quickly account for meshtool changes
    #if isinstance(op, str) and ',' in surf:
    #    if not ';' in op:
    #        os.error('Individual operations NOW need to be separated by ";"! You may need to update meshtool.')
    ## -------------------------------------------------------------------------

    cmd = ['generate', 'mesh']
    if isinstance(surf, (list, tuple)):
        # surf needs to be a string
        surf   = ';'.join(str(x) for x in surf)
        cmd += ['-surf={}'.format(surf)]
    else:
        cmd += ['-surf={}'.format(surf)]
    cmd += ['-outmsh={}'.format(outmsh)]
    if ins_tag is not None:
        # TODO: needs to be tested
        if isinstance(ins_tag, (list, tuple)):
            ins_tag = ';'.join((x) for x in ins_tag)
            cmd += ['-ins_tag={}'.format(ins_tag)]
        else:
            cmd += ['-ins_tag={}'.format(ins_tag)]
    if holes is not None:
        # TODO: needs to be tested
        if isinstance(holes, (list, tuple)):
            holes = '/'.join((x) for x in holes)
            cmd  += ['-holes={}'.format(holes)]
        else:
            cmd += ['-holes={}'.format(holes)]
    if scale is not None:
        # TODO: needs to be tested
        cmd += ['-scale={}'.format(scale)]
    if bdry_layers is not None:
        cmd += ['-bdry_layers={}'.format(int(bdry_layers))]
    if bdry_step is not None:
            cmd += ['-bdry_step={}'.format(float(bdry_step))]
    if bdry_inc is not None:
        cmd += ['-bdry_inc={}'.format(float(bdry_inc))]
    if bdry_symm is not None:
        cmd += ['-bdry_symm={}'.format(int(bdry_symm))]
    if mode is not None:
        cmd += ['-mode={}'.format(mode)]
    if prsv_bdry is not None:
        cmd += ['-prsv_bdry={}'.format(int(prsv_bdry))]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    print('Done')
    return


def query(job, cmode, msh, tags=None, op=None, ifmt=None, ofmt=None, edge=None, mode=None, coord=None,
          uvc=None, thr=None, surf=None, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'query' function.

    Examples:
        # query bbox: print the bounding box of a given mesh
        meshtool.query(job, 'bbox', msh, ifmt=)
    Args:
        job:    job.job.Job object
        cmode:  choose 'bbox'
        msh:    (input) path to basename of the mesh to query
        ifmt:   (optional) mesh input format.
    Kwargs:
        silence:    (optional) set True to silence terminal messages
    Note:
        The supported input formats -- carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow


    Examples:
        # query edges: print statistics of the node-to-node graph
        meshtool.query(job, 'edges', msh, ifmt=, tags=, odat=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'edges'
        msh:    (input) path to basename of the mesh to query
    Kwargs:
        ifmt:   (optional) mesh input format
        tags:   tag1,tag2 (optional) list of region tags to compute the edge statistics on.
        silent: (optional) set True to silence terminal messages


    Examples:
        # query graph: print the nodal connectivity graph
        meshtool.query(job, 'graph', msh, ifmt=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'graph'
        msh:    (input) path to basename of the mesh to query
    Kwargs:
        ifmt:   (optional) mesh input format.
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats -- carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow


    Examples:
        # query idx: print indices in a proximity to a given coordinate
        meshtool.query(job, 'idx', msh, coord, ifmt=, surf=, thr=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'idx'
        msh:    (input) path to basename of the mesh to query
        coord:  (input) triple of x,y,z coordinates
    Kwargs:
        ifmt:   (optional) mesh input format
        surf:   (optional) index query is restricted to this surface
        thr:    (optional input) threshold defining the proximity to the coordinate
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats ... carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow


    Examples:
        # query idxlist: generate an index list file from a given coord list file
        meshtool.query(job, 'idxlist', msh, coord, ifmt=, surf=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'idxlist'
        msh:    (input) path to basename of the mesh to query
        coord:  (input) the coordinates file
    Kwargs:
        ifmt:   (optional) mesh input format
        surf:   (optional) index query is restricted to this surface
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats ...carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow

    query idxlist_uvc: apply 'query idx' for all coordinates in a file based on uvc coordinates
    parameters:
        -msh=<path>	    ... (input) path to basename of the uvc points file to perform query on (must be .uvc_pts)
        -coord=<file>	... (input) the uvc coordinates file holding the query points in uvc
        -uvc=<file>     ... (optional input) path to the ucv points file (default `basename`.uvc_pts)

    query coords_xyz: get the XYZ coordinates for all UVC coordinates provided in a file
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to perform query on
        -coord=<file>	... (input) the uvc coordinates file holding the query points in uvc
        -uvc=<file>	    ... (optional input) path to the ucv points file (default `basename`.uvc_pts)

    Examples:
        # query quality: print mesh quality statistics
        meshtool.query(job, 'quality', msh, ifmt=, thr=, mode=0, odat=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'quality'
        msh:    (input) path to basename of the mesh to query
    Kwargs:
        ifmt:   (optional) mesh input format
        thr:    (optional) output exact number of elements with quality above this threshold
        mode:   (optional) 0 = query only quality, 1 = query also self-intersection. Default is 0.
        odat:   (optional) filename to store query data onto disk
    Note:
        The supported input formats ... carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow


    Examples:
        # query tags: print the tags present in a given mesh
        meshtool.query(job, 'tags', msh, ifmt=, silent=False)
    Args:
        job:    job.job.Job object
        cmode:  choose 'tags'
        msh:    (input) path to basename of the mesh to query
    Kwargs:
        ifmt:   (optional) mesh input format
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats ... carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow


    query smoothness: compute the nodal smoothness
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    outdir = None
    MODES  = ('bbox', 'edges', 'graph', 'idx', 'idxlist', 'idxlist_uvc',
              'coords_xyz', 'quality', 'tags', 'smoothness')
    assert cmode in MODES, 'meshtool query detected incorrect mode: {}'.format(cmode)

    cmd =  ['query', cmode]
    cmd += ['-msh={}'.format(msh)]

    # -------------------------------------------------------------------------
    if 'bbox' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
    # -------------------------------------------------------------------------
    elif 'edges' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if tags is not None:
            cmd += ['-tags={}'.format(tags)]
        cmd += ['>', msh + '.mtquery']
    # -------------------------------------------------------------------------
    elif 'graph' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
    #  ------------------------------------------------------------------------
    elif 'idxlist_uvc' in cmode:
        cmd += ['-coord={}'.format(coord)]
        if uvc is not None:
            cmd += ['-uvc={}'.format(uvc)]
    #  ------------------------------------------------------------------------
    elif 'coords_xyz' in cmode:
        cmd += ['-coord={}'.format(coord)]
        if uvc is not None:
            cmd += ['-uvc={}'.format(uvc)]
    #  ------------------------------------------------------------------------
    elif 'idxlist' in cmode:
        cmd += ['-coord={}'.format(coord)]
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if surf is not None:
            cmd += ['-surf={}'.format(surf)]
    elif 'idx' in cmode:
        cmd += ['-coord={}'.format(coord)]
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if surf is not None:
            cmd += ['-surf={}'.format(surf)]
        if thr is not None:
            cmd += ['-thr={}'.format(thr)]
        assert False, 'bbox mode not yet implemented'
    # -------------------------------------------------------------------------
    elif 'quality' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if thr is not None:
            cmd += ['-thr={}'.format(thr)]
    # -------------------------------------------------------------------------
    elif 'tags' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
    # -------------------------------------------------------------------------
    elif 'smoothness' in cmode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if surf is not None:
            cmd += ['-surf={}'.format(surf)]
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(cmode)

    print(' '.join(cmd))
    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return


def smooth(job, mode, msh, outmsh=None, surf=None, thr=None, lvl=None, iter=None, smth=None,
           edge=None, ifmt=None, ofmt=None, tags=None, idat=None, nodal=None, odat=None, norm=None,
           silent=False, stdout=None, stderr=None):

    """smooth surface: Smooth one or multiple surfaces of a mesh.
    parameters:
        -msh=<path>		        ... (input) path to basename of the mesh
        -surf=<path1>,<path2>	... (input) list of surfaces to smooth.
        -thr=<float>	        ... (optional) Maximum allowed element quality metric. default is 0.9. Set to 0 to disable quality checking.
        -lvl=<int>		        ... (optional) Number of volumetric element layers to to use when smoothing surfaces. Default is 2.
        -iter=<int>		        ... (optional) Number of smoothing iter (default 100).
        -smth=<float>	        ... (optional) Smoothing coefficient (default 0.15).
        -edge=<float>	        ... (optional) Normal-vector angle difference (in degrees, default 0) defining a sharp edge.
                                    If set to 0, edge detection is turned off.
        -ifmt=<format>		    ... (optional) mesh input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar)
        -ofmt=<format>		    ... (optional) mesh output format. (carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar)
        -outmsh=<path>		    ... (output) path to basename of the output mesh

    smooth mesh: Smooth a mesh (surfaces and volume).
    parameters:
        -msh=<path>		        ... (input) path to basename of the mesh
        -tags=ta1,ta2/tb1,tb2	... (input) List of tag sets. The tags in one set have a common surface.
                                    Surfaces between different tag sets will be smoothed. Use * to include all tags, one
                                    tag per set. Use + to include all tags into one set.
        -thr=<float>		    ... (optional) Maximum allowed element quality. default is 0.9. Set to 0 to disable quality checking.
        -iter=<int>	    	    ... (optional) Number of smoothing iter (default 100).
        -smth=<float>		    ... (optional) Smoothing coefficient (default 0.15).
        -edge=<float>		    ... (optional) Normal-vector angle difference (in degrees, default 0) defining a sharp edge.
                                    If set to 0, edge detection is turned off.
        -ifmt=<format>		    ... (optional) mesh input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar)
        -ofmt=<format>		    ... (optional) mesh output format. (carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar)
        -outmsh=<path>		    ... (output) path to basename of the output mesh

    smooth data: Smooth data.
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh
        -idat=<path>	... (input) path to the input data file
        -iter=<int>	    ... (optional) Number of smoothing iter (default 100).
        -smth=<float>	... (optional) Smoothing coefficient (default 0.15).
        -nodal=<0|1>	... (optional) Set data representation: 0 = element data, 1 = nodal data. (default is 1).
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -odat=<path>	... (output) path to the output data file
    """

    cmd =  ['smooth', mode]
    cmd += ['-msh={}'.format(msh)]

    # mode specific options
    if 'surface' in mode:
        cmd += ['-surf={}'.format(surf)]
    elif 'mesh' in mode:
        cmd += ['-tags={}'.format(tags)]
    elif 'data' in mode:
        cmd += ['-idat={}'.format(idat)]
    else:
        assert False, 'Unknown mode: {}'.format(mode)

    # surface and mesh mode options
    if mode in ['surface', 'mesh']:
        if thr is not None:
            cmd += ['-thr={}'.format(thr)]
        if edge is not None:
            cmd += ['-edge={}'.format(edge)]
        if ofmt is not None:
            cmd += ['-ofmt={}'.format(ofmt)]
        cmd += ['-outmsh={}'.format(outmsh)]
        outdir = os.path.dirname(outmsh)
    else:
        if nodal is not None:
            cmd += ['-nodal={}'.format(nodal)]
        if norm is not None:
            cmd += ['-norm={}'.format(norm)]
        cmd += ['-odat={}'.format(odat)]
        outdir = os.path.dirname(odat)

    # -------------------------------------------------------------------------
        print ('Warning: smooth data not yet tested!')
    # -------------------------------------------------------------------------

    # general commands
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if iter is not None:
        cmd += ['-iter={}'.format(iter)]
    if smth is not None:
        cmd += ['-smth={}'.format(smth)]

    # check if output directory exists
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return


def resample(job, mode, msh, outmsh, min=None, max=None, avrg=None, surf_corr=None, fix_bnd=None, postsmth=None, uniform=None, ifmt=None, ofmt=None,
             tags=None, silent=False, stdout=None, stderr=None):
    """ MESHTOOL
    resample volumetric or surface mesh: refine or coarsen a surface triangle mesh to fit a given edge size range
    parameters:
        -msh=<path>		    ... (input) path to basename of the input mesh
        -outmsh=<path>		... (output) path to basename of the output mesh
        -min=<float>		... (input) min edge size
        -max=<float>		... (input) max edge size
        -avrg=<float>		... (optional) average edge size. (min, max) are derived from it.
        -surf_corr=<float>	... (optional) surface correlation parameter (default 0.85).
		                	    Edges connecting surface nodes with surface normals correlating greater
			                    than the specified amount can be collapsed.
        -fix_bnd=<int>		... (optional) Fix boundary of non-closed surfaces. 0 = no, 1 = yes. Default is 1.
        -postsmth=<int>  	... (optional) Apply conservative smoothing between resampling iterations. 0 = no, 1 = yes. Default is 1.
        -uniform=<int>		... (optional) Edge-refinement is applied uniformly on selected tags.
                			    0 = no, 1 = yes. Default is 0.
        -ifmt=<format>		... (optional) format of the input mesh
        -ofmt=<format>		... (optional) format of the output mesh
        -tags=<tag lists>	... (optional) element tag lists specifying the regions
		                	    to perform refinement on

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        The supported output formats are:
        carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar

        The tag lists have the following syntax: multiple lists are seperated by a "/",
        while the tags in one list are seperated by a "," character. The surface smoothness
        of the submesh formed by one tag list will be preserved.
    """

    # check if output directory exists
    outdir = os.path.dirname(outmsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None
    if mode in ['surface', 'surfmesh']:
        cmd =  ['resample', 'surfmesh']
    elif mode in ['mesh', 'volume']:
        cmd =  ['resample', 'mesh']
    else:
        assert False, 'Unknown mode: {}'.format(mode)

    cmd += [f'-msh={msh}']
    cmd += [f'-outmsh={outmsh}']

    # -------------------------------------------------------------------------
    if avrg is not None:
        cmd += ['-avrg={}'.format(avrg)]
    if min is not None:
        cmd += ['-min={}'.format(min)]
    if max is not None:
        cmd += ['-max={}'.format(max)]
    # -------------------------------------------------------------------------
    if surf_corr is not None:
        cmd+= ['-surf_corr={}'.format(surf_corr)]
    if fix_bnd is not None:
        cmd += ['-fix_bnd={}'.format(fix_bnd)]
    if postsmth is not None:
        cmd += ['-postsmth={}'.format(postsmth)]
    if uniform is not None:
        cmd += ['-uniform={}'.format(uniform)]
    if tags is not None:
        cmd += ['-tags={}'.format(tags)]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]
    # -------------------------------------------------------------------------

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return


def map(job, submsh, files, outdir, mode='m2s', info=None, mapName=None, silent=False, stdout=None, stderr=None):
        """
        Python wrapper for MeshTool's 'map' function.

        map .vtx, .surf and .neubc files to the indexing of a submesh (obtained from 'meshtool extract mesh')

        Args:
            job:        job.job.Job object
            submsh:     (input) path to basename of the submesh
            files:      (input) files to map to submesh. Can include patterns using *
            outdir:     (output) directory to place mapped files
        Kwargs:
            mode:       (optional) Map mesh-to-submesh (m2s) <br>
                        or vice-versa (s2m). Default is: m2s
            info:       (optional) path to the information file
            mapName     (optional) change the basename of the files mapped <br>(Default: basename of the submsh)

        Note:
            'patterns' is a comma separated list of files or glob patterns, <br>
            e.g. mesh/endo.vtx,mesh/endo.surf,mesh/*.neubc <br>
            Any mapped files that would overwrite their source file are skipped.
        """
        assert mode in ('m2s', 's2m')

        mcmd = ['map',
                '-submsh=' + submsh,
                '-files=' + files,
                '-mode=' + mode,
                '-outdir=' + outdir]

        if info is not None:
            mcmd += ['-info=' + info]

        execute(job, mcmd, silent=silent, stdout=stdout, stderr=stderr)

        if mapName is not None:
            filesbasename = os.path.basename(files)
            for char2strp in ['*', '.vtx', '.surf']:
                if char2strp in files:
                    filesbasename = filesbasename.replace(char2strp,'')

            for item in os.listdir(outdir):
                if filesbasename + '.' in item:
                    ifile = os.path.join(outdir, item)
                    ofile = ifile.replace(filesbasename, mapName)
                    job.mv(ifile, ofile)
        return


def clean_quality(job, imsh, thr=0.98, surfs=None, smth=0, iter=100, edge=0, sigma=0.05,
                  ifmt=None, ofmt=None, outmsh=None, silent=False, stdout=None, stderr=None):
    """
    Python wrapper for MeshTool's 'clean quality'

    Deform mesh elements to reach a certain quality threshold value. Provided surfaces will be preserved.

    Args:
        job:    job.job.Job object
        imsh:   (input) path to basename of the mesh
        thr:    (input) threshold for mesh quality
    Kwargs:
        surf:   (optional) comma separated list of surfaces (without extension) which must be preserved
        smth:	(optional) Smoothing coefficient (default 0.00). If set to 0, mesh smoothing is turned off.
        iter:   (optional) Number of smoothing iterations (default 100).
        edge:   (optional) Edge detection threshold angle in degrees (default 0.00). If set to 0, edge detection is turned off.
        sigma:	(optional) Relative vertex translation step size (default 0.05).
        ifmt:   (optional) mesh output format.
        ofmt:	(optional) mesh output format.
        outmsh: (optional) output meshname. If not specified, overwrites imsh.
        silent: (optional) set True to silence terminal messages
    Note:
        The supported input formats are --  carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, obj, stellar, vcflow<br>
        The supported output formats are -- carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, neu, obj, stellar,
        <br> vcflow, ensight_txt
    """

    # check if output directory exists
    outdir = os.path.dirname(outmsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd  = ['clean', 'quality']
    cmd += ['-msh={}'.format(imsh)]
    cmd += ['-thr={}'.format(thr)]

    # -------------------------------------------------------------------------
    if surfs is not None:
        cmd += ['-surf={}'.format(surfs)]
    if smth is not None:
        cmd+= ['-smth={}'.format(smth)]
    if iter is not None:
        cmd += ['-iter={}'.format(iter)]
    if edge is not None:
        cmd += ['-edge={}'.format(edge)]
    if sigma is not None:
        cmd += ['-sigma={}'.format(sigma)]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]
    if outmsh is not None:
        cmd += ['-outmsh={}'.format(outmsh)]
    # -------------------------------------------------------------------------

    execute(job, cmd, outdir=outdir, silent=silent, stdout=stdout, stderr=stderr)
    return
