#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Provides a class for .dat and .vec IO to/from python numpy arrays.
"""
import os
import numpy as np
from carputils.carpio.filelike import FileLikeMixin
from carputils.carpio import igb
from carputils import settings

# Save original file open
fopen = open


class TxtFile(FileLikeMixin):
    """
    file IO class for reading ascii files with extensions
    - dat(.gz)
    - elem
    - vec(.gz)
    - vtx
    - pts

    Direct read/write to gzipped files is possible. The class automatically
    determines if the file is gzipped by its extension.

    This class is little more than an elaborate wrapper for the numpy loadtxt
    and savetxt functions, but is included for completeness in the
    :mod:`carputils.carpio` module.

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode
    """

    def __init__(self, filename, mode='r'):
        
        self._filename = filename
        self._mode     = mode

        # assert that file exists

    def count(self):

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return range(2)
        elif self.check_ext('.vtx'):
            return range(2)
        elif self.check_ext('.pts'):
            return range(2)
        elif self.check_ext('.elem'):
            return range(3)
        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__read_vec_file()


    def close(self):
        """
        Close the file object.
        """
        pass

    def data(self):
        """
        Return a numpy array of the file contents.

        Returns:
            numpy.ndarray
                A numpy array with the file contents
        """

        assert self._mode == 'r'

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return self.__read_dat_file()
        elif self.check_ext('.vtx'):
            return self.__read_vtx_file()
        elif self.check_ext('.pts'):
            return self.__read_pts_file()
        elif self.check_ext('.elem'):
            return self.__read_elem_file()
        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__read_vec_file()
        elif self.check_ext('.lon'):
            return self.__read_lon_file()
        elif self.check_ext('.vtk'):
            return self.__read_vtk_file()
        else:
            raise Exception('Not yet implemented reader for {}'.format(self._filename))


    def __read_dat_file(self, **kwargs):
        """
        Read dat from .dat file

        Returns:
            1D numpy array
        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        if self.check_ext('.gz'):
            # loadtxt automatically determines compression from file extension, so
            # no need to handle this ourselves
            data = np.loadtxt(self._filename)
        else:
            with fopen(self._filename, self._mode) as fp:
                data = fp.read()
                data = np.fromstring(data, dtype=float, sep='\n')

        return data


    def __write_dat_file(self, data, **kwargs):
        """
        Write data into .dat file

        Returns:
        """

        assert self._filename.endswith('.dat')
        assert self._mode == 'w'

        with fopen(self._filename, self._mode) as fp:
            # export data
            data_str = np.asarray(data, dtype=str)
            data_str = '\n'.join(data_str)
            fp.write(data_str)
            fp.write('\n')  # meshalyzer has an issue with missing CR at end of file
            fp.flush()
        return


    def __read_vtx_file(self):
        """
        Read data from .vtx file

        Returns:
            data ... 1D numpy array of
            domain ... 'intra' or 'extra

        """
        assert self._filename.endswith('.vtx')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            num = int(fp.readline().strip())
            domain = fp.readline().strip()
            data = fp.readlines()
            data = np.asarray(data, dtype=int)

        return data, domain


    def __write_vtx_file(self, data, **kwargs):
        """
        Write vtx data fo file

        Returns:
            data ... 1D numpy array of
            domain ... 'intra' or 'extra

        """
        assert self._filename.endswith('.vtx')
        domain = kwargs['domain'] if 'domain' in kwargs.keys() else 'extra'

        if not isinstance(data, np.ndarray):
            # input may not always be an numpy array
            data = np.asarray(data, dtype=int)
        else:
            data = data.astype(int)

        with fopen(self._filename, self._mode) as fp:
            # export header
            fp.write('{}\n'.format(len(data)))
            fp.write('{}\n'.format(domain))
            # export data
            data_str = np.asarray(data, dtype=str)
            data_str = '\n'.join(data_str)
            fp.write(data_str)
            fp.flush()
        return


    def __read_pts_file(self):
        """
        Read data from .pts file

        Returns:
            pts ... 2D numpy array of node coordinates
            numpts ... number of points

        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        assert self._filename.endswith('.pts')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            npts = int(fp.readline().strip())
            data = fp.read().replace('\n',' ')
            data = np.fromstring(data, dtype=float, sep=' ')
            data = np.reshape(data, (npts,3))
        return data, npts


    def __write_pts_file(self, data, **kwargs):
        """
        Write data into .pts file

        Returns:

        """

        assert self._filename.endswith('.pts')
        assert self._mode == 'w'
        np.set_printoptions(suppress=True)
        np.set_printoptions(threshold=np.prod(data.shape)) # avoid truncation of strings

        with fopen(self._filename, self._mode) as fp:

            data_str = np.array2string(data)
            data_str = data_str.replace('[','').replace(']','')
            data_str = data_str.replace("'",'')
            # enforce single blanks between numbers
            data_str = data_str.replace("\n",'n')
            data_str = ' '.join(data_str.split())
            data_str = data_str.replace("n ", chr(10))
            # add header
            data_str = str(data.shape[0]) + chr(10) + data_str + chr(10)
            # export entire string at once
            fp.write(data_str)
            fp.flush()
        return


    def __write_auxgrid_files(self, datafile, **kwargs):
        """
        Write data into .pts_t, dat_t and .elem_t files

        This function produces the same output as the
        '-dump_ecg_leads' flag in carpentry which is not
        present in openCARP!

        Returns:
        """

        assert self._filename.endswith('.pts_t')
        assert self._mode == 'w'

        # read data from given igb file
        data, header, _ = igb.read(datafile)

        # read pts associated with the igb file
        ptsfile = kwargs['ptsf'] if 'ptsf' in kwargs.keys() else None
        assert ptsfile is not None
        pts, npts = read(ptsfile)

        # one more check, if the pts file and the igb file belong to each other
        assert header['x'] == npts, 'Can not create auxilliary files from given files: {}, {}'.format(ptsfile, datafile)

        np.set_printoptions(suppress=True)
        np.set_printoptions(threshold=np.prod(data.shape)) # avoid truncation of strings

        # write pts_t file
        with fopen(self._filename, self._mode) as fp:

            data_str = np.array2string(pts)
            data_str = data_str.replace('[','').replace(']','')
            data_str = data_str.replace("'",'')
            # enforce single blanks between numbers
            data_str = data_str.replace("\n",'n')
            data_str = ' '.join(data_str.split())
            data_str = data_str.replace("n ", chr(10))
            # add header - number of nodes
            data_str = str(data.shape[0]) + chr(10) + data_str
            # add header - number of node sets (there could be another point sets)
            data_str = str(1) + chr(10) + data_str
            # export entire string at once
            fp.write(data_str)
            fp.flush()

        # write a dummy elem_t file
        with fopen(self._filename.replace('pts_t', 'elem_t'), self._mode) as fp:
            # add header - number of elements
            data_str = str(0) + chr(10)
            # add header - number of element sets (there could be another element sets)
            data_str = str(1) + chr(10) + data_str
            # export entire string at once
            fp.write(data_str)
            fp.flush()

        # write a dat_t file
        with fopen(self._filename.replace('pts_t', 'dat_t'), self._mode) as fp:
            # export data
            # add header: number of time steps
            data_str = str(header['t'])

            for i in range(header['t']):
                # add header: number of data points (one per node) at time step i
                data_str = data_str + chr(10) + str(npts) + chr(10)
                # add the data
                tmp_str = np.asarray(data[:,i], dtype=str)
                data_str = data_str + '\n'.join(tmp_str)
            # export entire string at once
            fp.write(data_str)
            fp.flush()
        return

    def __write_pts_file_(self, data, **kwargs):
        """
        Write data into .pts file

        Returns:

        """
        assert self._filename.endswith('.pts')
        assert self._mode == 'w'
        npoints = data.shape[0]

        # ready for export
        if self._mode == 'w':
            header = str(npoints)
        else:
            header = ''

        np.savetxt(self._filename, data, fmt='%f %f %f',    comments='', header=header)
        return

    def __read_lon_file(self):
        """
        Read data from .lon file

        Returns:
            fibers ... 2D numpy array of vectors
            sheets ... 2D numpy array of vectors

        """
        np.set_printoptions(suppress=True)  # suppress exponential notation

        assert self._filename.endswith('.lon')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            ncols = int(fp.readline().strip())
            data = fp.read().replace('\n', ' ')
            data = np.fromstring(data, dtype=float, sep=' ')
            nrows = int(len(data)/3/ncols)
            data = np.reshape(data, (nrows, 3*ncols))

        if ncols == 1:
            fibers = data
            sheets = []
        else:
            fibers = data[:,1:3]
            sheets = data[:,4:6]

        return fibers, sheets

    def __write_lon_file_(self, data, **kwargs):
        """
        Write data into .lon file

        Returns:

        """
        assert self._filename.endswith('.lon')
        assert self._mode == 'w'
        #nfibers = data.shape[0]
        if data.shape[1] == 3:
            header = str(1)
            np.savetxt(self._filename, data, fmt='%f %f %f', comments='',
                       header=header)
        else:
            header = str(2)
            np.savetxt(self._filename, data, fmt='%f %f %f %f %f %f', comments='',
                       header=header)




    def __read_elem_file(self):
        """
        Read data from .elem file
        Only tets are properly supported by now
        It should be easily extendable to elements of just one kind

        Returns:
            elem ... 2D numpy array of element indices
            tags ... 1D numpy array with element labels
            numelems ... number of elements in file

        """
        assert self._filename.endswith('.elem')
        assert self._mode == 'r'
        tt_found = False # found tetrahedron?
        qd_found = False # found quads?

        with fopen(self._filename, self._mode) as fp:
            nelems = int(fp.readline().strip())
            data = fp.read()
            if 'Tt' in data:
                tt_found = True
                data  = data.replace('Tt', '')
                ncols = 5
            elif 'Hx' in data:
                hx_found = True
                data  = data.replace('Hx', '')
                ncols = 9
            elif 'Qd' in data:
                qd_found = True
                data  = data.replace('Qd', '')
                ncols = 5
            elif 'Tr' in data:
                tr_found = True
                data = data.replace('Tr', '')
                ncols = 4
            else:
                assert 0, 'txt.read current works just for UNIFORM hexahedral, tetrahedral, quadrilateral and triangular meshes!'

            data = data.replace('\n',' ')
            data = np.fromstring(data, dtype=int, sep=' ')
            data = np.reshape(data, (nelems,ncols))

            # assuming tags are available
            tags = np.copy(data[:,-1])

            data = np.delete(data, [data.shape[1]-1], axis=1)
        return data, tags, nelems


    def __write_elem_file(self, elems, **kwargs):
        """
        Write element array (indices into pts file)
        into .elem file

        Note: Currently just implemented for tetrahedral element export

        Returns:
        """
        assert self._filename.endswith('.elem')
        assert self._mode == 'w'

        etags = kwargs['etags'] if 'etags' in kwargs.keys() else None
        if etags is None:
            print('Assuming the element file is saved without labels!')
            elems_mod = elems
        else:
            assert elems.shape[0] == len(etags), 'Dimension of element list and label information does not match!'
            elems_mod = np.column_stack((elems, etags))

        # read for export
        if elems.shape[1] == 4:
            np.savetxt(self._filename, elems_mod, fmt='Tt %u %u %u %u %i', comments='', header=str(len(etags)))
        elif elems.shape[1] == 3:
            np.savetxt(self._filename, elems_mod, fmt='Tr %u %u %u %i', comments='', header=str(len(etags)))
        else:
            raise NotImplementedError('Element type not currently supported!')

        return


    def __write_surf_file(self, surfs, **kwargs):
        """
        Write element array (indices into pts file)
        into .surf file

        Note: Currently just implemented for triangular element export

        Returns:
        """
        isTris  = False
        isQuads = False
        nsurfs = surfs.shape[0]
        if surfs.shape[1] == 3:
            isTris  = True
        elif surfs.shape[1] == 4:
            isQuads = True
        else:
            os.error('Export of only triangles and quads is supported!')

        assert self._filename.endswith('.surf')
        assert self._mode == 'w' or self._mode == 'a'

        etags = kwargs['etags'] if 'etags' in kwargs.keys() else None

        if etags is None:
            print('Assuming the element file is saved without labels!')
        else:
            assert nsurfs == len(etags), 'Dimension of element list and label information does not match!'
            elems_mod = np.column_stack((surfs, etags))

        # ready for export
        if self._mode == 'w':
            header = str(nsurfs)
        else:
            header = ''

        if isTris:
            np.savetxt(self._filename, surfs, fmt='Tr %u %u %u',    comments='', header=header)
        else:
            np.savetxt(self._filename, surfs, fmt='Qd %u %u %u %u', comments='', header=header)

        return

    def __read_vtk_file(self):
        """
        Read data from an ASCII vtk polydata file
        Only tris are properly supported by now

        Returns:
            CELLS ... 2D numpy array of element indices
            POLYGONS ... 2D numpy array of surface indices
            POINTS ... 2D numpy array with nodes
            CELL_DATA ... numpy array number with cell data (element tags)
            POINTS_DATA ... numpy array with data on nodes
        """
        assert self._filename.endswith('.vtk')
        assert self._mode == 'r'
        np.set_printoptions(suppress=True)  # avoid scientific notation

        FILETYPE = None
        DATASET = None
        NUMPOINTS = None
        NUMELEMS = None
        POINTS = ''
        POINTS_DATA = ''
        CELLS = ''
        LUT = ''
        POLYGONS = ''
        CELL_DATA = ''    # should be the element tags

        TOKENS = ['POINTS', 'POINT_DATA', 'POLYGONS', 'LOOKUP_TABLE']

        with fopen(self._filename, self._mode) as fp:

            while FILETYPE is None:
                line = (fp.readline().strip()).lower()
                if line == 'binary':
                    print('Reading binary vtk files is not supported with this script! Aborting...')
                    return
                elif line == 'ascii':
                    FILETYPE = 'ascii'

            DATASET = fp.readline().strip()


            # ---READING POINTS INFORMATION------------------------------------
            while NUMPOINTS is None:
                line = fp.readline().strip()
                if line.startswith('POINTS'):
                    _, NUMPOINTS, POINTS_DTYPE = line.split(' ')
                    NUMPOINTS = int(NUMPOINTS)

            # read the points
            # NOTE: there is no guarantee that a line contains three floats
            print('  ... reading POINTS')
            COUNTER = 0
            while COUNTER < NUMPOINTS*3:
                line = fp.readline().strip()
                #currentToken = line.split(' ')[0]
                #if currentToken in TOKENS:
                #    # finished reading POINTS
                #    break
                POINTS='{} {}'.format(POINTS, line).lstrip()
                COUNTER = POINTS.count(' ') + 1

            POINTS = np.fromstring(POINTS, dtype=float, sep=' ')
            POINTS = np.reshape(POINTS, (NUMPOINTS,3))


            # ---READING CELLS-------------------------------------------------
            # This will need some flexibility in the future, as there could be CELLS, POLYGONS or maybe others

            EOF = False
            while not EOF:
                line = (fp.readline()).strip()
                if not line:
                    # blank line
                    continue
                currentToken = line.split(' ')[0]
                if currentToken in TOKENS:

                    # ---READING POLYGONS--------------------------------------
                    if currentToken == 'POLYGONS':
                        print('  ... reading POLYGONS')
                        NUMPOLYGONS = int(line.split(' ')[1])
                        for lineCount in range(0,NUMPOLYGONS):
                            line = fp.readline().strip()
                            POLYGONS = '{} {}'.format(POLYGONS, line).lstrip()

                        POLYGONS = np.fromstring(POLYGONS, dtype=int, sep=' ')
                        NCOLS = int(len(POLYGONS)/NUMPOLYGONS)
                        POLYGONS = np.reshape(POLYGONS, (NUMPOLYGONS, NCOLS))
                        POLYGONS = np.delete(POLYGONS, 0, 1)
                        continue

                    # ---READING POINT DATA------------------------------------
                    if currentToken == 'POINT_DATA':
                        print('  ... reading POINT_DATA')
                        _, NUMPOINTS = line.split(' ')
                        NUMPOINTS = int(NUMPOINTS)
                        COUNTER = 0
                        line = fp.readline()  # SCALARS ...
                        line = fp.readline()  # LOOKUP_TABLE ...
                        while COUNTER < NUMPOINTS:
                            line = fp.readline().strip()
                            POINTS_DATA = '{} {}'.format(POINTS_DATA, line).lstrip()
                            COUNTER = POINTS_DATA.count(' ') + 1

                        POINTS_DATA = np.fromstring(POINTS_DATA, dtype=float, sep=' ')
                        continue

                    # ---READING LOOKUP_TABLE ???------------------------------
                    if currentToken == 'LOOKUP_TABLE':
                        print('  ... reading LOOKUP_TABLE')
                        _, _, NUMROWS = line.split(' ')
                        NUMROWS = int(NUMROWS)
                        for lineCount in range(0, NUMROWS):
                            line = fp.readline().strip()
                            LUT = '{} {}'.format(LUT, line).lstrip()

                        # TODO: finishing here, there are more TOKENS to be imported
                        break
                else:
                    print('Do not know what to do with this TOKEN: {}'.format(currentToken))

                if line == '':
                    EOF = True

        return CELLS, POLYGONS, POINTS, CELL_DATA, POINTS_DATA

    def __read_vec_file(self):
        """
        Read data from .vec file

        Returns:
            vec ... 2D numpy array of node coordinates
            numpts ... number of points

        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        assert self._filename.endswith('.vec')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            data = fp.read().replace('\n',' ')
            data = np.fromstring(data, dtype=float, sep=' ')
            nvec = len(data)//3     # floor division
            data = np.reshape(data, (nvec,3))
        return data, nvec


    def __write_vec_file(self, data, **kwargs):
        """
        Write data into .vec file

        Returns:

        """
        assert self._filename.endswith('.vec')
        assert self._mode == 'w'
        np.set_printoptions(suppress=True) # avoid scientific notation
        np.set_printoptions(threshold=np.prod(data.shape)) # avoid truncation of strings

        with fopen(self._filename, self._mode) as fp:

            data_str = np.array2string(data)
            data_str = data_str.replace('[','').replace(']','')
            data_str = data_str.replace("'",'')
            # enforce single blanks between numbers
            data_str = data_str.replace("\n",'n')
            data_str = ' '.join(data_str.split())
            data_str = data_str.replace("n ", chr(10))
            fp.write(data_str)
            fp.flush()
        return


    def write(self, data, **kwargs):
        """
        Write a numpy array to a .dat or .vec file.

        Args:
            data : numpy.ndarray
                The array to be written to the IGB file
        """
        assert self._mode == 'w'

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return self.__write_dat_file(data)

        elif self.check_ext('.vtx'):
            return self.__write_vtx_file(data, **kwargs)

        elif self.check_ext('.pts'):
            #return self.__write_pts_file_(data, **kwargs)
            return self.__write_pts_file(data, **kwargs)

        elif self.check_ext('.pts_t'):
            return self.__write_auxgrid_files(data, **kwargs)

        elif self.check_ext('.lon'):
            return self.__write_lon_file_(data, **kwargs)

        elif self.check_ext('.elem'):
            return self.__write_elem_file(data, **kwargs)

        elif self.check_ext('.surf'):
            return self.__write_surf_file(data, **kwargs)

        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__write_vec_file(data)
        else:
            raise Exception('Not yet implemented writer for {}'.format(self._filename))

        return


    def check_ext(self, ext):
        return self._filename.endswith(ext)


def open(*args, **kwargs):
    """
    Open an .dat or .vec file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode
    """
    return TxtFile(*args, **kwargs)

def read(*args, **kwargs):
    """
    read data based on file extension

    Handled file types: .elem, .dat, .vec, .vtx, .pts

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode

    Returns:
        When reading an .elem file a tuple containing data, tags, numbe
        \b data    ... 1D or 2D array of integers or float <br>
        \b tags    ... in the case of an .elem file <br>
	    \b nelems  ... in the case of an .elem file <br>

	    \b tags    ... in the case of an .elem file <br>
        \b npts    ... in case of a .pts file <br>
        \b domain  ... in case of .vtx file
    """
    if not settings.cli.silent:
            print('Reading file: '+str(*args))

    if not settings.cli.dry:
        return TxtFile(*args, **kwargs).data()
    else:
        return TxtFile(*args,**kwargs).count()


def write(filename, data, **kwargs):

    if not settings.cli.silent:
            print('Writing file: '+filename)

    if not settings.cli.dry:
        mode = kwargs['mode'] if 'mode' in kwargs.keys() else 'w'

        TxtFile(filename, mode=mode).write(data, **kwargs)

    return
