#!/usr/bin/env python
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import math, os, sys

import numpy as np
import pandas as pd

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False

DEFAULT = ' - default: %(default)s'

def run(args,meshbase, lap_apba, lap_circ, lvtags, output=None, negate_circ=False, write_elems=False):

  print('BULLSEYE PLOT GENERATION')
  print('Only runs on Tetrahedral Meshes!')
  print('Mesh: '+str(meshbase))


  # read mesh elements and points
    
  if os.path.isfile(meshbase+'.elem'):
        elems = pd.read_csv(meshbase+'.elem', skiprows=1, delim_whitespace=True, usecols=(1, 2, 3, 4, 5), dtype=int, header=None).values.squeeze()
        pnts = pd.read_csv(meshbase+'.pts', skiprows=1, delim_whitespace=True, dtype=float, header=None).values.squeeze()
  else: 
        OSError('Could not load the mesh!')

  # read laplace solutions in apical-basal-direction and phi-direction
  apba = pd.read_csv(lap_apba, delim_whitespace=True, dtype=float, header=None).values.squeeze()
  circ = pd.read_csv(lap_circ, delim_whitespace=True, dtype=float, header=None).values.squeeze()
  if negate_circ:
    circ *= -1.0
  # normalize phi-directional laplace solution
  circmin, circmax = np.amin(circ), np.amax(circ)
  circ = (circ - circmin)/(circmax - circmin)
  # define bullseye sections and indices
  apex   = {(-1.000, 2.000): 17}
  apical = {(-1.000, 0.125): 16, (0.125, 0.375): 15, (0.375, 0.635): 14, (0.635, 0.875): 13, (0.875, 2.000): 16}
  midcav = {(-1.000, 0.166): 11, (0.166, 0.333): 10, (0.333, 0.500):  9, (0.500, 0.666):  8, (0.666, 0.833):  7, (0.833, 2.000): 12}
  basal  = {(-1.000, 0.166):  5, (0.166, 0.333):  4, (0.333, 0.500):  3, (0.500, 0.666):  2, (0.666, 0.833):  1, (0.833, 2.000):  6}  
  idxmap = {0:apex, 1: apex, 2: apical, 3: midcav, 4: basal}
  # retag the elements
  tagidx = []  
  for i, elem in enumerate(elems):
    if elem[4] in lvtags:
      apbaval = int(math.ceil(np.mean(np.take(apba, elem[0:4]))*4.0))
      circvals = np.take(circ, elem[0:4])
      circval = np.mean(circvals)
      if (np.amin(circvals) < 0.1) and (np.amax(circvals) > 0.9):
        circval += 0.5*(-1.0 if circval < 0.5 else 1.0)    
      sections = idxmap.get(apbaval, None)
      if sections is None:
        tagidx.append(-1)        
      else:
        if isPy2: it = sections.iteritems
        else:     it = sections.items

        for rng, idx in it():
          if rng[0] < circval <= rng[1]:
            tagidx.append(idx)
            break
        else:
          tagidx.append(-1)        
    else:
      tagidx.append(-1)
  tagidx = np.array(tagidx, dtype=int)
  # write new tags
  if output is not None:
    np.savetxt(output, tagidx, fmt='%d')
  # write new element data
  if write_elems:
    print('Writing new elem file: '+meshbase+'.retag.elem')
    with open(meshbase+'.retag.elem', 'w') as fp:
      fp.write('{}\n'.format(elems.shape[0]))
      for i, elem in enumerate(elems):
        fp.write('Tt {0[0]} {0[1]} {0[2]} {0[3]} {1}\n'.format(elem, tagidx[i]))

  return tagidx


  
