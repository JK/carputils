#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Defines several types of commands to execute.
"""

import os
from subprocess import CalledProcessError

from carputils import settings
from carputils import divertoutput
from carputils import debug
from carputils import format

from carputils.job import serialise as serial
from carputils.job.optionlist import OptionList

class Command(object):
    """
    Interface that commands should provide.
    """

    def execute(self):
        """
        Run this command right now.
        """
        raise NotImplementedError()

    def bash(self):
        """
        Generate a bash statement executing this command.
        """
        raise NotImplementedError()

class PythonCommand(Command):
    """
    A command defined by a self-contained Python function.

    Parameters
    ----------
    function : function
        The command to execute in bash
    """

    def __init__(self, function, message=None):

        # Sanity check
        assert callable(function)

        self._function = function
        self._msg = message
        self._skipcmd = False
        self._skipmsg = None

    def execute(self):
        """
        Run this command right now.
        """

        # Show command if not silent mode
        if not settings.cli.silent:
            if self._msg is not None:
                print(format.header(self._msg))

            print(self.bash() + "\n")

        # Skip command execution
        if settings.cli.dry:
            return

        self._function()

    def bash(self):
        """
        Generate a bash statement executing this command.
        """
        fname = serial.serialise_function(self._function)
        # start python's system default: may be python2 or python3
        return format.command_line(['python', serial.EXECSCRIPT, fname])

class ShellCommand(Command):
    """
    A command executable in the bash shell.

    Parameters
    ----------
    command : list of str
        The command to execute in bash
    message : str, optional
        A descriptive message to execute when running the command
    skiperror : bool, optional
        When True, do not crash when the command fails in execute()
    """

    def __init__(self, command, message=None, skiperror=False, stdout=None, stderr=None):

        # Sanity check
        assert isinstance(command, (list, OptionList))

        # Store command and executable
        self._cmd = command
        try:
            self._exe = command[0]
        except IndexError:
            raise Exception('command must have at least an executable')

        # Store other arguments
        self._msg = message
        self._skiperror = skiperror
        self._stdout = stdout
        self._stderr = stderr
        self._skipcmd = False
        self._skipmsg = None

        # Do not call external visualization tools in an DOCKER environment
        if int(os.environ.get('OPENCARP_DOCKER', 0)):
            if 'meshalyzer' in self._exe.lower() or 'limpetgui' in self._exe.lower():
                self._skipcmd = True
                self._skipmsg = 'Skipping command...\n' \
                                'External visualization tools requiring the X server are \n'\
                                'currently not supported in the docker environment! \n'\
                                'However, you may run the command OUTSIDE docker in your \n' \
                                'X system!'
        return

    def execute(self):
        """
        Run this command right now.
        """

        # Show command if not silent mode
        if not settings.cli.silent:
            if self._msg is not None:
                print(format.header(self._msg))

            print(self.bash() + "\n")

        # Skip command execution
        if settings.cli.dry:
            return

        # Run the command
        # Check for errors
        with divertoutput.subprocess_exceptions():
            try:
                # Run the command
                divertoutput.call(self._cmd, stdout=self._stdout, stderr=self._stderr)
            except CalledProcessError as err:
                if self._skiperror:
                    # Just show a warning
                    tpl = '{} failed with error code {}'
                    print(tpl.format(self._cmd[0], err.returncode))
                else:
                    # Actually raise the error
                    raise

    def bash(self):
        """
        Generate a bash statement executing this command.
        """
        # Nicer formatting of meshalyzer command
        optionals = not str(self._exe).endswith('meshalyzer')
        return format.command_line(self._cmd, optionals=optionals)

class MPICommand(ShellCommand):
    """
    A shell command to be executed with MPI.

    Executes the command line with MPI, automatically adding MPI and
    runtime options such as debuggers and profilers.

    Parameters
    ----------
    command : list of str
        The command to execute in bash
    message : str, optional
        A descriptive message to execute when running the command
    skiperror : bool, optional
        When True, do not crash when the command fails in execute()
    """

    def __init__(self, *args, **kwargs):

        # Store the same stuff as in ShellCommand
        ShellCommand.__init__(self, *args, **kwargs)

        # Build kwargs for add_launcher
        cuda = settings.makevars.PT_CUDA or settings.makevars.LIMPET_CUDA
        kwargs = {'gdb_procs':  settings.cli.gdb,
                  'ddd_procs':  settings.cli.ddd,
                  'lldb_procs': settings.cli.lldb,
                  'ddt':        settings.cli.ddt,
                  'map':        settings.cli.map,
                  'cuda':       cuda}
        if settings.cli.valgrind is not None:
            kwargs['valgrind'] = settings.cli.valgrind_options

        # Add the MPI launcher and debugging options
        if settings.cli.np_job > 0:  # if np_job is set
            nproc = settings.cli.np_job
        else:
            nproc = settings.cli.np

        self._cmd = settings.platform.add_launcher(self._cmd, nproc, settings.cli.np_job, **kwargs)

        # Store the number of threads per process
        self._tpp = settings.cli.tpp

        # append user defined options
        if settings.cli.CARP_opts:
            for opts in settings.cli.CARP_opts:
                self._cmd += opts.split()

        # Add scalasca profiler if requested
        if settings.cli.scalasca:
            self._cmd = ['scan'] + self._cmd

    def execute(self):
        """
        Run this command right now.
        """

        # Write out the formatted command line
        if self._msg is not None:
            print(format.header(self._msg))
        print(self.bash() + "\n")

        # Determine some runtime options
        if settings.cli.gdb is not None:
            if settings.cli.valgrind is not None:
                # gdb with valgrind - gdb instances are hooked on separately so
                # interactive mode not needed
                interactive = False
                gdb_valgrind = True
            else:
                # No valgrind - run in interactive mode to enable gdb input by
                # user
                interactive = True
                gdb_valgrind = False
        else:
            # No gdb
            interactive = False
            gdb_valgrind = False

        if settings.cli.lldb is not None:
            interactive = True

        # Determine timeout in number of seconds
        timeout = None
        if settings.cli.runtime is not None:
            # Get number of seconds
            time = settings.cli.runtime
            timeout = time[0] * 3600 + time[1] * 60 + time[2]

        if settings.cli.dry:
            return

        # Determine whether to divert stderr to file
        stderr = None
        if isinstance(settings.cli.valgrind, str):
            stderr = open(settings.cli.valgrind, 'w')

        kwargs = {}
        if self._tpp is not None:
            env = os.environ.copy()
            env['OMP_NUM_THREADS'] = self._tpp
            kwargs['env'] = env

        if gdb_valgrind:
            debug.run_valgrind_interactive(self._cmd, self._exe, stderr=stderr)
        else:
            divertoutput.call(self._cmd, interactive, timeout, stderr=stderr)

        if stderr is not None:
            stderr.close()

    def bash(self):
        """
        Generate a bash statement executing this command.
        """

        cmd = self._cmd

        if self._tpp is not None:
            cmd = ['OMP_NUM_THREADS={}'.format(self._tpp)] + cmd

        if isinstance(settings.cli.valgrind, str):
            # Pipe stderr to specified file
            cmd = cmd + ['2>', settings.cli.valgrind]

        return format.command_line(cmd)
