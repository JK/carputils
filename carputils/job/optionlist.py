#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

class OptionList(object):
    """
    Special type which behaves like list but stores some arguments to be kept at
    end.

    Parameters
    ----------
    entries : list
        The main entries in this option list
    end_entries : list, optional
        The special entries to be kept at the end
    """

    def __init__(self, entries, end_entries=None):
        self._mainopts = list(entries)
        self._endopts = [] if end_entries is None else end_entries

    def at_end(self, opts):
        self._endopts += opts

    def __str__(self):
        return str(self._mainopts + self._endopts)

    def __len__(self):
        return len(self._mainopts) + len(self._endopts)

    def __iter__(self):
        return iter(self._mainopts + self._endopts)

    def __getitem__(self, index):
        return (self._mainopts + self._endopts)[index]

    def __setitem__(self, index, value):
        self._mainopts[index] = value

    def __add__(self, other):
        if isinstance(other, OptionList):
            return OptionList(self._mainopts + other._mainopts,
                              self._endopts + other._endopts)
        return OptionList(self._mainopts + other, self._endopts)

    def __radd__(self, other):
        if isinstance(other, OptionList):
            return OptionList(other._mainopts + self._mainopts,
                              other._endopts + self._endopts)
        return OptionList(other + self._mainopts, self._endopts)

    def __iadd__(self, other):
        if isinstance(other, OptionList):
            self._mainopts += other._mainopts
            self._endopts += other._endopts
        else:
            self._mainopts += other
        return self
