#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Serialise and de-serialise and run Python functions.
"""

import marshal
import types

# Standard library
import os
import argparse
import re

EXECSCRIPT = __file__

def filename(directory, name):
    """
    Generate a filename for the serialised function.

    Parameters
    ----------
    directory : str
        Path of the directory to place the function in
    name : str
        Name of the function

    Returns
    -------
    str
        Path of a file that does not yet exist
    """

    # Generate basename (without suffix)
    base = os.path.join(directory, name)

    # See if it works without appending a number
    if not os.path.exists(base + '.pyfunc'):
        return base + '.pyfunc'

    # If not, try successive numerical extensions until something works
    i = 1
    while True:
        candidate = '{}-{}.pyfunc'.format(base, i)
        if not os.path.exists(candidate):
            return candidate
        i += 1

def serialise_function(function, directory='.'):
    """
    Serialise the provided function, placing it in the specified directory.

    Parameters
    ----------
    function : function
        The function to serialise
    directory : str, optional
        The directory to place the serialised function in

    Returns
    -------
    str
        The filename of the serialised function
    """

    # Generate the filename
    fname = filename(directory, function.__name__)

    # Dump the function using marshal
    with open(fname, 'wb') as fp:
        marshal.dump(function.__code__, fp)

    # Return the filename
    return fname

def execute_serialised_function(filename):
    """
    Load and execute the serialised function

    Parameters
    ----------
    filename : str
        Path of the serialised function
    """

    # Load the serialised function
    with open(filename, 'rb') as fp:
        code = marshal.load(fp)

    # Determine function name from filename
    match = re.search('(.+?)(-\d+)?\..+', os.path.basename(filename))
    if match is None:
        # Doesn't really matter
        name = 'function'
    else:
        name = match.group(1)

    # Create the function object
    func = types.FunctionType(code, globals(), name)

    # Execute function
    try:
        func()
    except NameError as err:
        name = str(err).split("'")[1].strip()
        tpl = ('name "{}" missing from serialised function "{}" - probably a '
               'missing module. Put the import statement inside the function '
               'or add to top of carputils/commandsequence/serialise.py')
        msg = tpl.format(name, func.__name__)
        raise Exception(msg)

if __name__ == '__main__':

    # Read filename from command line with simple parser
    parser = argparse.ArgumentParser('Run a serialised Python function')
    parser.add_argument('filename',
                        help='filename of serialised Python function')
    args = parser.parse_args()

    # Load and execute the serialised function
    execute_serialised_function(args.filename)
