#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Defines the Job class for executing sequences of commands in a customisable
manner.
"""

import os
import subprocess
from sys import version_info
from carputils import settings
from carputils.settings.exceptions import CARPUtilsMissingLicenseError
from carputils.polling import set_polling_options, process_polling_file
from carputils.job.command import Command, ShellCommand, MPICommand, PythonCommand
from carputils.job.optionlist import OptionList
from carputils.clean import overwrite_check
from carputils import divertoutput
from carputils import format

class Job(object):
    """
    A sequence of commands plus the tools to execute them.

    A job defines a series of commands that can be executed in different ways.
    Each job has an ID, which defines where the output of its commands is
    placed, plus a series of commands that can either be run immediately as
    they are registered or delayed for later execution in different ways.

    A job is instantiated with its ID:

    >>> job = Job('2015-11-25_important_sim')

    and its ID can then be accessed as an attribute:

    >>> print job.ID
    2015-11-25_important_sim

    Commands are then registered for the job with the :meth:`~Job.register`
    method:

    >>> from carputils.job.command import ShellCommand
    >>> job.register(ShellCommand(['ls', '-l'], 'List directory'))

    however, for convenience, a number of methods such as :meth:`~Job.bash` are
    provided to avoid the need to construct your own commands manually:

    >>> job.bash(['ls', '-l'])

    On a normal system, commands are executed immeditately once registered
    (unless the 'delayed' keyword argument is set to True), however on batch
    systems, commands are stored. The :meth:`~Job.script` method may then be
    used to generate a bash script executing the stored commands.

    The :meth:`~Job.submit` method may also be used to automatically generate
    and submit the job on HPC systems, using the carputils platform.

    >>> job.submit()

    Users will generally only need to use the convenience submission methods,
    as the job creation and batch script generation and submission is done
    automatically by the carputils framework.

    Args:
        ID : str
            The identifier for this job, both the name of the output directory and
            the identifier on batch systems
    """

    def __init__(self, ID):
        self.ID = ID
        self._queue = []
        self._poll = None

    def register(self, command, delayed=False):
        """
        Register a command to be executed.

        Args:
            command : carputils.job.command.Command
                The command object
            delayed : bool, optional
                If True, never execute immediately - instead wait until first non-
                delayed command before processing the whole series
        """

        assert isinstance(command, Command)
        if command._skipcmd:
            # Show command if not silent mode
            if not settings.cli.silent:
                if command._msg is not None:
                    print(format.header(command._msg))
                print(command.bash() + "\n")

            # Provide hints why the command was ignored
            print(command._skipmsg)
            return

        self._queue.append(command)

        if not settings.platform.BATCH and not delayed:
            # Execute whole queue
            while len(self._queue) > 0:
                self._queue.pop(0).execute()

    def script(self):
        """
        Generate a bash script running the command sequence

        Returns:
            str
                The content of the batch script
        """

        content = ''

        while len(self._queue) > 0:
            content += self._queue.pop(0).bash()
            content += '\n\n'

        # Add line break if we don't have additional polling commands
        if self._poll is None:
            content += '\n\n'

        return content

    def submit(self, argv=None):
        """
        Submit the command sequence to the batch system.

        Args:
            argv : list, optional
                If provided, include the command line in the generated script, for
                providence
        """

        assert settings.platform.BATCH, 'Must run submit in batch mode'

        # Determine if CUDA
        cuda = settings.makevars.PT_CUDA or settings.makevars.LIMPET_CUDA

        # Get runtime string
        tpl = '{0[0]:02d}:{0[1]:02d}:{0[2]:02d}'
        try:
            runtime = tpl.format(settings.cli.runtime)
        except TypeError:
            # Runtime was not set on the command line
            raise Exception('Must specify runtime when running on batch system')

        # Generate header and footer for the specified platform
        # we have to add some sanity checks to the job_id (here self.ID)
        job_id = self.ID
        if job_id.find('/') > -1:
            last = job_id.split('/')[-1]
            job_id = 'JOB_'+last

        co_s = settings.config.config_file if settings.config.config_file else None
        header = settings.platform.header(job_id, settings.cli.np, runtime,
                                          settings.cli.queue, cuda,
                                          settings.config.EMAIL, co_s)
        footer = settings.platform.footer()

        # Assemble script content
        divider = '#' * 80 + '\n'
        content = header.rstrip() + '\n\n'

        # Add summary
        content += divider
        content += '# Summary\n\n'
        if argv is not None:
            content += '# Run script executed with options:\n'
            content += format.command_line(argv, cols=80, prefix='# ') + '\n\n'

        content += divider
        content += '# Execute shell commands\n\n'

        # Execute initial shell commands first
        while not isinstance(self._queue[0], MPICommand):
            content += self._queue.pop(0).bash() + '\n'

        content += '\n'
        content += divider
        content += '# Execute simulation\n\n'
        script = self.script()

        # Add script or polling options
        if self._poll is None:
            # Get the actual script to execute
            content += script.rstrip()
        else:
            content += settings.platform.polling(self._poll, settings.cli.np,
                                                 settings.cli.np_job, script)

        # Add footer
        if len(footer.strip()) > 0:
            content += '\n\n' + footer.strip()

        content += '\n\n'

        # Determine submission script name
        if hasattr(self, 'BATCH_ID'):
            filename = self.BATCH_ID + settings.platform.BATCH_EXT
        else:
            filename = self.ID + settings.platform.BATCH_EXT

        # Write the batch script
        with open(filename, 'w') as fp:
            fp.write(content)

        # Submission command
        cmd = [settings.platform.SUBMIT, filename]

        # Print command
        print(format.header('Batch Job Submission'))
        print(format.command_line(cmd))
        print('\n')

        if not settings.cli.dry:
            divertoutput.call(cmd)

    def bash(self, cmd, message=None, delayed=False, *args, **kwargs):
        """
        Execute an arbitrary command in the shell.

        Args:
            cmd : list
                Command to be executed
            message : str, optional
                Descriptive message to display when running command
        """
        self.register(ShellCommand(cmd, message, *args, **kwargs), delayed )

    def mpi(self, cmd, message='Executing MPI Command', *args, **kwargs):
        """
        Execute a command with MPI.

        Runs a command with mpiexec, automatically adding the number of
        processes and debugger/profiler options from the current run
        configuration.

        Args:
            cmd : list
                The command to be executed with MPI
            message : str, optional
                Descriptive message to display when running command
        """
        if settings.platform.BATCH:
            self.register(MPICommand(cmd, message), *args, **kwargs)
        else:
            if self._poll is None:
                self.register(MPICommand(cmd, message), *args, **kwargs)
            else:
                for poll in self._poll:
                    self.register(MPICommand(cmd + poll.split(), message),
                                  delayed=True, *args, **kwargs)
            if settings.cli.np_job > 0:
                # Execute queue with multiprocessing

                import multiprocessing as mp

                # Set number of parallel python jobs
                assert settings.cli.np % settings.cli.np_job == 0, \
                    'Use a multiple of \"--np-jobs\" for \"--np\"'
                np_mp = int(settings.cli.np / settings.cli.np_job)
                pool = mp.Pool(np_mp)

                # Execute initial shell commands first
                while not isinstance(self._queue[0], MPICommand):
                    self._queue.pop(0).execute()

                # Build command
                command = []
                count = 1
                num_runs = len(self._queue)
                while len(self._queue) > 0:
                    cmd = self._queue.pop(0).bash()
                    command.append([count, num_runs, cmd])
                    count += 1
                pool.map(divertoutput.mp_one_run, command)
            else:
                # Execute whole queue
                while len(self._queue) > 0:
                    self._queue.pop(0).execute()


    def bench(self, cmd, simname=None, msg=None, *args, **kwargs):
        """
        Execute a BENCH simulation.

        Runs a BENCH sim with mpiexec, automatically adding the number of
        processes and command configuration.

        Args:
            cmd : list
                The command to be executed with MPI
            simname : str, optional
                Name to display when running command, defaults to job ID
        """

        if not msg:
            msg = 'Launching BENCH Simulation'
            if simname is not None:
                msg += ' {}'.format(simname)

        # Process commands
        # Write options list to file
        cmd = OptionList([settings.execs.BENCH]) + cmd

        # Execute with MPI
        self.mpi(cmd, msg, *args, **kwargs)

        #Move directories if there is a simname
        if simname is not None:
            if not os.path.exists(simname):
                self.mkdir(simname,parents=True)
            self.mv('BENCH_REG.txt', simname)
            self.mv('Trace_0.dat', simname)

    def carp(self, cmd, simname=None, polling_subdirs=True, *args, **kwargs):
        """
        Execute an openCARP simulation.

        Runs an openCARP sim with mpiexec, automatically adding the number of
        processes and debugger/profiler options from the current run
        configuration.

        Args:
            cmd : list
                The command to be executed with MPI
            simname : str, optional
                Name to display when running command, defaults to job ID
        """
        if simname is None:
            simname = self.ID
        if settings.cli.postprocess is not None:
            msg = 'Launching openCARP Postprocessing {}'.format(simname)
        else:
            msg = 'Launching openCARP Simulation {}'.format(simname)

        # Process commands
        process_command_list(cmd)

        # Generate full command line options list
        full_cmd = generate_command_list(cmd, settings.cli.generate_parfile)

        # Write options list to file
        if settings.cli.generate_parfile is not None:
            cmd = OptionList([settings.execs.CARP])
            cmd += ['+F', settings.cli.generate_parfile]

        # Generate or check polling file
        process_polling_file(settings.cli.polling_file,
                             settings.cli.polling_param,
                             settings.cli.polling_range,
                             settings.cli.sampling_type)

        # Set polling options
        self._poll = set_polling_options(full_cmd, settings.cli.polling_file,
                                         polling_subdirs)


        # Execute with MPI
        self.mpi(cmd, msg, *args, **kwargs)

    def python(self, cmd, *args, **kwargs):
        """
        Execute a python script.

        # FIXME: update documentation
        Runs a pythonCARP sim with mpiexec, automatically adding the number of
        processes and debugger/profiler options from the current run
        configuration.

        Args:
            cmd : callable
                The function to be executed with python
        """

        ver = '{}.{}.{}'.format(version_info.major, version_info.minor,
                                version_info.micro)
        msg = 'Launching PYTHON ({}) Script'.format(ver)

        # start python's system default: may be python2 or python3
        self.register(PythonCommand(cmd, msg), *args, **kwargs)

    def meshtool(self, cmd, *args, stdout=None, stderr=None, **kwargs):
        """
        Execute meshtool

        Args:
            cmd : list
                The command to be executed with meshtool
        """

        # perform argument check on main functions
        for item in cmd:
            if not ' ' in item:
                continue

            if str(item).startswith('-') and 'msh' in str(item):
                # need to preserve blanks in filenames
                index = cmd.index(item)
                cmd.remove(item)
                cmd.insert(index, item)

            elif str(item).startswith('-'):
                # do not allow spaces for parameters
                index = cmd.index(item)
                cmd.remove(item)
                cmd.insert(index, item.replace(' ', ''))

            else:
                # - do not allow arguments with spaces: 'extract mesh'
                #   carputils' job scheduler adds undesired formatting
                index = cmd.index(item)
                cmd.remove(item)

                # split item and add it re-insert into the list
                item = item.split()
                for i in reversed(item):
                    cmd.insert(index, i)

        # check if a submesh directory exists
        for item in cmd:
            if str(item).startswith('-submsh='):
                item = item.replace('-submsh=', '')
                dirname = os.path.dirname(item)
                if not os.path.exists(dirname):
                    self.mkdir(dirname, None)

        msg = 'Launching meshtool'
        cmd.insert(0, settings.execs.MESHTOOL)

        self.register(ShellCommand(cmd, msg, stdout=stdout, stderr=stderr, *args, **kwargs))

    def cvstool(self, cmd, stdout=None, stderr=None, *args, **kwargs):
        """
        Execute cvstool

        Args:
            cmd : list
                The command to be executed with cvstool
        """

        msg = 'Launching cvstool'
        cmd.insert(0, settings.execs.CVSTOOL)
        self.register(ShellCommand(cmd, msg, stdout=stdout, stderr=stderr, *args, **kwargs))

    def mkdir(self, directory, parents=False, *args, **kwargs):
        """
        Make a directory.

        Args:
            directory : str
                Path of directory to make
            parents : bool, optional
               True to automatically generate parents (mkdir -p)
        """
        directory = os.path.abspath(directory)

        cmd = ['mkdir']
        if parents:
            cmd += ['-p']
        cmd += [directory]
        if not os.path.exists(directory):
            self.register(ShellCommand(cmd, None), *args, **kwargs)
        else:
            # no need to create something which already exists
            print('{} does already exist...'.format(directory))

    def cp(self, path_from, path_to, cpdir=False, *args, **kwargs):
        """
        Copy a file.

        Args:
            path_from : str
                Path of file to copy
            path_to : str
                Path to copy file to
        """
        if not cpdir:
            cmd = ['cp', path_from, path_to]
        else:
            cmd = ['cp', '-r',path_from, path_to]
        self.register(ShellCommand(cmd, None), *args, **kwargs)

    def mv(self, path_from, path_to, *args, **kwargs):
        """
        Move a file.

        Args:
            path_from : str
                Path of file to move
            path_to : str
                Path to move file to
        """
        cmd = ['mv', path_from, path_to]
        if os.path.exists(path_from):
            # move only files which do exist
            self.register(ShellCommand(cmd, None), *args, **kwargs)

    def rm(self, filename, rmdir=False, *args, **kwargs):
        """
        Remove a file or a directory

        Args:
            filename : str
                Path of file to remove
            rmdir : str
                Set to True if removing directory
        """
        if not rmdir:
            cmd = ['rm', filename]
        else:
            cmd = ['rm', '-r', filename]

        self.register(ShellCommand(cmd, None), *args, **kwargs)


    def link(self, target, link_name=None, *args, **kwargs):
        """
        Make a symbolic link.

        Args:
            target : str
                Target of link (first arg to 'ln -s')
            link_name : str, optional
                Place to put link (second arg to 'ln -s')
        """
        cmd = ['ln', '-s', target]
        if link_name is not None:
            cmd += [link_name]
        self.register(ShellCommand(cmd, None), *args, **kwargs)

    def gunzip(self, filename, *args, **kwargs):
        """
        Unzip a .gz file.

        Args:
            filename : str
                Path of file to unzip
        """

        if filename.endswith('.gz') or os.path.isfile(filename+'.gz'):
            cmd = ['gunzip', filename]
            self.register(ShellCommand(cmd, None), *args, **kwargs)

    def meshalyzer(self, geom, *args, **kwargs):
        """
        Run meshalyzer.

        Args:
            geom: str
                Basename of mesh to visualise
            args: str
                Additional data/display config files to pass as command line
                arguments to meshalyzer

            kwargs:
                compsurf: bool, optional
                    If True, generate a surface on the fly (default: False)
                monitor: int
                    Delay in seconds before spawning visualization process
                    after job launched (default: none)
                    Return the spawned process.
        """

        args = list(args)

        # Data file sanity check
        for arg in args:
            index = args.index(arg)
            if os.path.exists(arg):
                continue

            if arg.endswith('.gz'):
                arg = os.path.splitext(arg)[0]
            elif arg.endswith('.igb'):
                arg += '.gz'

            # Second data file sanity check
            if os.path.isfile(arg):
                print('Warning: Changing {} to {}!'.format(args[index], arg))
                args[index] = arg
            else:
                print('Error: {} does not exist!'.format(args[index]))

        # Build command
        cmd = [settings.execs.MESHALYZER, geom] + args

        # meshalyzer related key/(no)value parameters
        if kwargs.pop('compsurf', False):
            # Get meshalyzer to generate a surface but dump it to /dev/null
            cmd.append('--compSurf=+/dev/null')

        # meshalyzer parameters
        params = ['help', 'no_elem', 'thrdRdr', 'groupID', 'PNGfile', 'frame', 'numframe', 'size', 'nproc', 'flyby']
        for key, value in kwargs.items():
            if not key in params:
                continue
            if value in [None, False]:
                cmd.append('--{}'.format(key))
            else:
                cmd.append('--{}={}'.format(key, value))

        # remove processed params as there may be non-meshalyzer related kwargs
        for param in params:
            kwargs.pop(param, 'False')

        # process related key/value parameters
        delay = kwargs.pop('monitor', None)
        if delay:
            if not settings.platform.BATCH:
                cmd = ['sleep', str(delay), ';', str(cmd[0])] + cmd[1:]
                return subprocess.Popen(' '.join(cmd), shell=True)
            # else
            return None

        self.register(ShellCommand(cmd, 'Launching Meshalyzer',
                                   skiperror=True), **kwargs)
        return self

def generate_command_list(cmd, out_p_file=None):
    """
    Generate a list of all commands that were set

    Args:
        out_p_file: output parameter file
    """

    commands = [str(x).encode('utf-8') for x in cmd[1:]]
    f_cmd = []

    len_cmd = 0 if commands is None else len(commands)
    i = 0
    while i < len_cmd :
        if commands[i].decode('utf-8') == '+F':
            parfile = commands[i+1]
            with open(parfile) as src:
                for line in src:
                    if not line.startswith('#'):
                        line = line.replace("=", " ")
                        if line.strip():  # check if empty line
                            param = line.split()[0]
                            f_cmd += ["-" + param]
                            param = line.split()[1]
                            f_cmd += [param]
            i +=2
        else:
            f_cmd += [commands[i].decode('utf-8')]
            i += 1


    # write parameter file if given
    if out_p_file is not None:
        # check if output parfile already exists
        overwrite_check(out_p_file)
        # open parameter file
        tgt = open(out_p_file, "a")
        tgt.write("# --- Parameter list ---\n")
        for i in range(0, len(f_cmd), 2):
            try:  # write float
                float(f_cmd[i+1])
                tgt.write("{} = {}\n".format(f_cmd[i][1:], f_cmd[i+1]))
            except ValueError:
                if f_cmd[i+1].startswith('\"') or f_cmd[i+1].startswith('\''):
                    tgt.write("{} = {}\n".format(f_cmd[i][1:], f_cmd[i+1]))
                else:
                    tgt.write("{} = \"{}\"\n".format(f_cmd[i][1:], f_cmd[i+1]))
        tgt.close()

    # return full command list
    return f_cmd

def process_command_list(cmd):
    """
    Modify options that include "=" and "," which should be parsed as a string
    """

    # Ensure all strings
    for i in range(0, len(cmd)):
        if ("=" in str(cmd[i]) or "," in str(cmd[i])) and not \
           (str(cmd[i]).startswith('"') or str(cmd[i]).startswith("'")):
            cmd[i] = "\"" + str(cmd[i]) + "\""
