#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

class CMLConverter:
    """
    Class defining basic string conversion methods
    """

    def __init__(self):
        """
        Method to initialize the CMLConverter object
        """
        pass

    @staticmethod
    def is_valid_string(s):
        """
        Checks whether the given string is a valid string or not

        Args:
            s: The string to be checked

        Returns:
            True if the given object is a valid string,
            False otherwise
        """
        if type(s) != str:
            return False
        if len(s) == 0:
            return False
        return True

    @staticmethod
    def is_identifier(s):
        """
        Checks whether the given string is a valid identifier or not

        Args:
            s: The string to be checked

        Returns:
            True if the given object is a valid string,
            False otherwise
        """
        if not CMLConverter.is_valid_string(s):
            return False
        if not (s[0].isalpha() or s[0] == '_'):
            return False
        for c in s[1:]:
            if not (c.isalnum() or c == '_'):
                return False
        return True

    @staticmethod
    def make_identifier(s, substitute='_'):
        """
        Tries to convert the given string to a valid identifier

        Args:
            s: The string to be converted

        Kwargs:
            substitute: A valid identifier string used to replace
                        invalid characters in the `s` string

        Returns:
            The identifier string

        Raises:
            AttributeError
        """
        if not (CMLConverter.is_valid_string(substitute) and CMLConverter.is_identifier(substitute)):
            raise AttributeError('error, `{}` is not a valid substitute!'.format(substitute))
        if not CMLConverter.is_valid_string(s):
            raise AttributeError('error, `{}` is not a valid string!'.format(s))
        ids = ''
        if s[0].isalpha() or s[0] == '_':
            ids += s[0]
        elif s[0] == '-':
            ids += '_'
        else:
            ids += substitute
        for c in s[1:]:
            if c.isalnum() or c == '_':
                ids += c
            elif c == '-':
                ids += '_'
            else:
                ids += substitute
        return ids

    @staticmethod
    def to_int(s):
        """
        Tries to convert a given string to an integer

        Args:
            s: The string to be converted

        Returns:
            A tuple (state, value) with <br>
            \b state ... The state of the conversion (True or False) and<br>
            \b value ... the converted value (None if the state is False)
        """
        c,v = False, None
        try:
            v = int(s)
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def to_float(s):
        """
        Tries to convert a given string to a float

        Args:
            s: The string to be converted

        Returns:
            A tuple (state, value) with <br>
            \b state ... The state of the conversion (True or False) <br>
            \b value ... The converted value (None if the state is False)
        """
        c,v = False, None
        try:
            v = float(s)
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def to_string(s):
        """
        Tries to convert a given string to a string

        Args:
            s: The string to be converted

        Returns:
            A tuple (state, value) with <br>
            \b state ... The state of the conversion (True or False) <br>
            \b value ... The converted value (None if the state is False)
        """
        c,v = False, None
        try:
            v = str(s)
            if v[0] == v[-1] == '"':
                v = v[1:-1]
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def convert(s):
        """
        Tries to convert a given string

        Note:
            This is just a method interface, a `NotImplementedError`
            will be raised when the method is called.

        Args:
            s: The string to be converted

        Raises:
            NotImplementedError
        """
        raise NotImplementedError('error, `convert` method not implemented!')


class CMLConverterYAML(CMLConverter):
    """
    Class defining YAML conform string conversion methods
    """

    ## Dictionary holding all strings which are converted to True/False
    BOOL_DICT = {'true': True, 'yes': True, 'false': False, 'no': False}

    ## Tuple holding all strings which are converted to None
    NONE_LIST = ('none', 'null', 'nil')

    def __init__(self):
        """
        Method to initialize the CMLConverterYAML object
        """
        pass

    @staticmethod
    def to_bool(s):
        """
        Tries to convert a given string to a boolean

        Args:
            s: The string to be converted

        Returns:
            A tuple (state, value) with <br>
            \b state ... The state of the conversion (True or False) <br>
            \b value ... The converted value (None if the conversion failed)
        """
        c, v = False, None
        if s.lower() in CMLConverterYAML.BOOL_DICT:
            v = CMLConverterYAML.BOOL_DICT[s.lower()]
            c = True
        return c, v

    @staticmethod
    def to_none(s):
        """
        Tries to convert a given string to None

        Args:
            s: The string to be converted

        Returns:
            A tuple (state, value) with <br>
            \b state ... The state of the conversion (True or False) <br>
            \b value ... The converted value (None if the conversion failed)
        """
        c = s.lower() in CMLConverterYAML.NONE_LIST
        v = None
        return c, v

    @staticmethod
    def convert(s):
        """
        Convert a given string, the type is auto-detected

        Args:
            s: The string to be converted

        Returns:
            The converted value
        """
        fiter = iter([CMLConverter.to_int,
                      CMLConverter.to_float,
                      CMLConverterYAML.to_bool,
                      CMLConverterYAML.to_none,
                      CMLConverter.to_string,
                      lambda s: (True, s)])
        cv = (False, s)
        while not cv[0]:
            cv = next(fiter)(s)
        return cv[1]
