#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
A class and associated functions for generating geometries with mesher.
"""
import numpy as np
from carputils.mesh.region import Region

class Block(object):
    """
    Generate a block mesh with the mesher command line utility.

    This class describes a tissue block with optional additional bath region.
    For example, to define a 2x2x2mm block centred on the origin with 1mm bath
    on the positive z side:

    >>> geom = Block(size=(2,2,2))
    >>> geom.set_bath(thickness=(0,0,1), both_sides=False)

    Regions may also be specified using the :class:`BoxRegion`,
    :class:`SphereRegion` and :class:`CylinderRegion` objects:

    >>> reg = BoxRegion((-1,-1,-1), (1,1,0), tag=10)
    >>> geom.add_region(reg)

    Generate a list of command line options to pass to mesher to generate the
    described mesh with the :meth:`mesher_opts` method:

    >>> opts = geom.mesher_opts('meshname') # meshname.pts, meshname.elem, etc.

    .. note::

        While the mesher executable takes arguments in centimetres and
        micrometres, this class expects *all* physical sizes to be specified in
        millimetres. Arguments are converted to the correct units when
        generating the mesher command line.

    Args:
        centre: array-like, optional
            Coordinate of centre of Block, defaults to (0,0,0)
        size: array-like, optional
            Size of Block, defaults to (1,1,1)
        resolution : float, optional
            Resolution at which to tesselate block into mesh, defaults to 0.1
        etype: str, optional
            Element type to generate, either 'tetra' or 'hexa' in 3D,
            or 'quad' or 'tri' in 2D, defaults to 'tetra' in 3D and 'quad' in 2D
        periodic: int, optional
            If set, the edges along a given direction are connected by linear elements
            to implement periodic boundary conditions.
            The range of values spans 0 (none), 1 (X), 2 (Y) and 3 (XY)
        periodic_tag: int, optional
            Tag value assigned to the connecting linear elements.
            As default the value 1234 is used.
    """

    def __init__(self, centre=(0., 0., 0.), size=(1., 1., 1.), resolution=0.1,
                 etype='tetra', periodic=0, periodic_tag=1234, pertb=0.0):

        # Rationalise and save constructor arguments
        self._centre = np.array(centre, dtype=float)
        self._size = np.array(size, dtype=float)
        self._res = float(resolution)
        self._pertb = float(pertb)
        assert etype in ['tetra', 'hexa', 'quad', 'tri']
        self._etype = etype
        assert periodic in [0, 1, 2, 3]
        self._periodic = periodic
        self._periodic_tag = periodic_tag

        # Initliase other variables
        self._bath = np.array([0., 0., 0.])
        self._bath_both_sides = True

        # Fibres run along x-axis, sheet axis is along z
        self._fibre_endo = 0.
        self._fibre_epi = 0.
        self._sheet_endo = 0.
        self._sheet_epi = 0.

        self._regions = []

    def resolution(self):
        """
        Return the currently set mesh resolution.

        Returns:
            float
                The mesh resolution.
        """
        return self._res

    def set_bath(self, thickness, both_sides=True):
        """
        Add a bath region of the specified thickness.

        Note that this method does *not* use the mesher convention of positive
        bath size meaning bath on one side and negative bath size meaning bath
        on both sides. Instead always use positive thickness values and the
        optional both_sides parameter to control this behaviour.

        Args:
            thickness : array-like
                The thickness of bath to add, one value per coordinate axis
            both_side : bool or array-like of bools, optional
                If True, generate bath on both sides of the object, if False only
                on one side. Pass an array of bools to control this behaviour for
                individual directions.
        """

        self._bath = np.array(thickness, dtype=float)
        msg = ('Negative bath size not allowed - use the optional both_sides '
               'argument to control which sides the bath is on')
        assert all(self._bath >= 0), msg

        if isinstance(both_sides, bool):
            both_sides = np.array([both_sides]*3)

        self._bath_both_sides = both_sides

    def set_fibres(self, fibre_endo=None, fibre_epi=None,
                   sheet_endo=None, sheet_epi=None):
        """
        Set the angles for generated fibres in the generated geometry.

        Args:
            fibre_endo : float, optional
                Set the principal fibre direction on the endocardium
            fibre_epi : float, optional
                Set the principal fibre direction on the epicardium
            sheet_endo : float, optional
                Set the sheet direction on the endocardium
            sheet_epi : float, optional
                Set the shhet direction on the epicardium
        """

        if fibre_endo is not None:
            self._fibre_endo = float(fibre_endo)
        if fibre_epi is not None:
            self._fibre_epi = float(fibre_epi)
        if sheet_endo is not None:
            self._sheet_endo = float(sheet_endo)
        if sheet_epi is not None:
            self._sheet_epi = float(sheet_epi)

    def corner_at_origin(self):
        """
        Put the corner of the tissue block at the origin.
        """
        self._centre = 0.5 * self._size

    def tissue_limits(self):
        """
        Return the lower and upper limits of the block, excluding the bath.

        Returns:
            lower : array
                The coordinate of the lower left corner of the tissue block
            upper : array
                The coordinate of the upper right corner of the tissue block
        """
        lower = self._centre - 0.5 * self._size
        upper = self._centre + 0.5 * self._size
        return lower, upper

    def bath_limits(self):
        """
        Return the lower and upper limits of the block, including the bath.

        Returns:
            lower : array
                The coordinate of the lower left corner of the block
            upper : array
                The coordinate of the upper right corner of the block
        """
        lower, upper = self.tissue_limits()
        lower -= self._bath * self._bath_both_sides
        upper += self._bath
        return lower, upper

    def shape(self, bath=True):
        """
        Determine the number of points in each dimension.
        """

        if bath:
            lower, upper = self.bath_limits()
        else:
            lower, upper = self.tissue_limits()

        size = upper - lower

        # Duplicating mesher methodology
        cubes = size / self.resolution()
        cubes = np.array(cubes, dtype=int) # Round down

        return cubes + 1

    def add_region(self, region):
        """
        Add a region definition.

        Args:
            region : BoxRegion or SphereRegion or CylinderRegion
                The region description.
        """

        assert isinstance(region, Region)

        if region.tag is None:
            # Work out an unsed tag >= 2
            tag = 2
            taken = lambda t: any([t == r.tag for r in self._regions])
            while taken(tag):
                tag += 1
            region.tag = tag

        self._regions.append(region)

    def mesher_opts(self, meshname):
        """
        Generate a list of command line arguments for mesher.

        Args:
            meshname : str
                The base name of the mesh to be generated.

        Returns:
            list
                The mesher argument list.
        """

        # Mesher accepts 1 for hex meshes, 0 for tet meshes
        etype_3D = int(self._etype == 'hexa')

        # Mesher adds tri2D flag if etype is 'tri'
        etype_2D = bool(self._etype == 'tri')

        # Compute inputs in cm, as expected by mesher
        size = self._size / 10.
        bath = self._bath / 10.
        centre = self._centre / 10.

        # Mesher expects resolution in um
        res = self._res * 1000

        # Mesher interprets negative bath size to mean both sides
        bath *= 1 - 2 * np.array(self._bath_both_sides)

        # Generate option list
        opts = ['-size[0]', size[0],
                '-size[1]', size[1],
                '-size[2]', size[2],
                '-bath[0]', bath[0],
                '-bath[1]', bath[1],
                '-bath[2]', bath[2],
                '-center[0]', centre[0],
                '-center[1]', centre[1],
                '-center[2]', centre[2],
                '-resolution[0]', res,
                '-resolution[1]', res,
                '-resolution[2]', res,
                '-mesh', meshname,
                '-Elem3D', etype_3D,
                '-fibers.rotEndo', self._fibre_endo,
                '-fibers.rotEpi', self._fibre_epi,
                '-fibers.sheetEndo', self._sheet_endo,
                '-fibers.sheetEpi', self._sheet_epi,
                '-periodic', self._periodic,
                '-periodic_tag', self._periodic_tag,
                '-perturb', self._pertb]

        # in 2D case, add triangulation flag if needed
        if etype_2D:
            assert(size[2] == 0.0)
            opts += ['-tri2D']

        # Add region definitions, if any
        if len(self._regions) > 0:

            opts += ['-numRegions', len(self._regions)]

            for i, region in enumerate(self._regions):
                opts += region.mesher_opts(i)

        return opts

def block_boundary_condition(block, entity, index, coord, lower=True,
                             bath=False, verbose=False):
    """
    Generate a boundary condition definition for a :class:`Block`.

    Given a :class:`Block`, generate a set of openCARP options defining a boundary
    condition on one of the faces of the mesh. The face at the lower or upper
    bound of the specified coordinate is 'encased' in a box surrounding the
    selected surface at half its resolution.

    For example, for the default origin-centred box of size (1,1,1) and
    resolution 0.1:

    >>> geom = Block()

    calling :func:`block_boundary_condition` with coord='x' and lower=False:

    >>> block_boundary_condition(geom, 'stimulus', 0, 'x', False)

    will generate a BC for 0.45<=x<=0.55, -0.55<=y<=0.55 and -0.55<=z<=0.55.

    Args:
        block : Block
            The geometry to generate boundary conditions for
        entity : str
            The openCARP boundary condition type to use (e.g. 'stimulus')
        index : int
            The boundary condition index
        coord : str
            The direction perpendicular to which the BC is generated. One of
            ('x', 'y', 'z').
        lower : bool, optional
            True if to generate BC at lower bound of coord (default), False for
            upper bound
        bath : bool, optional
            Generate BC at bounds of tissue+bath if True, tissue only otherwise
            (default)
        verbose : bool, optional
            True to print summary of generated parameters to stdout

    Returns:
        list
            openCARP command line options to set up boundary condition.
    """

    # Sanity checks
    assert isinstance(block, Block)
    entity = str(entity)
    index = int(index)

    # Determine the 0-index for the specified coord
    i_coord = ['x', 'y', 'z'].index(coord)

    # Get relevant bounds
    low, up = block.bath_limits() if bath else block.tissue_limits()

    # Convert to um
    low *= 1000.
    up  *= 1000.

    # Get BC corner
    corner = low
    if not lower:
        corner[i_coord] = up[i_coord]

    # Perpendicular dimensions have full width
    mesh_size = up - low
    bc_size = mesh_size * (np.arange(3) != i_coord)

    # Add extra half-resolutions thickness to all sides
    res = block.resolution() * 1000 # Convert to um
    corner -= res/2.
    bc_size += res

    if verbose:
        print()
        print('----------------------------------------------')
        print('{}[{}]'.format(entity, index))
        print('x0: {:7.1f} xd: {:7.1f}'.format(corner[0], bc_size[0]))
        print('y0: {:7.1f} yd: {:7.1f}'.format(corner[1], bc_size[1]))
        print('z0: {:7.1f} zd: {:7.1f}'.format(corner[2], bc_size[2]))
        print('----------------------------------------------')

    tpl = '-{}[{}].{{}}'.format(entity, index)

    bc = [tpl.format('x0'), corner[0],
          tpl.format('xd'), bc_size[0],
          tpl.format('y0'), corner[1],
          tpl.format('yd'), bc_size[1],
          tpl.format('z0'), corner[2],
          tpl.format('zd'), bc_size[2]]

    return bc

def block_bc_opencarp(block, entity, index, coord, lower=True,
                             bath=False, verbose=False):
    """
    Does the same thing as `block_boundary_condition`, but for the new stim struct
    introduced in openCARP.

    Given a :class:`Block`, generate a set of openCARP options defining a boundary
    condition on one of the faces of the mesh. The face at the lower or upper
    bound of the specified coordinate is 'encased' in a box surrounding the
    selected surface at half its resolution.

    Args:
        block : Block
            The geometry to generate boundary conditions for
        entity : str
            The openCARP boundary condition type to use (e.g. 'stimulus')
        index : int
            The boundary condition index
        coord : str
            The direction perpendicular to which the BC is generated. One of
            ('x', 'y', 'z').
        lower : bool, optional
            True if to generate BC at lower bound of coord (default), False for
            upper bound
        bath : bool, optional
            Generate BC at bounds of tissue+bath if True, tissue only otherwise
            (default)
        verbose : bool, optional
            True to print summary of generated parameters to stdout

    Returns:
        list
            openCARP command line options to set up boundary condition.
    """

    # Sanity checks
    assert isinstance(block, Block)
    entity = str(entity)
    index = int(index)

    # Determine the 0-index for the specified coord
    i_coord = ['x', 'y', 'z'].index(coord)

    # Get relevant bounds
    low, up = block.bath_limits() if bath else block.tissue_limits()

    # Convert to um
    low *= 1000.
    up  *= 1000.

    # Get BC corner
    corner = low
    if not lower:
        corner[i_coord] = up[i_coord]

    # Perpendicular dimensions have full width
    mesh_size = up - low
    bc_size = mesh_size * (np.arange(3) != i_coord)

    # Add extra half-resolutions thickness to all sides
    res = block.resolution() * 1000 # Convert to um
    corner -= res/2.
    bc_size += res

    if verbose:
        print()
        print('----------------------------------------------')
        print('{}[{}]'.format(entity, index))
        print('x0: {:7.1f} xd: {:7.1f}'.format(corner[0], bc_size[0]))
        print('y0: {:7.1f} yd: {:7.1f}'.format(corner[1], bc_size[1]))
        print('z0: {:7.1f} zd: {:7.1f}'.format(corner[2], bc_size[2]))
        print('----------------------------------------------')

    tpl = '-{}[{}].elec.{{}}'.format(entity, index)

    bc = [tpl.format('p0[0]'), corner[0],
          tpl.format('p0[1]'), corner[1],
          tpl.format('p0[2]'), corner[2],
          tpl.format('p1[0]'), corner[0] + bc_size[0],
          tpl.format('p1[1]'), corner[1] + bc_size[1],
          tpl.format('p1[2]'), corner[2] + bc_size[2]]

    return bc




def block_region(block, entity, index, low, up, bath=False, verbose=False):
    """
    Generate a boundary condition definition for a :class:`Block`.

    Given a :class:`Block`, generate a set of openCARP options
    defining a block-shaped region on a given domain
    low and up specify lower and upper coordinates of the block region.

    For example, for the default origin-centred box of size (1,1,1) and
    resolution 0.1:

    >>> geom = Block()

    calling :func:`block_region` with low ='[-0.5, -0.5 -0.5]' and up='[0.0, 0.0, 0.0]':

    >>> block_region(geom, 'stimulus', 0, low, up, False)

    will generate a stimulus block definition for -0.5<=x<=0., -0.5<=y<=0. and -0.5<=z<=0.0.

    Args:
        block : Block
            The geometry to generate boundary conditions for
        entity : str
            The openCARP boundary condition type to use (e.g. 'stimulus')
        index : int
            The boundary condition index
        low : array
            x, y and z coordinate of lower left corner of block region
        up : array
            x, y and z coordinate of upper right corner of block region
        bath : bool, optional
            Generate BC at bounds of tissue+bath if True, tissue only otherwise
            (default)
        verbose : bool, optional
            True to print summary of generated parameters to stdout

    Returns:
        list
            openCARP command line options to set up boundary condition.
    """

    # Sanity checks
    assert isinstance(block, Block)
    entity = str(entity)
    index = int(index)
    low = np.array(low)
    up = np.array(up)

    # Convert to um
    low *= 1000.
    up *= 1000.

    # Add extra half-resultions thickness to all sides
    res = block.resolution() * 1000 # Convert to um
    low -= res/2.
    up += res/2.
    bc_size = up - low

    if verbose:
        print()
        print('----------------------------------------------')
        print('{}[{}]'.format(entity, index))
        print('x0: {:7.1f} xd: {:7.1f}'.format(low[0], bc_size[0]))
        print('y0: {:7.1f} yd: {:7.1f}'.format(low[1], bc_size[1]))
        print('z0: {:7.1f} zd: {:7.1f}'.format(low[2], bc_size[2]))
        print('----------------------------------------------')

    tpl = '-{}[{}].{{}}'.format(entity, index)

    br = [tpl.format('x0'), low[0],
          tpl.format('xd'), bc_size[0],
          tpl.format('y0'), low[1],
          tpl.format('yd'), bc_size[1],
          tpl.format('z0'), low[2],
          tpl.format('zd'), bc_size[2]]

    return br
