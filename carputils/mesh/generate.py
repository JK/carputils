#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Function to generate a CARP mesh if it does not yet exist.
"""
import os
from sys import version_info
import string
import random
from datetime import date
from glob import glob

from carputils.mesh.block import Block
from carputils import settings
from carputils import format
from carputils import divertoutput

# Python2 - Python3 compatibility
if version_info.major > 2:
    xrange = range

def generate(geom, dirname=None, rootdir='meshes'):
    """
    Generate the specified geometry, unless it already exists.

    This function checks whether the specified mesh already exists, and simply
    returns it base name if so. Otherwise, the mesh is generated in a new and
    uniquely named mesh directory, and the base name of this mesh is then
    returned. This currently only works with :class:`carputils.mesh.Block`.

    To use:

    >>> from carputils.mesh import Block, generate
    >>> geom = Block(size=(2,2,2))
    >>> meshname = generate(geom)

    Args:
        generate: Block
            The geometry to generate
        rootdir : str, optional
            The parent directory to store and search for meshes, defaults to
            'meshes'

    Returns:
        str
            The mesh base name of the requested name
    """

    assert isinstance(geom, Block), 'generate only works with Block for now'

    # Generate command line
    # Always generate with '<placeholder>' as mesh name to ensure accurate comp
    cmd = geom.mesher_opts('<placeholder>')
    cmd_string = '\n'.join([str(e) for e in cmd])

    pattern = os.path.join(rootdir, '*', 'mesher_cmd.txt')

    for fname in glob(pattern):

        with open(fname) as f_p:
            content = f_p.read()

        # Skip this candidate if the command used to generate mesh is different
        if content != cmd_string:
            continue # to next candidate

        # Generate paths
        meshdir = os.path.dirname(fname)
        meshname = os.path.join(meshdir, 'block')

        # Make sure meshfiles actually there
        missing = False
        for ext in ['pts', 'elem', 'lon', 'vpts', 'vec']:
            if not os.path.exists(meshname + '.' + ext):
                missing = True
                break # Don't need to check remaining

        if missing:
            continue # to next candidate

        if not settings.cli.silent:
            print()
            print('Requested mesh already exists, skipping generation.')

        # Break from loop to avoid mesh generation
        break

    else:

        # No existing mesh of specified parameters found, generate it!
        if not settings.cli.silent:
            print()
            print('Requested mesh does not yet exist - generating it...')

        # Generate a unique mesh name if no path is given
        if dirname:
            meshdir = os.path.join(rootdir, dirname)
        else:
            meshdir = _unique_dirname(rootdir)

        meshname = os.path.join(meshdir, 'block')

        # Run mesher
        cmd = [settings.execs.MESHER] + geom.mesher_opts(meshname)

        if not settings.cli.silent:
            print(format.header('Launching Mesher'))
            print(format.command_line(cmd))
            print()

        if not settings.cli.dry:

            # Generate directory
            os.makedirs(meshdir)

            # Run command
            with divertoutput.subprocess_exceptions():
                divertoutput.call(cmd)

            # Write out command line to file for later comparison
            with open(os.path.join(meshdir, 'mesher_cmd.txt'), 'w') as f_p:
                f_p.write(cmd_string)

    return meshname

def _unique_dirname(root):
    """
    Generate a unique directory name to store a mesh in.

    Args:
        root : str
            Parent directory of the new directory

    Returns:
        str
            The generated directory name
    """

    # Generate a date stamp
    today = date.today().isoformat()

    # Generate a random ASCII string
    rnd = ''.join(random.choice(string.ascii_letters) for i in xrange(10))

    # Generate candidate directory name
    path = os.path.join(root, '{}_{}'.format(today, rnd))

    # Call recursively if already exists
    if os.path.exists(path):
        path = _unique_dirname(root)

    return path
