#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Generate a simple ring geometry, providing a few parameters.
"""

from __future__ import division, print_function, absolute_import, unicode_literals
from sys import version_info
import numpy as np
from carputils.mesh.general import Mesh3D, closest_to_rangles

# Python2 - Python3 compatibility
if version_info.major > 2:
    xrange = range

class Ring(Mesh3D):
    """
    Generate a mesh of a ring/cylindrical shell.

    This class describes a tissue ring, and generates the described mesh. For
    example, to define a ring with a 5mm internal radius:

    >>> geom = Ring(5)

    By default, the thickness of the ring is 1/5 of the radius, and the height
    is the same as the thickness. These can be controlled exactly with the
    corresponding paramters:

    >>> geom = Ring(5, thickness=0.5, height=0.7)

    Discretisation of the ring is done into hexahedra, with one hex across both
    the thickness and height. The circumferential discretisation is chosen to
    ensure as regular aspect hexahedra as possible. If a tetrahedral mesh is
    generated, these hexahedra are further subdivided into tets. Discretisation
    can be controlled with the division ``div_`` parameters:

    >>> geom = Ring(5, 2, 0.7, div_transmural=3)

    The ring's fibre directions are, by default, oriented in the
    circumferential direction. To control the fibre direction, pass a function
    as the ``fibre_rule`` argument that itself takes a normalised transmural
    distance and returns a helix angle in radians:

    >>> rule = lambda t: (1 - t)/2
    >>> geom = Ring(5, 2, 0.7, fibre_rule=rule)

    :func:`carputils.mesh.ring.linear_fibre_rule` provides a convenient method
    to generate such rules. For example, to generate a +60/-60 degrees linear
    fibre rule:

    >>> rule = linear_fibre_rule(60, -60)
    >>> geom = Ring(5, 2, 7, fibre_rule=rule)

    To generate the actual mesh, use the :meth:`generate_carp` and
    :meth:`generate_vtk` methods.

    Args:
        radius : float
            Inner radius of the ring, in mm
        thickness : float, optional
            Radial thickness of the ring, in mm, defaults to 1/5 inner radius
        height : float, optional
            Height of the ring, in mm, by default set to a size to regularise
            aspect ratio
        div_transmural : int, optional
            Number of elements across the radial thickness, defaults to 1
        div_circum : int, optional
            Number of elements around the full circle of the ring, defaults to a
            number regularising the hexahedron aspects
        div_height : int, optional
            Number of elements across the height of the ring, defaults to 1
        tetrahedrise : bool, optional
            True to subdivide the mesh into tetrahedra
        fibre_rule : callable, optional
            Function describing the transmural variation of fibre angle, takes a
            single normalised transmural distance on [0,1] and returns a helix
            angle in radians, defaults to circumferentially oriented fibres
    """

    def __init__(self, radius, thickness=None, height=None,
                 div_transmural=1, div_circum=None, div_height=1,
                 *args, **kwargs):

        Mesh3D.__init__(self, *args, **kwargs)

        # Default thickness is radius/5
        if thickness is None:
            thickness = radius/5.0

        # If not specified, estimate div_circum for ideal aspect ratio
        if div_circum is None:
            depth_per_div = float(thickness)/div_transmural
            cirumference = 2 * np.pi * radius
            # Assuming arc length for a division is ideally equal to its depth
            div_circum = int(np.round(cirumference/depth_per_div))

        # If height not specified, again go for ideal aspect ratio
        if height is None:
            depth_per_div = float(thickness)/div_transmural
            height = depth_per_div * div_height

        self._radius = float(radius)
        self._thickness = float(thickness)
        self._height = float(height)
        self._div_trans = int(div_transmural)
        self._div_circ = int(div_circum)
        self._div_height = int(div_height)

    @classmethod
    def with_resolution(cls, radius, resolution, height=None,
                        wallthickness=None, **kwargs):
        """
        Simplified interface to generate ring with target resolution.

        Args:
            radius : float
                Inner radius of ellipsoid cavity
            resolution : float
                Target mesh edge length
            height : float, optional
                Vertical thickness of ring (default: radius/10)
            wallthickness : float, optional
                Thickness of ellipsoid wall (default: radius/5)
        """

        # Determine wall thickness and height, when not set
        if height is None:
            height = radius / 10.
        if wallthickness is None:
            wallthickness = radius / 5.

        # Determine number of divisions in each direction
        # Circumferential
        circumference = 2 * np.pi * radius
        div_circ = int(round(circumference / resolution))
        # Transmural
        div_trans = int(round(wallthickness / resolution))
        # Vertical
        div_height = int(round(height / resolution))

        # Generate object and return
        geom = cls(radius, wallthickness, height, div_trans,
                   div_circ, div_height, **kwargs)

        return geom

    def cavity_volume(self):
        """
        Calculate the volume of the cavity analytically.

        The actual volume may be less due to discretisation effects.

        Returns:
            float
                The cavity volume
        """
        return np.pi * self._radius**2 * self._height

    def _generate_points(self):
        """
        Generate the points of the mesh.
        """

        r_per_div = self._thickness / self._div_trans
        theta_per_div = 2*np.pi / self._div_circ
        z_per_div = self._height / self._div_height

        coords = []

        for i_circ in xrange(self._div_circ):
            theta = i_circ * theta_per_div

            for i_height in xrange(self._div_height+1):
                z = i_height * z_per_div

                for i_trans in xrange(self._div_trans+1):
                    r = self._radius + i_trans * r_per_div

                    # Calc Cartesian coordinates
                    x = r * np.cos(theta)
                    y = r * np.sin(theta)

                    coords.append([x, y, z])

        self._ptsarray = np.array(coords)

    def _generate_elements(self):
        """
        Generate the mesh connectivity, tesselated into hexahedra.
        """

        self._clear_mixedelem_data()

        # Number of points per slice in the circumferential direction
        pts_per_htdiv = self._div_trans + 1
        pts_per_circdiv = (self._div_trans + 1) * (self._div_height + 1)

        for i_circ in xrange(self._div_circ):

            circ_div = [i_circ     * pts_per_circdiv,
                        (i_circ+1) * pts_per_circdiv]

            # Wrap back to start of circle
            if i_circ + 1 == self._div_circ:
                circ_div[1] = 0

            for i_height in xrange(self._div_height):
                ht_offset = [i_height     * pts_per_htdiv,
                             (i_height+1) * pts_per_htdiv]

                bottom = i_height == 0
                top = i_height == self._div_height -1

                for i_trans in xrange(self._div_trans):
                    trans_offset = [i_trans, i_trans+1]

                    inside = i_trans == 0
                    outside = i_trans == self._div_trans - 1

                    points = []
                    for h in ht_offset:
                        for c in circ_div:
                            for t in trans_offset:
                                points.append(c + h + t)

                    self._mixedelem['hexa'].append(points)

                    if bottom:
                        self._register_face('hexa', 'bottom')
                    if top:
                        self._register_face('hexa', 'top')
                    if inside:
                        self._register_face('hexa', 'front', 'inside')
                    if outside:
                        self._register_face('hexa', 'back', 'outside')

    def _bc_nodes(self):
        """
        Determine nodes at 90 degree intervals on bottom face for BCs.

        Returns:
            list
                Indices of nodes at 0, 90, 180 and 270 degrees
        """

        # Get the points on the endocardium
        pts_endo = []
        for _, facepts in self.faces('inside'):
            pts_endo.append(facepts.flatten())
        pts_endo = np.concatenate(pts_endo)

        # Get the points on the base
        pts_bot = []
        for _, facepts in self.faces('bottom'):
            pts_bot.append(facepts.flatten())
        pts_bot = np.concatenate(pts_bot)

        # Get points in both
        candidates = np.intersect1d(pts_endo, pts_bot)

        # Get candidate coordinates
        coords = self.points()[candidates]

        # Get polar coordinate
        theta = np.arctan2(coords[:, 1], coords[:, 0])

        # Find BC nodes
        return candidates[closest_to_rangles(theta)]

    def _element_transmural_distance(self):
        centres = self.element_centres()
        r = np.sqrt((centres[:, :-1]**2).sum(axis=1))
        depth = (r - self._radius)/self._thickness
        return depth

    def _element_circum_direction(self):
        centres = self.element_centres()
        # Determine the circumferential direction vector
        circum = np.column_stack((-centres[:, 1], centres[:, 0],
                                  np.zeros(centres.shape[0])))
        # As unit vector
        circum /= np.sqrt((circum**2).sum(axis=1))[:, np.newaxis]
        return circum

    def _element_apical_basal_direction(self):
        return np.array([0., 0., 1.0])

if __name__ == '__main__':

    import argparse
    from carputils.mesh.fibres import linear_fibre_rule

    parser = argparse.ArgumentParser()

    parser.add_argument('meshname',
                        help='Basename for mesh output')
    parser.add_argument('--radius',
                        type=float, default=25.,
                        help='Internal radius of ring')
    parser.add_argument('--wall-thickness',
                        type=float, default=10.,
                        help='Transmural thickness of ring wall')
    parser.add_argument('--hexahedra',
                        action='store_true',
                        help='Generate ring with hexes rather than tets')

    args = parser.parse_args()

    # Generate a plus 60 - minus 60 fibre rule
    p60m60 = linear_fibre_rule(60, -60)

    faces = ['top', 'bottom', 'inside', 'outside']

    # Create ring object
    ring = Ring(args.radius, args.wall_thickness,
                div_transmural=5, div_height=2,
                tetrahedrise=not args.hexahedra,
                fibre_rule=p60m60)

    # Assign all nodes in x >= 0 new tag
    #xgeq0 = lambda coord: coord[0] >= 0.0
    #ring.add_region(2, xgeq0)

    # Generate CARP mesh
    ring.generate_carp(args.meshname, faces)

    # Generate VTK mesh
    ring.generate_vtk('{}.vtk'.format(args.meshname))
    for f in faces:
        ring.generate_vtk_face('{}_{}.vtk'.format(args.meshname, f), f)
