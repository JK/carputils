#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Class for generation of an ellipsoidal shell mesh.
"""
from sys import version_info
import numpy as np
from scipy.special import ellipeinc
from carputils.mesh.general import (Mesh3D, closest_to_rangles,
                                    distribute_elements)

if version_info.major > 2:
    xrange = range

def ellipse_t(ax_a, ax_b, theta):
    """
    Transform the central angle theta in an ellipse of semimajor axis a and
    semiminor axis b into the parameteric coordinate t. Valid on [-pi,pi].

    Args:
        ax_a : float
               Semimajor axis length
        ax_b : float
               Semiminor axis length
        theta : float
            Central angle, must be on [-pi, pi]

    Returns:
        float
            The ellipse patametric coordinate
    """

    assert -np.pi <= theta <= np.pi

    if theta > np.pi/2:
        return np.pi - np.arctan(np.tan(np.pi - theta) * ax_a / ax_b)

    elif theta < -np.pi/2:
        return -np.pi - np.arctan(np.tan(-np.pi - theta) * ax_a / ax_b)

    return np.arctan(np.tan(theta) * ax_a / ax_b)

def ellipse_arc_length(ax_a, ax_b, theta1, theta2):
    """
    Calculate the length of an ellipse arc between two angles.

    Args:
        ax_a : float
            Semimajor axis length
        ax_b : float
            Semiminor axis length
        theta1 : float
            Start of arc angle
        theta2 : float
            End of arc angle

    Returns:
        float
            The arc length
    """

    assert ax_a > ax_b, 'Semimajor axis must be longer than semiminor'

    # Calculate the ellipse eccentricity
    ecce = np.sqrt(1. - (ax_b/ax_a)**2)

    # Calculate the ellipse parametric corrindates
    t_1 = ellipse_t(ax_a, ax_b, theta1)
    t_2 = ellipse_t(ax_a, ax_b, theta2)

    # Calculate arc length using elliptic integral and return
    return ax_a * np.abs(ellipeinc(t_2, ecce) - ellipeinc(t_1, ecce))

class Ellipsoid(Mesh3D):
    """
    Generate a mesh of an ellipsoidal shell.

    An ellipsoidal shell is generated with hexahedral and prism elements,
    divided into tetrahedra if desired. The shell is defined in terms of its
    internal axes a, b and c, with the outer axes currently being a scalar
    multiple of those (thickness).

    v0 determines the azimuthal angle of the top (base) of the geometry, and
    v1 determines the azimuthal angle of the bottom (apex). By default, v1 is
    at the maximum of pi, and is therefore closed at that point. The mesh may
    be adjusted to have a flat (constant z coordiate) base place by use of the
    flat_top argument (on by default). Disabling this will case a constant-v
    base place.

    Mesh discretisation is controlled by the div_trans, div_vert and div_circ
    arguments. div_trans controls the number of hexahedra/prisms generated
    transmurally, div_vert controls the number of layers of elements are
    generated between the apex and base, and div_circ controls the number of
    hexahedra/prisms are in the layer at the equator (v=pi/2) layer of the
    ellipsoid. This is used to calculate a target circumferential element size,
    which is in turn used to determine the number of elements to be generated
    at other layers such that they are of a consistent size.

    Args:
        r_a : float
            Internal radius (x direction) of the ellipsoid shell
        r_b : float
            Internal radius (y direction) of the ellipsoid shell
        r_c : float
            Internal radius (z direction) of the ellipsoid shell
        aout : float
            External radius (x direction) of the ellipsoid shell, defaults to 1.25a
        bout : float
            External radius (y direction) of the ellipsoid shell, defaults to 1.25b
        cout : float
            External radius (z direction) of the ellipsoid shell, defaults to 1.15c
        v0 : float, optional
            Azimuthal angle of the top of the mesh, defaults to pi/3
        v1 : float, optional
            Aximuthal angle of the bottom of the mesh, defaults to pi (closed apex)
        div_trans : int, optional
            Number of elements across the radial thickness, defaults to 2
        div_vert : int, optional
            Number of layers of elements vertically, defaults to 25
        div_circ : int, optional
            Number of elements around the equator (v=pi/2), defaults to 50
        flat_top : bool, optional
            True to make a flat (constant z) top to the mesh, defaults to true
        cumulative_offset : bool, optional
            True to add a cumulative offset to the points, defaults to true
        tetrahedrise : bool, optional
            True to subdivide the mesh into tetrahedra
        fibre_rule : callable, optional
            Function describing the transmural variation of fibre angle, takes a
            single normalised transmural distance on [0,1] and returns a helix
            angle in radians, defaults to circumferentially oriented fibres
    """

    def __init__(self, r_a, r_b, r_c, aout=None, bout=None, cout=None,
                 v0=np.pi/3.0, v1=np.pi,
                 div_trans=2, div_vert=25, div_circ=50,
                 flat_top=True, cumulative_offset=True,
                 *args, **kwargs):

        Mesh3D.__init__(self, *args, **kwargs)

        self._r_a = float(r_a)
        self._r_b = float(r_b)
        self._r_c = float(r_c)

        self._a_outer = r_a * 1.25 if aout is None else aout
        self._b_outer = r_b * 1.25 if bout is None else bout
        self._c_outer = r_c * 1.15 if cout is None else cout

        self._v0 = float(v0)
        self._v1 = float(v1)

        self._flat_top = bool(flat_top)
        self._cumulative_offset = bool(cumulative_offset)

        self._trans_divs = int(div_trans)
        self._vert_divs = int(div_vert)
        self._circ_divs = int(div_circ)

    @classmethod
    def with_resolution(cls, radius, resolution, lengthratio=1.5,
                        wallthickness=None, apexthickness=None,
                        v0=np.pi/3.0, v1=np.pi, **kwargs):
        """
        Simplified interface to generate ellipsoid with target resolution.

        Args:
            radius : float
                Inner radius of ellipsoid cavity
            resolution : float
                Target mesh edge length
            lengthratio : float, optional
                Ratio of ellipsoid apical-basal radius to short axis-radius
                (default: 1.5)
            wallthickness : float, optional
                Thickness of ellipsoid wall (default: radius/5)
            apexthickness : float, optional
                Thickness of wall at apex (default: wallthickness)
        """

        # Determine inner axis lengths
        r_a = r_b = radius
        r_c = radius * lengthratio

        # Determine wall and apex thickness, when not set
        if wallthickness is None:
            wallthickness = radius / 5.
        if apexthickness is None:
            apexthickness = wallthickness

        # Determine outer axis lengths
        a_out = b_out = radius + wallthickness
        c_out = r_c + apexthickness

        # Determine number of divisions in each direction
        # At Equator
        circumference = 2. * np.pi * radius
        div_circ = int(round(circumference / resolution))
        # Transmural
        div_trans = int(round(wallthickness / resolution))
        # Vertical
        arc_length = ellipse_arc_length(r_c, r_a, v0, v1)
        div_vert = int(round(arc_length / resolution))

        # Generate object and return
        geom = cls(r_a, r_b, r_c, a_out, b_out, c_out, v0=v0, v1=v1,
                   div_trans=div_trans, div_vert=div_vert, div_circ=div_circ,
                   **kwargs)

        return geom

    def cavity_volume(self):
        """
        Calculate the volume of the cavity analytically.

        The actual volume may be less due to discretisation effects.

        Returns:
            float
                The cavity volume
        """

        # Get volume of non-truncated ellispoid
        full_vol = (4. * np.pi / 3.) * self._r_a * self._r_b * self._r_c

        # Get z coordinate of top and bottom
        z_top = self._r_c * np.cos(self._v0)
        z_bot = self._r_c * np.cos(self._v1)

        # Get distance from closest extremum
        if z_top >= 0:
            h_top = self._r_c - z_top
        else:
            h_top = -self._r_c - z_top
        if z_bot >= 0:
            h_bot = self._r_c - z_bot
        else:
            h_bot = -self._r_c - z_bot

        # Get volumes of caps
        # http://keisan.casio.com/has10/SpecExec.cgi?id=system/2006/1311572253
        factor = np.pi * self._r_a * self._r_b / (3 * self._r_c**2)
        v_top = factor * h_top**2 * (3 * self._r_c - h_top)
        v_bot = factor * h_bot**2 * (3 * self._r_c - h_bot)

        # Volume is full volume minus caps
        return full_vol - v_top - v_bot

    def _point_levels(self):
        # Figure out levels
        v_levels = np.linspace(self._v0, self._v1, self._vert_divs)
        # Bottom to top (i.e. decreasing v)
        v_levels = v_levels[::-1]

        # Determine number of points by level from circumference
        level_a = self._r_a * np.sin(v_levels)
        level_b = self._r_b * np.sin(v_levels)
        # Approximation from http://mathworld.wolfram.com/Ellipse.html:
        level_circ = np.pi * np.sqrt(2 * (level_a**2 + level_b**2))

        # Determine target division size
        equator_circ = np.pi * np.sqrt(2 * (self._r_a**2 + self._r_b**2))
        circ_division_size = equator_circ / self._circ_divs

        # Determine number of divisons per level
        level_divs = np.array(np.round(level_circ / circ_division_size), dtype=int)

        # Make sure never less than 3 divisions
        lt3 = level_divs < 3
        level_divs = level_divs * (1 - lt3) + 3 * lt3

        # Avoid any sharp transisitions in discretisation
        max_disc_ratio = 1.2
        # Bottom to top
        for i in xrange(len(level_divs) - 1):
            if level_divs[i+1] < level_divs[i] / max_disc_ratio:
                level_divs[i+1] = int(level_divs[i] / max_disc_ratio)
        # Top to bottom
        for i in xrange(len(level_divs) - 1, 0, -1):
            if level_divs[i-1] < level_divs[i] / max_disc_ratio:
                level_divs[i-1] = int(level_divs[i] / max_disc_ratio)

        # Reset ends to zero, if necessary, to indicate closure at poles
        if self._v0 == 0:
            level_divs[-1] = 0
        if self._v1 == np.pi:
            level_divs[0] = 0

        return v_levels, level_divs

    def _generate_points(self):

        trans_coord = np.linspace(0, 1, self._trans_divs + 1)

        v_levels, level_divs = self._point_levels()

        axes_lower = np.array([self._r_a, self._r_b, self._r_c])
        axes_upper = np.array([self._a_outer, self._b_outer, self._c_outer])
        d_axes = axes_upper - axes_lower

        coords = []

        cumulative_offset = 0.0

        for v_endo, circ_div in zip(v_levels, level_divs):

            if circ_div == 0:
                # Must be pole
                u_vals = np.array([0.])
            else:
                u_vals = 2*np.pi / circ_div * np.arange(circ_div, dtype=float)

                if self._tetrahedrise:
                    # Apply a half-spacing twist to each consecutive layer, to
                    # give better output tets
                    u_vals += cumulative_offset
                    if self._cumulative_offset:
                        cumulative_offset += np.pi/circ_div*0.5

            for u_endo in u_vals:
                cum_trans_offset = 0.0
                for t in trans_coord:
                    axes = axes_lower + t * d_axes

                    if self._flat_top:
                        # Calculate z coordinate of mesh top
                        z_top = axes_lower[2] * np.cos(self._v0)
                        # Get v coordinate of that z at this wall depth
                        v_trans_top = np.arccos(z_top / axes[2])
                        # Get the apex-to-base normalised v distance
                        v_norm = (v_endo - self._v0) / (self._v1 - self._v0)
                        # Re-interpolate v from apex to flat top
                        v = v_trans_top + v_norm * (self._v1 - v_trans_top)
                    else:
                        v = v_endo

                    if self._tetrahedrise and circ_div != 0:
                        u = u_endo - cum_trans_offset
                        # Smooth transistion from no adjustment at apex (where
                        # this can cause problems) to full adjustment in well
                        # populated layers
                        scale = 1. if circ_div >= 50 else circ_div * 1./50.
                        if self._cumulative_offset:
                            cum_trans_offset += np.pi/circ_div * scale
                    else:
                        u = u_endo

                    x = axes[0] * np.cos(u) * np.sin(v)
                    y = axes[1] * np.sin(u) * np.sin(v)
                    z = axes[2] * np.cos(v)

                    coords.append((x, y, z))

        self._ptsarray = np.array(coords)

    def _generate_elements(self):

        self._clear_mixedelem_data()

        _, level_divs = self._point_levels()

        rad_points = self._trans_divs + 1
        level_points = (level_divs + (level_divs == 0)) * rad_points

        prev_point_count = 0

        for elem_layer in xrange(len(level_divs) - 1):

            bottom = elem_layer == 0
            top = elem_layer == len(level_divs) - 2

            div_below = level_divs[elem_layer]
            div_above = level_divs[elem_layer+1]

            # Determine number of hexahedra and prisms
            div_diff = div_above - div_below

            slice_hexa = min([div_below, div_above])
            slice_prisms = abs(div_diff)

            # 'opening' as in it expands when moving from bottom to top
            prism_type = 'prism2' if div_diff > 0 else 'prism1'

            order = distribute_elements(slice_hexa, slice_prisms,
                                        'hexa', prism_type)

            lower_count = prev_point_count
            upper_count = prev_point_count + level_points[elem_layer]

            lower_max = lower_count + level_points[elem_layer]
            upper_max = upper_count + level_points[elem_layer+1]

            for etype in order:

                if etype == 'hexa':

                    inner = np.array([lower_count,
                                      lower_count + 1,
                                      lower_count + rad_points,
                                      lower_count + rad_points + 1,
                                      upper_count,
                                      upper_count + 1,
                                      upper_count + rad_points,
                                      upper_count + rad_points + 1])

                    # All hexahedra need both lower and upper count incremented
                    lower_count += rad_points
                    upper_count += rad_points

                    # Wrap to start correctly
                    inner[:4] -= (inner[:4] >= lower_max) * level_points[elem_layer]
                    inner[4:] -= (inner[4:] >= upper_max) * level_points[elem_layer+1]

                # Add chunk prisms
                elif etype == 'prism2':

                    inner = np.array([lower_count,
                                      lower_count + 1,
                                      upper_count + rad_points,
                                      upper_count + rad_points + 1,
                                      upper_count,
                                      upper_count + 1])

                    # Opening prism needs only top incremented
                    upper_count += rad_points

                    # Wrap to start correctly
                    inner[:2] -= (inner[:2] >= lower_max) * level_points[elem_layer]
                    inner[2:] -= (inner[2:] >= upper_max) * level_points[elem_layer+1]

                elif etype == 'prism1':

                    inner = np.array([lower_count,
                                      lower_count + 1,
                                      lower_count + rad_points,
                                      lower_count + rad_points + 1,
                                      upper_count,
                                      upper_count + 1])

                    # Closing prism needs only bottom incremented
                    lower_count += rad_points

                    # Wrap to start correctly
                    inner[:4] -= (inner[:4] >= lower_max) * level_points[elem_layer]
                    inner[4:] -= (inner[4:] >= upper_max) * level_points[elem_layer+1]

                for j in range(self._trans_divs):

                    inside = j == 0
                    outside = j == self._trans_divs - 1

                    # Transmural elem just need simple node increments
                    self._mixedelem[etype].append(inner + j)

                    if etype == 'hexa':
                        if inside:
                            self._register_face(etype, 'front', 'inside')
                        if outside:
                            self._register_face(etype, 'back', 'outside')
                        if bottom:
                            self._register_face(etype, 'bottom')
                        if top:
                            self._register_face(etype, 'top')
                    else:
                        if inside:
                            self._register_face(etype, 'front', 'inside')
                        if outside:
                            self._register_face(etype, 'back', 'outside')
                        if bottom and etype == 'prism1':
                            self._register_face(etype, 'base', 'bottom')
                        if top and etype == 'prism2':
                            self._register_face(etype, 'upperleft', 'top')

            prev_point_count += level_points[elem_layer]

    def _bc_nodes(self):
        """
        Determine nodes at 90 degree intervals on bottom face for BCs.

        Returns:
            list
                Indices of nodes at 0, 90, 180 and 270 degrees
        """

        # Get the points on the endocardium
        pts_endo = []
        for _, facepts in self.faces('inside'):
            pts_endo.append(facepts.flatten())
        pts_endo = np.concatenate(pts_endo)

        # Get the points on the base
        pts_base = []
        for _, facepts in self.faces('top'):
            pts_base.append(facepts.flatten())
        pts_base = np.concatenate(pts_base)

        # Get points in both
        candidates = np.intersect1d(pts_endo, pts_base)

        # Get candidate coordinates
        coords = self.points()[candidates]

        # Get u polar coordinate
        axes = np.array([self._r_a, self._r_b, self._r_c])
        u_polar = np.arctan2(coords[:, 1] / axes[1], coords[:, 0] / axes[0])

        # Find BC nodes
        return candidates[closest_to_rangles(u_polar)]

    def _element_transmural_distance(self):

        try:
            return self._transmural_distance_cache
        except AttributeError:
            pass

        centres = self.element_centres()

        # Initial guess of mid wall
        trans = np.ones(centres.shape[0]) * 0.5
        prev = np.zeros(centres.shape[0])

        # Get ellipsoid axes
        axes_lower = np.array([self._r_a, self._r_b, self._r_c])
        axes_upper = np.array([self._a_outer, self._b_outer, self._c_outer])
        d_axes = axes_upper - axes_lower

        while any(trans - prev > 0.0001):

            prev = trans

            # Get axes at current estimates
            axes = axes_lower + np.outer(trans, d_axes)

            # Estimate u and v from coords
            u = np.arctan2(centres[:, 1] / axes[:, 1], centres[:, 0] / axes[:, 0])
            v = np.arccos(_clamp(centres[:, 2] / axes[:, 2]))

            # Get min and max distance from centre at these coords
            r_0 = np.sqrt((axes_lower[0] * np.cos(u) * np.sin(v))**2
                          + (axes_lower[1] * np.sin(u) * np.sin(v))**2
                          + (axes_lower[2] * np.cos(v))**2)
            r_1 = np.sqrt((axes_upper[0] * np.cos(u) * np.sin(v))**2
                          + (axes_upper[1] * np.sin(u) * np.sin(v))**2
                          + (axes_upper[2] * np.cos(v))**2)

            # Get current r
            r = np.sqrt((centres**2).sum(axis=1))

            # Estimate new trans
            d_r = r_1 - r_0
            trans = (r - r_0)/d_r

        self._transmural_distance_cache = trans

        return trans

    def _element_circum_direction(self):

        centres = self.element_centres()
        trans = self._element_transmural_distance()

        # Get ellipsoid axes
        axes_lower = np.array([self._r_a, self._r_b, self._r_c])
        axes_upper = np.array([self._a_outer, self._b_outer, self._c_outer])
        d_axes = axes_upper - axes_lower
        axes = axes_lower + np.outer(trans, d_axes)

        # Get u and v
        u = np.arctan2(centres[:, 1] / axes[:, 1], centres[:, 0] / axes[:, 0])
        v = np.arccos(_clamp(centres[:, 2] / axes[:, 2]))

        # Get derivatives for circum direction
        dxdu = -axes[:, 0] * np.sin(u) * np.sin(v)
        dydu = axes[:, 1] * np.cos(u) * np.sin(v)
        dzdu = np.zeros(trans.shape)

        return np.column_stack((dxdu, dydu, dzdu))

    def _element_apical_basal_direction(self):

        centres = self.element_centres()
        trans = self._element_transmural_distance()

        # Get ellipsoid axes
        axes_lower = np.array([self._r_a, self._r_b, self._r_c])
        axes_upper = np.array([self._a_outer, self._b_outer, self._c_outer])
        d_axes = axes_upper - axes_lower
        axes = axes_lower + np.outer(trans, d_axes)

        # Get u and v
        u = np.arctan2(centres[:, 1] / axes[:, 1], centres[:, 0] / axes[:, 0])
        v = np.arccos(_clamp(centres[:, 2] / axes[:, 2]))

        # Get derivatives for circum direction
        dxdv = axes[:, 0] * np.cos(u) * np.cos(v)
        dydv = axes[:, 1] * np.sin(u) * np.cos(v)
        dzdv = -axes[:, 2] * np.sin(v)

        # Negative since we want -dx/dv
        return -np.column_stack((dxdv, dydv, dzdv))

    def generate_carp_apex_vtx(self, basename):
        """
        Generate vtx file with apex node.

        Args:
            basename : str
                Mesh base name (without extension)
        """

        coords = self.points()

        # Just get node with most negative z value
        node = coords[:, 2].argmin()

        # Write vtx file
        fname = '{}_apex.vtx'.format(basename)

        with open(fname, 'w') as f_p:
            # Header
            f_p.write('1\nintra\n')
            # Content
            f_p.write('{}\n'.format(node))

def _clamp(array):
    out_of_range = np.logical_or(array < -1, array > 1)
    return array * (1 - out_of_range) + out_of_range

if __name__ == '__main__':
    geom = Ellipsoid.with_resolution(50, 5, tetrahedrise=False)
    geom.generate_carp('ellipsoid', faces=['top', 'inside', 'outside'])
    geom.generate_vtk('ellipsoid.vtk')
