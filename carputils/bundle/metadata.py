#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
from os import path
import sys
import warnings 
from warnings import warn
from ruamel.yaml import YAML
from collections import OrderedDict
from datetime import date

from carputils import cml
from carputils import settings
from carputils import resources
from carputils.cml import cmltree

"""
This file contains functions allowing to create a metadata file for the bundle.
"""


# Cleaner warning messages
def _warning(message, category = UserWarning, filename = '', lineno = -1, file=None,  line=None):
    print('Warning: '+ str(message))
warnings.showwarning = _warning


METADATA_FILE = 'METADATA.yml'
METADATA_TPL_FILE = 'METADATA.yml.tpl'
yaml = YAML()


def load_metadata_file(exp_dir):
    """
    Creates an object containing metadata provided by the user in the
    file exp_dir/METADATA.yml and merge it with the metadata template.
    If this file doesn't exist, metadata template is loaded.

    Args:
        exp_dir : str
            directory where to find the metadata file.
    Returns:
        An object containing metadata.
    """
    # Load metadata template
    content = resources.load('METADATA.yml.tpl')
    metadata_obj = yaml.load(content)
    # Merge user metadata file with it if it exists
    if path.exists(path.join(exp_dir, METADATA_FILE)):
        with open(METADATA_FILE) as fp:
            metadata_user = yaml.load(fp)
            metadata_obj.update(metadata_user)
        check_critical_fields(metadata_obj)
    else:
        warn("""You don't provide any metadata for your experiment.
        An autopopulated METADATA.yml file was created in the bundle, but you should complete it.
        In the future, please consider creating a metadata file in the current directory by
        running {}, and filling it. 
        This file will then be automatically added in the bundles you create.""".format(settings.execs.GENERATEMETADATA))
    return metadata_obj


def check_critical_fields(metadata_obj):
    """
    Raise warnings if metadata template fields were not filled by the user.
    """
    if metadata_obj['title'] is None:
        warn("Please set up the 'title' field in METADATA.yml.")


def autopopulate_template(metadata_obj):
    """
    Automatically fills some fields in metadata object.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    metadata_obj = autopopulate_creators_names(metadata_obj)
    metadata_obj = autopopulate_date(metadata_obj)
    return metadata_obj


def autopopulate_creators_names(metadata_obj):
    """
    Create full name of the creators from 'given_name' and 'family_name' fields.
    If no name is given for the first creator, it is autopopulated with the user
    data in carputils settings file.
    It also fills the 'rights_holder' field with the names of the creators.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    if 'creators' in metadata_obj:
        if isinstance(metadata_obj['creators'], list):
            counter = 0
            for creator in metadata_obj['creators']:
                if 'name' not in creator or creator['name'] is None:
                    name = ''
                    if 'given_name' in creator:
                        if creator['given_name'] is not None:
                            name += creator['given_name'] + ' '
                    if 'family_name' in creator:
                        if creator['family_name'] is not None:
                            name += creator['family_name']
                    creator['name'] = name
                    # If the name of the first creator is not provided, it is autopopulated
                    if counter == 0 and name == '':
                        warn("Please add a creator to the 'creators' field in METADATA.yml")
                        creator['name'] = settings.config['NAME'] if settings.config['NAME'] is not None else 'Your Name'
                        creator['orcid'] = settings.config['ORCID'] if settings.config['ORCID'] is not None else '0000-0000-0000-0000'
                counter += 1
            # Fill the rights_holder field with a string containing the names of the creators
            if metadata_obj['rights_holder'] is None:
                rights_holder_list = [creator['name'] for creator in metadata_obj['creators']]
                metadata_obj['rights_holder'] = ', '.join(rights_holder_list)
    return metadata_obj


def autopopulate_date(metadata_obj):
    """
    Fills 'date' and 'publication_year' fields in metadata.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    today = date.today()
    # YYYY-MM-DD
    metadata_obj['publication_year'] = today.strftime("%Y")
    metadata_obj['date'] = today.strftime("%Y-%m-%d")
    return metadata_obj


def add_env_metadata(metadata_obj):
    """
    Adds to an existing metadata object the environment metadata included by
    default in the bundle.

    Args:
        metadata_obj :
            an object containing metadata.
    Returns:
        The updated metadata object.
    """
    metadata_obj = add_config_metadata(metadata_obj, 'NAME', 'bundle_creator_name')
    metadata_obj = add_config_metadata(metadata_obj, 'EMAIL', 'bundle_creator_email')
    metadata_obj = add_config_metadata(metadata_obj, 'ORCID', 'bundle_creator_orcid')
    pyinfo = '{}.{}.{}'.format(sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
    metadata_obj['python_version'] = pyinfo
    metadata_obj['opencarp_git_hash'] = get_opencarp_git_hash()
    return metadata_obj


def add_config_metadata(metadata_obj, name, metadata_name=None):
    """
    Adds a specfic field contained in carputils.config in a metadata object.
    If the field was manually set by the user, it will not be replaced.

    Args:
        metadata_obj :
            the object containing metadata.
        name : str
            the name of the carputils.config field to add to the metadata object.
        metadata_name : str
            name of the field in the metadata object
    Returns:
        the updated metadata object.
    """
    if metadata_name is None:
        metadata_name = name
    if metadata_name not in metadata_obj:
        metadata_obj[metadata_name] = settings.config[name]
    return metadata_obj


def add_exp_name(metadata_obj, exp_name):
    """
    Adds the experiment name to the metadata object.

    Args:
        metadata_obj :
            the object containing metadata
    Returns:
        The updated metadata object
    """
    res_metadata_obj = metadata_obj
    res_metadata_obj['experiment_name'] = exp_name
    return res_metadata_obj


def add_command_line(metadata_obj, exp_filename):
    """
    Adds the command line that should be used to run the same experiment
    to a metadata object.

    Args:
        metadata_obj :
            Object containng metadata
    Returns:
        Updated metadata object
    """

    res_metadata_obj = metadata_obj
    # Get the parameters used for creating the bundle
    cl_parameters = sys.argv[1:]
    # Remove the options related to bundling from the command line
    rm_indices = []
    for ind, param in enumerate(cl_parameters):
        if param.startswith('--bundle') or (param.startswith('--') and param.endswith('bundle')):
            rm_indices.append(ind)
            if len(cl_parameters) > ind+1 and not cl_parameters[ind+1].startswith('-'):
                rm_indices.append(ind+1)
    for ind in reversed(rm_indices):
        del cl_parameters[ind]
    # Add the command line to the metadata
    res_metadata_obj['experiment_command_line'] = exp_filename + ' ' + ' '.join(cl_parameters)
    return res_metadata_obj


def analyze_metadata(metadata_obj):
    """
    Analyzes the metadata and trigger warnings in case some critical metadata is missing.

    Args:
        metadata_obj :
            Object containing metadata
    """
    if metadata_obj['bundle_creator_name'] is None:
        warn("""Bundle creator name is not set and won't be added to the bundle metadata.
        Please consider setting your name in the carputils settings file.""")


def get_opencarp_git_hash():
    """
    Get the git hash of the openCARP version.

    Returns:
        the git hash as a string
    """
    f = os.popen(str(settings.execs.CARP) + ' -buildinfo')
    line_git_hash = f.read().splitlines()[0]
    f.close()
    git_hash = line_git_hash.split()[-1]
    return git_hash
