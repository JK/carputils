import webbrowser
import sys
if sys.version_info.major > 2:
    import urllib.parse as urlparse
else:
    import urllib as urlparse


def create_release_email(git_repo, user_email, user_name):
    """
    Opens an email client containing the email that has to be sent in order to make a release from the experiment.

    Args:
        git_repo : str
            Address of the git repository of the experiment
        user_email : str
            Email address of the user
        user_name : str
            Name of the user
    """
    to = 'opencarp-experiment-curation@lists.kit.edu'
    subject = 'New experiment to release'
    body = '''Dear opencarp.org admins,
    
I would like to create a release from the experiment that was uploaded on the following git repository:
{} 

If you need more information, please get in touch with me at: {}

Best regards,
{}
'''.format(git_repo, user_email, user_name)

    print("In order to ask for a release of your experiment, please send the following e-mail (click on the link or paste in in your web browser):")
    open_email_client(to, subject, body)


def open_email_client(to, subject, body):
    """
    Opens an email client with a pre-filled email.

    Args:
        to : str
            email address of the recipient
        subject : str
            Subject of the email
        body : str
            Content of the email body
    """

    email = "mailto:?to={}&subject={}&body={}".format(to, urlparse.quote(subject), urlparse.quote(body))

    # Automatically opens the mail client of the user with the generated email
    # This can be problematic if the user doesn't have a graphical interface...
    #try:
    #    webbrowser.get()
    #    webbrowser.open(email, new=1)
    #except webbrowser.Error:
    #    print("It seems that your e-mail client can't be open.")
    #    print("In order to ask for a release of your experiment, please send the following e-mail:")
    #    print(email)

    # A safer option is this one, providing the email url instead of trying to open the mail client:
    # It is yet less user-friendly
    print(email)
