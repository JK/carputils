#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some tools to assist in setting up electrophysiology simulations.
"""

import os
from math import log
from carputils.resources import trace
from carputils.carpio import dat2adj
from carputils import settings

# The EP model types, in the order that they are indexed by the openCARP -bidomain
# argument
MODEL_TYPES = ['monodomain', 'bidomain', 'pseudo_bidomain']
# List of supported propagation types
# R-D : standard reaction-diffusion model
# R-E+: reaction-eikonal model with diffusion on
# R-E-: reaction-eikonal model with diffusion off
# AP-E: action potential coupled to eikonal activation sequence
PROP_TYPES  = ['R-D', 'R-E+', 'R-E-', 'AP-E' ]


def model_type_opts(name, depth=None):
    """
    Generate the EP model type openCARP options.

    Parameters
    ----------
    name : str
        The name of the EP model type

    Returns
    -------
    list
        List of openCARP command line options to set up this model type
    """

    source = ['-bidomain', MODEL_TYPES.index(name)]

    if name == 'pseudo_bidomain':
        if 'open' in settings.execs.carp.__str__():
            print('WARNING: augmentation is not available with pseudo-bidomain in openCARP!')
        else:
            # use augmentation, set depth
            if depth is not None:
                d = depth
                source += ['-augment_depth', d]

    return source


def fake_ep_opts(stimIdx, act_sequence, pulse_file=None, num_pulses=1, bcl=400.0, stim_duration=2.0):
    """
    Set up a single stimulus for a 'fake EP' eikonal-bidomain simulation.

    Parameters
    ----------
    act_sequence : str
        Filename of the activation sequence to use
    pulse_file : str or bool, optional
        Filename, without the '.trc' extension, of a pulse shape to use, or True
        to use a standard shaped foot

    Returns
    -------
    list
        List of openCARP command line options to set up the fake EP
    """

    # Switch to prescribed AP stimulation
    stimtype = 8
    datafile = act_sequence

    tpl = '-stimulus[%d].{}'%(stimIdx)
    opts = [tpl.format('stimtype'), stimtype]

    if len(datafile) > 0 :
       opts += [tpl.format('data_file'), datafile]

    # Directly read in a prescribed foot
    if pulse_file is not None:
        if pulse_file == True:
            pulse_file = trace.path('standard_ap_foot')

        if not os.path.exists(str(pulse_file) + '.trc'):
            msg = ('pulse_file {}.trc does not exist, remember to omit the '
                   '.trc file extension').format(pulse_file)
            raise Exception(msg)
        opts += [tpl.format('pulse_file'), pulse_file]

    if num_pulses > 1 and bcl > 0.0:
        opts += [tpl.format('npls'), num_pulses,
                 tpl.format('bcl'), bcl]
    if stim_duration > 0.0:
         opts += [tpl.format('duration'), stim_duration]

    return opts, stimIdx+1


# --- set up propagation model ------------------------------------------------------
def setupPropagation(mode, numStims, stims, actSeq, pulse=None, IDs=None, names=None):
    """
    Set up propagation model 

    Parameters
    ----------
    mode : str
        Name of propagation mode, either R-D, R-E+ or R-E-, or AP-E
    numStims : int
        Current number of stimuli already added to a simulation setup
    stims: list
        list holding all information on previously added stimulus definitions
    actSeq: str
        Filename of a precomputed activation sequence to use
    IDs : list of integers (optional, only for setting up AP-E mode)
        list of region IDs for each imp_region
    names: list (optional, only for setting up AP-E mode)
        names of imp_regions


    Returns
    -------
    propOpts: list
        List of openCARP command line options to set up propagation model
    numStims: int
        updated number of stimuli
    stims: list
        list of stimuli commands needed for setting up the propagation model
    """

    propOpts = []
 
    if mode == 'R-D':
        propOpts += [] 
    else:
      if 'R-E' in mode:

        ekStim, numStims = fake_ep_opts(numStims, actSeq, pulse)
        stims    += ekStim

        # in this case we turn off diffusion while propagation is ongoing
        if mode == 'R-E-':
          diff = ['-diffusionOn', 0 ]
        else:
          diff = ['-diffusionOn', 1 ]

        # forced foot allows larger time stepping
        dt = 100.

        propOpts += diff + [ '-dt', dt ]

      elif mode == 'AP-E':
        propOpts += setup_AP_E(actSeq, IDs, names)
        propOpts += ['-diffusionOn',    0, 
                     '-dt',          1000 ]

      else:
        raise Exception('Unsupported type of propagation model') 

    return propOpts, numStims, stims


# --- set propagation with His Purkinje system------------------------------------------------
def setupPropagation_wHPS(mode, HPS, HPS_ek, numStims, stims, actSeq, pulse=None, IDs=None, names=None):
    """
    Set up propagation model with His-Purkinje system

    Parameters
    ----------
    mode : str
        Name of propagation mode, either R-D, R-E+ or R-E-, or AP-E
    HPS  : list 
        HPS version used for R-D simulations
    HPS_ek: list
        HPS version used for R-E simulations (often a coarser version of the R-D HPS)
    numStims : int
        Current number of stimuli already added to a simulation setup
    stims: list
        list holding all information on previously added stimulus definitions
    actSeq: str
        Filename, without the '.trc' extension, of a pulse shape to use, or True
        to use a standard shaped foot

    Returns
    -------
    propOpts: list
        List of openCARP command line options to set up propagation model
    numStims: int
        updated number of stimuli
    stims: list
        list of stimuli commands needed for setting up the propagation model
    """

    propOpts = HPS
 
    if 'R-E' in mode:
      propOpts += HPS_ek

      ekStim, numStims = fake_ep_opts(numStims, actSeq, pulse)
      stims    += ekStim

      # in this case we turn off diffusion while propagation is ongoing
      if mode == 'R-E-':
        diff = ['-diffusionOn', 0 ]
      else:
        diff = ['-diffusionOn', 1 ]
          
      propOpts += diff 

    elif mode == 'AP-E':
      propOpts += setup_AP_E(actSeq, IDs, names)
      propOpts += ['-diffusionOn', 0]

    return propOpts, numStims, stims


# --- set up AP eikonal propagation ----------------------------------------------------
def setup_AP_E(act_seq_file, IDs, names=None):
    """
    Set up AP-E propagation mode where an AP is prescribed to each individual imp_region
    of a given simulation. Note that the lists IDs and names (if given) must be of the same
    length or longer as any imp_region defined in the input parameter file

    Parameters
    ----------
    actSeq: str
        Filename, without the '.trc' extension, of a pulse shape to use, or True
        to use a standard shaped foot
    IDs : list of integers
        list of unique AP ID for each imp_region. As example, if there are three imp_regions,
        and we use the list IDs=[10, 20, 30], AP's must be stored in files named AP_10.trc,
        AP_20.trc and AP_30.trc.
    names: list (optional)
        names of imp_regions

    Returns
    -------
    AP_E_Opts: list
        List of openCARP command line options to set up AP-E propagation model
    """

    # reconfigure ionic models
    imps = []
    irtpl = '-imp_region[{}].{}'
    for i, val in enumerate(IDs):
        if names is not None:
            imps += [ irtpl.format(i,'name'), names[i] ]

        imps += [ irtpl.format(i,'im'), 'IION_SRC',
                  irtpl.format(i,'im_param'), 'AP_ID=%d'%(val) ]


    # convert activation sequence to nodal adjustment file
    adjFile = os.path.join(os.path.dirname(act_seq_file), 'vm_act_seq.adj')
    dat2adj(act_seq_file, adjFile, grid='intra')

    tactAdj = ['-num_adjustments',        1,
               '-adjustment[0].variable', 'IION_SRC.tact',
               '-adjustment[0].file',     adjFile,
               '-adjustment[0].dump',     1,]
 
    AP_E_Opts = imps + tactAdj

    return AP_E_Opts



