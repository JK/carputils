#!/usr/bin/env python
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
tuning

tune conductivity settings to match prescribed conduction velocities
"""


import os
import sys
import json
import numpy
from carputils import format
from carputils import settings
from carputils import tools
from carputils import ep
from carputils import stream
from carputils import mesh
from carputils import lats
from carputils import carpio
from numpy import nonzero, mean, loadtxt, all 
import matplotlib.pyplot as plt


# determine which Python is used
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False

class CVtuning():
    """
    class for managing CV tuning

    Parameters
    ----------

    Mesh parameters
    ---------------

    sres : float
           resolution of strand in um

    slen : float
           length of strand in cm


    Ionic model parameters
    ----------------------

    model  : name of ionic model
             default name is MBRDR

    mpar   : model parameter list
             default is empty

    plugin : name of plugin
             default is empty

    ppar   : plugin parameter list
             default is empty

    svinit : initial state vector file
             default is empty

    Tuning parameters
    -----------------

    vel : float
          desired velocity in m/s

    gi  : float
          intracellular conductivity in S/m
          default is 0.174 S/m

    ge  : float
          extracellular conductivity in S/m
          default is 0.625 S/m

    beta : float
           surface-to-volume ratio in 1/cm

    beqm:  int
           switch between harmonic mean and intracellular monodomain tensor
           0: intracellular monodomain tensor
           1: bidomain equivalent harmonic mean monodomain tensor

    conv : bool
           converge to prescribed velocity

    ctol : float
           stop tolerance for CV tuning

    Numerical parameters
    --------------------

    dt   : float
           integration time step in us

    ts   : int
           time stepping method
           0: explicit
           1: Crank-Nicolson

    stol : float
           solver tolerance

    lump : bool
           toggle mass lumping flag

    opsplt : bool
             toggle operator splitting flag

    srcMod : int
             select source model
             0: monodomain
             1: bidomain
             2: pseudo-bidomain
    """

    def __init__(self, vel=0.6, logfile='tuneCV.log', cvfile='results.dat', tablefile='table.dat'):

        # geometry for testing CVs
        self.sres       = 100.0
        self.slen       =   1.0
        self.geom       = None
        self.meshfiles  = None
        self.surf       = False

        # ionic setup
        self.model       = 'MBRDR'
        self.mpar        = ''
        self.plugin      = ''
        self.ppar        = ''
        self.svinit      = ''

        # conductivity/velocity tuning
        self.vel         =  vel    # m/s
        self.gi          =  0.174  # S/m
        self.ge          =  0.625  # S/m
        self.beta        =  0.14   # 1/cm
        self.beqm        =  1
        self.conv        =  False
        self.ctol        =  0.01   # relative tolerance for converged CV
        self.max_it      = 20.     # maximum number of iterations

        # CV restitution
        self.pcl         = None
        self.CVs         = None

        # tuned parameters
        self.vel_        = -1.
        self.gi_         = self.gi
        self.ge_         = self.ge

        # numerical settings
        self.dt          = 10.     # us
        self.ts          =  0      # time stepper
        self.stol        = 1e-9    # solver tol
        self.lump        = False   # mass lumping
        self.opsplt      = True    # operator splitting on
        self.srcMod      = 'monodomain'

        # mode of initiation of propagation
        self.stimS       = 250.    # Itr in uA/cm^2
        self.stimV       = False   # stimulate by voltage clamp

        # logging
        self.log         = logfile
        self.last_it     = -1

        # results
        self.dat         = cvfile
        self.table       = tablefile
        self.cv_res      = 'cv_restitution.dat'

        # visualization
        self.visualize   = False   # visualize CV experiment

        # remove old log file
        if os.path.isfile(logfile):
            os.remove(logfile)

    def config(self,args):

        # geometry for testing CVs
        self.sres        = args.resolution
        self.slen        = args.length
        self.surf        = args.surf

        # ionic setup
        self.model       = args.model
        self.mpar        = args.modelpar
        self.plugin      = args.plugin
        self.ppar        = args.plugpar
        self.svinit      = args.svinit

        # conductivity/velocity tuning
        self.vel         = args.velocity # m/s
        self.gi          = args.gi       # S/m
        self.ge          = args.ge       # S/m
        self.gi_         = args.gi       # S/m
        self.ge_         = args.ge       # S/m
        self.beta        = args.beta     # 1/cm
        self.beqm        = args.beqm     # bidomain equivalent monodomain
        self.conv        = args.converge # measure velocity only or tune
        self.mode        = args.mode     # tune harmonic mean or intracellular conductivity   
        self.ctol        = args.tol      # relative tolerance for converged CV
        self.max_it      = args.maxit    # maximum number of iterations

        # restitution settings
        self.pcl         = args.pcl      # list of pacing cycle length for CV restitution testing
        self.CVs         = []            # list of measured CVs for each tested pcl

        # numerical settings
        self.dt          = args.dt       # us
        self.ts          = args.ts       # time stepper
        self.stol        = args.stol     # solver tol
        self.lump        = args.lumping  # mass lumping
        self.opsplt      = args.opsplit  # operator splitting on
        self.srcMod      = args.sourceModel
        self.stimS       = args.stimS    # Strength of the stimulus
        self.stimV       = args.stimV    # use voltage clamp to initate propagation

        # logging
        self.log         = args.log
        self.table       = args.table
        self.cv_res      = args.cvres

        # visualization
        self.visualize   = args.visualize


    def configCells(self):
        """
        setup ionic model we want to use for CV tuning
        """

        ion_set = ['-imp_region[0].cellSurfVolRatio', self.beta,
                   '-imp_region[0].im',               self.model,
                   '-imp_region[0].im_param',         self.mpar,
                   '-imp_region[0].plugins',          self.plugin,
                   '-imp_region[0].plug_param',       self.ppar ]

        if self.svinit != '':
            # check whether file exists
            if not os.path.isfile(self.svinit):
                print('Given {} does not exist! Aborting...'.format(self.svinit))
                exit(1)
            ion_set += ['-imp_region[0].im_sv_init', self.svinit]

        return ion_set

    def hmean(self):
        """
        Compute harmonic mean conductivity
        """

        if self.conv:
          gm = self.gi_*self.ge_/(self.gi_ + self.ge_)
        else:
          gm = self.gi*self.ge/(self.gi + self.ge)

        return gm

    def configConductivities(self):
        """
        Define myocardial conductivities
        """

        if self.conv==True:
            gi = self.gi_
            ge = self.ge_
        else:
            gi = self.gi
            ge = self.ge

        cset = ['-gregion[0].g_il', gi,
                '-gregion[0].g_el', ge,
                '-bidm_eqv_mono', self.beqm ]

        return cset

    def configNumerics(self):
        """
        Define numerical settings for tuning experiment
        """
        num_set  = [ '-dt',                 self.dt,
                     '-parab_solve',        self.ts,
                     '-cg_tol_parab',       self.stol,
                     '-mass_lumping',       int(self.lump),
                     '-operator_splitting', int(self.opsplt) ]

        return num_set

    def summary(self):
        """
        Output summarized user settings
        """
        print('\n\n')
        print('Conduction velocity testing - parameter summary')
        print('_'*format.terminal_width() + '\n')

        print('# system settings')
        print('Python {}'.format(2 if isPy2 else 3))
        print('\n')

        print('# mesh settings')
        print('resolution : {} um'.format(self.sres))
        print('length     : {} cm'.format(self.slen))
        print('surf       : {}'.format(self.surf))
        print('\n')

        print('# ionic model settings')
        print('model     : {}'.format(self.model))
        print('model-par : {}'.format(self.mpar))
        print('plugin    : {}'.format(self.plugin))
        print('plug-par  : {}'.format(self.ppar))
        print('svinit    : {}'.format(self.svinit))
        print('\n')

        print('# conductivity/velocity settings')
        print('velocity : {} m/s '.format(self.vel))
        print('gil      : {} S/m '.format(self.gi))
        print('gel      : {} S/m '.format(self.ge))
        print('beta     : {} 1/cm'.format(self.beta))
        print('beqm     : {}     '.format(self.beqm))
        print('\n')

        print('# numerical settings')
        print('dt                : {} us'.format(self.dt))
        print('ts                : {}'.format(self.ts))
        print('solver tol        : {}'.format(self.stol))
        print('mass lumping      : {}'.format(self.lump))
        print('op splitting      : {}'.format(self.opsplt))
        print('source model      : {}'.format(self.srcMod))
        print('stimulus strength : {}'.format(self.stimS))

        print('\n')

    def carp_params(self):    
        print('\n\n')
        print('Translate to carp parameters - parameter summary')
        print('_'*format.terminal_width() + '\n')

        # for now we fix imp_region and gregion to index 0
        region = 0

        print('# ionic model settings')
        print('imp_region[{}].im               = {}'.format(region,self.model))
        # check of provided first
        if len(self.mpar.strip()):
            print('imp_region[{}].im_param         = {}'.format(region,self.mpar))
        if len(self.plugin.strip()):
            print('imp_region[{}].plugins          = {}'.format(region,self.plugin))
        if len(self.ppar.strip()):
            print('imp_region[{}].plug_param       = {}'.format(region,self.ppar))
        if len(self.svinit.strip()):
            print('imp_region[{}].im_sv_init       = {}'.format(region,self.svinit))
        print('imp_region[{}].cellSurfVolRatio = {}'.format(region,self.beta))
        print('\n')

        print('# numerical settings')
        print('dt                 = {}'.format(self.dt))
        print('parab_solve        = {}'.format(self.ts))
        print('cg_tol_parab       = {}'.format(self.stol))
        print('mass_lumping       = {}'.format(self.lump))
        print('bidm_eqv_mono      = {}'.format(self.beqm))

        if self.opsplt:
            splt = 1
        else:
            splt = 0
        print('operator_splitting = {}'.format(splt))

        if self.srcMod == 'bidomain':
            bidm = 1
        elif self.srcMod == 'pseudo_bidomain':
            bidm = 2
        else:
            bidm = 0
        print('bidomain           = {}'.format(bidm))
        print('\n')

        print('# conductivity/velocity settings')
        direction = 'f'

        if direction == 'f':
            # longitudinal, along the fiber
            print('gregion[{}].g_il = {}'.format(region,self.gi))
            print('gregion[{}].g_el = {}'.format(region,self.ge))
        elif direction == 't':
            # transversely isotropic
            print('gregion[{}].g_it = {}'.format(region,self.gi))
            print('gregion[{}].g_et = {}'.format(region,self.ge))
            print('gregion[{}].g_in = {}'.format(region,self.gi))
            print('gregion[{}].g_en = {}'.format(region,self.ge))
        elif direction == 's':
            # orthotropic, transverse along sheet direction
            print('gregion[{}].g_it = {}'.format(region,self.gi))
            print('gregion[{}].g_et = {}'.format(region,self.ge))
        elif direction == 'n':
            # orthotropic, transverse along sheet normal direction
            print('gregion[{}].g_in = {}'.format(region,self.gi))
            print('gregion[{}].g_en = {}'.format(region,self.ge))

        print('\n')

    def report(self):
        """
        fill dictionaries and output to log file
        """
        # check whether the report file exists
        tf = os.path.exists(self.table)

        header = '{:5s}\t{:3s}\t{:5s}\t' \
                 '{:4s}\t{:6s}\t'\
                 '{:6s}\t{:4s}\t{:4s}\t{:4s}\t'\
                 '{:2s}\t{:10s}\t{:5s}\t'\
                 '{:5s}\t{:6s}\t{:8s}\t' \
                 '{:12s}\t{:12s}\t{:12s}\t{:12s}'\
                 '\n'.format('res', 
                             'len', 
                             'srf', 
                             'vel', 
                             'gi',
                             'ge', 
                             'beta', 
                             'beqm', 
                             'dt',
                             'ts', 
                             'stol', 
                             'lump',
                             'splt', 
                             'CV',
                             'imp',
                             'imp_par', 
                             'plug', 
                             'plug_par',
                             'sv_init', )

        tpl = '{:4.1f}\t{:2.1f}\t{:5s}\t' \
              '{:1.3f}\t{:6.4f}\t' \
              '{:6.4f}\t{:4.2f}\t{:4d}\t{:4d}\t' \
              '{:2d}\t{:1.2e}\t{:5s}\t' \
              '{:5s}\t{:6.4f}\t{:8s}\t' \
              '{:12s}\t{:12s}\t{:12s}\t' \
              '{:12s}\n'

        with open(self.table, 'a') as fp:
            # append to parameters/results file
            if not tf:
                fp.write(header)

            # export current results
            fp.write(tpl.format(self.sres, self.slen, str(self.surf), 
                                self.vel_, self.gi_,
                                self.ge_, self.beta, self.beqm, int(self.dt),
                                self.ts, self.stol, str(self.lump),
                                str(self.opsplt), self.vel_, self.model,
                                self.mpar, self.plugin, self.ppar,
                                self.svinit))

            if not settings.cli.silent:
                print('Conduction velocity: {:.4f} m/s [gi={:.4f}, ge={:.4f}, gm={:.4f}]'.format(self.vel_, self.gi_, self.ge_, self.hmean()))
        return

    def saveCVResults(self):
        """
        write CV restitution results to .dat file
        """

        with open(self.cv_res, 'w') as fcvres:
            for i,cv in enumerate(self.CVs):
                fcvres.write('%4.1f %1.3f\n' % (self.pcl[i], cv))
            fcvres.close()


    def plotCVRestitutionCurve(self):

        plotCVRestitution([self.cv_res], ['label'])


    def saveTuningResults(self):
        """
        write tuning results to .dat file
        """

        # keep for reasons of backward compatibility
        cvf = '{:.4f} {:.5f} {:.5f} {:.5f} \n'

        with open(self.dat, 'a') as fcv:
            fcv.write(cvf.format(self.vel_,self.gi_,self.ge_, self.hmean() ))

        # better store parameters and tuning results in json format
        with open('tune.json', 'w') as fp:
            if not self.pcl:
                results = {'vel'       : self.vel_,
                           'gi'        : self.gi_,
                           'ge'        : self.ge_,
                           'gm'        : self.hmean()}
            else:
                # for restitution, add CV restitution file to results
                results = {'cv_res' : self.cv_res,
                           'pcls'   : self.pcl,
                           'cvs'    : self.CVs }
               
            params  = {'resolution' : self.sres, 
                       'length'     : self.slen,
                       'surf'       : str(self.surf),
                       'imp'        : self.model,
                       'imp_param'  : self.mpar,
                       'plugin'     : self.plugin,
                       'plug_par'   : self.ppar,
                       'sv_init'    : self.svinit,
                       'velocity'   : self.vel,
                       'gi'         : self.gi,
                       'ge'         : self.ge,
                       'beta'       : self.beta,
                       'beqm'       : self.beqm,
                       'dt'         : int(self.dt),
                       'parab_solve': self.ts,
                       'solver_tol' : self.stol,
                       'lumping'    : str(self.lump),
                       'opsplit'    : str(self.opsplt)}

            out     = {'results' : results,
                       'params'  : params }

            json.dump(out,fp,indent=4)

    def geomStrand(self):
        """
        Build strand-like FE mesh
        """
        # resolution in mm
        res = float(self.sres/1000.)

        # define strand size
        cm2mm = 10.
        self.geom  = mesh.Block(size=(self.slen*cm2mm, res, res), resolution=res)
        if self.surf:
            self.geom  = mesh.Block(size=(self.slen*cm2mm, res, 0.0), resolution=res)

        # Set fibre angle to 0, sheet angle to 0
        #self.geom.set_fibres(0, 0, 90, 90)

        with open(self.log, 'w') as fp:
            with stream.divert_std(fp):
                self.meshfiles = mesh.generate(self.geom)

        if not settings.cli.silent:
            print('\n\n')

        return self.geom


    def pcl2triggers(self):
        """
        Turn pcl list into stimulus trigger list, assuming a first trigger at t start = 0
        """

        start = 0.
        trgs  = [ start ]
        last  =   start
        for t in self.pcl:
            last = last + t
            trgs += [ last ]

        return trgs


    def configStimLHFaceNew(self):
        """
        Add stimulus definition to initiate propagation at the lhs face of the strand
        """

        lhface = mesh.block_bc_opencarp(self.geom, 'stim', 0, 'x', True)

        idx  = 0
        if self.stimV:
            # we clamp for 5 ms to be sure to initiate, if possible at all
            stim = ['-stim[%d].ptcl.duration'%(idx),  5.,
                    '-stim[%d].crct.type'%(idx),      9,
                    '-stim[%d].pulse.strength'%(idx), 0. ]
        else:
            # we compute stimulus duration to lift voltage by 150 mV
            sdur = 150/self.stimS
            stim = ['-stim[%d].ptcl.duration'%(idx),  sdur,
                    '-stim[%d].crct.type'%(idx),      0,
                    '-stim[%d].pulse.strength'%(idx), self.stimS ]

        # stimulus protocol 
        start = 0.
        stim += ['-stim[%d].ptcl.start'%(idx), start ]

        if self.pcl is not None:
            # turn pcl list into stimulus triggers
            trgs = self.pcl2triggers()
           
            # stim trigger list to csv list string
            trg_str = [str(t) for t in trgs]
            cslist = ",".join(trg_str)

            stimlist = '\'' + cslist + '\''
            stim += ['-stim[%d].ptcl.stimlist'%(idx), stimlist ]
            stim += ['-stim[%d].ptcl.npls'    %(idx), len(self.pcl)  ]

        else:
            stim += ['-stim[%d].ptcl.npls'    %(idx),    1  ]

        return ['-num_stim', 1 ] + stim + lhface

    def configStimLHFace(self):
        """
        Add stimulus definition to initiate propagation at the lhs face of the strand
        """

        lhface = mesh.block_boundary_condition(self.geom, 'stimulus', 0, 'x', True)

        idx  = 0
        if self.stimV:
            # we clamp for 5 ms to be sure to initiate, if possible at all
            stim = ['-stimulus[%d].duration'%(idx),   5.,
                    '-stimulus[%d].stimtype'%(idx),   9,
                    '-stimulus[%d].strength'%(idx),   0. ]
        else:
            # we compute stimulus duration to lift voltage by 150 mV
            sdur = 150/self.stimS
            stim = ['-stimulus[%d].duration'%(idx),   sdur,
                    '-stimulus[%d].stimtype'%(idx),   0,
                    '-stimulus[%d].strength'%(idx), self.stimS ]

        stim += ['-stimulus[%d].start'   %(idx),    2.,
                 '-stimulus[%d].npls'    %(idx),    1  ]

        return ['-num_stim', 1 ] + stim + lhface


    def configLATs(self):
        """
        Set up detection of local activation times
        """

        # default, we detect only the first lat
        lidx = 0
        latstr = '-lats[{}]'.format(lidx)
        lats_first = [latstr + '.measurand',   0,
                      latstr + '.method',      1,
                      latstr + '.all',         0,
                      latstr + '.threshold', -40.,
                      latstr + '.mode',        0,
                      latstr + '.ID',        'lats']

        lidx += 1
        latstr = '-lats[{}]'.format(lidx)
        repol      = [latstr + '.measurand',   0,
                      latstr + '.method',      1,
                      latstr + '.all',         0,
                      latstr + '.threshold', -70.,
                      latstr + '.mode',        1,
                      latstr + '.ID',        'repol_time']

        lats_all = []
        repol_all = []
        if self.pcl is not None:
            # repetitive simulation to measure CV restitution
            lidx += 1
            latstr = '-lats[{}]'.format(lidx)
            lats_all  = [latstr + '.measurand',   0,
                         latstr + '.method',      1,
                         latstr + '.all',         1,
                         latstr + '.threshold', -40.,
                         latstr + '.mode',        0,
                         latstr + '.ID',        'lats_all']


            lidx += 1
            latstr = '-lats[{}]'.format(lidx)
            repol_all = [latstr + '.measurand',   0,
                         latstr + '.method',      1,
                         latstr + '.all',         1,
                         latstr + '.threshold', -70.,
                         latstr + '.mode',        1,
                         latstr + '.ID',        'repol_all']


        lats = ['-num_LATs', lidx + 1 ] + lats_first + repol + lats_all + repol_all

        return lats

    def CV_obs_sites(self, rel_x):
        """
        Extract spatial observation points for CV measurement
        """
        
        # read ub strand
        strand = mesh.Mesh.from_files(self.meshfiles)
        pts    = strand.points()

        # figure out nodes which fall into transitions
        # between quarter 1-2 and 3-4
        lo, up = numpy.min(pts, axis=0), numpy.max(pts, axis=0)
        slen   = up[0]-lo[0]
        dx     = self.geom.resolution()

        x  = [ ]
        for rx in rel_x:
            x += [ lo[0] + slen*rx ]
 
        nq = []
        for xi in x: 
            # find points around xi +/- dx/2
            o_pts = numpy.where(numpy.logical_and(pts[:,0] >= (xi-dx/2), pts[:,0] < (xi+dx/2)))[0]
            nq.append(o_pts)

        # mean x coordinates of selected points
        xm = []
        for i,n in enumerate(nq):
            xs = pts[n,0]
            xm.append(mean(xs))

        return xm, nq


    def measureCV(self, actsFile):
        """
        Measure conduction velocity along a strand

        As input the given mesh of a strand and the recorded activation times are used.
        Arrival times are computed at the interfaces of the quarters Q1/Q2 and Q3/Q4.
        """

        # observation sites x location relative to strand length
        rel_x = [0., 0.25, 0.5, 0.75, 1.0]
        obs_x, obs_nodes = self.CV_obs_sites(rel_x)

        # load local activation times
        if actsFile.endswith('.dat'):
            lats = loadtxt(actsFile)
        elif actsFile.endswith('.igb'):
            igb_file = carpio.igb.IGBFile(actsFile, 'r')
            header = igb_file.header()
            lats = igb_file.data().reshape((header['t'], header['x'])).T
        else:
            raise IOError('Error, unsupported file format, .dat or .igb file expected!')

        # arrival times at observation points
        x0   = obs_x[0]
        at0  = mean(lats[obs_nodes[0]])   # lat at stimulus nodes
        x12  = obs_x[1]
        at12 = mean(lats[obs_nodes[1]])   # lat at 25% into the strand
        x34  = obs_x[3]
        at34 = mean(lats[obs_nodes[3]])   # lat at 75% into the strand


        # make sure we had capture and tissue activated
        if (at12 == -1) or (at34 == -1):
            if (at12 > 0.):
                if not settings.cli.silent:
                    print('Error: Duration of simulation too short')
                    print('Proximal tissue activated, but not distal tissue')
                    print('Estimating velocity based on activation in initial segment of the cable.')

                # estimate from initial segmentation
                CV = (x12 - x0)/(at12 - at0)

            else:
                if not settings.cli.silent:
                    print('No capture or decremental conduction, proximal quarter of tissue not fully activated')

                if self.conv:
                    CV = 0.05
                    if not settings.cli.silent:
                        print('Assuming slow conduction velocity of 0.05 m/s in next iteration')

                else:
                    CV = 0.0

        else:
            CV = (x34 - x12)/(at34 - at12)

        return CV

    def x_activated(self, o_lats, o_inds, pcl, siteID ):

        valid_lats = o_lats > 0
        if not all(valid_lats):
            # non_act = o_inds[valid_lats]
            # assume none of the nodes activated at this observation site
            non_act = o_inds
            non_act_s = ','.join([str(e) for e in non_act])
            print('\nLoss of capture or propagation failure detected at site {} for a PCL of {}.'.format(siteID, pcl))
            print('Nodes {} not activated.\n\n'.format(non_act_s))

            return False

        else:
            return True

    def measureCVRes(self, actsFile):
        """
        Measure conduction velocity along a strand

        As input the given mesh of a strand and the recorded activation times are used.
        Arrival times are computed at the interfaces of the quarters Q1/Q2 and Q3/Q4.
        """

        # observation sites x location relative to strand length
        rel_x = [0., 0.25, 0.5, 0.75, 1.0]
        obs_x, obs_nodes = self.CV_obs_sites(rel_x)

        # read in lat matrix
        if actsFile.endswith('.dat'):
            lat_mat = lats.read_lats_all(actsFile)
        elif actsFile.endswith('.igb'):
            igb_file = carpio.igb.IGBFile(actsFile, 'r')
            header = igb_file.header()
            lat_mat = igb_file.data().reshape((header['t'], header['x'])).T
        else:
            raise IOError('Error, unsupported file format, .dat or .igb file expected!')

        # dump lat matrix for external inspection
        f, ext = os.path.splitext(actsFile)
        lats.save_lats_all('{}_mat.dat'.format(f), lat_mat)

        print('\n\nAnalyzing CV restitution not implemented yet.')
        num_act_nodes = lat_mat.shape[0]
        num_acts      = lat_mat.shape[1]

        # pick observation site indices for computing CV
        obs_x0 = 1
        obs_x1 = 3

        # track number of pcls that captured and propagated over both observation sites
        n_pcls = 0
        trgs   = []
        trg    = 0
        for i in range(1,num_acts):
            # this propagation belongs to a given trigger
            trg += self.pcl[i-1]

            # extract nodal indices of observation site 0
            o_inds = obs_nodes[obs_x0]
            o_lats = lat_mat[o_inds,i]
            if not self.x_activated(o_lats, o_inds, self.pcl[i-1], 'observation site at {}% relative distance'.format(rel_x[obs_x0]*100)):
                break
            at0 = mean(o_lats)

            o_inds = obs_nodes[obs_x1]
            o_lats = lat_mat[o_inds,i]
            if not self.x_activated(o_lats, o_inds, self.pcl[i-1], 'observation site at {}% relative distance'.format(rel_x[obs_x1]*100)):
                break
            at1 = mean(o_lats)

            # make sure activation times fall into the right time window,
            # that is, we ascertain that we got capture for this pcl and did not miss one beat
            in_slot_0 = at0 > trg and at0 < trg+self.pcl[i-1]
            in_slot_1 = at1 > trg and at1 < trg+self.pcl[i-1]

            # we have another sound capture + propagation
            if in_slot_0 and in_slot_1:
                n_pcls += 1
                self.CVs += [ (obs_x[obs_x1] - obs_x[obs_x0]) / (at1 - at0) ]
                trgs += [ trg ]
            else:
                print('Loss of capture at a pcl of {} ms (stimulus delivered at {}).'.format(self.pcl[i-1],trg))
                break

        print('\n%8s %5s %5s'%('TRG','PCL','CV'))
        print('%8s %5s %5s'%('[ms]', '[ms]','[m/s]'))
        trg = 0.
        for i in range(n_pcls):
            trg += self.pcl[i]
            print('%8.1f %4.1f %5.4f'%(trg, self.pcl[i], self.CVs[i]))
        print('\n')


    # build command line
    def buildCmd(self):

        cmd = tools.carp_cmd()

        cmd += [ '-meshname', self.meshfiles ]
        cmd += ep.model_type_opts(self.srcMod)
        cmd += self.configCells()
        if 'carpentry' in settings.execs['carp'].__str__():
            cmd += self.configStimLHFace()
        else:
            cmd += self.configStimLHFaceNew()
        cmd += self.configNumerics()
        cmd += self.configLATs()

        if self.visualize:
            cmd += ['-gridout_i', 3]
            cmd += ['-spacedt',   1]

        # adjust simulation time to cable length and velocity
        cableLen_mm = self.slen*10
        delay       = cableLen_mm / self.vel

        # make simulated time a factor >=1 of the target delay
        tend = delay*3
        if self.pcl is not None:
            trgs  = self.pcl2triggers()
            tend += trgs[-1]

        cmd += ['-tend', tend ]

        return cmd


    # iterate to converge conduction velocity
    def iterateCV(self, job):

        # build strand
        self.geomStrand()

        # build command line
        cmd = self.buildCmd()


        # Run simulation
        err_flg = False
        i     =  0
        v_new = -1  # do at least one run

        # start tuning with chosen start values
        if not settings.cli.silent:
            print('Start iterating')
            print('-'*format.terminal_width() + '\n')

        if not self.conv:
            self.max_it = 1

        while abs(self.vel - self.vel_) > self.ctol and i < self.max_it:

            # rerun simulation
            g       = self.configConductivities()
            if self.conv:
                ItJobID = '{}_It_{}'.format(job.ID, i)
            else:
                ItJobID = job.ID

            with open(self.log, 'w') as fp:
               with stream.divert_std(fp):
                    job.carp(cmd + g + ['-simID', ItJobID ])

            # measure macroscopic conduction velocity
            lats_file = os.path.join(ItJobID, 'init_acts_lats-thresh.dat')
            if not os.path.exists(lats_file):
                lats_file = os.path.join(ItJobID, 'init_acts_lats-thresh.igb')
            self.vel_ = self.measureCV(lats_file)

            # export CV to keep track of parameter changes
            self.report()

            # adjust conductivities only if converge is True
            if self.max_it > 1 and self.vel_ > 0:
                alpha  = (self.vel/self.vel_)**2.
                
                if self.beqm:
                    if self.mode == 'gm':
                        self.gi_ = alpha * self.gi_
                        self.ge_ = alpha * self.ge_
                    else:
                        # mode is 'gi'
                        self.gi_ = alpha * self.gi_
                        self.ge_ = self.ge
                else:
                    # beqm is off
                    self.gi_ = alpha * self.gi_
                    self.ge_ = self.ge

            self.last_it = i
            i = i+1


        if not settings.cli.silent:
            print('\n\n')
            print('Tuning results')
            print('-'*format.terminal_width() + '\n')
            print('Conduction velocity: {:.4f} m/s [gi={:.4f}, ge={:.4f}, gm={:.4f}]'.format(self.vel_, self.gi_, self.ge_, self.hmean()))


        if self.conv and i==self.max_it and i!=1:
            err_flg = True
            print('CV tuning not converged with given max_it number of iterations!')

        if not settings.cli.silent:
            print('\n\n')

        # write tuning results to ascii file
        self.saveTuningResults()

        return self.gi_, self.ge_, self.vel, self.hmean(), i, err_flg


    # iterate to converge conduction velocity
    def restituteCV(self, job):

        # build strand
        self.geomStrand()

        # build command line
        cmd = self.buildCmd()

        # start CV restitution measurement
        if not settings.cli.silent:
            print('Start CV restitution experiment')
            print('-'*format.terminal_width() + '\n')

        # rerun simulation
        g       = self.configConductivities()
        ItJobID = '{}_{}'.format(job.ID, 'CVres')

        with open(self.log, 'w') as fp:
            with stream.divert_std(fp):
                job.carp(cmd + g + ['-simID', ItJobID ])

        # measure macroscopic conduction velocity
        lats_file = os.path.join(ItJobID, 'lats_all-thresh.dat')
        if not os.path.exists(lats_file):
            lats_file = os.path.join(ItJobID, 'lats_all-thresh.igb')
        self.measureCVRes(lats_file)

        # convert repolarization times to matrix format
        # read in lat matrix
        repolFile = os.path.join(ItJobID, 'repol_all-thresh.dat')
        if not os.path.exists(repolFile):
            repolFile = os.path.join(ItJobID, 'repol_all-thresh.igb')

        if repolFile.endswith('.dat'):
            repol_mat = lats.read_lats_all(repolFile)
        elif repolFile.endswith('.igb'):
            igb_file = carpio.igb.IGBFile(repolFile, 'r')
            header = igb_file.header()
            repol_mat = igb_file.data().reshape((header['t'], header['x'])).T
        else:
            raise IOError('Error, unsupported file format, .dat or .igb file expected!')

        # dump lat matrix for external inspection
        f, ext = os.path.splitext(repolFile)
        lats.save_lats_all('{}_mat.dat'.format(f), repol_mat)

        # write tuning results, including CV restitution file
        self.saveTuningResults()

        # write CV restitution data to file
        self.saveCVResults()

        return self.pcl, self.CVs


def CVres_min_max(cvs, pcl, lims, *args):

    marg = 0.1
    xmin = min(pcl)-marg
    xmax = max(pcl)+marg
    ymin = min(cvs)-marg
    ymax = max(cvs)+marg

    cur_lims = [ xmin, xmax, ymin, ymax ]

    if not len(lims):
        lims = cur_lims
    else:
        if cur_lims[0] < lims[0]:
            lims[0] = cur_lims[0]
        if cur_lims[2] < lims[2]:
            lims[2] = cur_lims[2]
        if cur_lims[1] > lims[1]:
            lims[1] = cur_lims[1]
        if cur_lims[3] > lims[3]:
            lims[3] = cur_lims[3]

    return lims


def plotCVRestitution(cvresfiles, labels):

    # Plot CV versus PCL
    fig = plt.figure()
    
    r_ax = fig.add_subplot(1,1,1)
    lims = []

    colors = ['b', 'r', 'g', 'c', 'm', 'y']
    ncols  = len(colors)

    for i,f in enumerate(cvresfiles):
 
        l = labels[i]
 
        # read restitution data
        if not os.path.isfile(f):
            print('Given CV restitution file {} does not exist! Abort plotting.'.format(f))
            continue
 
        cvr_data = loadtxt(f)
        pcl = cvr_data[:,0]
        cvs = cvr_data[:,1]

        # plot CV(pcl)
        r_ax.plot(pcl, cvs, color='black', linestyle='dashed', marker='o',
                  markerfacecolor=colors[i%ncols], markersize=8, label=l)

        # keep track of min/max
        lims = CVres_min_max(cvs,pcl,lims)

    if cvresfiles:
        # finish after adding all restitution curves to plot
        r_ax.set_title('Restitution CV(n+1)=f(PCL(n))')
        r_ax.set_xlabel('Pacing Cycle Length (ms)')
        r_ax.set_ylabel('Conduction velocity (m/s)')

        xmin, xmax, ymin, ymax = lims
        r_ax.set_ylim(ymin,ymax)
        r_ax.set_xlim(xmin,xmax)

        r_ax.legend()

        plt.show()

    else:
        plt.close(fig)


