# According to the DataCite 4.0 scheme: http://doi.org/10.5438/0012
# keyword-only entries need to be supplied by the user
title: # add the title of your experiment
keywords: # more can be added
- cardiac electrophysiology
- openCARP experiment
- in-silico medicine
- computational cardiology
descriptions:
- description: # add a brief description of your experiment
  description_type: Abstract
subjects: # more can be added
- subject: Computer simulation
  value_uri: http://id.loc.gov/authorities/subjects/sh85029533
  scheme_uri: http://id.loc.gov/authorities/subjects
- subject: Biomedical engineering
  value_uri: http://id.loc.gov/authorities/subjects/sh85014237
  scheme_uri: http://id.loc.gov/authorities/subjects
- subject: Cardiology
  value_uri: http://id.loc.gov/authorities/subjects/sh85020214
  scheme_uri: http://id.loc.gov/authorities/subjects
radar_subjects:
- LifeScience
- ComputerScience
- Medicine
creators:
- name: # to be auto-populated from given_name and family_name
  #given_name: # provide your given name
  #family_name: # provide your family name
  #orcid: # optional
  #affiliations: # optional
  #- name:
  #  ror: # See https://ror.org to find the ID for your organisation
publisher: openCARP
publication_year: # to be auto-populated during bundle creation
date: # to be auto-populated during bundle creation YYYY-MM-DD
date_type: collected
resource: Simulation code
resource_type: Workflow
related_identifiers: # add IsNewVersionOf IsDocumentedBy IsSupplementTo as desired
- relation_type: IsCompiledBy
  related_identifier: 10.35097/562
  related_identifier_type: DOI
- relation_type: References
  related_identifier: 10.1016/j.cmpb.2021.106223
  related_identifier_type: DOI
rights: Apache License 2.0
rights_url: https://git.opencarp.org/openCARP/experiment-template/-/blob/master/LICENSE
rights_holder: # to be autopopulated during bundle creation
funding_references:
- name: Deutsche Forschungsgemeinschaft
  ror: https://ror.org/018mejw64
  award_number: 391128822
  award_uri: https://gepris.dfg.de/gepris/projekt/391128822
  award_title: "Sustainable Lifecycle Management for Scientific Software (SuLMaSS) - Software Dissemination and Infrastructure Development Driven by a Cardiac Electrophysiology Simulator"
- name: European High-Performance Computing Joint Undertaking EuroHPC (JU)
  ror: https://ror.org/00k4n6c32
  award_number: 955495
  award_uri: https://eurohpc-ju.europa.eu/ongoing-projects#ecl-inpage-259
  award_title: "Numerical modeling of cardiac electrophysiology at the cellular scale"

# Information about software (automatically collected at bundle creation)

