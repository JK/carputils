#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os

def dir():
    """
    Get the directory path of this package.
    """
    return os.path.dirname(__file__)

def path(name):
    """
    Get the path of an option file. Raises an Exception when the file does not
    exist.
    """
    filename = os.path.join(dir(), name)
    if not os.path.exists(filename):
        tpl = 'No option file "{0}" found in "{1}"'
        raise Exception(tpl.format(name, dir()))
    return filename
