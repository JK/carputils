#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from __future__ import print_function, division, unicode_literals

import warnings
from collections import OrderedDict
from carputils.resources import options as optionfiles
from carputils.resources import petsc_options
from carputils.resources import petsc_block_options
from carputils.resources import ginkgo_options
from carputils.settings.exceptions import CARPUtilsSettingsError, CARPUtilsSettingsWarning

MECH_ELEMENT = ('P1-P0', 'P1-P1-DB', 'MINI')

FLAVORS = ('petsc', 'ginkgo', 'petsc-gamg-agg', 'petsc-pipelined', 'boomeramg', 'boomeramg-pipelined', 'parasails', 'pt', 'direct')

class SolverOptions(object):
    """
    A container class with some convenience methods for setting up openCARP options

    To add options:

    >>> opts = SolverOptions()
    >>> # Set directly
    >>> opts.option_1 = 10
    >>> opts.option_2 = 100
    >>> # Set from arg list
    >>> opts.update_from_args(['-opt3', 1000, '-opt4', 0.1])
    >>> # Add parfile to command line
    >>> opts.add_parfile('new.par')

    To generate the argument list:

    >>> opts.args()
    ['+F', 'new.par',
     '-option_1', 10,
     '-option_2', 100,
     '-opt3', 1000,
     '-opt4', 0.1]
    """

    def __init__(self):
        self._parfiles = []
        self._params = OrderedDict()

    def __setattr__(self, attr, value):
        """
        Custom attribute access code providing order logging.
        """
        # Any 'private' variables with leading underscore should be left alone
        if attr.startswith('_'):
            self.__dict__[attr] = value
        else:
            self._params[attr] = value

    def __getattr__(self, attr):
        try:
            return self._params[attr]
        except KeyError:
            raise AttributeError

    def args(self):
        """
        Return list of arguments for use in subprocess.
        """
        args = []

        for path in self._parfiles:
            args += ['+F', path]

        for param, value in list(self._params.items()):
            args += ['-{0}'.format(param), value]

        return args

    def update_from_args(self, args):
        """
        Update entries from a provided argument list.

        It is expected that even entries in the provided list are the argument
        names, and start with a '-', and that odd entries are the corresponding
        values.
        """
        for name, val in zip(args[::2], args[1::2]):
            assert name.startswith('-'), 'Invalid argument passed'
            setattr(self, name.lstrip('-'), val)

    def add_parfile(self, path):
        """
        Add a parfile to the argument list.
        """
        self._parfiles.append(str(path))

def options(config, cli, makevars, mech_symmetric=False):
    """
    Determine the solver options for the current run configuration

    Parameters
    ----------
    config : carputils.settings.namespace.SettingsNamespace
        Settings from the settings.yaml config file
    cli : argparse.Namespace
        The command line options
    mech_symmetric : bool, optional
        Use symmetric mechanics solver (default: False)

    Returns
    -------
    SolverOptions
        Object containing the generated solver options
    """

    # Independently of flavor set Purkinje solver
    mapping = {'PASTIX':       'pastix_opts',
               'MUMPS':        'mumps_opts_nonsymmetric',
               'GMRES':        'petsc_purk_gmres_opts',
               'SUPERLU_DIST': 'superlu_dist_opts'}
    try:
        purk_opts = mapping[config.PURK_SOLVER]
    except KeyError:
        tpl = 'Unsupported PURK_SOLVER specified: {}, must be one of {}'
        msg = tpl.format(config.PURK_SOLVER, list(mapping))
        raise CARPUtilsSettingsError(msg)

    # Test once
    mech_method = 'cg' if mech_symmetric else 'gmres'

    opts = SolverOptions()

    # PT off by default on all solvers
    opts.ellip_use_pt = 0
    opts.parab_use_pt = 0
    if makevars.WITH_PURK:
        opts.purk_use_pt = 0
    if makevars.MECH:
        opts.mech_use_pt = 0

    flavor = getattr(cli, 'flv', config.FLAVOR)
    mech_element = getattr(cli, 'mech_element', config.MECH_ELEMENT)

    if mech_element != 'P1-P0' and flavor == 'pt':
        msg = 'Element {} not supported in PT. Setting solver to \"petsc\"'.format(mech_element)
        warnings.warn(msg, CARPUtilsSettingsWarning)
        flavor = 'petsc'

    if flavor == 'petsc':
        opts.parab_options_file = petsc_options.path('ilu_cg_opts')
        opts.ellip_options_file = petsc_options.path('gamg_cg_opts')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            optfiles = {'P1-P0': petsc_options.path('gamg_gmres_opts'),
                        'P1-P1-DB': petsc_block_options.path('fgmres_fieldsplit_mech_petsc'),
                        'MINI': petsc_block_options.path('fgmres_fieldsplit_mech_petsc')}
            ofile = optfiles[mech_element]
            opts.mechanics_options_file = ofile
    elif flavor == 'petsc-gamg-agg':
        opts.parab_options_file = petsc_options.path('ilu_cg_opts')
        opts.ellip_options_file = petsc_options.path('gamg_gmres_opts_agg')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            optfiles = {'P1-P0': petsc_options.path('gamg_gmres_opts_agg'),
                        'P1-P1-DB': petsc_block_options.path('fgmres_fieldsplit_mech_petsc_agg'),
                        'MINI': petsc_block_options.path('fgmres_fieldsplit_mech_petsc_agg')}
            ofile = optfiles[mech_element]
            opts.mechanics_options_file = ofile

    elif flavor == 'ginkgo':
        opts.parab_options_file = ginkgo_options.path('parab_solver.json')
        opts.ellip_options_file = ginkgo_options.path('ellip_solver.json')
        opts.flavor = 'ginkgo'
        # WITH_PURK and MECH options are not handled yet.

    elif flavor == 'petsc-pipelined':
        opts.parab_options_file = petsc_options.path('ilu_pipecg_opts')
        opts.ellip_options_file = petsc_options.path('gamg_pipecg_opts')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            optfiles = {'P1-P0': petsc_options.path('gamg_pgmres_opts'),
                        'P1-P1-DB': petsc_block_options.path('fgmres_fieldsplit_mech_petsc'),
                        'MINI': petsc_block_options.path('fgmres_fieldsplit_mech_petsc')}
            ofile = optfiles[mech_element]
            opts.mechanics_options_file = ofile

    elif flavor == 'boomeramg':
        opts.parab_options_file = petsc_options.path('ilu_cg_opts')
        opts.ellip_options_file = petsc_options.path('amg_cg_opts')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            optfiles = {'P1-P0': petsc_options.path('boomerAMG_gmres_opts'),
                        'P1-P1-DB': petsc_block_options.path('fgmres_fieldsplit_mech_boomeramg'),
                        'MINI': petsc_block_options.path('fgmres_fieldsplit_mech_boomeramg')}
            ofile = optfiles[mech_element]
            opts.mechanics_options_file = ofile

    elif flavor == 'boomeramg-pipelined':
        opts.parab_options_file = petsc_options.path('ilu_pipecg_opts')
        opts.ellip_options_file = petsc_options.path('amg_pipecg_opts')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            optfiles = {'P1-P0': petsc_options.path('boomerAMG_pgmres_opts'),
                        'P1-P1-DB': petsc_block_options.path('fgmres_fieldsplit_mech_boomeramg'),
                        'MINI': petsc_block_options.path('fgmres_fieldsplit_mech_boomeramg')}
            ofile = optfiles[mech_element]
            opts.mechanics_options_file = ofile

    elif flavor == 'parasails':

        opts.parab_options_file = petsc_options.path('ilu_cg_opts')
        opts.ellip_options_file = petsc_options.path('amg_cg_opts')
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:

            if mech_element != 'P1-P0':
                tpl = 'P1-P1 elements cannot be used with flavor {}'
                raise CARPUtilsSettingsError(tpl.format(flavor))

            opts.mechanics_options_file = petsc_options.path('parasails_cg_opts')

    elif flavor == 'direct':

        # Assign predefined default direct solver
        mapping = {'MUMPS':        'mumps_opts',
                   'PASTIX':       'pastix_opts',
                   'SUPERLU_DIST': 'superlu_dist_opts'}

        try:
            direct_opts = mapping[config.DIRECT_SOLVER]
        except KeyError:
            tpl = 'Unsupported DIRECT_SOLVER specified: {}, must be one of {}'
            msg = tpl.format(config.DIRECT_SOLVER, list(mapping))
            raise CARPUtilsSettingsError(msg)

        opts.parab_options_file = petsc_options.path(direct_opts)
        opts.ellip_options_file = petsc_options.path(direct_opts)
        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path(purk_opts)

        if makevars.MECH:
            if config.DIRECT_SOLVER == 'MUMPS' and not mech_symmetric:
                mech_direct_opts = 'mumps_opts_nonsymmetric'
            else:
                mech_direct_opts = direct_opts
            opts.mechanics_options_file = petsc_options.path(mech_direct_opts)

            # element specific settings
            if mech_element != 'P1-P0':
                raise CARPUtilsSettingsError('P1-P1 may not be supported with '
                                             '--flavor direct - please check the '
                                             'logic in carputils.settings.'
                                             'solvers')
                optfile = petsc_options.path('mumps_opts_nonsymmetric')
                opts.mechanics_options_file = optfile

    elif flavor == 'pt':
        # Use '_large' config file variants for large runs
        suffix = '_large' if getattr(cli, 'np', 1) >= 128 else ''

        ellip = optionfiles.path('pt_ell_amg' + suffix)
        opts.add_parfile(ellip)

        parab = optionfiles.path('pt_para_amg' + suffix)
        opts.add_parfile(parab)

        if makevars.WITH_PURK:
            opts.purk_options_file = petsc_options.path('petsc_purk_gmres_opts')

        opts.ellip_use_pt = 1
        opts.parab_use_pt = 1

        if makevars.MECH:
            mech = optionfiles.path('pt_mech_amg' + suffix)
            opts.add_parfile(mech)
            opts.mech_use_pt = 1

    else:
        raise CARPUtilsSettingsError('Unknown flavor {0}'.format(flavor))

    return opts
