#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import shutil
import sys
from carputils.settings.namespace import SettingsNamespace
from carputils.settings.exceptions import *

if sys.version_info.major > 2:
    isPy2 = False
else:
    isPy2 = True

class Path(object):

    def __init__(self, *parts):
        self._parts = parts

    def exists(self):
        return os.path.exists(str(self))

    def lower(self):
        return self.__str__().lower()

    def __str__(self):
        return os.path.join(*self._parts)

    def __add__(self, other):
        return Path(*(self._parts + other._parts))

    def __iadd__(self, other):
        self._parts += other._parts
        return self

class PathSettingsNamespace(SettingsNamespace):
    """
    Modified namespace with extra error handling for paths
    """

    def __init__(self, missing_error=True):
        SettingsNamespace.__init__(self, missing_error, CARPUtilsMissingPathError)

    def __getattr__(self, attr):
        # Case insensitive
        attr = attr.upper()

        path = self._data.get(attr)

        if self._missingerr:
            if path is None:
                raise self._exception(attr)
            if not path.exists():
                raise self._exception(attr, path)

        return path

def join_add(*parts):
    """
    Build path from supplied parts using os.path.join for OS independence.
    Returns None if the specified path does not exist, otherwise returns the
    path.
    """

    if None in parts:
        return None

    path = os.path.join(*parts)

    if os.path.exists(path):
        return path
    else:
        return None

def carp_executable(exedir, start='carp'):
    """
    Select the most recent openCARP/bench/etc. executable in the exe dir

    Parameters
    ----------
    exedir : str or Path
        The path of the openCARP bin directory
    start : str, optional
        The beginning of the executable name (default: 'openCARP.')

    Returns
    -------
    str
        The path of the file (or link) to the most recently built executable
        starting with the specified string
    """

    exedir = str(exedir)

    candidates = []
    blacklist = ['carphelp', 'carptests', 'update-carp']

    for fname in os.listdir(exedir):

        start_lc = start.lower()
        fname_lc = fname.lower()
        if not start_lc in fname_lc:
            continue

        # Resolve links
        real = os.path.realpath(os.path.join(exedir, fname))

        # Check if it is an executable
        if not os.path.exists(real) or not os.access(real, os.X_OK):
            continue

        # Make sure file is not blacklisted
        if fname in blacklist:
            continue

        # Store orig path and exe mod time
        candidates.append((fname, os.path.getmtime(real)))

    if len(candidates) == 0:
        # No suitable executable, return cleaned dummy exe name
        return start.rstrip('.')

    # Pick newest executable
    candidates = sorted(candidates, key=lambda t: t[1])
    return candidates[-1][0]

def dirs(config, cli):
    """
    Determine the directories to look for executables.
    """

    build = getattr(cli, 'build', config.BUILD)

    dirs = PathSettingsNamespace()
    dirs.CARP       = Path(config.CARP_EXE_DIR[build])
    dirs.MESHTOOL   = Path(config.MESHTOOL_DIR) if config.MESHTOOL_DIR else config.MESHTOOL_DIR
    dirs.MESHALYZER = Path(config.MESHALYZER_DIR)
    dirs.LIMPETGUI  = Path(config.LIMPETGUI_DIR)
    dirs.CARPUTILS  = Path(config.CARPUTILS_ROOT_DIR)

    # add REGRESSION_REF to dirs if REGRESSION_REF exists in the settings.yaml file
    REGRESSION_REF = config.get('REGRESSION_REF', None)
    if REGRESSION_REF is not None:
        dirs.REGRESSION_REF = Path(REGRESSION_REF)

    # add MODELGEN to dirs if MODELGEN_DIR exists in the settings.yaml file
    MODELGEN_DIR = config.get('MODELGEN_DIR', None)
    if MODELGEN_DIR is not None:
        dirs.MODELGEN = Path(MODELGEN_DIR)

    return dirs

def execs(config, dirs):
    """
    Generate the executable paths for the current run configuration
    """

    # Prepare namespace
    execs = PathSettingsNamespace()

    # CARP
    carp         = carp_executable(dirs.get('CARP'), start='carp')
    execs.CARP   = dirs.get('CARP') + Path(carp)
    execs.MESHER = dirs.get('CARP') + Path('mesher')
    execs.FSM    = dirs.get('CARP') + Path('fsm_fcc')

    # LIMPET
    bench = carp_executable(dirs.get('CARP'), 'bench.')
    execs.BENCH = dirs.get('CARP') + Path(bench)

    # FEMLIB
    for name in ['GlInterpolate', 'GlGradient', 'GlElemCenters',
                 'GlMeshConvert', 'GlVTKConvert', 'GlTransfer',
                 'GlRuleFibers', 'GlRuleFibersMult', 'EllipticSolver',
                 'ParabolicSolver', 'AdjointSolver', 'GlFilament',
                 'GlIGBProcess', 'SurfaceMesh', 'GlSurfaceConvert',
                 'correspondance']:
        execs[name] = dirs.get('CARP') + Path(name)

    # Elasticity
    execs.ELASTICITY = dirs.get('CARP') + Path('ElasticityTest')

    # FluidTest
    execs.FLUIDSOLVE = dirs.get('CARP') + Path('FluidSolve')

    # CirculatorySystemTest
    execs.CVSTOOL = dirs.get('CARP') + Path('cvstool')

    # IGB Utils
    for name in ['igbhead', 'igbops', 'igbextract', 'igbapd', 'igbdft']:
        execs[name] = dirs.get('CARP') + Path(name)

    # Meshalyzer
    execs.MESHALYZER = dirs.get('MESHALYZER') + Path('meshalyzer')

    # MODH5 -> convert carp models to hdf5 format
    execs.MODH5 = dirs.get('MESHALYZER') + Path('utils/modh5conv/bin/modh5conv')

    # DATH5 -> convert carp data to hdf5 format
    execs.DATH5 = dirs.get('MESHALYZER') + Path('utils/package_data/package_data')

    # LIMPET_GUI
    execs.LIMPETGUI = dirs.get('LIMPETGUI') + Path('main.py')
    execs.SV2H5B = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'bin2h5.py'))
    execs.SV2H5T = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'txt2h5.py'))

    # CARPUTILS/MISC
    execs.CLOSEST_HC = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'closest_hc'))
    execs.APDRESTITUTION = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'APDrestitution'))
    execs.TUNECV = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'tuneCV'))
    execs.RESTITUTECV = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'restituteCV'))
    execs.GENERATEMETADATA = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'generateMetadata'))

    # EIKONAL
    execs.EKBATCH = dirs.get('CARP') + Path('ekbatch')
    execs.EKSOLVE = dirs.get('CARP') + Path('eksolve')

    # MESHTOOL
    execs.MESHTOOL = dirs.get('MESHTOOL') + Path('meshtool') if dirs.get('MESHTOOL') else Path('meshtool')

    # MODELGEN
    # add modelgen tools only if MODELGEN exists in dirs
    MODELGEN_DIR = dirs.get('MODELGEN', None)
    if MODELGEN_DIR is not None:
        execs.MGUVC = MODELGEN_DIR + Path('mguvc')
        execs.MGSPATH = MODELGEN_DIR + Path('mgspath')
        execs.MGQRYIDX = MODELGEN_DIR + Path('mgqueryidx')
        execs.MGBULLSEYE = MODELGEN_DIR + Path('mgbullseye')
        execs.MGTAGREG = MODELGEN_DIR + Path('mgtagreg')

    # LIMPET code generation
    execs.LIMPETFE = dirs.get('CARP') + Path('limpet_fe.py')
    execs.MAKEDYNAMICMODEL = dirs.get('CARP') + Path('make_dynamic_model.sh')

    # BEM
    execs.BEMECG = dirs.get('CARP') + Path('bem-ecg')

    return execs
