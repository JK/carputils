#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Class and Exception for global settings object
"""
import os

from carputils.settings.namespace import SettingsNamespace
from carputils.settings.exceptions import CARPUtilsSettingsError

from carputils.settings import config
from carputils.settings import paths
from carputils.settings import buildinfo
from carputils.settings import solver

from carputils.settings.summary import summary

from carputils import machines

class SettingsModule(object):

    MECH_ELEMENT = solver.MECH_ELEMENT

    FLAVORS = solver.FLAVORS

    def __init__(self):
        self._config = None
        self._cli = None # Command line interface

    @property
    def config(self):
        if self._config is None:
            self._config = config.load()
        return self._config

    @property
    def cli(self):
        if self._cli is None:
            msg = ('Command line arguments not yet assigned to settings - this '
                   'should be done automatically by the '
                   'carputils.tools.carpexample decorator')
            raise CARPUtilsSettingsError(msg)
        return self._cli
    
    @cli.setter
    def cli(self, cli):
        self._cli = cli
    
    @property
    def dirs(self):
        return paths.dirs(self.config, self.cli)
    
    @property
    def revision(self):
        return buildinfo.revision(self.execs)

    @property
    def dependency_revisions(self):
        return buildinfo.dependency_revisions(self.execs)

    @property
    def makevars(self):
        return buildinfo.makevars(self.execs)

    @property
    def execs(self):
        return paths.execs(self.config, self.dirs)

    @property
    def platform(self):
        return machines.get(self.cli.platform)

    def solver(self, *args, **kwargs):
        return solver.options(self.config, self.cli, self.makevars,
                              *args, **kwargs)

    @property
    def summary(self):
        return summary(self.config, self.execs, self.solver(), self.cli)

    @property
    def is_open_carp(self):
        carp_exec = os.path.basename(str(self.execs.CARP))
        return 'open' in carp_exec.lower()

    def default_cli(self, dry=False, **kwargs):
        cli_dict = dict(dry=dry,
                        silent=self.config.SILENT,
                        flv=self.config.FLAVOR,
                        platform=self.config.PLATFORM,
                        build=self.config.BUILD)
        cli_dict.update(kwargs)
        return type('CLI', (object,), cli_dict)
