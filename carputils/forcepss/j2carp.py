# ---( system imports )--------------------------
import os
import sys
import json
import copy
import numpy
import pickle
import itertools
import functools

try:
    import SALib
except ImportError as err:
    SA_AVAILABLE = False
else:
    from SALib.sample import saltelli
    from SALib.sample import latin
    SA_AVAILABLE = True

try:
    import UncertainSCI
except ImportError as err:
    UQ_AVAILABLE = False
else:
    from UncertainSCI.distributions import BetaDistribution
    from UncertainSCI.pce import PolynomialChaosExpansion
    UQ_AVAILABLE = True


# ---( carputils imports )-----------------------
from carputils.carpio import meshtool
from carputils import settings

from carputils.forcepss.planio import load_simulation_plan
from carputils.forcepss.planio import save_simulation_plan
from carputils.forcepss.planio import DEFAULT_TUNING_DIR
from carputils.forcepss.planio import DEFAULT_SOLVER
from carputils.forcepss.planio import j2cKey


# ===( CONFIGURATIONS )==========================
CONFIGURATIONS = 'config'
CONFIGURATION_FUNC = 'func'
CONFIGURATION_TAGS = 'tags'

# ===( FUNCTIONS )===============================
FUNCTIONS = 'functions'
FUNCTION_CV = 'CV'
FUNCTION_CV_CVMEAS = 'CVmeas'
FUNCTION_CV_CVREF = 'CVref'
FUNCTION_CV_COND = 'G'
FUNCTION_EP = 'EP'
FUNCTION_EP_MODEL = 'model'
FUNCTION_EP_MODELPAR = 'modelpar'
FUNCTION_EP_BCL = 'bcl'
FUNCTION_EP_INIT = 'init'
FUNCTION_EP_CYCLES = 'cycles'
FUNCTION_EP_PLUGINS = 'plugins'
FUNCTION_EP_PLUGPARS = 'plugin-par'
FUNCTION_EP_APDRESFILE = 'apdres'
FUNCTION_EP_APDRESPRTCL = 'apdresptcl'

# ---( AUXILIARY LISTS & NAMES )-----------------
FUNCTION_CV_COND_ACTIVE_NAMES = ('gil', 'git', 'gin', 'gel', 'get', 'gen')
FUNCTION_CV_COND_PASSIVE_NAMES = 'gbath'
FUNCTION_CV_VELOCITY_NAMES = ('vf', 'vs', 'vn')

# ===( ELECTRODES )==============================
ELECTRODES = 'electrodes'
ELECTRODE_TYPE = 'type'

# ===( SIMULATION )==============================
SIMULATION = 'simulation'
STIMULATION_STIMULI = 'stimuli'
STIMULATION_TYPE = 'type'
#STIMULATION_CAVITY = {'la': -2, 'lv': -1, 'rv': 1, 'ra': 2}
STIMULATION_ELECTRODE = 'electrode'
STIMULATION_PROTOCOL = 'protocol'
STIMULATION_STRENGTH = 'strength'
STIMULATION_TYPE_MAP = {'Transmembrane Current': 0,
                        'Transmembrane Voltage clamp': 9,
                        'extracellular current': 1,
                        'extracellular voltage (closed loop)': 2,
                        'extracellular ground': 3,
                        'intracellular current': 4,
                        'extracellular voltage (open loop)': 5,
                        'prescribed takeoff': 8,
                        'Vm clamp': 9,
                        'prescribed phie': 10}

# ===( SPLIT )===================================
SPLIT = 'split'
SPLIT_OPSTR = 'opstr'
SPLIT_APPLY = 'apply'

# ===( LFSETUP )==================================
LFSETUP = 'lfsetup'

# ===( SOLVER )==================================
SOLVER = 'solver'
SOLVER_DT = 'dt'
SOLVER_DX = 'dx'
SOLVER_PARABSOLVE = 'ts'
SOLVER_MASSLUMPING = 'lumping'
SOLVER_OPSPLIT = 'opsplit'
SOLVER_CGTOL = 'stol'
SOLVER_BEQM = 'beqm'
SOLVER_FIXGE = 'fixge'
SOLVER_SRC_MODEL = 'sourceModel'

# ===( PROTOCOLS )===============================
PROTOCOLS = 'protocols'
PROTOCOLS_BCL = 'bcl'
PROTOCOLS_CYCLES = 'cycles'
PROTOCOLS_ELECTRODES = 'electrodes'
PROTOCOLS_PROPAGATION = 'propagation'
PROTOCOLS_LATFILE = 'lat'
PROTOCOLS_PREPACE = 'prepace'
PROTOCOLS_RESTART = 'restart'
PROTOCOLS_RELTIMINGS = 'rel-timings'

PROTOCOLS_APD_RESTITUTION = 'APD-restitution'
PROTOCOLS_APD_RESTITUTION_TYPE = 'type'
PROTOCOLS_APD_RESTITUTION_BCL = 'bcl'
PROTOCOLS_APD_RESTITUTION_CI0 = 'CI0'
PROTOCOLS_APD_RESTITUTION_CI1 = 'CI1'
PROTOCOLS_APD_RESTITUTION_PPBEATS = 'pp_beats'
PROTOCOLS_APD_RESTITUTION_PMBEATS = 'pm_beats'
PROTOCOLS_APD_RESTITUTION_PMDEC = 'pm_dec'

# ===( SAMPLING METHODS )===============================
SAMPLING_METHODS = ['file']
if UQ_AVAILABLE:
    SAMPLING_METHODS.append('uq')
if SA_AVAILABLE:
    SAMPLING_METHODS.append('saltelli')
    SAMPLING_METHODS.append('lhs')


# load dictionary from json file
def load_json(file_name):
    jdata = None
    # check json file exists
    if os.path.exists(file_name):
        with open(file_name, 'r') as fp:
            jdata = json.load(fp)
    else:
        print('\n\nJSON file {} not found.'.format(file_name))
    return jdata


def print_subdict(key, val, shift_num=0):
    str_shift = ' '*shift_num
    if not isinstance(val, dict):
        if val is None:
            print(str_shift + '*{}: MISSING'.format(key))
        else:
            if isinstance(val, (list, tuple)):
                if len(val) > 20:
                    list_str = '{}, {}, ..., {}, {}'.format(val[0], val[1], val[-2], val[-1])
                    print(str_shift + '{}:    [{}] N={}'.format(key, list_str, len(val)))
                else:
                    print(str_shift + '{}:    {}'.format(key, val))
            else:
                print(str_shift + '{}:    {}'.format(key, val))
    elif isinstance(val, dict):
        print(str_shift + str(key) + ':')


def print_settings_config(config, config_header=False):
    assert isinstance(config, dict), 'The specified config is not a dictionary!'

    def header(str_2_emph=None):
        str_2_emph = str(str_2_emph) if str_2_emph is not None else ''
        len_dash = int((80-len(str_2_emph))/2)
        side_dash = '-'*len_dash
        print('\n'+side_dash+str_2_emph+side_dash)

    if config_header:
        header(config_header)

    def iterate_dict(obj, depth=0):
        for key, val in obj.items():
            print_subdict(key, val, shift_num=depth*5)
            if isinstance(val, dict):
                iterate_dict(val, depth=depth+1)

    iterate_dict(config)


def save_json(jdata, file_name, indent=2, overwrite=True):
    if not overwrite and os.path.exists(file_name):
        raise IOError('Error, file "{}" already exists!'.format(file_name))
    with open(file_name, 'w') as fp:
        json.dump(jdata, fp, indent=indent)


def write_vtx_file(filename, vtx_list):
    fp = open(filename, 'wt')
    fp.write('{}\n'.format(len(vtx_list)))
    fp.write('extra\n')
    for v in vtx_list:
        fp.write('{}\n'.format(v))
    fp.close()


def write_carp_par_file(filename, opts, start_num=1):
    assert len(opts) > start_num
    print('\nWriting settings to carp parameter file: {}'.format(filename))
    fp = open(filename, 'w')
    for i in numpy.arange(start_num, len(opts), 2):
        if str(opts[i])[0] == '-':
            fp.write('{:<48s}=   {}\n'.format(opts[i][1:], opts[i+1]))
    fp.close()


# check validity of keys
def valid_keys(jsec, key_lst):
    valid_keys = []
    for key, sec in jsec.items():
        valid_keys += [key]

    for k in key_lst:
        if k not in valid_keys:
            print('Removing invalid key {} specified.'.format(k))
            key_lst.remove(k)

    # returned cleaned key list only containing valid keys
    return key_lst


def get_init_dir(init_dir, up):
    """
    Retrieve/create path to initialization directory
    :param init_dir:
    :param up:
    :return: path to directory for storing initialization files (state vectors, lat sequences, checkpoints, ...)
    """
    if up:
        init_prfx = init_dir
    else:
        pth, idir = os.path.split(init_dir)
        init_prfx = os.path.join(pth, 'tmp-' + idir)

    if not os.path.exists(init_prfx):
        os.mkdir(init_prfx)

    return init_prfx


# dealing with mutable flag
def is_mutable(sec, ignore):
    if not ignore:
        mutable = sec.get('mutable')
        if mutable is None:
            # mutable not specified in dictionary, we assume non-mutable here
            mutable = False
    else:
        mutable = True

    return mutable


# turn CVs and Gs of a CV section into list of values
# four lists are generated
# v_lst   [ v_f,  v_s,  (v_n)  ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# axes    [ 'f',  't',  ('n')  ]
# values for normal axis are optional, in case of rotational isotropy n will be omitted
def vel2List(CVs, G):
    v_lst = [CVs['vf'], CVs['vs']]
    gi_lst = [G['gil'], G['git']]
    ge_lst = [G['gel'], G['get']]
    axes = ['f', 't']
    if CVs['vn'] != CVs['vs']:
        v_lst.append(CVs['vn'])
        gi_lst.append(G['gin'])
        ge_lst.append(G['gen'])
        axes.append('n')

    return v_lst, gi_lst, ge_lst, axes


# manage 'BATH' section to exclude regions from being active EP
def get_bath_labels(cnf, pop=False):
    # double check first whether a global BATH region is used
    bl = cnf.get('BATH')
    if bl:
        # extract list of names only
        bl = bl.get('labels')
        if pop:
            cnf = cnf.copy()
            # remove 'BATH' from dictionary
            cnf.pop('BATH')
    return bl, cnf


# translate EP dictionary sections to carp input
def ep_secs_to_carp(cnf, fncs):
    carp = []
    imp_cnt = 0
    purk = []
    purk_cnt = 0
    for dkey, domain in cnf.items():
        # get function defintion
        tags = domain.get(CONFIGURATION_TAGS)
        if not tags:
            continue

        fnc_name = domain.get(CONFIGURATION_FUNC)
        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, dkey))

        else:
            ep = fnc_def.get(FUNCTION_EP)
            # if ep is not null we have a valid EP definition, increment imp region count
            if ep:
                if 'PURK' in dkey:
                    purk += ep_sec_to_carp(tags, dkey, ep, purk_cnt)
                    purk_cnt = purk_cnt + 1
                else:
                    carp += ep_sec_to_carp(tags, dkey, ep, imp_cnt)
                    imp_cnt = imp_cnt + 1

    if imp_cnt > 0:
        carp = ['-num_imp_regions', imp_cnt] + carp

    if purk_cnt > 0:
        purk = ['-numPurkIon', purk_cnt] + purk

    return carp + purk


# translate single EP dictionary section to carp input
def ep_sec_to_carp(tags, key, ep, idx):

    # dealing with regular imp region or purk region?
    if 'PURK' in key:
        reg = '-PurkIon[{}]'.format(idx)
    else:
        reg = '-imp_region[{}]'.format(idx)

    # name the imp_region
    carp = [reg + '.name', key]

    # list IDs forming the G region
    carp += [reg+'.num_IDs'.format(idx), len(tags)]
    for i, l in enumerate(sorted(tags)):
        carp += [reg+'.ID[{}]'.format(i), l]

    # model is mandatory
    epmodel = ep[FUNCTION_EP_MODEL]
    """
    if not settings.is_open_carp:
        carp += [reg+'.im', epmodel]
    else:
        if epmodel not in OPENCARP_IONIC_MAP.keys():
            print('Ionic model mapping for openCARP not supported: {}'.format(epmodel))
            sys.exit()
        carp += [reg+'.im', OPENCARP_IONIC_MAP.get(epmodel)]
    """
    carp += [reg + '.im', epmodel]

    modelpar = ep.get(FUNCTION_EP_MODELPAR, None)
    if modelpar and len(modelpar) > 0:
        carp += [reg+'.im_param', modelpar]

    # plug-ins still missing
    init = ep.get(FUNCTION_EP_INIT, None)
    if init and len(init) > 0:
        carp += [reg+'.im_sv_init', os.path.abspath(init)]

    return carp


# translate conductivity sections to carp input
def G_secs_to_carp(cnf, fncs):

    carp = []
    reg_cnt = 0
    for dkey, domain in cnf.items():
        # get function defintion
        tags = domain.get(CONFIGURATION_TAGS)
        fnc_name = domain.get(CONFIGURATION_FUNC)
        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def,dkey))

        else:
            cv = fnc_def.get(FUNCTION_CV)

            # skip if CV not defined
            if cv is None:
                print('\nNo CV defined for function domain {} !\n'.format(fnc_def))
                continue

            carp += G_sec_to_carp(tags, dkey, cv['G'], reg_cnt)
            reg_cnt += 1

    carp = ['-num_gregions', reg_cnt] + carp

    return carp


# translate single conductivity section to carp input
def G_sec_to_carp(tags, key, G, idx):

    if not tags:
        return []

    prefix = '-gregion[{}].'.format(idx)
    # assign name
    carp = [prefix+'name', key]

    # list IDs forming the G region
    carp += [prefix+'num_IDs', len(tags)]
    for i, l in enumerate(sorted(tags)):
        carp += [prefix+'ID[{}]'.format(i), l]

    # assign conductivity values
    if G.get('gil') is not None:
        carp += [prefix+'g_il', G['gil'],
                 prefix+'g_it', G['git'],
                 prefix+'g_in', G['gin'],
                 prefix+'g_el', G['gel'],
                 prefix+'g_et', G['get'],
                 prefix+'g_en', G['gen']]
    else:
        # bath domain
        carp += [prefix+'g_bath'.format(idx), G['gbath']]

    return carp


# translate conduction velocity sections to carp input
def CV_secs_to_carp(cnf, fncs):

    # dictionary of defined functions
    fnc_lst = fncs.get(FUNCTIONS)

    carp = []
    ek_cnt = 0
    for dkey, domain in cnf.items():

        # get function defintion
        tags = domain.get(CONFIGURATION_TAGS)
        fnc_name = domain.get(CONFIGURATION_FUNC)

        # definition of function
        fnc_def = fncs.get(fnc_name)

        #
        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, dkey))
        else:
            cv = fnc_def.get(FUNCTION_CV)

            # skip if CV not defined
            if cv.get(FUNCTION_CV_CVREF) is None:
                print('\nNo CV defined for function domain {} !\n'.format(dkey))
                continue

            carp += CV_sec_to_carp(cv, ek_cnt, dkey)
            ek_cnt = ek_cnt + 1

    carp = ['-num_ekregions', ek_cnt] + carp

    return carp


# translate single conduction velocity section to carp input
def CV_sec_to_carp(cv, idx, key):
    prefix = '-ekregion[{}].'.format(idx)
    cv_ref = cv[FUNCTION_CV_CVREF]
    carp = [prefix+'ID',    idx,
            prefix+'name',  key,
            prefix+'vel_f', cv_ref['vf'],
            prefix+'vel_s', cv_ref['vs'],
            prefix+'vel_n', cv_ref['vn']]

    return carp


# build physics regions from tag sets
def gen_physics(ExtraTags=None, IntraTags=None, EikonalTags=None, MechTags=None, FluidTags=None):
    num_phys = 0
    phys_opts = list()

    if IntraTags is not None:
        phys_opts += domain_to_phys_region(0, '"Intracellular domain"', IntraTags, num_phys)
        num_phys += 1
    if ExtraTags is not None:
        phys_opts += domain_to_phys_region(1, '"Extracellular domain"', ExtraTags, num_phys)
        num_phys += 1

    #This causes an error in LeadField Computation. Needs to be debugged!
    #if EikonalTags is not None:
    #    phys_opts += domain_to_phys_region(2, '"Eikonal domain"', EikonalTags, num_phys)
    #    num_phys += 1
    if MechTags is not None:
        phys_opts += domain_to_phys_region(3, '"Mechanics domain"', MechTags, num_phys)
        num_phys += 1
    if FluidTags is not None:
        phys_opts += domain_to_phys_region(4, '"Fluid domain"', FluidTags, num_phys)

    phys_opts = ['-num_phys_regions', num_phys] + phys_opts

    return phys_opts


# build single physics region definition
def domain_to_phys_region(ptype, name, domain, idx):
    preg = '-phys_region[{}]'.format(idx)
    phys_reg = [preg+'.ptype', ptype,
                preg+'.name', name,
                preg+'.num_IDs', len(domain)]
    for i, l in enumerate(domain):
        phys_reg += [preg+'.ID[{}]'.format(i), l]
    return phys_reg


def elec_sec_to_carp(job, elecs, idx, act_lst, **kwargs):
    # make sure active list is given as list
    if isinstance(act_lst, str):
        act_lst = [act_lst]

    stm_elec = []
    for stm_name in act_lst:
        # get listed electrode description from dictionary
        elec = elecs.get(stm_name)
        # build electrode
        if elec is not None:
            stm_elec += build_stim_geom(job, elec, idx, stm_name, **kwargs)
            idx += 1

    return stm_elec, idx


def get_ground_stimulus(lfsetup, ground_vtx_file, stim_num, dry=False):
    ground_opts = []
    if lfsetup is not None:
        if not dry:
            ref_vtx = [int(lfsetup['refvtx'])]
            write_vtx_file(ground_vtx_file, ref_vtx)

        ground_vtx_base = ground_vtx_file.replace('.vtx', '')
        stim_prefix = '-stim[{}]'.format(stim_num)
        ground_opts = [stim_prefix+'.elec.vtx_file', ground_vtx_base,
                       stim_prefix+'.crct.type', 3]

    return ground_opts


def stim_sec_to_carp(job, stimuli, lfsetup, rmode, output_dir, **kwargs):
    # assign protocols to all electrodes used for pre-pacing

    elec, error_check, _ = check_stim_protocol(stimuli)
    if error_check:
        print_settings_config(elec)
        print('Missing entries for stimulus definition!')
        sys.exit()

    stims = []
    num_stim = 0
    for stim_name, elec in stimuli.items():
        stim_prefix = '-stim[{}]'.format(num_stim)
        if elec['active'] == 1:
            stim_filename = os.path.join(output_dir, stim_name + '.vtx')
            stims += build_stim_geom(job, elec.get(STIMULATION_ELECTRODE), num_stim,
                                     stim_name, stim_filename=stim_filename, **kwargs)
            ptcl_info = elec.get(STIMULATION_PROTOCOL)

            for prot_key, prot_val in ptcl_info.items():
                if prot_val is not None:
                    if not (prot_key == 'bcl' and prot_val == 0.0):
                        stims += [stim_prefix+'.ptcl.{}'.format(prot_key), prot_val]

            stims += [stim_prefix+'.pulse.strength', elec.get(STIMULATION_STRENGTH),
                      stim_prefix+'.crct.type', STIMULATION_TYPE_MAP[elec[ELECTRODE_TYPE]]]
            '''
            ptcl = ['-stim[{}].ptcl.start'.format(i),     pp_rtimes[i],
                    '-stim[{}].ptcl.duration'.format(i),  2,
                    '-stim[{}].pulse.strength'.format(i), s_strength,
                    '-stim[{}].crct.type'.format(i),      s_type]
            '''
            num_stim += 1
        else:
            print('The electrode {} is not active!'.format(stim_name))

    if rmode == 're':
        stim_prefix = '-stim[{}]'.format(num_stim)
        stims += [stim_prefix+'.crct.type', 8]
        num_stim += 1

    if lfsetup is not None:
        ground_vtx_file = os.path.join(os.path.abspath(output_dir),'ground.vtx')
        stims += get_ground_stimulus(lfsetup, ground_vtx_file, num_stim)
        num_stim += 1

    return stims, num_stim


# pick a single point [x,y,z] and turn it into stim[].elec format
def get_elec_point(pt, pt_idx, stm):
    e = []
    if len(pt) == 3 and pt_idx in {'p0', 'p1'}:
        for i, x in enumerate(pt):
            e += [stm+'.{}[{}]'.format(pt_idx, i), x]
    return e


def mgquery_idx_call(job, spec_line, stim_name, model_name, model_format, ucc_file):
    if job is None:
        print('Error, failed to call "mgquery_idx_call", empty job object !')
        return None
    if model_name is None:
        print('Error, failed to call "mgquery_idx_call", empty model_name string !')
        return None
    if model_format is None:
        print('Error, failed to call "mgquery_idx_call", empty model_format string !')
        return None
    if ucc_file is None:
        print('Error, failed to call "mgquery_idx_call", empty ucc_file string !')
        return None

    cmd = [settings.execs.MGQRYIDX,
           '--model-name', model_name,
           '--model-format', model_format,
           '--ucc-file', ucc_file]
    cmd += spec_line
    job.bash(cmd)
    base_name = model_name
    if model_format == 'carp_txt' and model_name.endswith('.elem'):
        base_name = model_name[:-5]
    elif model_format == 'carp_bin' and model_name.endswith('.belem'):
        base_name = model_name[:-6]
    vtx_file = base_name + '.query_{}.vtx'.format(stim_name)

    return vtx_file


# build a single electrode based on dictionary description
# stash carp electrode in stim array in slot idx
def build_stim_geom(job, elec, idx, stm_name, **kwargs):
    model_name = kwargs.get('model_name', None)
    model_format = kwargs.get('model_format', None)
    ucc_file = kwargs.get('ucc_file', None)
    stim_filename = kwargs.get('stim_filename', None)
    dry = kwargs.get('dry', False)

    # electrode type to build
    et = elec.get(ELECTRODE_TYPE)
    stm = '-stim[{}]'.format(idx)
    stm_e = '-stim[{}].elec'.format(idx)
    e = [stm + '.name', stm_name]

    if et == 'vtxlist':
        # check vertex file
        vf = elec.get('vtxfile')
        if not vf:
            print('\n\nMissing \'vtxfile\' entry in definition of electrode')
        else:
            if os.path.exists(vf):
                e += [stm_e+'.vtx_file', vf]
            else:
                print('\n\nVertex file {} does not exist.\n'.format(vf))

    elif et == 'vtxdata':
        # read in vertex indices
        vtcs = elec.get('vtxdata')
        # write vtx data to vtx file
        if stim_filename is not None:
           vf = stim_filename
        else:
            vf = stm_name+'.vtx'

        if not dry:
            write_vtx_file(vf, vtcs)
        e += [stm_e + '.vtx_file', vf]

    elif et == 'tag':
        stm_tag = elec.get('tag')
        e += [stm_e+'.geomID', stm_tag]

    elif et == 'sphere':
        e += [stm_e + '.geom_type', 1]
        e += get_elec_point(elec.get('p0'), 'p0', stm_e)
        e += [stm_e + '.radius', elec.get('radius')]

    elif et == 'cylinder':
        e += [stm_e + '.geom_type', 3]
        e += get_elec_point(elec.get('p0'), 'p0', stm_e)
        e += get_elec_point(elec.get('p1'), 'p1', stm_e)
        e += [stm_e + '.radius', elec.get('radius')]

    elif et == 'block':
        e += [stm_e + '.geom_type', 2]
        e += get_elec_point(elec.get('p0'), 'p0', stm_e)
        e += get_elec_point(elec.get('p1'), 'p1', stm_e)

    elif et == 'ucc_sphere':
        pnt_info = elec.get('p0')
        cavity = elec.get('cavity')
        spec_line = ['--sph-query', stm_name, cavity]

        if cavity in ('lv', 'rv'):
            spec_line += [pnt_info.get('z'), pnt_info.get('rho'), pnt_info.get('phi'),
                          elec.get('radius')]

        elif cavity in ('la', 'ra'):
            spec_line += [pnt_info.get('alpha'), pnt_info.get('beta'), pnt_info.get('gamma'),
                          elec.get('radius')]

        spec_line += [elec.get('searchdom')]
        if stim_filename is not None:
            spec_line += ['--out-format', stim_filename]
        vf = mgquery_idx_call(job, spec_line, stm_name, model_name, model_format, ucc_file)
        if vf is None:
            print('Skipping UCC sphere electrode "{}" !'.format(stm_name))
        else:
            if stim_filename is not None:
                e += [stm_e + '.vtx_file', stim_filename]
            else:
                e += [stm_e + '.vtx_file', vf]

    elif et == 'ucc_cylinder':
        pnt_info_0 = elec.get('p0')
        pnt_info_1 = elec.get('p1')
        cavity = elec.get('cavity')

        spec_line = ['--cyl-query', stm_name, cavity]

        if cavity in ('lv', 'rv'):
            spec_line += [pnt_info_0.get('z'), pnt_info_0.get('rho'), pnt_info_0.get('phi'),
                          pnt_info_1.get('z'), pnt_info_1.get('rho'), pnt_info_1.get('phi'),
                          elec.get('radius')]

        elif cavity in ('la', 'ra'):
            spec_line += [pnt_info_0.get('alpha'), pnt_info_0.get('beta'), pnt_info_0.get('gamma'),
                          pnt_info_1.get('alpha'), pnt_info_1.get('beta'), pnt_info_1.get('gamma'),
                          elec.get('radius')]

        spec_line += [elec.get('searchdom')]
        if stim_filename is not None:
            spec_line += ['--out-format', stim_filename]
        vf = mgquery_idx_call(job, spec_line, stm_name, model_name, model_format, ucc_file)
        if vf is None:
            print('Skipping UCC cylinder electrode "{}" !'.format(stm_name))
        else:
            if stim_filename is not None:
                e += [stm_e + '.vtx_file', stim_filename]
            else:
                e += [stm_e + '.vtx_file', vf]
    elif et == 'ucc_block':
        pnt_info_0 = elec.get('p0')
        pnt_info_1 = elec.get('p1')
        cavity = elec.get('cavity')
        spec_line = ['--box-query', stm_name, cavity]

        if cavity in ('lv', 'rv'):
            spec_line += [pnt_info_0.get('z'), pnt_info_0.get('rho'), pnt_info_0.get('phi'),
                          pnt_info_1.get('z'), pnt_info_1.get('rho'), pnt_info_1.get('phi'),
                          elec.get('radius')]

        elif cavity in ('la', 'ra'):
            spec_line += [pnt_info_0.get('alpha'), pnt_info_0.get('beta'), pnt_info_0.get('gamma'),
                          pnt_info_1.get('alpha'), pnt_info_1.get('beta'), pnt_info_1.get('gamma'),
                          elec.get('radius')]
        spec_line += [elec.get('searchdom')]
        if stim_filename is not None:
            spec_line += ['--out-format', stim_filename]
        vf = mgquery_idx_call(job, spec_line, stm_name, model_name, model_format, ucc_file)
        if vf is None:
            print('Skip UCC box electrode "{}" !'.format(stm_name))
        else:
            if stim_filename is not None:
                e += [stm_e + '.vtx_file', stim_filename]
            else:
                e += [stm_e + '.vtx_file', vf]

    else:
        print('Electrode definition of type {} not supported.'.format(et))

    # write dump for now to check correct selection
    e += [stm_e + '.dump_vtx_file', 1]

    return e


# define single lat detector for recording the activation sequence
def ek_lat_detect(idx, threshold=-10, **kwargs):
    lat_prefix = '-lats[{}]'.format(idx)
    lat_opts = [lat_prefix + '.ID', 'vm_act_seq.dat',
                lat_prefix + '.measurand', 0,
                lat_prefix + '.all', 0,
                lat_prefix + '.mode', 0,
                lat_prefix + '.threshold', threshold,
                lat_prefix + '.method', 1]

    if 'verbose' in kwargs:
        hdr = 'LAT detector added with index {}.'.format(idx)
        print(hdr+'\n'+'-'*len(hdr))
        print(*lat_opts)

    return lat_opts


# define single lat detector for recording the activation sequence
def lat(idx, ID, threshold=-10, measurand=0, mode=0, detect_all=0, method=1):
    lat_prefix = '-lats[{}]'.format(idx)
    lat_opts = [lat_prefix + '.ID', ID,
                lat_prefix + '.measurand', measurand,
                lat_prefix + '.all',  detect_all,
                lat_prefix + '.mode', mode,
                lat_prefix + '.threshold', threshold,
                lat_prefix + '.method', method]
    return lat_opts


# add a sentinel for activity checking
def sentinel(idx, start, window):
    sentinel_opts = ['-sentinel_ID', idx,
                     '-t_sentinel_start', start,
                     '-t_sentinel', window]
    return sentinel_opts


# select propagation model
def propagation_mode(mode):
    pmode_dict = {'ek': 'ek', 'EK': 'ek', 'eikonal': 'ek',
                  're': 're', 'R-E+': 're', 'R-E-': 're',
                  'rd': 'rd', 'R-D': 'rd'}
    pmode = pmode_dict.get(mode, None)
    if pmode is None:
        print('\nUnknown propagation mode selected.\n')
    return pmode


# setup mesh-related command options
def setup_mesh(meshname, use_split=True, split_file=None, tags_file=None):
    """
    Set up mesh command including the use of a split file as well as an alternative
    element tag vector

    :param meshname:
    :param use_split:
    :param split_file:
    :param tags_file:
    :return: mesh_opts command line parameters for selecting mesh, split file and alternative tag vector
    """
    mesh_opts = ['-meshname', meshname]
    # use split information?
    if use_split:
        if split_file is None:
            # use default split file
            split_file = '{}.splt'.format(meshname)

        if os.path.exists(split_file):
            mesh_opts += ['-use_splitting', 1,
                          '-splitname', split_file]
        else:
            print('Splitting selected, but split file {} does not exist!'.format(split_file))
            print('Proceed without splitting.')
    else:
        print('Use of split file disabled.')

    if tags_file is not None:
        if tags_file.endswith('.tag'):
            tags_file = tags_file[:-4]
        elif tags_file.endswith('.btag'):
            tags_file = tags_file[:-5]
        if os.path.exists(tags_file+'.tag') or os.path.exists(tags_file+'.btag'):
            mesh_opts += ['-etagsname', tags_file]
        else:
            print('Tags selected, but tag file {}.{{tag,btag}} does not exist!'.format(tags_file))
            print('Proceed without re-tagging.')

    return mesh_opts


# return intra mesh extension as function of run mode
def intra_mesh_ext(rmode):
    return '_ek' if rmode == 'ek' else '_i'


# set up tissue properties
def setup_tissue(cnf, fncs, rmode):
    # define excitable regions based on the definition of EP
    i_domain = []
    e_domain = []

    for dkey, domain in cnf.items():
        n_domain = domain.get(CONFIGURATION_TAGS)
        # if no tags assigned, empty domain, skip
        if not n_domain:
            continue
        fnc_name = domain.get(CONFIGURATION_FUNC)
        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            # function name is not defined
            print('Function {} not defined!'.format(fnc_name))
            continue

        # all domains are part of extra
        e_domain += n_domain

        if fnc_def.get(FUNCTION_EP) is not None:
            i_domain += n_domain

    i_domain = sorted(i_domain)
    e_domain = sorted(e_domain)

    phys_regs = []
    pmode = propagation_mode(rmode)
    if pmode == 'ek':
        phys_regs = gen_physics(IntraTags=i_domain, EikonalTags=i_domain)
    elif pmode == 'rd':
        phys_regs = gen_physics(IntraTags=i_domain, ExtraTags=e_domain)
    elif pmode == 're':
        phys_regs = gen_physics(IntraTags=i_domain, ExtraTags=e_domain, EikonalTags=i_domain)
    else:
        print('Unknown propagation mode {}.'.format(rmode))

    # set up gregion and ek_region
    g_regs = G_secs_to_carp(cnf, fncs)

    ek_regs = []
    if pmode == 'ek' or pmode == 're':
        ek_regs += CV_secs_to_carp(cnf, fncs)

    # set up EP
    imp_regs = []
    #if p_mode(rmode) != 'ek':
    imp_regs += ep_secs_to_carp(cnf, fncs)

    cmd = imp_regs
    cmd += g_regs

    if not settings.is_open_carp:
        cmd += ek_regs
    cmd += phys_regs

    return cmd


# check configuration
def check_config(cnf, fncs):

    chk_msg = 'Checking configuration and function definitions'
    print('\n{}'.format(chk_msg))
    print('-'*len(chk_msg))

    # get bath labels for marking non active EP
    #bl, cnf = get_bath_labels(cnf, pop=True)

    max_len = max(list(map(lambda s: len(s), cnf.keys())))

    ok = True
    bl_flg = '- [ passive (\'BATH\')] '
    for name, conf in cnf.items():
        tags = conf.get(CONFIGURATION_TAGS)
        fnc_name = conf.get(CONFIGURATION_FUNC)

        # look up in function definition
        name_just = name.ljust(max_len, ' ')
        func = fncs.get(fnc_name)
        if func and (func_def := func.get(FUNCTION_CV)) is not None:
            G = func_def.get(FUNCTION_CV_COND)
            is_active = G is not None and all(cond_name in G for cond_name in FUNCTION_CV_COND_ACTIVE_NAMES)
            label = '' if is_active else bl_flg
            print('Domain {} : tags {} use function {} {}'.format(name_just, tags, fnc_name, label))
        else:
            print('Domain {} : tags {} use function {} -- not defined.'.format(name_just, tags, fnc_name))
            ok = False

    if not ok:
        print('\n\n')
        print('Configuration check failed, domains using undefined functions found.\n')

    return ok


# add solver settings
def add_slv_settings(slv):

    cmd = []
    if (dt := slv.get(SOLVER_DT, None)) is not None:
        cmd += ['-dt', float(dt)]
    if (ts := slv.get(SOLVER_PARABSOLVE, None)) is not None:
        cmd += ['-parab_solve', int(ts)]

    mass_lump = slv.get(SOLVER_MASSLUMPING, 0)
    cmd += ['-mass_lumping', int(mass_lump)]
    opsplit = slv.get(SOLVER_OPSPLIT, 0)
    cmd += ['-operator_splitting', int(opsplit)]

    if (stol := slv.get(SOLVER_CGTOL)) is not None:
        cmd += ['-cg_tol_parab', float(stol)]

    beqm = slv.get(SOLVER_BEQM, 0)
    cmd += ['-bidm_eqv_mono', int(beqm)]

    srcModel = slv.get(SOLVER_SRC_MODEL, 'monodomain')
    if srcModel.startswith('mono'):
        bidm = 0
    elif srcModel.startswith('pseudo'):
        bidm = 2
    elif srcModel.startswith('bido'):
        bidm = 1
    else:
        print('Unknown source model {} given, switching to default monodomain')
        bidm = 0
    cmd += ['-bidomain', int(bidm)]

    return cmd


#Get the duration of the simulation. This differs depending on which version of studio you are using!
def setup_timings(plan):

    if plan.get(SIMULATION).get('end time') is not None:
        tend = plan.get(SIMULATION).get('end time')
    elif plan.get(SIMULATION).get('timesteps') is not None:
        tend = plan.get(SIMULATION).get('timesteps')
    else:
        print('No entry for tend detected. Using default of 100 ms.')
        tend = 100.0

    time_cmd = ['-tend', tend,
                '-spacedt', plan.get(SOLVER).get('ts', 1),
                '-timedt', plan.get(SOLVER).get('ts', 1)]

    return time_cmd


# make sure all specified initial state vectors are found
def check_initial_state_vectors(funcs, verbose):

    max_len = max(list(map(lambda s: len(s), funcs.keys())))
    if verbose:
        header = 'Checking specified initialization files:'
        print('\n'+header+'\n'+'-'*len(header))

    missing = 0
    for name, func in funcs.items():
        if func:
            ep = func.get(FUNCTION_EP)
            if ep:
                init_file = ep.get(FUNCTION_EP_INIT)
                if init_file is not None and len(init_file):
                    name_just = name.ljust(max_len, ' ')
                    if not os.path.exists(init_file):
                        if verbose:
                            print('{} : Initialization uses file {} -- missing.'.format(name_just, init_file))
                        missing += 1
                    else:
                        if verbose:
                            print('{} : Initialization uses file {}.'.format(name_just, init_file))

    return not missing


# update experimental description
def update_exp_desc(up_flag, exp_file, exp, indent=2):
    if up_flag:
        # update experimental settings file
        fname = exp_file
    else:
        # dump to alternative file for review/comparison
        dname = os.path.dirname(exp_file)
        bname = os.path.basename(exp_file)
        base, ext = os.path.splitext(bname)
        fname = os.path.join(dname, 'tmp-{}{}'.format(base, ext))

    print('Updating experimental settings to {}'.format(fname))
    # save_json(exp, fname, indent=indent, overwrite=True)
    save_simulation_plan(exp, fname, indent=indent, overwrite=True)


def generate_split_file(job, plan, mesh_name, force_recreation=False):
    # check if 'SPLIT' entry exists.
    # if not, return None
    if (split := plan.get(SPLIT, None)) is None:
        return None, False

    msg = 'Found splitting definition.'
    msg += '\n'+'-'*len(msg)
    print('\n'+msg)

    active = split.get(SPLIT_APPLY, True)
    if not active:
        print('Splitting deactivated!')
        return None, False

    splt_name = mesh_name+'.splt'

    # if the file exists and creation is not forced
    # return the file name
    if os.path.exists(splt_name) and not force_recreation:
        print('Splitting file {} already exists!'.format(splt_name))
        return splt_name, True

    op_str = split.get(SPLIT_OPSTR, None)
    if op_str is None:
        print('Failed to generate op-string, deactivate splitting')
        return None, False

    meshtool.generate_split(job, mesh_name, op_str, splt_name)
    print('Splitting file {} created!'.format(splt_name))

    return splt_name, True


def plan2carp(job, plan_file, meshname, rmode):

    plan_abs = os.path.abspath(plan_file)
    mesh_abs = os.path.abspath(meshname)
    plan_dir = os.path.dirname(plan_abs)

    # we navigate into the directory of the plan file in order to resolve the relative paths of the plan
    workdir = os.getcwd()
    os.chdir(plan_dir)

    plan = load_simulation_plan(plan_abs, verbose=True)
    confs = plan[CONFIGURATIONS]
    funcs = plan[FUNCTIONS]
    slv = plan[SOLVER]

    # create split file
    split_file, split_active = generate_split_file(job, confs, mesh_abs, force_recreation=False)

    # setup mesh
    cmd = setup_mesh(mesh_abs, split_file)

    # set up tissue properties
    cmd += setup_tissue(confs, funcs, rmode)

    # set up solver settings
    cmd += add_slv_settings(slv)

    # when done, we go back to the actual working director
    os.chdir(workdir)

    return cmd


class SweepSetup:

    def __init__(self, plan, purkinje_plan=None, ext_values_output=None, pce_order=5,
                 num_samples=2, sampling_method='lhs', write_files=True):

        assert sampling_method in ('saltelli', 'lhs', 'file', 'uq')

        print('Setup of Parameter Sweeping')

        self.__orig_plan = plan

        assert isinstance(plan, dict), 'The model configuration must be a dictionary!'
        param_sweep_info = self.__get_sampling_from_dict(plan)

        if purkinje_plan is not None:
            self.purkinje_plan = load_json(purkinje_plan)
            assert isinstance(self.purkinje_plan, dict), 'The purkinje configuration must be a dictionary!'
            param_sweep_info = self.__get_sampling_from_dict(self.purkinje_plan, param_sweep_info=param_sweep_info)

        # if not param_sweep_info['sampling'] and not param_sweep_info['map'].keys() and not param_sweep_info['sweep']:
        #    print('No parameters specified for sweeping!\n')
        # else:
        print('sampling Method: {}'.format(sampling_method))
        print('Number of samples taken for unset parameters: {}'.format(num_samples))
        if write_files:
            param_map = self.__expand_parameters(param_sweep_info, num_samples=num_samples,
                                                 sampling_method=sampling_method, pce_order=pce_order,
                                                 ext_values_output=ext_values_output)
        else:
            param_map = self.__expand_parameters(param_sweep_info, num_samples=num_samples,
                                                 pce_order=pce_order, sampling_method=sampling_method)
        self.param_map = param_map
        self.sweep_info = param_sweep_info

    def set_configuration(self, param_map):
        def assign_dict_value(dictobj, keys, value):
            if len(keys) > 1:
                assign_dict_value(dictobj[keys[0]], keys[1:], value)
            else:
                dictobj[keys[0]] = value

        plan_config = self.__orig_plan.copy()

        for key, value in param_map.items():
            if key != 'suffix':
                key_split = key.split('.')
                if 'G' in key_split or 'EP' in key_split:
                    for LF_name, LF_info in plan_config[LFSETUP]['leadfields'].items():
                        plan_config[LFSETUP]['leadfields'][LF_name]['file'] = None
                        #raise NotImplementedError('Sweeping is not supported for conductivity or ionic model regions yet as it depends on leadfield!')
                assign_dict_value(plan_config, key_split, value)

        stimuli = plan_config[SIMULATION][STIMULATION_STIMULI]
        elecs = plan_config[ELECTRODES]
        fncs = plan_config[FUNCTIONS]
        slv = plan_config[SOLVER]
        cnf = plan_config[CONFIGURATIONS]
        lfsetup = plan_config.get(LFSETUP, None)

        print_settings_config(stimuli)

        return stimuli, elecs, cnf, fncs, slv, lfsetup, plan_config

    @staticmethod
    def __get_sampling_from_dict(config, param_sweep_info=None):
        def iterate_dict(param_sweep_info, obj, str_label='', depth=0):
            if isinstance(obj, dict):
                for key, val in obj.items():
                    if str_label == '':
                        curr_label = str(key)
                    else:
                        curr_label = str_label + '.' + str(key)
                    if not isinstance(val, dict):
                        param_sweep_info = SweepSetup.check_sampling(curr_label, val, param_sweep_info)
                    else:
                        param_sweep_info = iterate_dict(param_sweep_info, val, str_label=curr_label)

            return param_sweep_info

        if param_sweep_info is None:
            param_sweep_info = {'sweep': [], 'sampling': [], 'map': {}}
        else:
            for x in {'sweep', 'sampling', 'map'}:
                if x not in param_sweep_info.keys():
                    raise IOError('Missing key in param info: {}'.format(x))

        param_sweep_info = iterate_dict(param_sweep_info, config, '')

        return param_sweep_info
        # read first line

    @staticmethod
    def check_sampling(label, value_string, param_sweep_info):

        assert isinstance(label, str)

        if isinstance(value_string, str):

            if value_string.startswith('limits(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[7:-1].split(',')))
                if len(values) != 2:
                    raise ValueError('Error in key {}, limits expects two arguments, got {}!'.format(label, len(values)))
                min_value, max_value = float(values[0]), float(values[1])
                if max_value < min_value:
                    raise ValueError('Error, bad limit values {}!'.format(value_string))
                param_sweep_info['map']['{}_min'.format(label)] = min_value
                param_sweep_info['map']['{}_max'.format(label)] = max_value

            elif value_string.startswith('range(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[6:-1].split(',')))
                if len(values) != 3:
                    raise ValueError('Error in key {}, range expects three arguments, got {}!'.format(label, len(values)))
                start_value, stop_value, step_value = float(values[0]), float(values[1]), float(values[2])
                if start_value > stop_value or not step_value > 0.0:
                    raise ValueError('Error, bad limit values {}!'.format(value_string))

                param_sweep_info['map'][label] = start_value
                sweep_values = numpy.arange(start_value, stop_value, step_value, dtype=numpy.float64)  # start stop step
                param_sweep_info['sweep'].append((label, sweep_values))

            elif value_string.startswith('linspace(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[9:-1].split(',')))
                if len(values) != 3:
                    raise ValueError('Error in key {}, linspace expects three arguments, got {}!'.format(label, len(values)))
                start_value, stop_value, num_value = float(values[0]), float(values[1]), int(values[2])
                if start_value > stop_value or not num_value >= 1:
                    raise ValueError('Error, bad limit values {}!'.format(value_string))
                param_sweep_info['map'][label] = start_value
                sweep_values = numpy.linspace(start_value, stop_value, num=num_value,
                                              endpoint=True, dtype=numpy.float64)
                param_sweep_info['sweep'].append((label, sweep_values))

            elif value_string.startswith('sample(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[7:-1].split(',')))
                if len(values) != 2:
                    raise ValueError('Error in key {}, sample expects two arguments, got {}!'.format(label, len(values)))
                start_value, stop_value = float(values[0]), float(values[1])
                if start_value > stop_value:
                    raise ValueError('Error, bad sampling values in key {}!'.format(label))
                param_sweep_info['map'][label] = start_value
                param_sweep_info['sampling'].append((label, start_value, stop_value))

            elif value_string[0:7] == '"sample' and value_string[-1] == '"':
                value_string = value_string[1:-1]
                values = tuple(map(lambda s: s.strip(), value_string[7:-1].split(',')))
                if len(values) != 2:
                    raise ValueError('Error in key {}, sample expects two arguments, got {}!'.format(label, len(values)))
                start_value, stop_value = float(values[0]), float(values[1])
                if start_value > stop_value:
                    raise ValueError('Error, bad sampling values in key {}!'.format(label))
                param_sweep_info['map'][label] = start_value
                param_sweep_info['sampling'].append((label, start_value, stop_value))

            elif value_string.startswith('set(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[4:-1].split(',')))
                if len(values) > 0:
                    sweep_values = tuple(sorted(set(map(float, values))))
                    param_sweep_info['map'][label] = sweep_values[0]
                    param_sweep_info['sweep'].append((label, sweep_values))
                else:
                    param_sweep_info['map'][label] = float(values[0])

            elif value_string.startswith('set_sample(') and value_string[-1] == ')':
                values = tuple(map(lambda s: s.strip(), value_string[11:-1].split(',')))
                if len(values) > 0:
                    sweep_values = tuple(map(float, values))
                    param_sweep_info['map'][label] = sweep_values[0]
                    param_sweep_info['sweep'].append((label, sweep_values))
                else:
                    param_sweep_info['map'][label] = float(values[0])

        return param_sweep_info

    @staticmethod
    def __expand_parameters(param_sweep_info, num_samples=100, param_suffix=True,
                            ext_values_output=None, sample_problem_output=None, pce_order=5,
                            sampling_method='saltelli', calc_second_order=False):

        if len(param_sweep_info['sampling']) == 0 and len(param_sweep_info['sweep']) == 0:
            return [param_sweep_info['map']]

        # extend sampling parameters
        sampling_problem = dict(num_vars=0, names=list(), bounds=list())
        sampling_values = list()
        if len(param_sweep_info['sampling']) > 0:
            for i, (label, start_value, stop_value) in enumerate(param_sweep_info['sampling']):
                sampling_problem['num_vars'] += 1
                sampling_problem['names'].append(label)
                sampling_problem['bounds'].append([start_value, stop_value])

            if sampling_method == 'saltelli' and SA_AVAILABLE:
                sample = functools.partial(saltelli.sample, calc_second_order=calc_second_order)  # , seed=None)
            elif sampling_method == 'lhs' and SA_AVAILABLE:
                sample = functools.partial(latin.sample, seed=None)
            elif sampling_method == 'uq' and UQ_AVAILABLE:

                distribution = []
                for num in range(0, sampling_problem['num_vars']):
                    distribution.append(
                        BetaDistribution(alpha=1., beta=1., domain=numpy.array(sampling_problem['bounds'][num])))

                uq_info = {'distribution': [x for x in distribution],
                           'order': pce_order,
                           'plabels': sampling_problem['names']}

                pce = PolynomialChaosExpansion(distribution=distribution,
                                               order=pce_order,
                                               plabels=sampling_problem['names'])
                pce.generate_samples()

            if sampling_method == 'uq':
                sampling_values = pce.samples
                uq_info['samples'] = pce.samples.tolist()
            else:
                sampling_values = sample(sampling_problem, num_samples)

            if sample_problem_output is not None and isinstance(sample_problem_output, str):
                with open(sample_problem_output, 'w') as fp:
                    json.dump(sampling_problem, fp, indent=2)

        # extend sweep parameters
        sweep_values, sweep_labels = list(), list()

        if len(param_sweep_info['sweep']) > 0:
            param_values, sweep_indices = list(), list()
            for i, (label, values) in enumerate(param_sweep_info['sweep']):
                param_values.append(values)
                sweep_labels.append(label)
                sweep_indices.append(numpy.arange(0, len(values), dtype=numpy.int32))

            if sampling_method != 'file':
                for idx in itertools.product(*sweep_indices):
                    values = list()
                    for i, j in enumerate(idx):
                        values.append(param_values[i][j])
                    sweep_values.append(values)
            else:

                for k in sweep_indices[0]:
                    values = list()
                    for idx in range(0, len(sweep_labels)):
                        values.append(param_values[idx][k])
                    sweep_values.append(values)

            sweep_values = numpy.array(sweep_values)

        # merge sampling and sweep parameters
        values, labels = list(), list()
        if len(param_sweep_info['sampling']) == 0:
            values, labels = sweep_values, sweep_labels
        elif len(param_sweep_info['sweep']) == 0:
            values, labels = sampling_values, sampling_problem['names']
        else:
            labels = sweep_labels + sampling_problem['names']
            for sweep_param_set in sweep_values:
                for sampling_param_set in sampling_values:
                    values.append(list(sweep_param_set) + list(sampling_param_set))
            values = numpy.array(values)

        if ext_values_output is not None and isinstance(ext_values_output, str):
            print('\nWriting sampling to file: {}'.format(ext_values_output))
            if not os.path.exists(os.path.dirname(ext_values_output)):
                os.mkdir(os.path.dirname(ext_values_output))
            numpy.savetxt(ext_values_output, values, fmt='%.4f', delimiter=',', header=','.join(labels))

            if sampling_method == 'uq':
                with open(ext_values_output.replace('.csv', '.uq.pce'), 'wb') as uq_out:
                    pickle.dump(pce, uq_out)

        # fill parameter sets with values
        param_maps = list()
        for param_set in values:
            param_maps.append(copy.deepcopy(param_sweep_info['map']))

            param_suffix_list = list()
            for j, value in enumerate(param_set):
                label = labels[j]
                param_maps[-1][label] = value
                # replace some characters in the label
                suffix_label = label.replace('[', '').replace(']', '').replace('.', '_').lower()
                param_suffix_list.append('{}{}'.format(suffix_label, numpy.round(value, 4)))

            suffix = '_'.join(param_suffix_list) if param_suffix else '{:06d}'.format(len(param_maps))
            param_maps[-1]['suffix'] = suffix

        return param_maps


def check_stim_protocol(config, check_type='stimulus'):

    assert check_type in ('electrodes', 'stimulus'), 'Check type must be either electrodes or stimulus!'

    STIMULUS_KEYS = ('active', 'electrode', 'protocol', 'type', 'strength')
    PROTOCOL_KEYS = ('start', 'duration', 'npls', 'bcl')

    print_settings_config(config)
    missing_keys = config
    error_check = False
    given_stim = 0
    print('Check type: {}'.format(check_type))
    for key, val in config.items():
        if isinstance(val, dict):
            if not isinstance(config[key], dict):
                raise IOError('The entry {} must be a dictionary with the labels: {}'.format(key, val.keys()))
            else:
                if check_type == 'stimulus':
                    for test_key in STIMULUS_KEYS:
                        if test_key not in val.keys():
                            missing_keys[key][test_key] = 'MISSING'
                            error_check = True

                    if val['active']:
                        given_stim += 1
                    if config[key]['type'] not in STIMULATION_TYPE_MAP.keys():
                        print('stim type "{}" for stim "{}" is not in Stim map!'.format(config[key][STIMULATION_TYPE], key))
                        sys.exit()

        if check_type == 'stimulus':
            protocol_info = config[key]['protocol']
            for subkey in PROTOCOL_KEYS:
                if subkey not in protocol_info.keys():
                    missing_keys[key]['protocol'][subkey] = 'MISSING'
                    error_check = True

            electrode_info = config[key]['electrode']

        else:
            electrode_info = config[key]

        electrode_type = electrode_info['type']
        if electrode_type == 'vtxdata':
            if 'vtxdata' not in electrode_info.keys():
                missing_keys[key]['electrode']['vtxdata'] = 'MISSING'
                error_check = True

        elif electrode_type == 'sphere':
            for entry in ('p0', 'radius'):
                if entry not in electrode_info.keys():
                    missing_keys[key]['electrode'][entry] = 'MISSING'
                    error_check = True

        elif electrode_type == 'ucc':
            if 'cavity' not in electrode_info.keys():
                missing_keys[key]['electrode']['cavity'] = 'MISSING'
                error_check = True
            else:
                if float(electrode_info['cavity']) in {'la', 'ra'}:
                    STIM_KEYS = ('alpha', 'beta', 'gamma')
                else:
                    STIM_KEYS = ('z', 'rho', 'phi')

                for subkey in STIM_KEYS:
                    if subkey not in electrode_info.keys():
                        missing_keys[key]['electrode'][subkey] = 'MISSING'
                        error_check = True

    return missing_keys, error_check, given_stim


class LeadfieldComputationOptions:

    def __init__(self, job, lfsetup, output_dir, dry=False ):

        self.__job = job
        self.__lfsetup = lfsetup
        self.__output_dir = output_dir
        self.__dry = dry

        if not os.path.exists(self.__output_dir):
            job.mkdir(self.__output_dir, parents=True)

        self.__lookup_file = os.path.join(output_dir, 'leadfield_lookup.txt')
        #ss This must go last in the initialization as it depends on the other entries
        self.__check_lfsetup()

    def leadfield_check(self, base_opts):
        
        log_opts = self.log_options(base_opts)
        def Diff(li1, li2):
            li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
            return li_dif

        lf_exists = None
        leadfield_log = {}

        print('Checking for existance of leadfields: {}'.format(os.path.abspath(self.__lookup_file)))
        if os.path.isfile(os.path.abspath(self.__lookup_file)):
            leadfield_log = load_json(self.__lookup_file)
            for dir_key, entry in leadfield_log.items():
                check_lf_str = numpy.asarray(log_opts, dtype=str).tolist()
                is_equal = check_lf_str == entry
                if is_equal:
                    lf_exists = os.path.join(self.__output_dir, dir_key)
                    break
                #else:
                    #diff_opts = Diff(entry, check_lf_str)
                    #print('Difference in options: {}'.format(diff_opts))

        else:
            print('Could not find the lookup file: {}'.format(self.__lookup_file))
            print('Leadfield computation is necessary!')

        self.__leadfield_log = leadfield_log

        return lf_exists

    def update_log(self, base_opts):
        out_base_name = os.path.basename(self.__output_dir)
        self.__leadfield_log[out_base_name] = numpy.asarray(self.log_options(base_opts), dtype=str).tolist()
        save_json(self.__leadfield_log, self.__lookup_file)

    def update_config(self):
        self.leadfield_dir = os.path.abspath(self.__output_dir)
        for LF_name, LF_info in self.__lfsetup['leadfields'].items():
            LF_file_basename = 'LF_Z_extra_Ref_{}_Field_{}.dat'.format(self.__lfsetup['refvtx'], LF_info['srcvtx'])
            LF_file = os.path.join(os.path.abspath(self.__output_dir), LF_file_basename)
            self.__lfsetup['leadfields'][LF_name]['file'] = LF_file
        return self.__lfsetup

    def log_options(self, carp_options):
        return carp_options[numpy.where(numpy.asarray(carp_options) == '-meshname')[0][0]::]

    def __check_lfsetup(self):

        self.__field_vtx_file = os.path.join(self.__output_dir, 'lfsetup.vtx')
        num_LFs = len(self.__lfsetup['leadfields'].keys())

        count_LFs = 0
        leadfield_dirs = []
        srcvtx_list = []

        for LF_name, LF_info in self.__lfsetup['leadfields'].items():
            if LF_info['file'] is not None and os.path.isfile(LF_info['file']):
                count_LFs += 1
                print('Leadfield already computed: {}'.format(LF_info['file']))
                leadfield_dirs.append(os.path.dirname(LF_info['file']))

            srcvtx_info = LF_info['srcvtx']
            if srcvtx_info is not None:

                if isinstance(srcvtx_info,dict):
                    lf_vtx_config, error_check, _ = check_stim_protocol(srcvtx_info, check_type='electrodes')
                    if error_check:
                        print_settings_config(lf_vtx_config)
                        print('Missing entries for computation!')
                        sys.exit()

                elif isinstance(srcvtx_info, int):
                    srcvtx_list.append(int(LF_info['srcvtx']))
                else:
                    print(type(srcvtx_info))
                    print('Not a supported vtx type!')
                    sys.exit()

        if count_LFs == num_LFs == len(srcvtx_list):
            leadfield_dirs = numpy.unique(leadfield_dirs)
            assert len(leadfield_dirs) == 1, 'More than one leadfield dir is specified: {}'.format(leadfield_dirs)
            leadfield_dir = os.path.abspath(str(leadfield_dirs[0]))

        elif count_LFs == 0:
            leadfield_dir = None
            print('Leadfields have not yet been computed')

        else:
            print('Number of computed LFs: {}'.format(count_LFs))
            print('Number of field vertices: {}'.format(len(srcvtx_list)))
            print('Expected number of leadfields: {}'.format(num_LFs))
            leadfield_dir = None
            print('Leadfields have not been properly computed!')

        self.__srcvtcs = srcvtx_list

        if not self.__dry:
            write_vtx_file(self.__field_vtx_file, self.__srcvtcs)

        self.leadfield_dir = leadfield_dir

        return

    def __lf_base_opts(self):
        lf_opts = ['-lead_field_sources', self.__field_vtx_file.replace('.vtx', ''),
                   '-lead_field_mode', 0,
                   '-lead_field_meth', 1,
                   '-lead_field_egm_file', 'lfEGMs',
                   '-lead_field_ref', self.__lfsetup['refvtx']]
        return lf_opts

    def recover_opts(self):
        rlf_opts = self.__lf_base_opts()
        rlf_opts += ['-lead_field_dir', self.leadfield_dir]
        return rlf_opts

    def computation_opts(self, base_opts, ground_vtx_file):
        clf_opts = []
        clf_opts += base_opts
        # overrule 'bidomain' and 'simID' argument
        # the last occurrence in the options is taken
        clf_opts += ['-bidomain', 1,
                     '-simID', self.__output_dir]
        clf_opts += ['-num_stim', 1]
        clf_opts += get_ground_stimulus(self.__lfsetup, ground_vtx_file, 0)
        clf_opts += self.__lf_base_opts()
        clf_opts += ['-dump_lead_fields', 1]
        clf_opts += ['-experiment', 9]

        return clf_opts