#!/usr/bin/env python3

# ---( system imports )--------------------------
import os
import sys
# ---( carputils imports )-----------------------
from carputils import tools
from carputils.forcepss import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Prepacing and validation of tissue/organ models'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)


def setup_lats_prepacing(pp_bcl, pp_cycs, lat_file):
    # compute lats sequence based on S1_elecs
    # assemble lats prepacing structure
    return ['-prepacing_bcl', pp_bcl,
            '-prepacing_beats', pp_cycs,
            '-prepacing_lats', lat_file]


# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prop):
    return (0, 250) if prop in {'ek', 're'} else (9, -30)


# prepace tissue to generate initial state
def setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode,
                  use_split=True, split_file=None, tags_file=None):
    # unpack experimental settings
    pp_bcl = pp_prtcl.get(j2c.PROTOCOLS_BCL)
    pp_cyc = pp_prtcl.get(j2c.PROTOCOLS_CYCLES)
    pp_elecs = pp_prtcl.get(j2c.PROTOCOLS_ELECTRODES)
    pp_rtimes = pp_prtcl.get(j2c.PROTOCOLS_RELTIMINGS)
    # check whether all prepacing electrodes are defined
    for e in pp_elecs:
        if e not in elecs:
            print('\n\nElectrode {} required for prepacing, but not defined.\n'.format(e))
            sys.exit()

    # decide on stimulus type as function of propagation mode
    s_type, s_strength = stim_mode(rmode)

    # assign protocols to all electrodes used for pre-pacing
    stims = []
    for i, e in enumerate(pp_elecs):
        elec, idx_hd = j2c.elec_sec_to_carp(None, elecs, i, e)
        ptcl = ['-stim[{}].ptcl.start'.format(i), pp_rtimes[i],
                '-stim[{}].ptcl.duration'.format(i), 2,
                '-stim[{}].pulse.strength'.format(i), s_strength,
                '-stim[{}].crct.type'.format(i), s_type]
        stims += elec + ptcl
    
    num_stims = len(pp_elecs)
    if rmode == 're':
        stims += ['-stim[{}].crct.type'.format(num_stims), 8]
        num_stims += 1
    stims = ['-num_stim', num_stims] + stims

    # set up simulation command
    cmd = tools.carp_cmd()
    cmd += stims

    # setup mesh
    # splt = True # use split-list if available
    cmd += j2c.setup_mesh(meshname, use_split=use_split, split_file=split_file, tags_file=tags_file)
    # set up tissue properites
    cmd += j2c.setup_tissue(cnf, fncs, rmode)

    return cmd


# create mesh ID
def mesh_ID(meshname):
    return os.path.basename(meshname)


def sim_ID(prefix, pp_prtcl, caseID):
    PCL = pp_prtcl.get(j2c.PROTOCOLS_BCL)
    # determine electrodes involved in pre-pacing
    pp_elecs = ''
    for e in pp_prtcl.get(j2c.PROTOCOLS_ELECTRODES):
        pp_elecs += '_{}'.format(e)
    return '{}{}_PCL_{}_ms_{}'.format(prefix, pp_elecs, PCL, caseID)


# generate lat vector bei running eikonal or rd
def generate_lat(meshname, configs, funcs, elecs, solver, pp_prtcl, gen_lat, lats_file, visual,
                 use_split=True, split_file=None, tags_file=None):

    cmd_lat = setup_prepace(meshname, configs, funcs, elecs, pp_prtcl, gen_lat,
                            use_split=use_split, split_file=split_file, tags_file=tags_file)

    # total activation time must be shorter than bcl
    # use bcl as an upper limit for duration of simulation
    # in the rd case it would be more efficient if simulator could detect total activation time
    dur = pp_prtcl.get(j2c.PROTOCOLS_BCL)
    PCL = '{:.1f}'.format(dur)
    cmd_lat += ['-tend', dur]

    # determine electrodes involved in pre-pacing
    pp_elecs = ''
    for e in pp_prtcl.get(j2c.PROTOCOLS_ELECTRODES):
        pp_elecs += '_{}'.format(e)

    if gen_lat == 'ek':
        # generate lats by eikonal solve
        simID = sim_ID('ek_lat', pp_prtcl, mesh_ID(meshname))
        grdout = 'ek'
        cmd_lat += ['-simID', simID, '-experiment', 6]
        # lat vector handle
        tmp_lats = os.path.join(simID, 'vm_act_seq.dat')

    elif gen_lat == 'rd':
        # generate lats by rd solve
        simID = sim_ID('rd_lat', pp_prtcl, mesh_ID(meshname))
        grdout = 'i'
        lats_idx = 0
        lats = ['-num_LATs', 1] + j2c.ek_lat_detect(lats_idx, threshold=-20) + j2c.sentinel(lats_idx, 0, 20)
        cmd_lat += ['-simID', simID, '-experiment', 0, '-spacedt', 1.0] + lats
        # lat vector handle
        tmp_lats = os.path.join(simID, 'init_acts_vm_act_seq.dat-thresh.dat')
        cmd_lat += j2c.add_slv_settings(solver)

        # before running, check all specified initial states are there
        if not j2c.check_initial_state_vectors(funcs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, exiting!\n\n')
            sys.exit()

    # prepare visualization
    if visual:
        vis = ['-gridout_{}'.format(grdout), 3, '-spacedt', 1]
        cmd_lat += vis

    # generate activation sequcne
    return cmd_lat, tmp_lats, simID


# create carp checkpoint array based on tsav's
def setup_tsavs(tsav, tsav_basename):
    # checkpoint timings
    chkpt = ['-num_tsav', len(tsav)]
    for i, t in enumerate(tsav):
        chkpt += ['-tsav[{}]'.format(i), t]
    chkpt += ['-write_statef', tsav_basename]
    return chkpt


# actual prepacing
def prepace(meshname, cnf, fncs, elecs, pp_prtcl, mode, lats_file, rmode, visual,
            use_split=True, split_file=None, tags_file=None):
    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode, use_split, split_file, tags_file)

    # unpack prepacing protocol
    pp_bcl = pp_prtcl.get(j2c.PROTOCOLS_BCL)
    pp_cyc = pp_prtcl.get(j2c.PROTOCOLS_CYCLES)
    pp_elecs = pp_prtcl.get(j2c.PROTOCOLS_ELECTRODES)

    # earliest stimulus timing
    stim_trg_timings = pp_prtcl.get(j2c.PROTOCOLS_RELTIMINGS)
    start = min(stim_trg_timings)
    stop = max(stim_trg_timings)

    # specialize command for different prepacing strategies
    chkpt = []
    lats_pp = []
    simID = None
    t_chks = [0]
    chkptf = 'prepaced'
    stims = []
    if mode == 'off':
        # don't pre-pace, use only the pre-paced initial state vectors
        simID = 'prepace_off'
        # as we write only an initial checkpoint, no actual pre-pacing
        # we turn off all stimuli active in the pre-pacing protocol in this case
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.start'.format(i), 0,
                      '-stim[{}].ptcl.npls'.format(i), 0,
                      '-stim[{}].ptcl.duration'.format(i), 0]
        cmd += ['-tend', 1., '-spacedt', 1.]

    elif mode == 'lat':
        # use fast lats-based pre-pacing
        simID = sim_ID('prepace_lat', pp_prtcl, mesh_ID(meshname))
        # simID = 'prepace_lat'
        # chkpt = ['-num_tsav', 1, '-tsav[0]', t_chks] + [ '-num_stim', 0]
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)
        # turn off any stimuli as in the off case
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.start'.format(i), 0,
                      '-stim[{}].ptcl.npls'.format(i), 0,
                      '-stim[{}].ptcl.duration'.format(i), 0]
        cmd += ['-tend', 1., '-spacedt', 1.]

    elif mode.startswith('lat-'):
        # use fast lats-based pre-pacing + one full activation sequence to account for diffusion
        simID = sim_ID('prepace_{}'.format(mode), pp_prtcl, mesh_ID(meshname))
        # by default, we use the cycles from the protocol definition
        if pp_cyc is not None:
            pp_n = pp_cyc
        # parse argument to overrule protocol definition    
        rep = mode[4:]
        if rep.isdigit():
            n = int(rep)
            if n > 0:
                pp_n = n
                
        assert(pp_n > 0)
        
        # checkpoint timings at any end of a pcl
        for i in range(0, pp_n):
            t_chks += [(i+1) * pp_bcl]

        # lat-based prepacing, this is cheap, we can do a number of cycles
        pp_cyc = 8
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), pp_n]
    
        # adjust simulation duration
        cmd += ['-tend', pp_n * pp_bcl + stop, '-spacedt', 1.]

    else:  # mode == 'full':
        # no LAT vector needed, we do brute force pacing
        print('Full prepacing procedure')
        simID = sim_ID('prepace_full', pp_prtcl, mesh_ID(meshname))
        # checkpoint timings at any end of a pcl
        for i in range(0, pp_cyc):
            t_chks += [(i+1) * pp_bcl + start]

        # checkpoint at t=0 ms
        # t_chks  += [ pp_bcl * pp_cyc + start ]
        # add prepacing protocol to stimuli
        for i, e in enumerate(pp_elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), pp_cyc]

        # adjust simulation duration
        cmd += ['-tend', pp_bcl*pp_cyc + start, '-spacedt', 1.]

    vis = []
    if visual:
        vis += ['-gridout_i', 3, '-spacedt', 1]

    # add checkpoint for restarting
    cmd += setup_tsavs(t_chks, chkptf)
    cmd += stims
    cmd += lats_pp
    # input forces simulation beyond the end of latest stimulus
    # this is efficient, but input logic doesn't allow for early stopping with a tend of 0
    cmd += ['-simID', simID, '-experiment', 0] + vis

    return cmd, chkptf, t_chks, simID


# pick run mode 'rd' or 're'
def restart(meshname, cnf, fncs, elecs, pp_prtcl, mode, rmode, restart_chkpt, visual):
    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode)
    simID = 'validate_pp_{}'.format(mode)
    # determine checkpoint timing
    pp_bcl, t_chk = pp_prtcl[j2c.PROTOCOLS_BCL], 0
    if mode == 'full':
        pp_cyc = pp_prtcl[j2c.PROTOCOLS_CYCLES]
        t_chk = pp_bcl * pp_cyc

    # determine checkpoint to restart from
    cmd += ['-start_statef', restart_chkpt]
    cmd += ['-simID', simID, '-experiment', 0]
    # simulate one bcl
    cmd += ['-tend', t_chk + pp_bcl]

    if visual:
        vis = ['-gridout_i', 3, '-spacedt', 1]
        cmd += vis

    return cmd, simID


# print a summary of prepacing protocol
def print_prepace_summary(pprtcl, plan, prepace, lat, rstrt_file):
    confs = plan.get(j2c.CONFIGURATIONS)
    funcs = plan.get(j2c.FUNCTIONS)
    # determine used lat vector
    pp_lat = lat 
    if lat is None:
        pp_lat = pprtcl.get(j2c.PROTOCOLS_LATFILE)
    if prepace == 'off' or prepace == 'full':
        pp_lat = None

    hd = 'Prepacing Summary:'
    print('\n{}'.format(hd))
    print('='*len(hd)+'\n')
    # method used to compute prepaced state
    print('Prepacing mode: {}'.format(prepace))
    # protocol description
    print('{:14s}: {}'.format('Basic CL', pprtcl.get(j2c.PROTOCOLS_BCL)))
    print('{:14s}: {}'.format('#cycles', pprtcl.get(j2c.PROTOCOLS_CYCLES)))
    # state after prepacing stored in checkpoint
    print('{:14s}: {}'.format('Checkpoint', rstrt_file))
    print('{:14s}: {}'.format('LAT vector', pp_lat))
    # in all cases we use an initial state vector
    hd = 'Region-specific initialization state vectors to be used are:'
    print('\n{}'.format(hd))
    print('-' * len(hd) + '\n')

    for name, conf in confs.items():
        # ignore null domains
        if conf is None:
            continue
        # get function defintion
        tags = conf.get(j2c.CONFIGURATION_TAGS)
        fnc_name = conf.get(j2c.CONFIGURATION_FUNC)
        # definition of function
        fnc_def = funcs.get(fnc_name)
        if not fnc_def:
            print('Function "{}" assigned in "{}" not defined.'.format(fnc_def, name))
        else:
            ep = fnc_def.get(j2c.FUNCTION_EP)
            if ep is None:
                print('{:14s}: no EP defined in passive domain ("{}")'.format(name, tags))
                continue
            print('{:14s}: {} in ({})'.format(name, ep.get(j2c.FUNCTION_EP_INIT), tags))
    print()
