# ---( system imports )--------------------------
import os
import enum
import json
import copy


class j2cKey(str, enum.Enum):

    VERSION = 'version'

    # ===( SECTION-NAMES )===========================
    SECTION_CONFIGURATIONS = 'config'
    SECTION_CONFIGURATIONS_NEW = 'configurations'
    SECTION_FUNCTIONS = 'functions'
    SECTION_ELECTRODES = 'electrodes'
    SECTION_SOLVER_SETUP = 'solver'
    SECTION_SOLVER_SETUP_NEW = 'solver_setup'
    SECTION_PROTOCOLS = 'protocols'
    SECTION_SIMULATION_SETUPS = 'simulation'
    SECTION_SIMULATION_SETUPS_NEW = 'simulation_setups'
    SECTION_SPLITTING = 'split'
    SECTION_SPLITTING_NEW = 'splitting'

    # ===( CONFIGURATIONS )==========================
    CONFIGURATION_FUNC = 'func'
    CONFIGURATION_TAGS = 'tags'

    # ===( FUNCTIONS )===============================
    FUNCTION_CV = 'CV'
    FUNCTION_CV_CVMEAS = 'CVmeas'
    FUNCTION_CV_CVREF = 'CVref'
    FUNCTION_CV_COND = 'G'
    FUNCTION_CV_COND_GBATH = 'gbath'
    FUNCTION_EP = 'EP'
    FUNCTION_EP_MODEL = 'model'
    FUNCTION_EP_MODELPAR = 'modelpar'
    FUNCTION_EP_BCL = 'bcl'
    FUNCTION_EP_INIT = 'init'
    FUNCTION_EP_CYCLES = 'cycles'
    FUNCTION_EP_PLUGINS = 'plugins'
    FUNCTION_EP_PLUGPARS = 'plugin-par'
    FUNCTION_EP_APDRESFILE = 'apdres'
    FUNCTION_EP_APDRESPRTCL = 'apdresptcl'

    # ===( ELECTRODES )==============================
    ELECTRODE_TYPE = 'type'

    # ===( SOLVER-SETUP )============================
    SOLVER_DT = 'dt'
    SOLVER_DX = 'dx'
    SOLVER_PARABSOLVE = 'ts'
    SOLVER_MASSLUMPING = 'lumping'
    SOLVER_OPSPLIT = 'opsplit'
    SOLVER_TOLERANCE = 'stol'
    SOLVER_BEQM = 'beqm'
    SOLVER_FIXGE = 'fixge'
    SOLVER_SRC_MODEL = 'sourceModel'
    SOLVER_SUBSTEP = 'dtsubstep'

    # ===( PROTOCOLS )===============================
    PROTOCOLS_PROPAGATION = 'propagation'
    PROTOCOLS_BCL = 'bcl'
    PROTOCOLS_CYCLES = 'cycles'
    PROTOCOLS_LATFILE = 'lat'
    PROTOCOLS_PREPACE = 'prepace'
    PROTOCOLS_RESTART = 'restart'
    PROTOCOLS_ELECTRODES = 'electrodes'
    PROTOCOLS_RELTIMINGS = 'rel-timings'

    PROTOCOLS_APD_RESTITUTION = 'APD-restitution'
    PROTOCOLS_APD_RESTITUTION_TYPE = 'type'
    PROTOCOLS_APD_RESTITUTION_BCL = 'bcl'
    PROTOCOLS_APD_RESTITUTION_CI0 = 'CI0'
    PROTOCOLS_APD_RESTITUTION_CI1 = 'CI1'
    PROTOCOLS_APD_RESTITUTION_PPBEATS = 'pp_beats'
    PROTOCOLS_APD_RESTITUTION_PMBEATS = 'pm_beats'
    PROTOCOLS_APD_RESTITUTION_PMDEC = 'pm_dec'

    # ===( TUNING )==================================
    TUNING_DIR = 'tuning_dir'

    # ===( SPLITTING )===============================
    SPLIT_OPSTR = 'opstr'
    SPLIT_APPLY = 'apply'


DEFAULT_TUNING_DIR = './init'

ACTIVE_FUNCTION_COND_NAMES = ('gil', 'git', 'gin', 'gel', 'get', 'gen')

ACTIVE_FUNCTION_CV_NAMES = ('vf', 'vs', 'vn')

DEFAULT_VERSIONS = {j2cKey.SECTION_CONFIGURATIONS: 1,
                    j2cKey.SECTION_FUNCTIONS: 1,
                    j2cKey.SECTION_ELECTRODES: 1,
                    j2cKey.SECTION_SOLVER_SETUP: 1,
                    j2cKey.SECTION_SIMULATION_SETUPS: 1,
                    j2cKey.SECTION_SPLITTING: 1}

DEFAULT_SOLVER = {j2cKey.SOLVER_SRC_MODEL: 'monodomain',
                  j2cKey.SOLVER_BEQM: True,
                  j2cKey.SOLVER_MASSLUMPING: False,
                  j2cKey.SOLVER_OPSPLIT: True,
                  j2cKey.SOLVER_FIXGE: True,
                  j2cKey.SOLVER_DT: 25.0,
                  j2cKey.SOLVER_DX: 250.0,
                  j2cKey.SOLVER_TOLERANCE: 1.0e-6,
                  j2cKey.SOLVER_PARABSOLVE: 1,
                  j2cKey.SOLVER_SUBSTEP: 1}


class j2cError(Exception):
    pass


def private_key(key):
    return '_{}'.format(key)


def remove_entries_from_dict(dobj, *args, make_copy=False):
    if make_copy:
        dobj = copy.deepcopy(dobj)
    for key in args:
        if key in dobj:
            del dobj[key]
    return dobj


# load dictionary from json file
def load_dict_from_json(file_name, verbose=False):
    # check json file exists
    if not os.path.exists(file_name):
        error_msg = 'J2C: json file "{}" not found !'.format(file_name)
        raise j2cError(error_msg)
    if verbose:
        print('J2C: reading json dictionary from file "{}" !'.format(file_name))
    with open(file_name, 'r') as fp:
        json_dict = json.load(fp)
    return json_dict


def get_dict_from_obj(obj, verbose=False):
    json_dict = None
    if isinstance(obj, str) and os.path.isfile(obj):
        if verbose:
            print('J2C: reading json dictionary from file "{}" !'.format(obj))
        with open(obj, 'r') as fp:
            json_dict = json.load(fp)
    elif isinstance(obj, (str, bytes, bytearray)):
        if verbose:
            print('J2C: create json dictionary from {} object !'.format(type(obj)))
        json_dict = json.loads(obj)
    elif hasattr(obj, 'read') and callable(getattr(obj, 'read')):
        if verbose:
            print('J2C: reading json dictionary from {} object !'.format(type(obj)))
        json_dict = json.load(obj)
    elif isinstance(obj, dict):
        if verbose:
            print('J2C: json dictionary from dictionary !')
        json_dict = obj
    elif obj is not None:
        error_msg = 'J2C: can not create json dictionary from object of type {} !'.format(type(obj))
        raise j2cError(error_msg)
    return json_dict


def configurations_section_from_dict_v1(confs, verbose=False):
    if confs is None:
        error_msg = 'J2C: configurations dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling configurations section V1 !')
    return confs


def configurations_section_to_dict_v1(confs, verbose=False):
    if confs is None:
        error_msg = 'J2C: configurations section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling configurations dictionary V1 !')
    return confs


def functions_section_from_dict_v1(funcs, verbose=False):
    if funcs is None:
        error_msg = 'J2C: functions dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling functions section V1 !')
    return funcs, DEFAULT_TUNING_DIR


def functions_section_to_dict_v1(funcs, verbose=False):
    if funcs is None:
        error_msg = 'J2C: functions section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling functions dictionary V1 !')
    return funcs


def electrodes_section_from_dict_v1(elecs, verbose=False):
    if elecs is None:
        error_msg = 'J2C: electrodes dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling electrodes section V1 !')
    return elecs


def electrodes_section_to_dict_v1(elecs, verbose=False):
    if elecs is None:
        error_msg = 'J2C: electrodes section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling electrodes dictionary V1 !')
    return elecs


def solver_section_from_dict_v1(solver, verbose=False):
    if solver is None:
        error_msg = 'J2C: solver dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling solver section V1 !')
    return solver


def solver_section_to_dict_v1(solver, verbose=False):
    if solver is None:
        error_msg = 'J2C: solver section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling solver dictionary V1 !')
    return solver


def simulation_section_from_dict_v1(simulation, verbose=False):
    if simulation is None:
        error_msg = 'J2C: simulation dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling simulation section V1 !')
    return simulation


def simulation_section_to_dict_v1(simulation, verbose=False):
    if simulation is None:
        error_msg = 'J2C: simulation section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling simulation dictionary V1 !')
    return simulation


def protocols_section_from_dict_v1(protocols, verbose=False):
    if protocols is None:
        error_msg = 'J2C: protocols dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling protocols section V1 !')
    return protocols


def protocols_section_to_dict_v1(protocols, verbose=False):
    if protocols is None:
        error_msg = 'J2C: protocols section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling protocols dictionary V1 !')
    return protocols


def splitting_section_from_dict_v1(splitting, verbose=False):
    if splitting is None:
        error_msg = 'J2C: splitting dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling splitting section V1 !')
    return splitting


def splitting_section_to_dict_v1(splitting, verbose=False):
    if splitting is None:
        error_msg = 'J2C: splitting section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling splitting dictionary V1 !')
    return splitting


def configurations_section_from_dict_v2(confs, verbose=False):
    if confs is None:
        error_msg = 'J2C: configurations dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling configurations section V2 !')

    if 'definitions' not in confs:
        error_msg = 'J2C: no definitions block in the configurations dictionary !'
        raise j2cError(error_msg)

    confs_sec = confs['definitions'].copy()

    return confs_sec


def configurations_section_to_dict_v2(confs, verbose=False):
    if confs is None:
        error_msg = 'J2C: configurations section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling configurations dictionary V2 !')

    confs_dict = dict()
    confs_dict[j2cKey.VERSION] = 2
    confs_dict['definitions'] = confs

    return confs_dict


def functions_section_from_dict_v2(funcs, verbose=False):
    if funcs is None:
        error_msg = 'J2C: functions dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling functions section V2 !')

    if 'definitions' not in funcs:
        error_msg = 'J2C: no definitions block in the functions dictionary !'
        raise j2cError(error_msg)

    calibration_sec = funcs['calibration']
    funcs_sec = dict()
    for funcdef_key, funcdef_value in funcs['definitions'].items():
        func_def = dict()
        cond_sec = funcdef_value['conductivity']
        if isinstance(cond_sec, (float, int)):
            cv_sec = dict()
            cv_sec[j2cKey.FUNCTION_CV_COND] = {j2cKey.FUNCTION_CV_COND_GBATH: float(cond_sec)}

            func_def[j2cKey.FUNCTION_CV] = cv_sec
        else:
            iomodel_sec = funcdef_value['ionic_model']
            cvel_sec = funcdef_value['conduction_velocity']
            cond_sec = funcdef_value['conductivity']

            # convert ionic_model section to EP section
            ep_sec = dict()
            ep_sec[j2cKey.FUNCTION_EP_MODEL] = iomodel_sec['model']
            ep_sec[j2cKey.FUNCTION_EP_MODELPAR] = iomodel_sec.get('model_par', None)
            ep_sec[j2cKey.FUNCTION_EP_PLUGINS] = iomodel_sec.get('plugins', None)
            ep_sec[j2cKey.FUNCTION_EP_PLUGPARS] = iomodel_sec.get('plugins_par', None)
            if (init_sec := iomodel_sec.get('initialization', None)) is not None:
                ep_sec[j2cKey.FUNCTION_EP_CYCLES] = init_sec['num_cycles']
                ep_sec[j2cKey.FUNCTION_EP_BCL] = init_sec['bcl']
                ep_sec[j2cKey.FUNCTION_EP_INIT] = init_sec.get('init', None)
                ep_sec[j2cKey.FUNCTION_EP_APDRESFILE] = init_sec.get('apdres_file', None)
                ep_sec[j2cKey.FUNCTION_EP_APDRESPRTCL] = init_sec.get('apdres_protocol', None)

            remove_entries_from_dict(iomodel_sec, 'model', 'model_par', 'plugins', 'plugins_par', 'initialization')
            # add possible extra function entries
            for extra_key, extra_value in iomodel_sec.items():
                if extra_key not in func_def:
                    func_def[extra_key] = extra_value

            # convert conduction_velocity and conductivity section to CV section
            cv_sec = dict()
            ref_cvel_sec = cvel_sec['reference']
            cv_sec[j2cKey.FUNCTION_CV_CVREF] = ref_cvel_sec.copy()
            if (meas_cvel_sec := cvel_sec.get('measured', None)) is not None:
                cv_sec[j2cKey.FUNCTION_CV_CVMEAS] = meas_cvel_sec.copy()
            else:
                cv_sec[j2cKey.FUNCTION_CV_CVMEAS] = ref_cvel_sec.copy()
            cv_sec[j2cKey.FUNCTION_CV_COND] = cond_sec.copy()

            func_def[j2cKey.FUNCTION_EP] = ep_sec
            func_def[j2cKey.FUNCTION_CV] = cv_sec

            remove_entries_from_dict(funcdef_value, 'ionic_model', 'conduction_velocity', 'conductivity')
            # add possible extra function entries
            for extra_key, extra_value in funcdef_value.items():
                if extra_key not in func_def:
                    func_def[extra_key] = extra_value

        funcs_sec[funcdef_key] = func_def

    tuning_dir = calibration_sec.get('tuning_dir', DEFAULT_TUNING_DIR)
    return funcs_sec, tuning_dir


def functions_section_to_dict_v2(funcs, verbose=False, tuning_dir=None):
    if funcs is None:
        error_msg = 'J2C: functions section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling functions dictionary V2 !')

    func_ep_keys = (j2cKey.FUNCTION_EP_MODEL, j2cKey.FUNCTION_EP_MODELPAR, j2cKey.FUNCTION_EP_BCL,
                    j2cKey.FUNCTION_EP_INIT, j2cKey.FUNCTION_EP_CYCLES, j2cKey.FUNCTION_EP_PLUGINS,
                    j2cKey.FUNCTION_EP_PLUGPARS)

    funcs_dict = dict()
    funcs_dict[j2cKey.VERSION] = 2

    if tuning_dir is not None:
        funcs_dict['calibration'] = {'tuning_dir': str(tuning_dir)}

    func_defs = dict()
    for funcdef_key, funcdef_value in funcs.items():
        func_def = dict()
        ep_sec = funcdef_value.get(j2cKey.FUNCTION_EP, None)
        cv_sec = funcdef_value.get(j2cKey.FUNCTION_CV, None)
        if ep_sec is None:
            func_def['conductivity'] = cv_sec[j2cKey.FUNCTION_CV_COND][j2cKey.FUNCTION_CV_COND_GBATH]
        else:
            init_dict, iomodel_dict, cond_dict, cvel_dict = dict(), dict(), dict(), dict()

            init_dict['num_cycles'] = ep_sec[j2cKey.FUNCTION_EP_CYCLES]
            init_dict['bcl'] = ep_sec[j2cKey.FUNCTION_EP_BCL]
            init_dict['init'] = ep_sec.get(j2cKey.FUNCTION_EP_INIT, None)
            init_dict['apdres_file'] = ep_sec[j2cKey.FUNCTION_EP_APDRESFILE]
            init_dict['apdres_protocol'] = ep_sec[j2cKey.FUNCTION_EP_APDRESPRTCL]

            iomodel_dict['initialization'] = init_dict
            iomodel_dict['model'] = ep_sec[j2cKey.FUNCTION_EP_MODEL]
            iomodel_dict['model_par'] = ep_sec.get(j2cKey.FUNCTION_EP_MODELPAR, None)
            iomodel_dict['plugins'] = ep_sec.get(j2cKey.FUNCTION_EP_PLUGINS, None)
            iomodel_dict['plugins_par'] = ep_sec.get(j2cKey.FUNCTION_EP_PLUGPARS, None)

            # remove already processed entries
            remove_entries_from_dict(ep_sec, *func_ep_keys)
            # add remaining entries
            for extra_key, extra_value in ep_sec.items():
                if extra_key not in iomodel_dict:
                    iomodel_dict[extra_key] = extra_value

            cond_dict = cv_sec[j2cKey.FUNCTION_CV_COND].copy()
            cvel_dict['reference'] = cv_sec[j2cKey.FUNCTION_CV_CVREF].copy()
            cvel_dict['measured'] = cv_sec[j2cKey.FUNCTION_CV_CVMEAS].copy()

            func_def['ionic_model'] = iomodel_dict
            func_def['conductivity'] = cond_dict
            func_def['conduction_velocity'] = cvel_dict

        # remove already processed entries
        remove_entries_from_dict(funcdef_value, j2cKey.FUNCTION_EP, j2cKey.FUNCTION_CV)
        # add remaining entries
        for extra_key, extra_value in funcdef_value.items():
            if extra_key not in func_def:
                func_def[extra_key] = extra_value

        func_defs[funcdef_key] = func_def

    funcs_dict['definitions'] = func_defs

    return funcs_dict


def electrodes_section_from_dict_v2(elecs, verbose=False):
    if elecs is None:
        error_msg = 'J2C: electrodes dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling electrodes section V2 !')

    if 'definitions' not in elecs:
        error_msg = 'J2C: no definitions block in the electrodes dictionary !'
        raise j2cError(error_msg)

    elecs_sec = elecs['definitions'].copy()

    return elecs_sec


def electrodes_section_to_dict_v2(elecs, verbose=False):
    if elecs is None:
        error_msg = 'J2C: electrodes section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling electrodes dictionary V2 !')

    elecs_dict = dict()
    elecs_dict[j2cKey.VERSION] = 2
    elecs_dict['definitions'] = elecs

    return elecs_dict


def solver_section_from_dict_v2(solver, verbose=False):
    if solver is None:
        error_msg = 'J2C: solver dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling solver section V2 !')

    solver_sec = dict()
    solver_sec[j2cKey.SOLVER_DT] = solver.get('dt', DEFAULT_SOLVER[j2cKey.SOLVER_DT])
    solver_sec[j2cKey.SOLVER_DX] = solver.get('dx', DEFAULT_SOLVER[j2cKey.SOLVER_DX])
    solver_sec[j2cKey.SOLVER_BEQM] = solver.get('beqm', DEFAULT_SOLVER[j2cKey.SOLVER_BEQM])
    solver_sec[j2cKey.SOLVER_FIXGE] = solver.get('fixge', DEFAULT_SOLVER[j2cKey.SOLVER_FIXGE])
    solver_sec[j2cKey.SOLVER_MASSLUMPING] = solver.get('lumping', DEFAULT_SOLVER[j2cKey.SOLVER_MASSLUMPING])
    solver_sec[j2cKey.SOLVER_OPSPLIT] = solver.get('opsplit', DEFAULT_SOLVER[j2cKey.SOLVER_OPSPLIT])
    solver_sec[j2cKey.SOLVER_SRC_MODEL] = solver.get('source_model', DEFAULT_SOLVER[j2cKey.SOLVER_SRC_MODEL])
    solver_sec[j2cKey.SOLVER_TOLERANCE] = solver.get('tolerance', DEFAULT_SOLVER[j2cKey.SOLVER_TOLERANCE])
    solver_sec[j2cKey.SOLVER_PARABSOLVE] = solver.get('ts', DEFAULT_SOLVER[j2cKey.SOLVER_PARABSOLVE])
    solver_sec[j2cKey.SOLVER_SUBSTEP] = solver.get('dtsubstep', DEFAULT_SOLVER[j2cKey.SOLVER_SUBSTEP])

    return solver_sec


def solver_section_to_dict_v2(solver, verbose=False):
    if solver is None:
        error_msg = 'J2C: solver section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling solver dictionary V2 !')

    solver_dict = dict()
    solver_dict[j2cKey.VERSION] = 2
    solver_dict['dt'] = solver.get(j2cKey.SOLVER_DT, DEFAULT_SOLVER[j2cKey.SOLVER_DT])
    solver_dict['dx'] = solver.get(j2cKey.SOLVER_DX, DEFAULT_SOLVER[j2cKey.SOLVER_DX])
    solver_dict['beqm'] = solver.get(j2cKey.SOLVER_BEQM, DEFAULT_SOLVER[j2cKey.SOLVER_BEQM])
    solver_dict['fixge'] = solver.get(j2cKey.SOLVER_FIXGE, DEFAULT_SOLVER[j2cKey.SOLVER_FIXGE])
    solver_dict['lumping'] = solver.get(j2cKey.SOLVER_MASSLUMPING, DEFAULT_SOLVER[j2cKey.SOLVER_MASSLUMPING])
    solver_dict['opsplit'] = solver.get(j2cKey.SOLVER_OPSPLIT, DEFAULT_SOLVER[j2cKey.SOLVER_OPSPLIT])
    solver_dict['source_model'] = solver.get(j2cKey.SOLVER_SRC_MODEL, DEFAULT_SOLVER[j2cKey.SOLVER_SRC_MODEL])
    solver_dict['tolerance'] = solver.get(j2cKey.SOLVER_TOLERANCE, DEFAULT_SOLVER[j2cKey.SOLVER_TOLERANCE])
    solver_dict['ts'] = solver.get(j2cKey.SOLVER_PARABSOLVE, DEFAULT_SOLVER[j2cKey.SOLVER_PARABSOLVE])
    solver_dict['dtsubstep'] = solver.get(j2cKey.SOLVER_SUBSTEP, DEFAULT_SOLVER[j2cKey.SOLVER_SUBSTEP])

    return solver_dict


def simulation_section_from_dict_v2(simulation, verbose=False):
    if simulation is None:
        error_msg = 'J2C: simulation dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling simulation section V2 !')

    simulation_sec = dict()

    return simulation_sec


def simulation_section_to_dict_v2(simulation, verbose=False):
    if simulation is None:
        error_msg = 'J2C: simulation section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling simulation dictionary V2 !')

    simulation_dict = dict()

    return simulation_dict


def protocols_section_from_dict_v2(protocols, verbose=False):
    if protocols is None:
        error_msg = 'J2C: protocols dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling protocols section V2 !')

    prtcls_sec = dict()

    apd_prtcls_sec = dict()
    for apd_key, apd_value in protocols['apd_restitution'].items():
        apd_prtcl_sec = dict()
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_TYPE] = apd_value['type']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_BCL] = apd_value['bcl']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_CI0] = apd_value['CI0']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_CI1] = apd_value['CI1']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_PPBEATS] = apd_value['pp_beats']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_PMBEATS] = apd_value['pm_beats']
        apd_prtcl_sec[j2cKey.PROTOCOLS_APD_RESTITUTION_PMDEC] = apd_value['pm_dec']
        apd_prtcls_sec[apd_key] = apd_prtcl_sec

    prtcls_sec[j2cKey.PROTOCOLS_APD_RESTITUTION] = apd_prtcls_sec

    for pp_key, pp_value in protocols['prepacing'].items():
        if pp_key == j2cKey.PROTOCOLS_APD_RESTITUTION:
            print('J2C: skipping prepacing protocol "{}", not a valid name !'.format(pp_key))
            continue
        pp_prtcl_sec = dict()
        pp_prtcl_sec[j2cKey.PROTOCOLS_PROPAGATION] = pp_value['propagation']
        pp_prtcl_sec[j2cKey.PROTOCOLS_BCL] = pp_value['bcl']
        pp_prtcl_sec[j2cKey.PROTOCOLS_CYCLES] = pp_value['num_cycles']
        pp_prtcl_sec[j2cKey.PROTOCOLS_LATFILE] = pp_value.get('lat_file', None)
        pp_prtcl_sec[j2cKey.PROTOCOLS_RESTART] = pp_value.get('restart', None)
        pp_prtcl_sec[j2cKey.PROTOCOLS_ELECTRODES] = pp_value['electrodes']
        pp_prtcl_sec[j2cKey.PROTOCOLS_RELTIMINGS] = pp_value['rel_timings']

        prtcls_sec[pp_key] = pp_prtcl_sec

    return prtcls_sec


def protocols_section_to_dict_v2(protocols, verbose=False):
    if protocols is None:
        error_msg = 'J2C: protocols section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling protocols dictionary V2 !')

    protocols_dict = dict()
    protocols_dict[j2cKey.VERSION] = 2
    # convert APD-restitution protocols
    if (apd_prtcls_sec := protocols.get(j2cKey.PROTOCOLS_APD_RESTITUTION, None)) is not None:
        apd_prtcls_dict = dict()
        for apd_key, apd_value in apd_prtcls_sec.items():
            apd_prtcl_dict = dict()
            apd_prtcl_dict['type'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_TYPE]
            apd_prtcl_dict['bcl'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_BCL]
            apd_prtcl_dict['CI0'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_CI0]
            apd_prtcl_dict['CI1'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_CI1]
            apd_prtcl_dict['pp_beats'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_PPBEATS]
            apd_prtcl_dict['pm_beats'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_PMBEATS]
            apd_prtcl_dict['pm_dec'] = apd_value[j2cKey.PROTOCOLS_APD_RESTITUTION_PMDEC]
            apd_prtcls_dict[apd_key] = apd_prtcl_dict

        protocols_dict['apd_restitution'] = apd_prtcls_dict

    # convert prepacing protocols
    pp_prtcls_dict = dict()
    for pp_key, pp_value in protocols.items():
        if pp_key == j2cKey.PROTOCOLS_APD_RESTITUTION:
            continue
        pp_prtcl_dict = dict()
        pp_prtcl_dict['propagation'] = pp_value[j2cKey.PROTOCOLS_PROPAGATION]
        pp_prtcl_dict['bcl'] = pp_value[j2cKey.PROTOCOLS_BCL]
        pp_prtcl_dict['num_cycles'] = pp_value[j2cKey.PROTOCOLS_CYCLES]
        pp_prtcl_dict['lat_file'] = pp_value.get(j2cKey.PROTOCOLS_LATFILE, None)
        pp_prtcl_dict['restart'] = pp_value.get(j2cKey.PROTOCOLS_RESTART, None)
        pp_prtcl_dict['electrodes'] = pp_value[j2cKey.PROTOCOLS_ELECTRODES]
        pp_prtcl_dict['rel_timings'] = pp_value[j2cKey.PROTOCOLS_RELTIMINGS]

        pp_prtcls_dict[pp_key] = pp_prtcl_dict

    protocols_dict['prepacing'] = pp_prtcls_dict

    return protocols_dict


def splitting_section_from_dict_v2(splitting, verbose=False):
    if splitting is None:
        error_msg = 'J2C: splitting dictionary is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling splitting section V2 !')

    splitting_sec = dict()
    splitting_sec[j2cKey.SPLIT_OPSTR] = splitting['opstr']
    splitting_sec[j2cKey.SPLIT_APPLY] = splitting['apply']

    return splitting_sec


def splitting_section_to_dict_v2(splitting, verbose=False):
    if splitting is None:
        error_msg = 'J2C: splitting section is None !'
        raise j2cError(error_msg)
    if verbose:
        print('J2C: filling splitting dictionary V2 !')

    splitting_dict = dict()
    splitting_dict[j2cKey.VERSION] = 2
    splitting_dict['opstr'] = splitting[j2cKey.SPLIT_OPSTR]
    splitting_dict['apply'] = splitting[j2cKey.SPLIT_APPLY]

    return splitting_dict


def get_configurations_section(confs_obj, verbose=False):
    confs_dict = get_dict_from_obj(confs_obj, verbose=verbose)
    if confs_dict is None:
        if verbose:
            print('J2C: configurations dictionary is None, return empty dictionary !')
        return dict(), None

    # check which configurations section is in the file
    has_old_confs_section = j2cKey.SECTION_CONFIGURATIONS in confs_dict
    has_new_confs_section = j2cKey.SECTION_CONFIGURATIONS_NEW in confs_dict
    if has_old_confs_section and has_new_confs_section:
        error_msg = 'J2C: old and new configurations section found !'
        raise j2cError(error_msg)
    if not has_old_confs_section and not has_new_confs_section:
        if verbose:
            print('J2C: configurations section is None, return empty dictionary !')
        return dict(), None
    # get configurations sections
    if has_old_confs_section:
        confs_dict = confs_dict[j2cKey.SECTION_CONFIGURATIONS]
    else:
        confs_dict = confs_dict[j2cKey.SECTION_CONFIGURATIONS_NEW]
    # check version of configurations section
    version = 1
    if j2cKey.VERSION in confs_dict:
        version = confs_dict[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                confs = configurations_section_from_dict_v2(confs_dict, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in configurations section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in configurations section !'
            raise j2cError(error_msg)
    else:
        confs = configurations_section_from_dict_v1(confs_dict, verbose=verbose)

    return confs, version


def get_functions_section(funcs_obj, verbose=False):
    funcs_dict = get_dict_from_obj(funcs_obj, verbose=verbose)
    if funcs_dict is None:
        if verbose:
            print('J2C: functions dictionary is None, return empty dictionary and default tuning dir "{}" !'.format(DEFAULT_TUNING_DIR))
        return dict(), DEFAULT_TUNING_DIR, None

    # get functions sections
    if j2cKey.SECTION_FUNCTIONS not in funcs_dict:
        if verbose:
            print('J2C: functions section is None, return empty dictionary and default tuning dir "{}" !'.format(DEFAULT_TUNING_DIR))
        return dict(), DEFAULT_TUNING_DIR, None
    funcs_dict = funcs_dict[j2cKey.SECTION_FUNCTIONS]
    # check version of functions section
    version = 1
    if j2cKey.VERSION in funcs_dict:
        version = funcs_dict[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                funcs, tuning_dir = functions_section_from_dict_v2(funcs_dict, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in functions section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in functions section !'
            raise j2cError(error_msg)
    else:
        funcs, tuning_dir = functions_section_from_dict_v1(funcs_dict, verbose=verbose)

    return funcs, tuning_dir, version


def get_electrodes_section(elecs_obj, verbose=False):
    json_elecs = get_dict_from_obj(elecs_obj, verbose=verbose)
    if json_elecs is None:
        if verbose:
            print('J2C: electrodes dictionary is None, return empty dictionary !')
        return dict(), None

    # get electrodes sections
    if j2cKey.SECTION_ELECTRODES not in json_elecs:
        if verbose:
            print('J2C: electrodes section is None, return empty dictionary !')
        return dict(), None
    json_elecs = json_elecs[j2cKey.SECTION_ELECTRODES]
    # check version of electrodes section
    version = 1
    if j2cKey.VERSION in json_elecs:
        version = json_elecs[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                elecs = electrodes_section_from_dict_v2(json_elecs, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in electrodes section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in electrodes section !'
            raise j2cError(error_msg)
    else:
        elecs = electrodes_section_from_dict_v1(json_elecs, verbose=verbose)

    return elecs, version


def get_solver_setup_section(solver_obj, verbose=False):
    json_solver = get_dict_from_obj(solver_obj, verbose=verbose)
    if json_solver is None:
        if verbose:
            print('J2C: solver dictionary is None, return default dictionary !')
        return DEFAULT_SOLVER.copy(), None

    # check which solver section is in the file
    has_old_solver_section = j2cKey.SECTION_SOLVER_SETUP in json_solver
    has_new_solver_section = j2cKey.SECTION_SOLVER_SETUP_NEW in json_solver
    if has_old_solver_section and has_new_solver_section:
        error_msg = 'J2C: old and new solver section found !'
        raise j2cError(error_msg)
    if not has_old_solver_section and not has_new_solver_section:
        if verbose:
            print('J2C: solver section is None, return default dictionary !')
        return DEFAULT_SOLVER.copy(), None
    # get solver sections
    if has_old_solver_section:
        json_solver = json_solver[j2cKey.SECTION_SOLVER_SETUP]
    else:
        json_solver = json_solver[j2cKey.SECTION_SOLVER_SETUP_NEW]
    # check version of solver section
    version = 1
    if j2cKey.VERSION in json_solver:
        version = json_solver[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                solver = solver_section_from_dict_v2(json_solver, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in solver section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in solver section !'
            raise j2cError(error_msg)
    else:
        solver = solver_section_from_dict_v1(json_solver, verbose=verbose)

    # update missing values
    for key, value in DEFAULT_SOLVER.items():
        if key not in solver:
            solver[key] = value

    return solver, version


def get_simulation_setups_section(simulation_obj, verbose=False):
    json_simulation = get_dict_from_obj(simulation_obj, verbose=verbose)
    if json_simulation is None:
        if verbose:
            print('J2C: simulations dictionary is None, return empty dictionary !')
        return dict(), None

    # check which solver section is in the file
    has_old_simulation_section = j2cKey.SECTION_SIMULATION_SETUPS in json_simulation
    has_new_simulation_section = j2cKey.SECTION_SIMULATION_SETUPS_NEW in json_simulation
    if has_old_simulation_section and has_new_simulation_section:
        error_msg = 'J2C: old and new simulation sections found !'
        raise j2cError(error_msg)
    if not has_old_simulation_section and not has_new_simulation_section:
        if verbose:
            print('J2C: simulation section is None, return empty dictionary !')
        return dict(), None
    # get solver sections
    if has_old_simulation_section:
        json_simulation = json_simulation[j2cKey.SECTION_SIMULATION_SETUPS]
    else:
        json_simulation = json_simulation[j2cKey.SECTION_SIMULATION_SETUPS_NEW]
    # check version of solver section
    version = 1
    if j2cKey.VERSION in json_simulation:
        version = json_simulation[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                simulation = simulation_section_from_dict_v2(json_simulation, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in simulation section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in simulation section !'
            raise j2cError(error_msg)
    else:
        simulation = simulation_section_from_dict_v1(json_simulation, verbose=verbose)

    return simulation, version


def get_protocols_section(protocols_obj, verbose=False):
    json_protocols = get_dict_from_obj(protocols_obj, verbose=verbose)
    if json_protocols is None:
        if verbose:
            print('J2C: protocols dictionary is None, return empty dictionary !')
        return dict(), None

    # get electrodes sections
    if j2cKey.SECTION_PROTOCOLS not in json_protocols:
        if verbose:
            print('J2C: protocols section is None, return empty dictionary !')
        return dict(), None
    json_protocols = json_protocols[j2cKey.SECTION_PROTOCOLS]
    # check version of electrodes section
    version = 1
    if j2cKey.VERSION in json_protocols:
        version = json_protocols[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                prtcls = protocols_section_from_dict_v2(json_protocols, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in protocols section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in protocols section !'
            raise j2cError(error_msg)
    else:
        prtcls = protocols_section_from_dict_v1(json_protocols, verbose=verbose)

    return prtcls, version


def get_splitting_section(splitting_obj, verbose=False):
    json_splitting = get_dict_from_obj(splitting_obj, verbose=verbose)
    if json_splitting is None:
        if verbose:
            print('J2C: splitting dictionary is None, return empty dictionary !')
        return dict(), None

    # check which splitting section is in the file
    has_old_splitting_section = j2cKey.SECTION_SPLITTING in json_splitting
    has_new_splitting_section = j2cKey.SECTION_SPLITTING_NEW in json_splitting
    if has_old_splitting_section and has_new_splitting_section:
        error_msg = 'J2C: old and new splitting sections found !'
        raise j2cError(error_msg)
    if not has_old_splitting_section and not has_new_splitting_section:
        if verbose:
            print('J2C: splitting section is None, return empty dictionary !')
        return dict(), None
    # get solver sections
    if has_old_splitting_section:
        json_splitting = json_splitting[j2cKey.SECTION_SPLITTING]
    else:
        json_splitting = json_splitting[j2cKey.SECTION_SPLITTING_NEW]
    # check version of solver json_splitting
    version = 1
    if j2cKey.VERSION in json_splitting:
        version = json_splitting[j2cKey.VERSION]
        if isinstance(version, int):
            if version == 2:
                splitting = splitting_section_from_dict_v2(json_splitting, verbose=verbose)
            else:
                error_msg = 'J2C: version {} not supported in simulation section !'.format(version)
                raise j2cError(error_msg)
        else:
            error_msg = 'J2C: version number not of type int in simulation section !'
            raise j2cError(error_msg)
    else:
        splitting = splitting_section_from_dict_v1(json_splitting, verbose=verbose)

    return splitting, version


def load_simulation_plan(plan_file, confs_file=None, funcs_file=None, elecs_file=None,
                         protocols_file=None, simulations_file=None, update_confs=True,
                         update_funcs=True, update_elecs=True, update_protocols=True,
                         update_simulations=True, verbose=False):
    # load plan from file
    plan = load_dict_from_json(plan_file, verbose=verbose)

    # get sections from loaded plan
    confs_sec, confs_version = get_configurations_section(plan, verbose=verbose)
    funcs_sec, tuning_dir, funcs_version = get_functions_section(plan, verbose=verbose)
    elecs_sec, elecs_version = get_electrodes_section(plan, verbose=verbose)
    solver_sec, solver_version = get_solver_setup_section(plan, verbose=verbose)
    protocols_sec, protocols_version = get_protocols_section(plan, verbose=verbose)
    simulations_sec, simulations_version = get_simulation_setups_section(plan, verbose=verbose)
    splitting_sec, splitting_version = get_splitting_section(plan, verbose=verbose)

    # load configurations and update plan
    if confs_file is not None:
        extra_confs, extra_conf_version = get_configurations_section(confs_file, verbose=verbose)
        if extra_conf_version is not None and \
                (confs_version is None or extra_conf_version > confs_version):
            confs_version = extra_conf_version
        if update_confs:
            confs_sec.update(extra_confs)
        else:
            confs_sec = extra_confs

    # load functions and update plan
    if funcs_file is not None:
        extra_funcs, extra_tuning_dir, extra_funcs_version = get_functions_section(funcs_file, verbose=verbose)
        if extra_funcs_version is not None and \
                (funcs_version is None or extra_funcs_version > funcs_version):
            funcs_version = extra_funcs_version
        if update_funcs:
            funcs_sec.update(extra_funcs)
        else:
            funcs_sec = extra_funcs
            tuning_dir = extra_tuning_dir

    # load electrodes and update plan
    if elecs_file is not None:
        extra_elecs, extra_elecs_version = get_electrodes_section(elecs_file, verbose=verbose)
        if extra_elecs_version is not None and \
                (elecs_version is None or extra_elecs_version > elecs_version):
            elecs_version = extra_elecs_version
        if update_elecs:
            elecs_sec.update(extra_elecs)
        else:
            elecs_sec = extra_elecs

    # load protocols and update plan
    if protocols_file is not None:
        extra_protocols, extra_protocols_version = get_protocols_section(protocols_file, verbose=verbose)
        if extra_protocols_version is not None and \
                (protocols_version is None or extra_protocols_version > protocols_version):
            protocols_version = extra_protocols_version
        if update_protocols:
            protocols_sec.update(extra_protocols)
        else:
            protocols_sec = extra_protocols

    # load simulations and update plan
    if simulations_file is not None:
        extra_simulations, extra_simulations_version = get_simulation_setups_section(simulations_file, verbose=verbose)
        if extra_simulations_version is not None and \
                (simulations_version is None or extra_simulations_version > simulations_version):
            simulations_version = extra_simulations_version
        if update_simulations:
            simulations_sec.update(extra_simulations)
        else:
            simulations_sec = extra_simulations

    version_sec = {j2cKey.SECTION_CONFIGURATIONS: confs_version,
                   j2cKey.SECTION_FUNCTIONS: funcs_version,
                   j2cKey.SECTION_ELECTRODES: elecs_version,
                   j2cKey.SECTION_SOLVER_SETUP: solver_version,
                   j2cKey.SECTION_PROTOCOLS: protocols_version,
                   j2cKey.SECTION_SIMULATION_SETUPS: simulations_version,
                   j2cKey.SECTION_SPLITTING: splitting_version}

    final_plan = dict()
    final_plan[private_key(j2cKey.VERSION)] = version_sec
    final_plan[j2cKey.TUNING_DIR] = tuning_dir
    final_plan[j2cKey.SECTION_CONFIGURATIONS] = confs_sec
    final_plan[j2cKey.SECTION_FUNCTIONS] = funcs_sec
    final_plan[j2cKey.SECTION_ELECTRODES] = elecs_sec
    final_plan[j2cKey.SECTION_SOLVER_SETUP] = solver_sec
    final_plan[j2cKey.SECTION_PROTOCOLS] = protocols_sec
    final_plan[j2cKey.SECTION_SIMULATION_SETUPS] = simulations_sec
    final_plan[j2cKey.SECTION_SPLITTING] = splitting_sec

    return final_plan


def save_simulation_plan(plan, file_name, indent=2, verbose=False, overwrite=True):
    if not overwrite and os.path.exists(file_name):
        raise j2cError('J2C: error, file "{}" already exists !'.format(file_name))

    versions = plan.get(private_key(j2cKey.VERSION), DEFAULT_VERSIONS)

    plan_dict = dict()
    if (confs_sec := plan.get(j2cKey.SECTION_CONFIGURATIONS, None)) is not None:
        if versions[j2cKey.SECTION_CONFIGURATIONS] == 2:
            confs_dict = configurations_section_to_dict_v2(confs_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_CONFIGURATIONS_NEW] = confs_dict
        else:
            confs_dict = configurations_section_to_dict_v1(confs_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_CONFIGURATIONS] = confs_dict

    if (funcs_sec := plan.get(j2cKey.SECTION_FUNCTIONS, None)) is not None:
        tuning_dir = plan.get(j2cKey.TUNING_DIR, DEFAULT_TUNING_DIR)
        if versions[j2cKey.SECTION_FUNCTIONS] == 2:
            funcs_dict = functions_section_to_dict_v2(funcs_sec, verbose=verbose, tuning_dir=tuning_dir)
            plan_dict[j2cKey.SECTION_FUNCTIONS] = funcs_dict
        else:
            funcs_dict = functions_section_to_dict_v1(funcs_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_FUNCTIONS] = funcs_dict

    if (elecs_sec := plan.get(j2cKey.SECTION_ELECTRODES, None)) is not None:
        if versions[j2cKey.SECTION_ELECTRODES] == 2:
            elecs_dict = electrodes_section_to_dict_v2(elecs_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_ELECTRODES] = elecs_dict
        else:
            elecs_dict = electrodes_section_to_dict_v1(elecs_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_ELECTRODES] = elecs_dict

    if (solver_sec := plan.get(j2cKey.SECTION_SOLVER_SETUP, None)) is not None:
        if versions[j2cKey.SECTION_SOLVER_SETUP] == 2:
            solver_dict = solver_section_to_dict_v2(solver_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SOLVER_SETUP_NEW] = solver_dict
        else:
            solver_dict = solver_section_to_dict_v1(solver_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SOLVER_SETUP] = solver_dict

    if (protocols_sec := plan.get(j2cKey.SECTION_PROTOCOLS, None)) is not None:
        if versions[j2cKey.SECTION_PROTOCOLS] == 2:
            protocols_dict = protocols_section_to_dict_v2(protocols_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_PROTOCOLS] = protocols_dict
        else:
            protocols_dict = protocols_section_to_dict_v1(protocols_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_PROTOCOLS] = protocols_dict

    if (simulations_sec := plan.get(j2cKey.SECTION_SIMULATION_SETUPS, None)) is not None:
        if versions[j2cKey.SECTION_SIMULATION_SETUPS] == 2:
            simulations_dict = simulation_section_to_dict_v2(simulations_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SIMULATION_SETUPS_NEW] = simulations_dict
        else:
            simulations_dict = simulation_section_to_dict_v1(simulations_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SIMULATION_SETUPS] = simulations_dict

    if (splitting_sec := plan.get(j2cKey.SECTION_SPLITTING, None)) is not None:
        if versions[j2cKey.SECTION_SPLITTING] == 2:
            splitting_dict = splitting_section_to_dict_v2(splitting_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SPLITTING_NEW] = splitting_dict
        else:
            splitting_dict = splitting_section_to_dict_v1(splitting_sec, verbose=verbose)
            plan_dict[j2cKey.SECTION_SPLITTING] = splitting_dict

    with open(file_name, 'w') as fp:
        json.dump(plan_dict, fp, indent=indent)
