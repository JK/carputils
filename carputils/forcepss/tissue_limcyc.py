#!/usr/bin/env python3

# ---( system imports )--------------------------
import os
import json
import numpy as np
# ---( carputils imports )-----------------------
from carputils import settings
from carputils.forcepss import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Tissue experiment velocity (restitution)/conductivity measuring and tuning'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)


def vel2List(CVs, G, cvg):
    # assume rotational isotropy first
    v_list = [CVs['vf'], CVs['vs']]
    gi_list = [G['gil'], G['git']]
    ge_list = [G['gel'], G['get']]
    axes = ['f', 't']

    rot_iso = True
    if cvg:
        # we tune Gs to match CVmeas with CVref
        # only tune for sheet normal if reference vn and vs differ
        if CVs['vn'] != CVs['vs']:
            rot_iso = False
    else:
        # we only measure sheet normal if sheet-normal conductivity settings 
        # differ from transverse (sheet) conductivity settings 
        if G.get('git') != G.get('gin') or G.get('get') != G.get('gen'):
            rot_iso = False

    if not rot_iso:
        # append sheet-normal data
        v_list.append(CVs['vn'])
        gi_list.append(G['gin'])
        ge_list.append(G['gen'])
        axes.append('n')

    return v_list, gi_list, ge_list, axes


def converge_CV(job, solver, converge, tol, tmode, dx, restitute_pcls,
                dkey, ep, cv, trg_dir, np, tbl, ID, carp_opts, visual,
                delete_simID=False):
    exp_type = 'measuring conduction velocity'
    if converge:
        exp_type = 'tuning conduction velocity'
    if restitute_pcls:
        exp_type = 'measuring conduction velocity restitution'

    print('\n\nInitializing tissue domain {} - {}.'.format(dkey, exp_type))
    print('=' * 88 + '\n\n')

    G = cv[j2c.FUNCTION_CV_COND]
    length = 2      # length of strand for testing 
    # tol    = 1e-5   # tolerance for velocity convergence
    cvg = converge

    # reference velocities
    if (cvref := cv.get(j2c.FUNCTION_CV_CVREF, None)) is None:
        return None

    # measured velocities
    cvmeas = cv.get(j2c.FUNCTION_CV_CVMEAS)
    cvtrg = cvref if cvg else cvmeas
    cvs, gis, ges, ax = vel2List(cvtrg, G, cvg)

    # iterate over velocities
    tune_cv = dict()
    for i, v in enumerate(cvs):
        # velocity must be > 0
        if not v > 0.0:
            print('Measured velocity CVmeas {} in {} must be > 0!'.format(ax[i], dkey))
            continue

        # create command line for each direction   
        cmd = [settings.execs.TUNECV,  '--np', np, '--model', ep[j2c.FUNCTION_EP_MODEL]]
        # add optional parameters
        if (modelpar := ep.get(j2c.FUNCTION_EP_MODELPAR, None)) is not None:
            cmd += ['--modelpar', modelpar]
        if (plugin := ep.get(j2c.FUNCTION_EP_PLUGINS, None)) is not None:
            cmd += ['--plugin',  plugin]
        if (plugpar := ep.get(j2c.FUNCTION_EP_PLUGPARS, None)) is not None:
            cmd += ['--plugpar', plugpar]
        if (svinit := ep.get(j2c.FUNCTION_EP_INIT, None)) is not None:
            cmd += ['--svinit', svinit]

        # spatial test resolution
        cmd += ['--converge', cvg,
                '--velocity', v,
                '--resolution', dx,
                '--gi', gis[i],
                '--ge', ges[i],
                '--beta', G.get('surf2vol'),
                '--dt', solver.get(j2c.SOLVER_DT),
                '--sourceModel', str(solver.get(j2c.SOLVER_SRC_MODEL)),
                '--beqm', int(solver.get(j2c.SOLVER_BEQM)),
                '--ts', solver.get(j2c.SOLVER_PARABSOLVE),
                '--lumping', str(solver.get(j2c.SOLVER_MASSLUMPING)),
                '--opsplit', str(solver.get(j2c.SOLVER_OPSPLIT)),
                '--stol', solver.get(j2c.SOLVER_CGTOL),
                '--tol', str(tol),
                '--length', str(length),
                '--stimS', 500,
                '--stimV',
                '--mode', tmode,
                '--log', 'tunecv_{}_{}.log'.format(dkey, ax[i]),
                '--silent']
        if delete_simID:
            cmd += ['--overwrite-behaviour', 'delete']

        if tbl is not None:
            cmd += ['--table', '{}.dat'.format(tbl)]
        if carp_opts is not None:
            cmd += ['--CARP-opts', '"{}"'.format(carp_opts[0])]
        if ID is not None:
            cmd += ['--ID', '{}_{}'.format(ID, ax[i])]

        if restitute_pcls is not None:
            cvres_file = os.path.join(trg_dir, '{}_{}_CV_restitution.dat'.format(dkey, ax[i]))
            cmd += ['--pcl'] + restitute_pcls + ['--cvres', cvres_file]

        if visual:
            cmd += ['--visualize']
    
        # run tuneCV 
        job.bash(cmd)

        with open('tune.json', 'r') as fp:
            tune = json.load(fp)
            tune_cv['{}'.format(ax[i])] = tune
            # check for rotational isotropy
            if ax[i] == 't' and 'n' not in ax:
                tune_cv['n'] = tune

    # dump combined tune_cv for all directions
    fname = '{}_tune_cv.json'.format(dkey)
    with open(fname, 'w') as fp:
        json.dump(tune_cv, fp, indent=4)

    return tune_cv


def print_tune_cv_results(tune_cv, domains, solver):
    print('\n\nTuning Summary')
    print('='*88)
    n_active = 0
    for dkey, res in tune_cv.items():

        # experimental settings in domain
        domain = domains.get(dkey)
        cvs = domain.get(j2c.FUNCTION_CV)
        on = cvs.get('mutable', True)
        G = cvs.get(j2c.FUNCTION_CV_COND)

        # skip empty results
        if res is None or not on:
            continue

        n_active += 1
        print('\nGlobal numerical settings')
        print('Surface-to-volume  = {:.4f} 1/cm'.format(G['surf2vol']))
        print('source model       = {}'.format(solver.get(j2c.SOLVER_SRC_MODEL)))
        print('bidm eqv monodm    = {}'.format(bool(solver.get(j2c.SOLVER_BEQM))))
        print('fixge              = {}'.format(bool(solver.get(j2c.SOLVER_FIXGE))))
        print('dt                 = {:.1f} um'.format(solver[j2c.SOLVER_DT]))
        print('dx                 = {:.1f} um'.format(solver[j2c.SOLVER_DX]))
        print('time stepper       = {:d}'.format(solver[j2c.SOLVER_PARABSOLVE]))
        print('solver tolerance   = {:g}'.format(solver[j2c.SOLVER_CGTOL]))
        print('mass lumping       = {}'.format(solver[j2c.SOLVER_MASSLUMPING]))
        print('operator splitting = {}'.format(solver[j2c.SOLVER_OPSPLIT]))
        # stimulus strength : 250.0

        print('\n\nVelocity and conductivity settings for domain {})'.format(dkey))
        print('='*88)

        # deal with inconsistent (historically) naming of axes
        cv_axs = ['vf', 'vs', 'vn']
        for i, ax in enumerate(['f', 't', 'n']):
            print('\nSettings along axis {}'.format(ax))
            print('-'*50)
            ax_data = res.get(ax)
            if ax_data is not None:
                r_ax = ax_data.get('results')
                ref_cv = cvs[j2c.FUNCTION_CV_CVREF][cv_axs[i]]
                print('    v{:s}   = {:.4f} m/s (v_ref = {:.3f} m/s)'.format(ax, r_ax['vel'], ref_cv))
                print('    g_i{:s} = {:.4f} S/m'.format(ax, r_ax['gi']))
                print('    g_e{:s} = {:.4f} S/m'.format(ax, r_ax['ge']))
                print('    g_m{:s} = {:.4f} S/m'.format(ax, r_ax['gm']))
            else:
                print('Tuning of conduction velocity along axis {} failed'.format(ax))

        print('\n')

    # inform in case there were no active domains
    if not n_active:
        print('Conduction velocity measurement/tuning not triggered.')
        print('No domains were active, set mutable true for domains of interest.\n')


def print_restitute_results(cv_tune):
    print('\n\nCV Restitution Summary')
    print('='*88)

    # CV_res_files, CV_res_labels = get_cv_res_files(cv_tune)

    # get pcl test vector
    pcls = []
    for dom_key, dom_val in cv_tune.items():
        for dir_key, dir_val in dom_val.items():
            d_pcl = dir_val.get('results').get('pcls')
            pcls.append(d_pcl)

    # assume the same pcl vector was tested in all domains
    pcl_vec = pcls[0]

    # print table header
    print('{:^15s} | {:^3s} | {:^5s}'.format('domain', 'ax', 'PCLs'))
    print('{:^15s} | {:^3s} |'.format('', ''), end='')
    for pcl in pcl_vec:
        print('{:7.1f}'.format(pcl), end='')
    print('\n'+'-'*88)

    # print CV(PCL) results for all domains
    for dom_key, dom_vals in cv_tune.items():
        # get results of domain
        for dir_key, dir_vals in dom_vals.items():
            # domain & direction
            print('{:^15s} | {:^3s} |'.format(dom_key, dir_key), end='')

            # data
            res = dir_vals.get('results')
            cvrf = res.get('cv_res')
            pcls = res.get('pcls')

            # read CV restitution data from file
            pcls, cvs = read_cv_res_file(cvrf)
            for i, cv in enumerate(cvs):
                print('{:7.4f}'.format(cv), end='')
            # indicate pcls with failed capture
            if len(cvs) < len(pcl_vec):
                for i in range(len(cvs), len(pcl_vec)):
                    print('{:^7s}'.format('--'), end='')
            print()
    print('='*88)
    print()


def read_cv_res_file(cvrf):
    if os.path.isfile(cvrf):
        cvr_data = np.loadtxt(cvrf)
        pcls, cvs = cvr_data[:, 0], cvr_data[:, 1]
    else:
        print('\n\nCV restitution file "{}" not found.'.format(cvrf))
        pcls, cvs = None, None
    return pcls, cvs


def get_cv_res_files(cv_tune):
    """
    :param cv_tune:
    :return: list of CV restitution files, list of according labels
    """
    CV_res_files, CV_res_labels = [], []
    for dom_key, dom_vals in cv_tune.items():
        for dir_key, dir_vals in dom_vals.items():
            res = dir_vals.get('results')
            cv_res_file = res.get('cv_res')
            CV_res_files += [cv_res_file]
            CV_res_labels += ['{}-{}'.format(dom_key, dir_key)]

    return CV_res_files, CV_res_labels


def update_exp_CV(exp, tune_cv):
    cv_axs = ('vf', 'vs', 'vn')
    gi_axs = ('gil', 'git', 'gin')
    ge_axs = ('gel', 'get', 'gen')

    for dkey, res in tune_cv.items():

        if res is None:
            continue

        # experimental settings in domain
        domains = exp.get(j2c.FUNCTIONS)
        domain = domains.get(dkey)
        cvs = domain.get(j2c.FUNCTION_CV)
        G = cvs.get(j2c.FUNCTION_CV_COND)
        cvs_m = cvs.get(j2c.FUNCTION_CV_CVMEAS)
        # deal with inconsistent (historically) naming of axes

        for i, ax in enumerate(['f', 't', 'n']):
            ax_data = res.get(ax)
            if ax_data is not None:
                # tuning along this axes worked, we do have data
                r_ax = ax_data.get('results')
                p_ax = ax_data.get('params')
                # update experiment with tuned values
                G[gi_axs[i]] = r_ax['gi']
                G[ge_axs[i]] = r_ax['ge']
                cvs_m[cv_axs[i]] = r_ax['vel']
                # update also surf2vol
                G['surf2vol'] = p_ax.get('beta')

    return exp


def update_exp_CVres(exps, tune_cv):

    # deal with inconsistent (historically) naming of axes
    cvr_f = ('f_file', 's_file', 'n_file')
    cvr_p = ('f_pcl', 's_pcl', 'n_pcl')
    cvr_v = ('f_v', 's_v', 'n_v')

    # walk over all tuned domains
    for dkey, dval in tune_cv.items():
        if dval is None:
            continue

        # experimental settings in domain
        domains = exps.get(j2c.FUNCTIONS)
        domain = domains.get(dkey)
        cvs = domain.get(j2c.FUNCTION_CV)
        # on = cvs.get('mutable')

        if 'CVres' not in cvs:
            cvs['CVres'] = dict()
        cvres = cvs['CVres']

        for i, ax in enumerate(['f', 't', 'n']):
            ax_data = dval.get(ax)
            if ax_data is not None:
                # tuning along this axis worked, we do have data
                r_ax = ax_data.get('results')
                cvres[cvr_f[i]] = r_ax.get('cv_res')
                # copy over pcl and cv data
                pcls = r_ax.get('pcls')
                cvs = r_ax.get('cvs')
                cvres[cvr_p[i]] = pcls
                cvres[cvr_v[i]] = cvs

    return exps


# update experimental description
def update_exp_desc(up_flag, exp_file, exp):
    fname = exp_file if up_flag else 'tmp-{}'.format(os.path.basename(exp_file))
    print('Updating experimental settings to {}\n'.format(fname))
    with open(fname, 'w') as f:
        json.dump(exp, f, indent=4)


def converge_CVs(job, exp, converge, tol, tmode, dx, res_pcls, lim_cyc_dir,
                 funcs_ignored, tag, np, visual, ID, carp_opts, dry):
    # get domain dictionary
    funcs = exp.get(j2c.FUNCTIONS, None)
    # get solver options
    solver = exp.get(j2c.SOLVER, j2c.DEFAULT_SOLVER)
    # compute limit cycle for each region, if mutable flag is set
    cv_tune = dict()

    for name, func in funcs.items():
        # if domain list provided, skip any domain not in list
        if funcs_ignored is not None and name in funcs_ignored:
            print('Ignoring function "{}" ...\n'.format(name))
            continue

        ep = func.get(j2c.FUNCTION_EP, None)
        cv = func.get(j2c.FUNCTION_CV, None)
        if ep is None or cv is None:
            print('\nNo EP or CV defined for domain {} !\n'.format(name))
            continue

        cv_tune[name] = converge_CV(job, solver, converge, tol, tmode, dx, res_pcls, name,
                                    ep, cv, lim_cyc_dir, np, tag, ID, carp_opts, visual,
                                    delete_simID=True)
        # save tuning results
        if not dry:
            with open('./{}/tune_cv.json'.format(job.ID), 'w') as f:
                json.dump(cv_tune, f, indent=4)

    return cv_tune
