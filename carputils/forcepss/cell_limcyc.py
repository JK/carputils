#!/usr/bin/env python3

# ---( system imports )--------------------------
import os
import sys
import shutil
# ---( carputils imports )-----------------------
from carputils import settings
from carputils import restitute as res
from carputils.forcepss import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle experiment'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)


def get_sv_init_file_name(name, model, bcl, init_dir):
    return init_dir+'/{}_{}_bcl_{:.1f}.sv'.format(name, model, bcl)


def get_apdres_file_name(name, model, bcl, init_dir):
    return init_dir+'/{}_{}_bcl_{:.1f}_APD_restitution.dat'.format(name, model, bcl)


def get_apbeats_file_name(name, model, bcl, init_dir):
    return init_dir+'/{}_{}_bcl_{:.1f}_AP_traces.dat'.format(name, model, bcl)


def print_init_summary(funcs, cycles):
    print('Single cell limit cycle initialization completed.')
    print('='*88+'\n')
    print('{:18s} : {:8s} {:6s} {}'.format('cell', 'bcl [ms]', 'cycles', 'init vector'))
    print('-'*88)
    for name, func in funcs.items():
        # skip domains without EP definitions
        if (ep := func.get(j2c.FUNCTION_EP)) is not None:
            fmt = '{:18s} : {:5.1f} ms {:6d} {}'
            num_cycles = cycles if cycles is not None else ep.get(j2c.FUNCTION_EP_CYCLES)
            print(fmt.format(name, ep.get(j2c.FUNCTION_EP_BCL), num_cycles, ep.get(j2c.FUNCTION_EP_INIT)))
        else:
            print('\nNo EP defined for domain {} !\n'.format(name))


# initialize cell, launch limit cycle pacing experiment for a given cell
def init_cell(ep, cycs, job, lim_cyc_dir, dkey, visual, verify):
    model = ep.get(j2c.FUNCTION_EP_MODEL)
    bcl = ep.get(j2c.FUNCTION_EP_BCL)

    # assemble name of file for storing initial state vector
    init_file = get_sv_init_file_name(dkey, model, bcl, lim_cyc_dir)
    ep[j2c.FUNCTION_EP_INIT] = init_file

    # configure cellular dynamics
    cell_dyn = ['--imp', model]
    if (imp_par := ep.get(j2c.FUNCTION_EP_MODELPAR)) is not None and len(imp_par) > 0:
        cell_dyn += ['--imp-par', imp_par]
    if (plg := ep.get(j2c.FUNCTION_EP_PLUGINS)) is not None and len(plg) > 0:
        cell_dyn += ['--plug-in', plg]
    if (plg_par := ep.get(j2c.FUNCTION_EP_PLUGPARS)) is not None and len(plg_par) > 0:
        cell_dyn += ['--plug-par', plg_par]

    # configure bench
    duration = cycs * bcl
    bcmd = [settings.execs.BENCH] + cell_dyn
    bcmd += ['--bcl', bcl,
             '--stim-curr', 40,
             '--stim-dur', 1.,
             '--stim-start', 0,
             '--dt', 20.0/1000.0,
             '--no-trace', 'on']

    if not verify:
        # run full limit cycle protocol
        lc_cmd = bcmd.copy()
        lc_cmd += ['--numstim', cycs,
                   '--duration', duration,
                   '--fout={}/{}_limit_cycle'.format(job.ID, dkey),
                   '-S', duration,
                   '-F', init_file]

        if visual:
            lc_cmd += ['--dt-out', 1.0, '--validate']
        else:
            lc_cmd += ['--dt-out', duration]

        job.bash(lc_cmd)

    else:
        # check whether limit cycle state vector has been generated
        if not os.path.isfile(init_file):
            print('\n\nInitial state file {init_file} not found!')
            print('Run limit cycle experiment first, initial state is required for verifying the limit cycle.\n\n')
            return
            sys.exit()

        # create another state vector offset by 1 cycle
        p1_cmd = bcmd.copy()

        # read in limit cycle initial state vector and run for one cycle
        p1_cmd += ['--numstim', 1,
                   '--duration', bcl,
                   '-R', init_file,
                   '-S', bcl,
                   '-F', init_file + '.p1',
                   '--fout={}/{}_limit_cycle_p1'.format(job.ID, dkey),
                   '--dt-out', bcl]

        job.bash(p1_cmd)

        # Now restart from both initial state vectors and write out the traces
        num_verify_cycs = 3

        p0_restart_cmd = bcmd.copy()

        p0_restart_cmd += ['--dt-out', 1.0,
                           '--validate',
                           '--numstim', num_verify_cycs,
                           '--duration', bcl * num_verify_cycs]

        p1_restart_cmd = p0_restart_cmd.copy()

        p0_restart_cmd += ['-R', init_file,
                           '--fout={}/{}_limit_cycle_p0_restart'.format(job.ID, dkey)]

        p1_restart_cmd += ['-R', init_file + '.p1',
                           '--fout={}/{}_limit_cycle_p1_restart'.format(job.ID, dkey)]

        job.bash(p0_restart_cmd)
        job.bash(p1_restart_cmd)

    return ep


# create dictionary entry describing a restitution protocol
def create_restitution_prtcl(prtclID, bcl, CI0, CI1, CIinc, nbeats, prebeats):
    p = {}
    if 's1s2' in prtclID.lower():
        p[j2c.PROTOCOLS_APD_RESTITUTION_TYPE] = 'S1S2'
    elif 'dyn' in prtclID.lower():
        p[j2c.PROTOCOLS_APD_RESTITUTION_TYPE] = 'dynamic'
    else:
        msg_tpl = 'Unknown restitution protocol type "{}" specified, switching to default S1S2 protocol.'
        print(msg_tpl.format(p.get('type')))
        p[j2c.PROTOCOLS_APD_RESTITUTION_TYPE] = 'S1S2'

    p[j2c.PROTOCOLS_APD_RESTITUTION_PPBEATS] = prebeats
    p[j2c.PROTOCOLS_APD_RESTITUTION_BCL] = bcl
    p[j2c.PROTOCOLS_APD_RESTITUTION_CI0] = CI0
    p[j2c.PROTOCOLS_APD_RESTITUTION_CI1] = CI1
    p[j2c.PROTOCOLS_APD_RESTITUTION_PMBEATS] = nbeats
    p[j2c.PROTOCOLS_APD_RESTITUTION_PMDEC] = CIinc

    prtcl_dict = {prtclID: p}
    return prtcl_dict


# measured cell restitute restitution
def restitute_cell(ep, rptcls, job, lim_cyc_dir, name, visual, dry):
    model = ep.get(j2c.FUNCTION_EP_MODEL)
    bcl = ep.get(j2c.FUNCTION_EP_BCL)
    # assemble name of file for storing initial state vector 
    init_file = get_sv_init_file_name(name, model, bcl, lim_cyc_dir)
    ep[j2c.FUNCTION_EP_INIT] = init_file
    # configure cellular dynamics
    cell_dyn = ['--imp', model]
    if (imp_par := ep.get(j2c.FUNCTION_EP_MODELPAR)) is not None and len(imp_par) > 0:
        cell_dyn += ['--imp-par', imp_par]
    if (plg := ep.get(j2c.FUNCTION_EP_PLUGINS)) is not None and len(plg) > 0:
        cell_dyn += ['--plug-in', plg]
    if (plg_par := ep.get(j2c.FUNCTION_EP_PLUGPARS)) is not None and len(plg_par) > 0:
        cell_dyn += ['--plug-par', plg_par]
    # carry out restitution experiment

    # set default APD-restitution parameters
    CI0 = 50
    CI1 = 600
    CIinc = 25
    nbeats = 5
    bcl = 600
    ptcl = 'S1S2'
    pbeats = 20

    if (rp := ep.get(j2c.FUNCTION_EP_APDRESPRTCL)) is not None:

        if (rp_spec := get_apd_restitution_prtcl(rp, rptcls)) is not None:
            # unpack apd protocol restitution parameters
            print('\nUsing APD restitution protocol {}.\n\n'.format(rp))
            ptcl = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_TYPE)
            CI0 = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_CI0)
            CI1  = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_CI1)
            CIinc = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_PMDEC)
            nbeats = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_PPBEATS)
            bcl = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_BCL)
            pbeats = rp_spec.get(j2c.PROTOCOLS_APD_RESTITUTION_PMBEATS)
        else:
            print('\nAPD restitution protocol "{}" not found, using default settings.\n\n'.format(rp))

    else:
        print('\nNo APD restitution protocol found, using default settings.\n\n')

    # read in limit cycle state vector
    sv_init = get_sv_init_file_name(name, model, bcl, lim_cyc_dir)
    print('Running restitution experiments to determine APD(DI)')
    res.restitute(job, ep.get('model'), imp_par, plg, plg_par, CI0, CI1, CIinc, nbeats, bcl, ptcl, pbeats, sv_init, dry)

    # move APD restitution directory to avoid overwriting
    src_dir = os.path.join(job.ID)
    trg_dir = os.path.join('{}_{}'.format(job.ID, name))
    trg_apdfile = get_apdres_file_name(name, model, bcl, lim_cyc_dir)
    trg_apbeats_file = get_apbeats_file_name(name, model, bcl, lim_cyc_dir)

    if not dry:
        # copy output restitution data to target directory
        shutil.copytree(src_dir, trg_dir, dirs_exist_ok=True)
        # copy APD restitution output to init directory
        # pass back final apd restitution file for plotting
        src_apdfile = os.path.join(trg_dir, 'restout_APD_restitution.dat')
        shutil.copy(src_apdfile, trg_apdfile)
        # same for AP Vm trace output
        src_apbeats_file = os.path.join(trg_dir, 'restout.txt')
        shutil.copy(src_apbeats_file, trg_apbeats_file)

    ep[j2c.FUNCTION_EP_APDRESFILE] = trg_apdfile
    return trg_apdfile, trg_apbeats_file


def ep_limit_cycle(job, exp, bcl, cycles, lim_cyc_dir, funcs_ignored, visual, verify):
    # compute limit cycle or restitution for each region, if mutable flag is set
    funcs = exp[j2c.FUNCTIONS]
    n_acts = 0
    for name, func in funcs.items():
        if funcs_ignored is not None and name in funcs_ignored:
            print('Ignoring function "{}" ...\n'.format(name))
            continue

        print('# ' + '=' * 80 + '\n# Domain "{}" ...'.format(name) + '\n# ' + '=' * 80)

        # get EP related data
        ep = func.get(j2c.FUNCTION_EP)
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(name))
            continue

        # select bcl and #cycles for limit cycle experiment
        if bcl is not None:
            ep[j2c.FUNCTION_EP_BCL] = float(bcl)
        if cycles is not None:
            ep[j2c.FUNCTION_EP_CYCLES] = int(cycles)

        bcl = ep[j2c.FUNCTION_EP_BCL]
        cycles = ep[j2c.FUNCTION_EP_CYCLES]

        # dealing with mutable flag
        #print('\nDomain {:s}'.format(name))
        #print('-'*(len(name)+7))

        print('\nLimit cycle at bcl {:.1f} ms for {:d} cycles.\n'.format(bcl, cycles))
        n_acts += 1
        ep = init_cell(ep, cycles, job, lim_cyc_dir, name, visual, verify)

        """
        if mutable:
            print('\nLimit cycle at bcl {:.1f} ms for {:d} cycles.\n'.format(ep.get('bcl'), cycles))
            n_acts += 1
            ep = init_cell(ep, cycles, job, lim_cyc_dir, name, visual)
        else:
            # no limit cycle pacing, check for valid initial state vector
            init_sv_file = ep.get('init')
            if init_sv_file:
                print('\nNo limit cycle pacing, use pre-existing initial state vector {:s}.'.format(init_sv_file))
                # provided init file not modified, but make sure that name is consistent
                sv_init = get_sv_init_file_name(name, ep.get('model'), ep.get('bcl'), lim_cyc_dir)
                # if file exist, rename to ensure consistent naming
                if init_sv_file != sv_init and os.path.isfile(init_sv_file):
                    print('Rename init file {:s} to {:s}.'.format(init_sv_file, sv_init))
                    os.rename(init_sv_file, sv_init)
                print('\n\n')
            else:
                # init file string was null or empty
                print('No valid initial state vector provided!')
        """

    return n_acts, exp


def get_apd_restitution_prtcl_sec(exp):
    # check whether we have protocol descriptions
    # if protocol given as arg, overrule all other settings
    if (prtcls := exp.get(j2c.PROTOCOLS)) is not None:
        # Are there APD restitution protocols?
        return prtcls.get(j2c.PROTOCOLS_APD_RESTITUTION)
    else:
        return None


def get_apd_restitution_prtcl(p, prtcls):
    # retrieve parameters of specific restitution protocol, if given
    if prtcls is not None and (apd_res_prtcl := prtcls.get(p)) is not None:
        print('Using APD restitution protocol {}.'.format(p))
        return apd_res_prtcl
    else:
        print('Definition of specified protocol {} not found, using default settings.'.format(p))
        return None


def ep_apd_restitution(job, exp, bcl, cycles, lim_cyc_dir, funcs_ignored, visual, dry):
    # compute limit cycle or restitution for each region, if mutable flag is set
    funcs = exp.get(j2c.FUNCTIONS)
    prtcls = get_apd_restitution_prtcl_sec(exp)
    n_acts = 0
    apdResFiles = []
    apdLabels = []
    apResBeatFiles = []

    for name, func in funcs.items():
        # if domain list provided, skip any domain not in list
        if funcs_ignored is not None and name in funcs_ignored:
            continue

        print('# ' + '=' * 80 + '\n# Domain "{}" ...'.format(name) + '\n# ' + '=' * 80)

        ep = func.get(j2c.FUNCTION_EP)
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(name))
            continue

        # select bcl and #cycles for limit cycle experiment
        if bcl is not None:
            # print('Overruling bcl settings given at command line in {:s}.'.format(args.experiment))
            ep[j2c.FUNCTION_EP_BCL] = float(bcl)
        if cycles is not None:
            ep[j2c.FUNCTION_EP_CYCLES] = int(cycles)

        # dealing with mutable flag
        mutable = True
        #mutable = j2c.is_mutable(ep, ignore_mutable)
        # TODO: mutable !!!
        #print('\nDomain {:s}'.format(name))
        #print('-'*(len(name)+7))

        if mutable:
            n_acts += 1
            apdf, apbeatsf = restitute_cell(ep, prtcls, job, lim_cyc_dir, name, visual, dry)
            apdResFiles += [apdf]
            apdLabels += [name]
            apResBeatFiles += [apbeatsf]
            # read out apd(n), di(n-1)
            apdTbl = res.readAPDrestitutionFile(apdf, flt='res-curve')
            # add restitution curve to json
            ep['DI'] = apdTbl['DIp']
            ep['APD'] = apdTbl['APD']

    return n_acts, exp, apdResFiles, apdLabels, apResBeatFiles


