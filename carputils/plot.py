#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Some commonly used functionality with Matplotlib
"""

from __future__ import print_function, division, unicode_literals

import os
import sys
import copy
import math
from collections import OrderedDict
import numpy as np


from pandas import read_csv

from matplotlib import pyplot, rcParams
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_pdf import PdfPages

PRESSURE_SCALE = 0.133322387415 # mmHg to kPa
TIME_SCALE = 1e-3 # ms to s

Q_UNIT = 'mL/s'
Q_DOT_UNIT = 'mL/s^2'

def configure_ieee(columns=1, shape='square'):
    """
    Configure default Matplotlib figure size for IEEE publications.

    Width and max height in inches for IEEE journals taken from
    computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    Parameters
    ----------
    columns : int
        Number of columns, must be 1 or 2
    shape : string
        One of 'square', 'golden'
    """

    assert columns in [1, 2]
    width = 3.39 if columns == 1 else 6.9  # Width in inches

    if shape == 'square':
        height = width
    elif shape == 'golden':
        golden_mean = (1.0 + np.sqrt(5.0)) / 2.0  # Aesthetic ratio
        height = width / golden_mean
    else:
        raise Exception('Unsupported shape "{}"'.format(shape))

    params = {'axes.labelsize':  6, # fontsize for x and y labels (was 10)
              'axes.titlesize':  6,
              'font.size':       6, # was 10
              'legend.fontsize': 6, # was 10
              'xtick.labelsize': 6,
              'ytick.labelsize': 6,
              'figure.figsize':  (width, height)}

    rcParams.update(params)

def configure_a4(landscape=False):
    """
    Set default Matplotlib figure size to A4.

    Parameters
    ----------
    landscape : bool
        Set to True to make a landscape A4 page
    """

    width = 8.27 # inches
    height = 11.69 # inches

    if landscape:
        width, height = height, width

    rcParams['figure.figsize'] = (width, height)

def configure_latex():
    """
    Configure Matplotlib for LaTeX-style plotting.

    Adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples.
    """

    pyplot.rc('text', usetex=True)
    pyplot.rc('text.latex', preamble=r'\usepackage{gensymb}')
    pyplot.rc('font', family='serif')

def as_float(obj, default=0.0, scale=1.0):
    try:
        value = float(obj)*scale
    except TypeError:
        value = default
    finally:
        return value


class PVPlots(object):
    """
    Generate plots from pressure-volume data.

    Parameters
    ----------
    datanames : list of str
        Basenames of the data files to be loaded
    labels : list of str, optional
        Labels of the data sets (default: basenames)
    """

    LINESTYLE_MAP = {'dotted': ':', 'dashdot': '-.', 'dashed': '--', 'solid': '-'}

    LINEWIDTH = 1.5

    def __init__(self, params, timeinterval=None, loading=True, Zv=0.0,
                 legend=True, legend_fontsize=10.0, legend_loc='auto',
                 legend_alpha=0.5, title=None, title_fontsize=16.0,
                 quantile_view=False):

        self._data = OrderedDict()
        self._keys = []
        self._vunit = 'mL'
        self._pscale = 1.0
        self._punit = 'kPa'
        self._pdotunit = 'kPa/s'
        self._tscale = 1.0
        self._tunit = 's'
        self._zv = float(Zv)
        self._legend = bool(legend)
        self._legend_fontsize = max(1.0, float(legend_fontsize))
        self._legend_loc = legend_loc
        self._legend_alpha = min(1.0, max(0.0, float(legend_alpha)))
        self._title = str(title) if title else None
        self._title_fontsize = max(1.0, float(title_fontsize))
        self._qview = bool(quantile_view)
        self._state_color = ['#AA00CC', '#AA7700', '#CC00AA', '#0000AA', '#00CCAA']
        self._has_stress = False
        self._has_strain = False

        num_params = len(params)
        while num_params > 0:
            time_interval = timeinterval

            param = params.pop(0)
            num_params = len(params)

            opts = param.split(':')
            fname = opts[0]

            data_opts = dict()
            data_opts['Label'] = os.path.basename(fname).split('.')[1]
            data_opts['Label_Ext'] = True
            data_opts['TimeShift'] = 0.0
            data_opts['VolumeShift'] = 0.0
            data_opts['VolumeScale'] = 1.0
            data_opts['PressureShift'] = 0.0
            data_opts['PressureScale'] = 1.0
            data_opts['FluxShift'] = 0.0
            data_opts['FluxScale'] = 1.0
            data_opts['Color'] = None
            data_opts['LineStyle'] = '-'
            data_opts['State'] = 0
            data_opts['CycleLength'] = None
            data_opts['MeanPressure'] = False
            data_opts['Extended'] = False

            for opt in opts[1:]:
                if opt.startswith('l=') and len(opt) > 2:
                    data_opts['Label'] = opt[2:]
                    data_opts['Label_Ext'] = False
                if opt.startswith('l+=') and len(opt) > 3:
                    data_opts['Label'] += '.'+opt[3:]
                    data_opts['Label_Ext'] = True
                elif opt.startswith('t+=') and len(opt) > 3:
                    data_opts['TimeShift'] = as_float(opt[3:])
                elif opt.startswith('t-=') and len(opt) > 3:
                    data_opts['TimeShift'] = -as_float(opt[3:])
                elif opt.startswith('v+=') and len(opt) > 3:
                    data_opts['VolumeShift'] = as_float(opt[3:])
                elif opt.startswith('v-=') and len(opt) > 3:
                    data_opts['VolumeShift'] = -as_float(opt[3:])
                elif opt.startswith('v*=') and len(opt) > 3:
                    data_opts['VolumeScale'] = as_float(opt[3:])
                elif opt.startswith('v/=') and len(opt) > 3:
                    data_opts['VolumeScale'] = 1./as_float(opt[3:])
                elif opt.startswith('p+=') and len(opt) > 3:
                    data_opts['PressureShift'] = as_float(opt[3:])
                elif opt.startswith('p-=') and len(opt) > 3:
                    data_opts['PressureShift'] = -as_float(opt[3:])
                elif opt.startswith('p*=') and len(opt) > 3:
                    data_opts['PressureScale'] = as_float(opt[3:])
                elif opt.startswith('p/=') and len(opt) > 3:
                    data_opts['PressureScale'] = 1./as_float(opt[3:])
                elif opt.startswith('f+=') and len(opt) > 3:
                    data_opts['FluxShift'] = as_float(opt[3:])
                elif opt.startswith('f-=') and len(opt) > 3:
                    data_opts['FluxShift'] = -as_float(opt[3:])
                elif opt.startswith('f*=') and len(opt) > 3:
                    data_opts['FluxScale'] = as_float(opt[3:])
                elif opt.startswith('f/=') and len(opt) > 3:
                    data_opts['FluxScale'] = 1./as_float(opt[3:])
                elif opt.startswith('c=') and opt[2] == '#':
                    data_opts['Color'] = opt[2:]
                elif opt.startswith('ls='):
                    data_opts['LineStyle'] = PVPlots.LINESTYLE_MAP.get(opt[3:], '-')
                elif opt.startswith('bcl=') and len(opt) > 4:
                    data_opts['CycleLength'] = as_float(opt[4:], None, scale=TIME_SCALE)
                elif opt == 'state':
                    data_opts['State'] = 1
                elif opt == 'stateonly':
                    data_opts['State'] = 2
                elif opt == 'meanp':
                    data_opts['MeanPressure'] = True
                elif opt == 'ext':
                    data_opts['Extended'] = True

            if not os.path.exists(fname):
                print('File "{}" does not exist!'.format(fname))
                continue

            key = data_opts['Label']
            count = 0
            while key in self._keys:
                count += 1
                key = data_opts['Label'] + str(count)
            self._keys.append(key)

            if data_opts['Label_Ext']:
                data_opts['Label'] = key

            print('Loading file "{}", label "{}"'.format(fname, data_opts['Label']))

            # Load data file
            data = {}
            csv = read_csv(fname, delimiter=",", skipinitialspace=True,
                           header=0, comment='#')

            if loading:
                init_time = csv['Time'][0]
                time_interval = [init_time, 0]
            else:
                if time_interval is None:
                    init_time = 0. if csv['Time'][0] < 0. else csv['Time'][0]
                    end_time = csv['Time'].iloc[-1]
                    time_interval = [init_time*TIME_SCALE, end_time*TIME_SCALE]

            if time_interval is not None:
                if len(time_interval) == 1:
                    filtered = csv[csv['Time'] >= float(time_interval[0])/TIME_SCALE]
                    csv = filtered
                elif len(time_interval) == 2:
                    if float(time_interval[1]) < float(time_interval[0]):
                        raise ValueError('Invalid time interval specified.')
                    filtered = csv[csv['Time'] >= float(time_interval[0])/TIME_SCALE]
                    filtered = filtered[filtered['Time'] <= float(time_interval[1])/TIME_SCALE]
                    csv = filtered
                else:
                    raise ValueError('Invalid time interval specified.')

            basename = os.path.basename(fname)

            data['Options'] = data_opts
            data['Time'] = (csv['Time']+data_opts['TimeShift'])*TIME_SCALE
            if basename.startswith('cav'):
                if data_opts['State'] != 2:
                    cavity = basename.split('.')[1]
                    proper_cavity = cavity in ('LV', 'RV', 'LA', 'RA')

                    data['Volume'] = (csv['Volume']*data_opts['VolumeScale'] + data_opts['VolumeShift'])
                    data['Pressure'] = (csv['Pressure']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                    if proper_cavity and 'Flux' in csv:
                        data['Flux'] = csv['Flux']
                    elif proper_cavity and 'Q_in' in csv:
                        data['Flux'] = (csv['Q_in'] - csv['Q_out'])*data_opts['FluxScale'] + data_opts['FluxShift']
                    if proper_cavity and 'P_out' in csv:
                        data['D_Flux'] = np.gradient(data['Flux'], 0.001)
                        data['P_Out'] = (csv['P_out']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                        data['P_In'] = (csv['P_in']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                        data['Q_Out'] = csv['Q_out']*data_opts['FluxScale'] + data_opts['FluxShift']
                        data['Q_In'] = csv['Q_in']*data_opts['FluxScale'] + data_opts['FluxShift']
                        data['P_Tube'] = data['P_In'] - self._zv*data['Q_Out']

                    # Compute pressure gradients (scale ms to s)
                    data['D_P'] = np.gradient(data['Pressure'], 0.001)
                    if proper_cavity and 'P_Out' in csv:
                        # Compute pressure gradients (scale ms to s)
                        data['D_P_Out'] = np.gradient(data['P_Out'], 0.001)

                    if data_opts['Extended']:
                        if 'sf_act' in csv:
                            self._has_stress = True
                            data['S_act'] = csv['sf_act']
                        if 'sf_pas' in csv:
                            self._has_stress = True
                            data['S_pas'] = csv['sf_pas']
                        if 'Ef' in csv:
                            self._has_strain = True
                            data['Ef'] = csv['Ef']

                if data_opts['State'] > 0:
                    #L_state = (csv['State']=='load').astype(int)
                    C_state = (csv['State'] == 'IVC').astype(int)
                    R_state = (csv['State'] == 'IVR').astype(int)
                    E_state = (csv['State'] == 'ejec').astype(int)
                    F_state = (csv['State'] == 'fill').astype(int)
                    data['State'] = C_state+2*R_state+3*E_state+4*F_state

            elif basename.startswith('tube'):
                data['Volume'] = (csv['Volume']*data_opts['VolumeScale'] + data_opts['VolumeShift'])
                data['Diameter'] = 2.*np.sqrt(csv['CSArea']/math.pi)
                tube = basename.split('.')[1]
                if tube in ['AO', 'AP']:  # for arteries we use in pressure/flux
                    data['Pressure'] = (csv['P_in']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                    data['Flux'] = csv['Q_in']*data_opts['FluxScale'] + data_opts['FluxShift']
                elif tube in ['VC', 'VP']:  # for venes we use out pressure/flux
                    data['Pressure'] = (csv['P_out']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                    data['Flux'] = csv['Q_out']*data_opts['FluxScale'] + data_opts['FluxShift']
                else:
                    sys.exit('Unable to process file %s' % fname)
                data['D_Flux'] = np.gradient(data['Flux'], 0.001)

            elif basename.startswith('valve'):
                if data_opts['State'] != 2:
                    data['P_in'] = (csv['P_in']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                    data['P_out'] = (csv['P_out']*data_opts['PressureScale'] + data_opts['PressureShift'])*PRESSURE_SCALE
                    data['Flux'] = csv['Flux']*data_opts['FluxScale'] + data_opts['FluxShift']
                    data['D_Flux'] = np.gradient(data['Flux'], 0.001)
                if data_opts['State'] > 0:
                    data['State'] = (csv['State'] == 'open').astype(int)

            elif basename.startswith('NektarInlet'): # coupling with Nektar1D BF solver
                data['Diameter'] = 2.*np.sqrt(csv['CSArea']/math.pi)
            else:
                if 'Volume' in csv:
                    data['Volume'] = (csv['Volume']*data_opts['VolumeScale'] + data_opts['VolumeShift'])
                if 'Pressure' in csv:
                    data['Pressure'] = csv['Pressure']*PRESSURE_SCALE
                if 'Flux' in csv:
                    data['Flux'] = csv['Flux']

            # Store data
            self._data[key] = data

    def use_mmhg(self):
        """
        Configure to plot mmHg
        """
        self._pscale = 7.50061561303
        self._punit = 'mmHg'
        self._pdotunit = 'mmHg/s'

    def use_ms(self):
        """
        Configure to plot ms
        """
        self._tscale = 1.0e+3
        self._tunit = 'ms'

    @staticmethod
    def _axes_relim_y(axes, q_min=0.05, q_max=0.95, padding=0.1):
        """
        Re-limit y axis
        """
        assert 0.0 < q_min < q_max < 1.0

        quantile_range = None
        for line in axes.get_lines():
            _, ydata = line.get_data(orig=True)
            quantile_min = np.quantile(ydata, q_min)
            quantile_max = np.quantile(ydata, q_max)
            if quantile_range is None:
                quantile_range = [quantile_min, quantile_max]
            quantile_range[0] = min(quantile_range[0], quantile_min)
            quantile_range[1] = max(quantile_range[1], quantile_max)

        if quantile_range is not None:
            padding = min(1.0, max(padding, 0.0))
            padding_range = (quantile_range[1]-quantile_range[0])*padding
            axes.set_ylim(quantile_range[0]-padding_range, quantile_range[1]+padding_range)

    @staticmethod
    def _axes_relim_x(axes, q_min=0.05, q_max=0.95, padding=0.1):
        """
        Re-limit x axis
        """
        assert 0.0 < q_min < q_max < 1.0

        quantile_range = None
        for line in axes.get_lines():
            xdata, _ = line.get_data(orig=True)
            quantile_min = np.quantile(xdata, q_min)
            quantile_max = np.quantile(xdata, q_max)
            if quantile_range is None:
                quantile_range = [quantile_min, quantile_max]
            quantile_range[0] = min(quantile_range[0], quantile_min)
            quantile_range[1] = max(quantile_range[1], quantile_max)

        if quantile_range is not None:
            padding = min(1.0, max(padding, 0.0))
            padding_range = (quantile_range[1]-quantile_range[0])*padding
            axes.set_xlim(quantile_range[0]-padding_range, quantile_range[1]+padding_range)

    def _plot_data(self, axes, i_x, i_y, l_s='-', **kwargs):
        """
        Plot all data sets on the specified axes.
        """

        xscale, yscale = 1.0, 1.0
        if i_x in ['Pressure', 'P_In', 'P_Out']:
            xscale = self._pscale
        elif i_x in ['Time']:
            xscale = self._tscale

        if i_y in ['Pressure', 'P_In', 'P_Out']:
            yscale = self._pscale
        elif i_y in ['Time']:
            yscale = self._tscale

        for key, data in self._data.items():
            data_opts = data['Options']
            try:
                args = copy.deepcopy(kwargs)
                if data_opts['Color'] is not None:
                    args['color'] = data_opts['Color']
                args['linestyle'] = data_opts.get('LineStyle', l_s)
                args['linewidth'] = self.LINEWIDTH
                if data_opts['Label_Ext']:
                    args['label'] = data_opts['Label'] + '-' + i_y
                else:
                    args['label'] = data_opts['Label']
                axes.plot(data[i_x] * xscale, data[i_y] * yscale, **args)
            except:
                pass

    def _plot_state(self, axes, **kwargs):
        """
        Plot states.
        """
        xscale = self._tscale

        for key, data in self._data.items():
            data_opts = data['Options']
            time = data.get('Time', None)
            state = data.get('State', None)
            if time is None or state is None:
                continue

            spans = {}
            v_prev = 0

            for i, v in enumerate(state):
                if v != v_prev or i+1 == len(state):
                    if v != 0 and v not in spans:
                        spans[v] = [time.iloc[i]*xscale]
                    if v_prev in spans and len(spans[v_prev]) == 1:
                        spans[v_prev].append(time.iloc[i]*xscale)

                for w, span in spans.items():
                    if len(span) == 2:
                        if span[1]-span[0] > 0.01:
                            col_idx = w % len(self._state_color)
                            args = copy.deepcopy(kwargs)
                            args['color'] = self._state_color[col_idx]
                            if data_opts['Color'] is not None:
                                args['color'] = data_opts['Color']
                            axes.axvspan(span[0]*xscale, span[1]*xscale, **args)
                        del spans[w]

                v_prev = v

    @staticmethod
    def _create_legend(*axis, **kwargs):
        handles, labels = [], []
        for axes in axis:
            axes_handles, axes_labels = axes.get_legend_handles_labels()
            handles += axes_handles
            labels += axes_labels
        for idx, artist in enumerate(handles):
            setattr(artist, 'legendID', idx)
        return axes.legend(handles, labels, **kwargs)

    def volume_trace(self):
        """
        Generate a figure of the volume traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Volume ({})'.format(self._vunit))

        # Plot data
        self._plot_data(axes, 'Time', 'Volume', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def pressure_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Pressure ({})'.format(self._punit))

        # Plot data
        self._plot_data(axes, 'Time', 'Pressure', picker=True)
        self._plot_data(axes, 'Time', 'P_Out', picker=True)
        if self._zv > 0.0:
            self._plot_data(axes, 'Time', 'P_Tube', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def pressuredot_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Pressure Rate ({})'.format(self._pdotunit))

        # Plot data
        self._plot_data(axes, 'Time', 'D_P', picker=True)
        self._plot_data(axes, 'Time', 'D_P_Out', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def flux_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Flux ({})'.format(Q_UNIT))

        # Plot data
        self._plot_data(axes, 'Time', 'Flux', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def q_in_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Inflow ({})'.format(Q_UNIT))

        # Plot data
        self._plot_data(axes, 'Time', 'Q_In', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def q_out_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Outflow ({})'.format(Q_UNIT))

        # Plot data
        self._plot_data(axes, 'Time', 'Q_Out', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def fluxdot_trace(self):
        """
        Generate a figure of the pressure traces.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label axes
        axes.set_xlabel('Time (s)')
        axes.set_ylabel('Flux Rate ({})'.format(Q_DOT_UNIT))

        # Plot data
        self._plot_data(axes, 'Time', 'D_Flux', picker=True)
        if self._qview:
            self._axes_relim_y(axes)

        # Generate legend
        if len(self._data) > 1 and self._legend:
            loc = 'lower right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def pv_trace(self, combined=True):
        """
        Generate a combined figure of the pressure and volume traces.

        Parameters
        ----------
        combined : bool, optional
            If True (default), plot on a shared x axis, with independent y axes
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)

        if combined:
            # Shared x axis
            axp = fig.add_subplot(1, 1, 1)
            axv = axp.twinx()
            axp.set_xlabel('Time (s)')
        else:
            # Two plots vertically
            axp = fig.add_subplot(2, 1, 1)
            axv = fig.add_subplot(2, 1, 2)
            axv.set_xlabel('Time (s)')

        # Assign common axis labels
        axp.set_ylabel('Pressure ({})'.format(self._punit))
        axv.set_ylabel('Volume (ml)')

        # Plot data
        # Pressure traces
        self._plot_data(axp, 'Time', 'Pressure', picker=True)
        # Volume traces
        self._plot_data(axv, 'Time', 'Volume', '--' if combined else '-', picker=True)
        if self._qview:
            self._axes_relim_y(axp)
            self._axes_relim_y(axv)

        if len(self._data) > 1 and self._legend:
            loc = 'upper left' if self._legend_loc == 'auto' else self._legend_loc
            if combined:
                self._create_legend(axp, axv, loc=loc, fontsize=self._legend_fontsize,
                                    framealpha=self._legend_alpha)
            else:
                self._create_legend(axp, loc=loc, fontsize=self._legend_fontsize,
                                    framealpha=self._legend_alpha)
                self._create_legend(axv, loc=loc, fontsize=self._legend_fontsize,
                                    framealpha=self._legend_alpha)

        return fig

    def pvloop(self):
        """
        Generate a figure of the PV loops.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        axes = fig.add_subplot(1, 1, 1)

        # Label the axes
        axes.set_xlabel('Volume (ml)')
        axes.set_ylabel('Pressure ({})'.format(self._punit))

        # Plot the data
        self._plot_data(axes, 'Volume', 'Pressure', picker=True)
        if self._qview:
            self._axes_relim_y(axes)
            self._axes_relim_x(axes)

        # Generate a legend
        if len(self._data) > 1 and self._legend:
            loc = 'upper left' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axes, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

        return fig

    def combined(self):
        """
        Generate a single figure with PV traces and PV loop.
        """
        fig = pyplot.figure()
        # Set figure title
        if self._title is not None:
            fig.suptitle(self._title, fontsize=self._title_fontsize)
        else:
            fig.suptitle('Cavity Pressure & Volume', fontsize=16)

        # Custom layout
        sz = [2, 2]

        stress_idx = -1
        if self._has_stress:
            stress_idx = sz[0]
            sz[0] += 1

        strain_idx = -1
        if self._has_strain:
            strain_idx = sz[0]
            sz[0] += 1

        grid_s = GridSpec(sz[0], sz[1], wspace=0.25, hspace=0.08, left=0.1, right=0.92)
        # PV Loop
        axl = fig.add_subplot(grid_s[:, 0])
        # Label axes
        axl.set_xlabel('Volume (ml)')
        axl.set_ylabel('Pressure ({})'.format(self._punit))
        # Plot data
        self._plot_data(axl, 'Volume', 'Pressure', picker=True)
        if self._qview:
            self._axes_relim_y(axl)
            self._axes_relim_x(axl)

        # Pressure trace
        axp = fig.add_subplot(grid_s[0, 1])
        # Label axes
        axp.set_ylabel('Pressure ({})'.format(self._punit))
        # Hide axis values
        pyplot.setp(axp.get_xticklabels(), visible=False)

        # Plot data
        self._plot_data(axp, 'Time', 'Pressure', picker=True)
        #self._plot_data(axp, 'Time', 'P_Out', picker=True)
        #self._plot_data(axp, 'Time', 'P_In', picker=True)
        self._plot_state(axp, alpha=0.125)
        if self._qview:
            self._axes_relim_y(axp)

        # Volume trace
        axv = fig.add_subplot(grid_s[1, 1], sharex=axp)
        # Label axes
        axv.set_ylabel('Volume (ml)')
        # Hide axis values
        if self._has_stress or self._has_strain:
            pyplot.setp(axv.get_xticklabels(), visible=False)
        else:
            axv.set_xlabel('Time (s)')

        # Plot data
        self._plot_data(axv, 'Time', 'Volume', picker=True)
        self._plot_state(axv, alpha=0.125)
        if self._qview:
            self._axes_relim_y(axv)

        if self._has_stress:
            # Stress trace
            axs = fig.add_subplot(grid_s[stress_idx, 1], sharex=axp)
            # Label axes
            if not self._has_strain:
                axs.set_xlabel('Time (s)')
            axs.set_ylabel('Stress (kPa)')
            # Hide axis values
            if self._has_strain:
                pyplot.setp(axs.get_xticklabels(), visible=False)

            # Plot data
            self._plot_data(axs, 'Time', 'S_act', picker=True)
            self._plot_data(axs, 'Time', 'S_pas', picker=True)
            if self._qview:
                self._axes_relim_y(axs)

        if self._has_strain:
            # Stress trace
            axe = fig.add_subplot(grid_s[strain_idx, 1], sharex=axp)
            # Label axes
            axe.set_xlabel('Time (s)')
            axe.set_ylabel('Strain (-)')
            # Plot data
            self._plot_data(axe, 'Time', 'Ef', picker=True)
            if self._qview:
                self._axes_relim_y(axe)

        if len(self._data) > 1 and self._legend:
            loc = 'upper right' if self._legend_loc == 'auto' else self._legend_loc
            self._create_legend(axl, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)
            self._create_legend(axv, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)
            self._create_legend(axp, loc=loc, fontsize=self._legend_fontsize,
                                framealpha=self._legend_alpha)

            if self._has_stress:
                self._create_legend(axs, loc=loc, fontsize=self._legend_fontsize,
                                    framealpha=self._legend_alpha)
            if self._has_strain:
                self._create_legend(axe, loc=loc, fontsize=self._legend_fontsize,
                                    framealpha=self._legend_alpha)

        return fig

    def report(self, filename):
        """
        Generate a multi-page PDF report with PV loop and traces.

        Parameters
        ----------
        filename : str
            File to save report to
        """

        # Generate and store the figure objects
        figs = [self.pvloop(),
                self.pressure_trace(),
                self.volume_trace()]

        # Generate a multi-page PDF report
        with PdfPages(filename) as pdf:
            for fig in figs:
                pdf.savefig(fig)
                pyplot.close(fig)
