#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Tags for the categorisation of tests in the carputils framework.

A list of tags may be passed to the constructor of a test::

    from carputils.testing import Test, tag
    newtest = Test('mytest', run, ['--experiment', 'new'],
                   tags=[tag.SERIAL, tag.FAST])

These tags may then be used to filter the tests you wish to run on the command
line with carptests:

.. code-block:: bash

    carptests devtests.mechanics --tag fast
"""

import sys
from .exception import CARPTestError

class Tag(object):
    """
    Class to represent tags, for more descriptive printing than simple ints.

    Parameters
    ----------
    name : str
        Short identifying name for the tag, which will be treated as case
        insensitive
    description : str, optional
        Longer descriptive name for the tag to be used in documentation
    duration : int, optional
        When the tag corresponds to an expected simulation duration, this
        specifys the maximum duration in seconds
    """

    _next_tagid = 0

    def __init__(self, name, description=None, duration=None):

        self.name = name.upper()
        self.description = None if description is None else str(description)
        self.runtime = None if duration is None else int(duration)

        # Get tagid and increment counter
        self._tagid = self.__class__._next_tagid
        self.__class__._next_tagid += 1
        self._tests = []

    def __eq__(self, other):
        return self._tagid == other._tagid

    def __str__(self):
        return self.name

    def __repr__(self):
        extra = ''
        if self.description is not None:
            extra = ', \'{}\''.format(self.description)
        return 'Tag(\'{}\'{})'.format(self.name, extra)

    def register(self, test):
        self._tests.append(test)

    def tests(self):
        return self._tests

    def runtime_string(self, factor=1):

        if self.runtime is None:
            return None

        hours = 0
        minutes = 0
        seconds = int(self.runtime * factor)

        while seconds >= 60:
            minutes += 1
            seconds -= 60

        while minutes >= 60:
            hours += 1
            minutes -= 60

        return '{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds)

class TestTags(object):
    """
    Class to contain and control access to test tags.

    This class, when instantiated, makes a backup of the tags module, and then
    allows access to the contents of that module through the __getattr__ and
    __getitem__ functions. When an instance of this class is inserted in place
    of the tags module, this enables greater control of access to the module
    attributes, in particular allowing case-insensitive access and convenient
    access with strings using the [] syntax.
    """

    EXCEPTION = CARPTestError

    def __init__(self):
        self._all_tags = []

    def get(self, name):

        # Try to retrieve tag
        for tag in self._all_tags:
            if name.upper() == tag.name:
                return tag

        # Does not exist!
        cls = self.EXCEPTION
        tpl = 'tried to access non-existent tag {0}'
        raise cls(tpl.format(name.upper()))

    def add(self, name, *args, **kwargs):

        # Make sure it doesn't already exist
        try:
            return self.get(name)

        except self.EXCEPTION:
            tag = Tag(name, *args, **kwargs)
            self._all_tags.append(tag)
            return tag

    def __getattr__(self, attr):
        try:
            return object.__getattribute__(self, attr)
        except:
            return self.get(attr)

    def __getitem__(self, key):
        return getattr(self, key)

    def __iter__(self):
        for tag in self._all_tags:
            yield tag

# Default test tags
tags = TestTags()

# A few tricks to keep Sphinx happy
tags.__file__ = __file__
tags.__loader__ = None
tags.__doc__ = __doc__
tags.__all__ = []

# Est. runtime tags
tags.add('FAST', 'Runtime less than 30 seconds.', duration=30)
tags.add('SHORT', 'Runtime less than 1 minute.', duration=60)
tags.add('MEDIUM', 'Runtime less than 20 minutes.', duration=1200)
tags.add('LONG', 'Runtime greater than 20 minutes.')

# Processing
tags.add('SERIAL', 'Serial executed job.')
tags.add('PARALLEL', 'Parallel executed job.')
tags.add('GPU', 'GPU executed job.')

# Simulation type tags
tags.add('MONODOMAIN', 'Tests monodomain EP functionality.')
tags.add('BIDOMAIN', 'Tests bidomain EP functionality.')
tags.add('PSEUDO_BIDOMAIN', 'Tests pseudo_bidomain EP functionality.')
tags.add('EIKONAL', 'Tests eikonal EP functionality.')
tags.add('PURKINJE', 'Tests purkinje EP functionality.')
tags.add('MECHANICS', 'Tests mechanics functionality.')

# Misc tags
tags.add('FEMLIB', 'Tests a FEMLIB tool.')
tags.add('IO', 'Tests openCARP I/O routines.')
tags.add('ANALYTIC', 'Compare simulation with analytic result.')
tags.add('POSTPROCESS', 'Tests postprocessing routines.')
tags.add('OPENSRC', 'Example supported by the community edition.')

# Replace this module with the tags object
sys.modules[__name__] = tags
