#!/usr/bin/env python3
import copy
# ---( system imports )--------------------------
import os
import sys
import json
import fnmatch
from datetime import date
# ---( carputils imports )-----------------------
from carputils import tools
from carputils import settings
from carputils import restitute as res
from carputils.forcepss import j2carp as j2c
from carputils.forcepss import cell_limcyc as clc


EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle experiment'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)

CLEAN_PATTERN = r'^(\d{4}-)|(_restitute)|(_ep_init)|(trace_header)|(Trace)'


def argument_parser():
    parser = tools.standard_parser()

    exp = parser.add_argument_group('experiment specific options')
    exp.add_argument('--plan', type=str, required=True, default='plan.json',
                     help='Macro parameter experiment description in .json format.')
    exp.add_argument('--protected-funcs', type=str, nargs='+', default=None,
                     help='List of functions not to process.')
    exp.add_argument('--list-funcs', action='store_true',
                     help='List functions on experiment description and exit.')
    exp.add_argument('--update', action='store_true',
                     help='Update ep initial state in experiment description.')

    # set up limit cycle protocol
    lc = parser.add_argument_group('limit cycle protocol')
    lc.add_argument('--bcl', type=float, default=None,
                    help='Basic cycle length for limit cycle experiment in [ms].')
    lc.add_argument('--cycles', type=int, default=None,
                    help='Number of cycles to approximated stable limit cycle.')
    lc.add_argument('--verify', action='store_true',
                    help='Verify limit cycle by comparing state variable traces after cycles offset by 1 cycle.')

    # APD restitution settings
    apd_res = parser.add_argument_group('action potential duration restitution options')
    apd_res.add_argument('--restitute', action='store_true',
                         help='Compute restitution curves from limit cycle state.')
    apd_res.add_argument('--plot-apd-restitution',  action='store_true', help='Plot APD restitution curves')

    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    exp_str = 'restitute' if args.restitute else 'ep_init'
    today = date.today()
    out_DIR = '{}_{}'.format(today.isoformat(), exp_str)
    return out_DIR


def visualize_limit_cycle(job, exp, experiment, funcs_ignored, verify):
    # limit cycle visualization
    print('Visualize limit cycle experiments')
    print('=' * 88 + '\n\n')
    funcs = exp.get(j2c.FUNCTIONS)
    for name, func in funcs.items():
        # only look at functions in func_list
        if funcs_ignored is not None and name in funcs_ignored:
            continue
        ep = func.get(j2c.FUNCTION_EP, None)
        if ep is None:
            print('\nNo EP defined for function {} !\n'.format(name))
            continue

        print('\nVisualize {} limit cycle experiment.'.format(name))
        print('-' * 88 + '\n\n')
        if not verify:
            # create binary state variable trace file
            b2h5 = [settings.execs.SV2H5B,
                    '{}/{}_limit_cycle_header.txt'.format(job.ID, name),
                    '{}/{}.h5'.format(job.ID, name)]
            job.bash(b2h5)

            # call visualization
            vh5 = [settings.execs.LIMPETGUI,
                   '{}/{}.h5'.format(job.ID, name)]
            job.bash(vh5)
        else:
            # create binary state variable trace files for both restarts
            b2h5_p0 = [settings.execs.SV2H5B,
                       '{}/{}_limit_cycle_p0_restart_header.txt'.format(job.ID, name),
                       '{}/{}.p0.h5'.format(job.ID, name)]
            job.bash(b2h5_p0)

            b2h5_p1 = [settings.execs.SV2H5B,
                       '{}/{}_limit_cycle_p1_restart_header.txt'.format(job.ID, name),
                       '{}/{}.p1.h5'.format(job.ID, name)]
            job.bash(b2h5_p1)

            # call visualization
            vh5_p0_vs_p1 = [settings.execs.LIMPETGUI,
                            '{}/{}.p0.h5'.format(job.ID, name),
                            '{}/{}.p1.h5'.format(job.ID, name)]
            job.bash(vh5_p0_vs_p1)


def get_matching_names(pattern, name_list):
    matching_names = list()
    for name in name_list:
        if fnmatch.fnmatch(name, pattern):
            matching_names.append(name)
    return matching_names


@tools.carpexample(argument_parser, jobID, clean_pattern=CLEAN_PATTERN)
def run(args, job):
    # read experiment description
    exp = j2c.load_simulation_plan(args.plan, verbose=True)
    if exp is None or j2c.FUNCTIONS not in exp:
        print('No functions defined in the experiment!')
        sys.exit(0)

    funcs = exp.get(j2c.FUNCTIONS)
    func_names = list(funcs.keys())
    if args.list_funcs:
        yesno = lambda b: 'yes' if b else 'no'
        header = 'Functions in "{}":'.format(args.plan)
        print('\n' + header + '\n' + '=' * len(header) + '\n')
        max_len = max([len(name)+5 for name in func_names])
        fmt_base = '   {{: <{}}} CV: {{: >3}}; EP: {{: >3}}'.format(max_len)
        for name, func in funcs.items():
            has_CV = j2c.FUNCTION_CV in func
            has_EP = j2c.FUNCTION_EP in func and func[j2c.FUNCTION_EP] is not None
            if has_EP:
                init_EP = func[j2c.FUNCTION_EP].get(j2c.FUNCTION_EP_INIT)
                init_exists = init_EP is not None and os.path.exists(init_EP)
                fmt = fmt_base + ', init: "{}" (exists: {})'
                print(fmt.format('"{}":'.format(name), yesno(has_CV), yesno(has_EP), init_EP, yesno(init_exists)))
            else:
                fmt = fmt_base
                print(fmt.format('"{}":'.format(name), yesno(has_CV), yesno(has_EP)))
        print()
        sys.exit(0)

    # find ignored functions
    funcs_ignored = set()
    if args.protected_funcs is not None:
        for protected_func in args.protected_funcs:
            if protected_func.startswith('~'):
                matches = get_matching_names(protected_func[1:], func_names)
                funcs_ignored = funcs_ignored.difference(matches)
            else:
                matches = get_matching_names(protected_func, func_names)
                funcs_ignored = funcs_ignored.union(matches)
    if not len(funcs_ignored) > 0:
        funcs_ignored = None
    else:
        header = 'Ignored functions:'
        print('\n' + header + '\n' + '=' * len(header))
        print('\n'.join(funcs_ignored)+'\n')


    # store all limit cycle initial state vectors in one place
    tuning_dir = exp.get(j2c.j2cKey.TUNING_DIR, j2c.DEFAULT_TUNING_DIR)
    lim_cyc_dir = j2c.get_init_dir(tuning_dir, args.update)

    # make sure limit cycle directory exists
    if not os.path.isdir(lim_cyc_dir):
        os.mkdir(lim_cyc_dir)

    if not args.restitute:
        n_acts, exp = clc.ep_limit_cycle(job, exp, args.bcl, args.cycles, lim_cyc_dir, funcs_ignored, args.visualize, args.verify)
        if args.verify:
            args.visualize = True
            print('Verify limit cycle stabilization by comparing state variable traces after --cycles, offset by one cycle.')
            clc.ep_limit_cycle(job, exp, args.bcl, args.cycles, lim_cyc_dir, funcs_ignored, args.visualize, args.verify)

    else:
        n_acts, exp, res_files, res_labels, res_beats = clc.ep_apd_restitution(job, exp, args.bcl, args.cycles,
                                                                               lim_cyc_dir, funcs_ignored,
                                                                               args.visualize, args.dry)

        if args.plot_apd_restitution:
            # restitution experiment
            if args.dry:
                res_file_str = ' '.join(res_files)
                res_label_str = ' '.join(res_labels)
                res_beat_str = ' '.join(res_beats)
                print('Show APD restitution curves')
                print('-'*88)
                fmt = 'plotAPDrestitution {} --labels {} --slope\n'
                print(fmt.format(res_file_str, res_label_str))
                fmt = 'plotAPDrestitution {} --labels {} --beats {}\n'
                print(fmt.format(res_file_str, res_label_str, res_beat_str))
            else:
                # plot restitution curve
                res.plotAPDrestitution(res_files, res_labels, plt_slope=True, plt_pcl=True)
                # plot restituted APs
                res.plotRestitutionAPs(res_files, res_labels, res_beats)

    if n_acts > 0:
        clc.print_init_summary(exp.get(j2c.FUNCTIONS), args.cycles)
        if args.update:
            j2c.save_simulation_plan(exp, args.plan, verbose=True)

    if args.visualize:
        if not args.restitute:
            # limit cycle experiment
            visualize_limit_cycle(job, exp, args.plan, funcs_ignored, args.verify)


if __name__ == '__main__':
    run()
