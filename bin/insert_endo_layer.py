#!/usr/bin/env python3

import os
import sys
import subprocess
import numpy

import carputils.tools

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import carputils.cext.mshwrite as mshwrite
import carputils.cext.mshread as mshread
import carputils.cext.mshdata as mshdata

from carputils.carpio import msh_stats

class TagHandler:
    """
    simple class to handle element-tag data
    """
    def __init__(self, filename):
        self.__file_name = str(filename)
        self.__tag_map = TagHandler.__from_file(self.__file_name)

    @property
    def filename(self):
        return self.__file_name

    def add(self, label, tags):
        """
        add tags with a label

        Parameters:
        -----------
            label : str
                label of the tags to add
            tags : int, list, tuple
                tags to add, order is not preserved
        """
        label = str(label)
        if isinstance(tags, (list, tuple)):
            self.__tag_map[label] = tuple(sorted(set(map(int, tags))))
        elif isinstance(tags, int):
            self.__tag_map[label] = int(tags)
        else:
            raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))

    def copy(self):
        return self


    def clean(self, labels):

        if not isinstance(labels, (list,tuple)):
            raise TypeError('Provided labels must be a list!')

        for label in labels:
            self.__tag_map.pop(label)
        return

    def concatenate(self, *labels):

        tag_list = list()
        for label in labels:
            tags = self.__tag_map.get(label, None)
            if tags is None:
                continue
            if isinstance(tags, (list, tuple)):
                tag_list += tags
            elif isinstance(tags, int):
                tag_list += [tags]
            else:
                raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))
        return tuple(sorted(set(tag_list)))

    def get(self, label):
        return self.__tag_map.get(label, None)

    def get_as_tuple(self, label):
        tags = self.__tag_map.get(label, None)
        if tags is None:
            return tuple()
        elif isinstance(tags, int):
            return tags,
        elif isinstance(tags, (list, tuple)):
            return tuple(tags)
        else:
            raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))
        return tuple()

    def get_as_string(self, label):
        tags = self.__tag_map.get(label, None)
        if tags is None:
            return ''
        elif isinstance(tags, int):
            return str(tags)
        elif isinstance(tags, (list, tuple)):
            return ','.join(map(str, tags))
        else:
            raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))
        return str()

    @staticmethod
    def __from_file(file_name):
        with open(file_name, 'r') as fp:
            lines = fp.read().splitlines()
        return TagHandler.__from_string(lines)

    @staticmethod
    def __from_string(lines):
        tag_map = dict()
        # read first line
        for line_count, line in enumerate(lines):
            # trim string
            line = line.strip()
            # if empty line continue with next line
            if not len(line) > 0:
                continue
            idx = line.find('#')
            # if comment continue with next line
            if idx == 0:
                continue
            # remove comment from line
            if idx != -1:
                line = line[:idx]
            # split at '=' symbol and trim strings
            tag_def = tuple(map(lambda s: s.strip(), line.split('=')))
            if len(tag_def) == 2:
                (label, tag_string) = tag_def
                if len(label) > 0 and len(tag_string) > 0:
                    tags = tag_string.split(',')
                    if len(tags) > 1:
                        tag_map[label] = tuple(sorted(set(map(int, tags))))
                    else:
                        tag_map[label] = int(tags[0])

                elif len(label) > 0 and len(tag_string) == 0:
                    print('Warning, empty tags in line {}, "{}"!'.format(line_count, line))
                    tag_map[label] = None
                else:
                    print('Warning, empty label in line {}, "{}"!'.format(line_count, line))
            else:
                print('Warning, failed to parse line {}, "{}"!'.format(line_count, line))

        return tag_map

    def write_to(self, file_name):
        fp = open(file_name, 'w')
        for label, tags in self.__tag_map.items():
            if tags is None:
                fp.write('{}=\n'.format(label))
            elif isinstance(tags, int):
                fp.write('{}={}\n'.format(label, tags))
            elif isinstance(tags, (list, tuple)):
                fp.write('{}={}\n'.format(label, ','.join(map(str, tags))))
            else:
                fp.close()
                raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))

    def write(self):
        self.write_to(self.__file_name)

    def __str__(self):
        str_rep = ''
        for label, tags in self.__tag_map.items():
            if tags is None:
                str_rep += '{} =\n'.format(label)
            elif isinstance(tags, int):
                tag_string = str(tags)
                str_rep += '{} = {}\n'.format(label, tag_string)
            elif isinstance(tags, (list, tuple)):
                tag_string = ','.join(map(str, tags))
                str_rep += '{} = {}\n'.format(label, tag_string)
            else:
                raise TypeError('Warning, unexpected type "{}"!'.format(type(tags)))

        return str_rep

    def keys(self):
        return self.__tag_map.keys()



def argument_parser():

    parser = carputils.tools.basic_parser()

    io_opts = parser.add_argument_group(':::::::::: IO Options :::::::::')
    io_opts.add_argument('--mesh',
                         type=str,
                         required=True,
                         help='Name of the mesh. ')

    vis_opts = parser.add_argument_group(':::::::::: Visualization Options :::::::::')
    vis_opts.add_argument('--visualize',
                         action='store_true',
                         help='Visualize the endocardial layer in the mesh with studio.')

    endo_opts = parser.add_argument_group(':::::::::: Endocardial Layer Options :::::::::')
    endo_opts.add_argument('--rv-apba-min-max',
                         type=float,
                         default=[0.3,0.9],
                         nargs=2,
                         help='Apico-basal extent of the endocardial layer in the RV. Default: [0.3,0.9]')

    endo_opts.add_argument('--lv-apba-min-max',
                         type=float,
                         default=[0.3,0.9],
                         nargs=2,
                         help='Apico-basal extent of the endocardial layer in the LV. Default: [0.3,0.9]')

    endo_opts.add_argument('--rv-tag',
                         type=int,
                         default=800,
                         help='Tag to be used for the endocardial layer in the RV. Default: 800')

    endo_opts.add_argument('--lv-tag',
                         type=int,
                         default=600,
                         help='Tag to be used for the endocardial layer in the LV. Default: 600')

    args, unknown = parser.parse_known_args()

    return parser

def jobID(args):
    if args.ID is None:
        args.ID = f'insert_endo_layer_{os.path.basename(args.mesh)}'
    return args.ID

def check_cutoff_regions(rv_apba_min_max, lv_apba_min_max):
    """
    Check the rho and apba bounds of the input arguments for the LV and the RV.

    Parameters:
    -----------
        args: str
            Carputils argument parser consisting of bound args.
    """

    if not 0 <= rv_apba_min_max[0] <= rv_apba_min_max[0] <= 1.0:
        print('Unrealistic values applied for RV apba bound values. Expected bounds: [0, 1.0]')
        sys.exit()

    if not 0 <= lv_apba_min_max[0] <= lv_apba_min_max[0] <= 1.0:
        print('Unrealistic values applied for LV apba bound values. Expected bounds: [0, 1.0]')
        sys.exit()
        
        
def check_binary_mesh(job,meshname):

    bmesh=True
    print(meshname[-1])
    if meshname[-1] == '.':
        meshname = meshname[:-1]

    for x in ('.belem', '.bpts','.blon'):
        if not os.path.exists(meshname + x):
            print(meshname + x)
            bmesh=False

        if not bmesh:
            print('Binary mesh does not exist. Converting..')
            cmd=[carputils.settings.execs.MESHTOOL,'convert',f'-imsh={meshname}.elem', f'-omsh={meshname}.belem']
            job.bash(cmd)

    return os.path.abspath(meshname)

@carputils.tools.basicexample(argument_parser, jobID )
def run(args,job):

    check_binary_mesh(job, args.mesh)

    generate(job, args.mesh, args.ID,
             lv_tag=args.lv_tag, rv_tag=args.rv_tag,
             rv_apba_min_max=args.rv_apba_min_max, lv_apba_min_max=args.lv_apba_min_max,
             visualize=args.visualize, dry=args.dry)

def get_tag_split(meshname):

    query_cmd = [str(carputils.settings.execs.MESHTOOL), 'query', 'tags', f'-msh={meshname}']

    print(' '.join(query_cmd))

    output = subprocess.run(query_cmd, capture_output=True).stdout

    str_myo_list = str(output).split('Myocardium tags:')[1].split('Bath tags:')[0].replace('\\n','').split(',')
    str_bath_list = str(output).split('Bath tags:')[1].split('Histograms:')[0].replace('\\n','').split(',')

    if len(str_myo_list) == 0:
        print('No tags in the intracellular domain. Check your fibers!')
        sys.exit()

    myo_tags = tuple(numpy.asarray(str_myo_list, dtype=int))

    if len(str_bath_list) != 0 and str_bath_list[0] != ' ':
        bath_tags = tuple(numpy.asarray(str_bath_list,dtype=int))
    else:
        bath_tags = None
    print(f'Myocardium tags: {myo_tags}')
    print(f'Bath tags: {bath_tags}')

    if numpy.isin(myo_tags, bath_tags).any() or numpy.isin(bath_tags, myo_tags).any():
            print('Some labels are defined in both the myocardium and bath. Please check!')
            print(f'Bath Tags: {bath_tags}\n Myo Tags: {myo_tags}')
            sys.exit()

    return myo_tags, bath_tags

def check_mesh(job, meshname):

    check_binary_mesh(job,meshname)

    uhc_pts = meshname + '.ucc'
    hpts_file = meshname + '.hpts'

    if not os.path.isfile(uhc_pts) and not os.path.isfile(hpts_file):
        print(f'Missing heart coordinate file: {uhc_pts}!')
        sys.exit()

    if os.path.isfile(uhc_pts) and not os.path.isfile(hpts_file):
        job.cp(uhc_pts, hpts_file)

    if os.path.isfile(hpts_file) and not os.path.isfile(uhc_pts):
        job.cp(hpts_file, uhc_pts)

    return hpts_file
def extract_endo_surfaces(job, mesh_name, endo_surf_filename):

    print('Generating Endocardial Surfaces')

    tags = None
    if not os.path.isfile(endo_surf_filename +'.lv.surf.vtx'):

        print(f'LV endo surface: {endo_surf_filename + ".lv.surf.vtx"}')

        tags = load_tags(mesh_name)

        if not 'T_LVBP' in tags.keys() or not 'T_LV' in tags.keys():
        #    raise AssertionError('LVBP and LV are not present in tags. Cannot generate LV endocardial surface!')
            if os.path.isfile(mesh_name + '.lvendo.surf.vtx'):
                job.cp(mesh_name + '.lvendo.surf.vtx', endo_surf_filename + '.lv.surf.vtx')
            else:
                raise AssertionError('LVBP and LV are not present in tags. Cannot generate LV endocardial surface! '
                                 'There is also not a file called lvendo.surf.vtx')
        else:
            cmd = [str(carputils.settings.execs.MESHTOOL), 'extract', 'surface', f'-msh={mesh_name}',
                   f'-surf={endo_surf_filename}.lv', f'-op={tags.get_as_string("T_LV")}:{tags.get_as_string("T_LVBP")}']
            job.bash(cmd)
    else:
        print(f'LVendo surface already generated: {endo_surf_filename + ".lv.surf.vtx"} ')

    if not os.path.isfile(endo_surf_filename +'.rv.surf.vtx'):

        print(f'LV endo surface: {endo_surf_filename + ".lv.surf.vtx"}')

        if tags is None:
            tags = load_tags(mesh_name)

        if not 'T_RVBP' in tags.keys() or not 'T_RV' in tags.keys():
            if os.path.isfile(mesh_name + '.rvendo.surf.vtx'):
                job.cp(mesh_name + '.rvendo.surf.vtx', endo_surf_filename + '.rv.surf.vtx')
            else:
                raise AssertionError('RVBP and LV are not present in tags. Cannot generate RV endocardial surface!'
                                     'There is also not a file called rvendo.surf.vtx')
        else:
            cmd = [str(carputils.settings.execs.MESHTOOL), 'extract', 'surface', f'-msh={mesh_name}',
                   f'-surf={endo_surf_filename}.rv', f'-op={tags.get_as_string("T_RV")},{tags.get_as_string("T_LV")}:{tags.get_as_string("T_RVBP")}']
            job.bash(cmd)
    else:
        print(f'RVendo surface already generated: {endo_surf_filename + ".rv.surf.vtx"} ')

    return tags

def load_tags(meshname, tags_list=None):

    tags = TagHandler(meshname + '.etags.sh')

    if tags_list:
            tags_2_clean = [x for x in tags.keys() if x not in tags_list]
            tags.clean(tags_2_clean)

            if len(tags.keys()) == 0:
                raise AssertionError('All tags removed from list! Please update the tags_list.')

    return tags

def generate(job, mesh_name, output_dir, rv_tag, lv_tag, rv_apba_min_max=[0, 1.0], lv_apba_min_max=[0, 1.0],
             visualize=False, dry=False):

    check_cutoff_regions(rv_apba_min_max, lv_apba_min_max)

    print('Loading the Mesh Information')
    print(f'Meshname: {mesh_name}')

    ucc_file = check_mesh(job, mesh_name)

    print(f'Output Directory: {output_dir}')
    if not os.path.exists(output_dir) and not dry:
        job.mkdir(output_dir, parents=True)

    print('Inserting Endocardial Region')
    print(f'LV: \n Tag: {lv_tag}\n apba: {lv_apba_min_max}\n')
    print(f'RV: \n Tag: {rv_tag}\n apba: {rv_apba_min_max}\n')

    rv_tpl = f'RV{rv_tag}_z{rv_apba_min_max[0]}_{rv_apba_min_max[1]}'
    lv_tpl = f'LV{lv_tag}_z{lv_apba_min_max[0]}_{lv_apba_min_max[1]}'

    endo_region_tag_basename = os.path.join(output_dir, f'endo_')
    endo_tags_filename = endo_region_tag_basename + f'{rv_tpl}_{lv_tpl}'
    endo_surf_filename = os.path.join(output_dir, os.path.basename(mesh_name) + '.endo')

    tags = extract_endo_surfaces(job, mesh_name, endo_surf_filename)

    ucc_points = None
    msh_tags = None
    new_msh_tags = None

    print(f'Loading surface vtx data for LV: {endo_region_tag_basename + lv_tpl + ".vtx"}')
    if not os.path.isfile(endo_region_tag_basename + lv_tpl + '.vtx') and not dry:
        ucc_points = numpy.loadtxt(ucc_file, skiprows=1)

        lv_endo_vtx_data = mshread.read_vertex_data(endo_surf_filename + '.lv.surf.vtx')[0]
        lv_endo_vtx_data = numpy.asarray(lv_endo_vtx_data, dtype=numpy.int32)

        restrict_ucc_vtx_data( lv_apba_min_max,
                                         ucc_points[:,0],
                                         endo_region_tag_basename + lv_tpl,
                                         lv_endo_vtx_data)

    print(f'Loading surface vtx data for RV: {endo_region_tag_basename + rv_tpl + ".vtx"}')
    if not os.path.isfile(endo_region_tag_basename + rv_tpl + '.vtx') and not dry:

        if ucc_points is None:
            ucc_points = numpy.loadtxt(ucc_file, skiprows=1)

        rv_endo_vtx_data = mshread.read_vertex_data(endo_surf_filename + '.rv.surf.vtx')[0]
        rv_endo_vtx_data = numpy.asarray(rv_endo_vtx_data, dtype=numpy.int32)

        restrict_ucc_vtx_data(rv_apba_min_max,
                                    ucc_points[:,0],
                                    endo_region_tag_basename + rv_tpl,
                                    rv_endo_vtx_data)

    regions = [['lv_endo', endo_region_tag_basename + lv_tpl + '.vtx', lv_tag],
            ['rv_endo', endo_region_tag_basename + rv_tpl + '.vtx', rv_tag]]

    region_tag_files = generate_tagging_files(job, mesh_name, regions, output_dir)
    assert len(region_tag_files) == 2

    elm_fem = mshread.read_elements_bin(mesh_name + '.belem')
    num_elem = elm_fem["num"]
    
    print(f'Number of elements in mesh: {num_elem}')

    if not dry and not os.path.isfile(endo_tags_filename + '.tag'):
        print(f'Writing merged endocardial tags file: {endo_tags_filename}.tag')
        print(f'Region files: {region_tag_files}')
        mshdata.merge_element_tags(region_tag_files[0], region_tag_files[1], num_elem, endo_tags_filename + '.tag')#,fmt='belem')
    else:
        print(f'Merged endocardial tags file generated: {endo_tags_filename + ".tag"}')

    #Write the COMPLETE MERGED FILE
    print('Merging the endo tags into the mesh tags!')
    msh_endo_tags_filename = endo_tags_filename + f'.{os.path.basename(mesh_name)}.tag'

    if not os.path.isfile(msh_endo_tags_filename):

        print(f'Loading original tags from element file: {mesh_name + ".belem"}')
        if not dry:
            msh_elem_info = mshread.read_elements_bin(mesh_name + '.belem')
            msh_tags = msh_elem_info['tag']

        print(f'Loading merged endocardial tags file: {endo_tags_filename + ".tag"}')
        if not dry:
            endo_tags = mshread.read_scalar_data_int(endo_tags_filename + '.tag', num_elem)#[1::]

            endo_ind = numpy.where(endo_tags != 0)
            new_msh_tags = msh_tags.copy()
            new_msh_tags[endo_ind] = endo_tags[endo_ind]

        print('Cleaning up any endocardial tags that may have been interpolated into the bath..')
        myo_labels, bath_labels = get_tag_split(mesh_name)

        if not dry:
            for key in bath_labels:
                ind = numpy.concatenate([numpy.where(msh_tags == key)[0]])
                new_msh_tags[ind] = msh_tags[ind]

        print(f'Writing complete mesh tags file: {msh_endo_tags_filename}')
        if not dry:
            mshwrite.write_scalar_data_int(msh_endo_tags_filename, numpy.asarray(new_msh_tags, dtype=numpy.int32))
            mshdata.element_tag2btag(msh_endo_tags_filename,num_elem,msh_endo_tags_filename.replace('.tag','.btag'))

    else:
        print(f'Complete mesh tags file already generated: {msh_endo_tags_filename}')

    ofile = os.path.join(output_dir, os.path.basename(mesh_name)+ '.endo')
    print('Generating endocardial surface mesh..')
    cmd = [str(carputils.settings.execs.MESHTOOL), 'extract', 'surface', f'-msh={mesh_name}','-ofmt=carp_txt',
           f'-surf={ofile}', f'-tag_file={msh_endo_tags_filename}',
           f'-op={lv_tag},{rv_tag}']

    job.bash(cmd)


    if visualize and os.path.isfile(msh_endo_tags_filename+ '.tag'):
        print('Visualization not yet supported!')
    #   exit(1)

    #    vis_cmd = ['studio', mesh_name, msh_endo_tags_filename]
    #    job.bash(vis_cmd)
    assert os.path.isfile(msh_endo_tags_filename)

    return msh_endo_tags_filename.replace('.tag','')


def restrict_ucc_vtx_data(bounds, ucc_data, filename, vtx_data):
    """
    Restrict vtx_data list according to bounds in certain ucc_data.

    Parameters:
    -----------
        bounds : 2-element array
            Bounds of the ucc coordinate data. For example, [0,0.3]
        ucc_data : data array (N,1)
            Single coordinate data array. Vtx data must correspond to this ucc_data array.
        filename : str
            Output filename returned with ending '.vtx'
        vtx_data : data array (M,1) s.t. M in N
            array of indices to perform the bounding on.
            
    Returns:
    --------
        Writes restricted vertex data to filenmae +'.vtx'
    """

    vtx_ucc_data = ucc_data.take(vtx_data)
    vtx_ucc_data_min = vtx_ucc_data.min()
    vtx_ucc_data_max = vtx_ucc_data.max()
    vtx_ucc_data = (vtx_ucc_data-vtx_ucc_data_min)/(vtx_ucc_data_max-vtx_ucc_data_min)

    vtx_ucc_idx = numpy.where(numpy.logical_and(bounds[0] < vtx_ucc_data, bounds[1] > vtx_ucc_data))[0]
    vtx_ucc_idx_data = vtx_data.take(vtx_ucc_idx)

    mshwrite.write_vertex_data(filename +'.vtx', vtx_ucc_idx_data)

    return

def generate_tagging_files(job, mesh_name, regions, output_dir):

    """
    Generate the tagging files for the mesh from provided regions.

    Parameters:
    -----------
        job : carputils.job
            Job operator from carputils.
        mesh_name : str
            Full file pathname to the mesh
        regions : data array
            List of regions to be tagged within the Mesh. Each region has 3 entries: [label, vtx_filename, tag]
        output_dir: str
            Specify the directory where the files are written.

    Returns:
    --------
        Writes restricted vertex data to filenmae +'.vtx'
    """
    cmd = [str(carputils.settings.execs.MGTAGREG),
           '--model-name', mesh_name,
            #'--tag-format','binhdr',
           '--model-format','carp_bin']

    region_cmd = []
    tag_files = []
    for region in regions:
        assert len(region) == 3, 'Regions passed for tagging must contain 3 entries of information!'
        tag_file = f'{output_dir}/vtx_region_{region[0]}.tag'
        if not os.path.isfile(tag_file):
            region_cmd += ['--vtx-region', region[0], region[1], 0, int(region[2])]
        else:
            print(f'Region tagging file generated: {tag_file}')

        tag_files.append(tag_file)

    if region_cmd:
        cmd += region_cmd
        cmd += ['--out-format', f'{output_dir}/vtx_region_%L']

        job.bash(cmd)

    for tag_file in tag_files:
        assert os.path.exists(tag_file), f'Failure to create tag file: {tag_file}'

    return tag_files

if __name__ == '__main__':
    run()
