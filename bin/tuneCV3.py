#!/usr/bin/env python3

r"""
"""
import os, sys
import argparse
from datetime import date
from json import load, dump
import numpy as np
import re

from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh
from carputils import ep
from carputils.forcepss import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Orthotropic conduction velocity test in 2D/3D domain'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    parser._negative_number_matcher = re.compile(r'^-(\d+\.?|\d*\.\d+)([eE][+\-]?\d+)?$')

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    exp.add_argument('--domain',
                        type=str, nargs='+', default=argparse.SUPPRESS,
                        help='List of domains to be processed, ignoring any mutable flags')

    exp.add_argument('--sourceModel',
                        default=argparse.SUPPRESS,
                        help='pick type of source model')

    exp.add_argument('--propagation',
                        default='R-D',
                        choices=ep.PROP_TYPES,
                        help='pick propagation mode, either full R-D, R-E+, R-E-, '
                            +'or prescribe AP shape with trigger instant')

    exp.add_argument('--lf',
                        type=float, default=20.0,
                        help='slab length along (f)iber axis in [mm], default is: 20. mm' +
                             'Pick --lf 0. to collapse domain to 2D (s-n plane).' )

    exp.add_argument('--ls',
                        type=float, default=7.0,
                        help='slab length along (s)heet axis in [mm], default is: 7. mm' +
                             'Pick --ls 0. to collapse domain to 2D (f-n plane).' )

    exp.add_argument('--ln',
                        type=float, default=3.0,
                        help='slab length along sheet-(n)ormal axis in [mm], default is: 3. mm' +
                             'Pick --ln 0. to collapse domain to 2D (f-s plane).' )   
    exp.add_argument('--bf',
                        type=float, default=0.0,
                        help='add bath along (f)iber axis in [mm], default is: 0. mm' +
                             'Pick --bf > 0. to add bath to f+ face, or, <0 to add bath to both f+ and f- face.' ) 
    exp.add_argument('--bs',
                        type=float, default=0.0,
                        help='add bath along (s)heet axis in [mm], default is: 0. mm' +
                             'Pick --bs > 0. to add bath to s+ face, or, <0 to add bath to both s+ and s- face.' )    
    exp.add_argument('--bn',
                        type=float, default=0.0,
                        help='add bath along (n)ormal axis in [mm], default is: 0. mm' +
                             'Pick --bn > 0. to add bath to n+ face, or, <0 to add bath to both n+ and n- face.' ) 
    exp.add_argument('--d',
                        type=float, default=1.,
                        help='edge length of cube to be stimulated in [mm], default is: 1. mm' +
                             'Small choices may lead to failure to initiate propagation' )   

    exp.add_argument('--resolution',
                        type=float, default=argparse.SUPPRESS,
                        help='mesh resolution in [um], default is: 250. um' +
                             'if given, overrules any resolution settings  given in the --experiment file.' ) 

    exp.add_argument('--duration',
                        type=float, default=50.0,
                        help='duration of experiment, typically 25. for '
                            +'activation only, 400 to include repolarization')

    exp.add_argument('--stim-geometry',
                        default='block',
                        choices=['block', 'sphere', 'f', 's', 'n'],
                        help='pick geometry of stimulus region, size is set by --d '
                            +'default is block. '
                            +'(block): lower left corner, block size is --d.'
                            +'(sphere): lower left corner, sphere radius is --d.'
                            +'(f): entire s-n face of width --d, planar wavefront along f'
                            +'(s): entire f-n face of width --d, planar wavefront along s'
                            +'(n): entire f-s face of width --d, planar wavefront along n')

    return parser


# return mesh extension as function of run
def mesh_ext(rmode):

    if rmode == 'ek':
        ext = '_ek'
    else:
        ext = '_i'

    return ext


# translate geometry name to index
def geom_type_to_idx(rtype):

    if rtype == 'sphere':
        type_idx = 1
    elif rtype == 'block':
        type_idx = 2
    elif rtype == 'cylinder' or rtype == 'cyl':
        type_idx = 3
    elif rtype == 'elem_list':
        type_idx = 4
    elif rtype == 'vtx_list':
        type_idx = 5
    else:
        type_idx = None

    return type_idx


# setup different stimulus types
def setup_stimulus(geom, d, rtype):

    # lower left corner, tissue limits and resolution
    llc, lims = geom.tissue_limits()
    res = geom.resolution()

    # corners of slab
    p0 = llc - res/2

    # block types
    planar = ('f', 's', 'n')
    if rtype in planar:
        # stimulate entire s-n plane
        p1 = lims + res/2
        idx = planar.index(rtype)
        p1[idx] = p0[idx] + d + res
        rtype = 'block'
    else:
        p1 = p0 + [d, d, d] + res

    rd = d + res

    # change to um
    p0 *= 1000
    p1 *= 1000
    rd *= 1000

    # define electrode
    e0 = []
    e1 = []
    for i, x in enumerate(p0):
        e0 += ['-stim[0].elec.p0[{}]'.format(i), '{:.1f}'.format(p0[i])]
        e1 += ['-stim[0].elec.p1[{}]'.format(i), '{:.1f}'.format(p1[i])]
        
    g_type = ['-stim[0].elec.geom_type', geom_type_to_idx(rtype)]
    
    elec = []
    if rtype == 'block':
        elec += g_type + e0 + e1

    elif rtype == 'sphere':
        elec += g_type + e0 + ['-stim[0].elec.radius', rd]

    else:
        print('Unsupported geometry type {} for stimulus definition.'.format(rtype))

    prtcl = ['-stim[0].ptcl.duration', 2.00,
             '-stim[0].ptcl.start', 0.00]

    stmV = True 
    if stmV:
        pulse = ['-stim[0].pulse.strength', -40.0,
                 '-stim[0].crct.type', 9]

    else:
        pulse = ['-stim[0].pulse.strength', 50.0,
                 '-stim[0].crct.type', 0]

    numstim = 1
    stim = elec + pulse + prtcl

    return stim, numstim


# create a tissue slab, specify dimensions along f, s and n
# resolution specified by dx, domain_tag to be assigned to entire domain
# all lengths must be given in [mm]
def make_slab(lf, ls, ln, bf, bs, bn, dx, domain_tag, bath_tag):

    # check for collapsed domains
    slab_dims = 3
    if lf == 0.:
        lf = dx
        slab_dims -= 1
    if ls == 0.:
        ls = dx
        slab_dims -= 1
    if ln == 0.:
        ln = dx
        slab_dims -= 1

    print('\nSlab dimension is {}-D.'.format(slab_dims))

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(lf, ls, ln), centre=(0, 0, 0), resolution=dx)

    # add a bath?
    if abs(bf) > 0.0 or abs(bs) > 0.0 or abs(bn) > 0.0:
        geom.set_bath(thickness=(abs(bf), abs(bs), abs(bn)), both_sides=(bf<0. or bs<0. or bn<0.))
    
    # whole block is one region
    reg = mesh.BoxRegion((-lf/2,-ls/2,-ln/2), (lf/2, ls/2, ln/2), tag=domain_tag)
    geom.add_region(reg)
    
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    return meshname, geom


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_ortho_velocity_test'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(ortho_velocity_)')
def run(args, job):

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    # retrieve definition of all functional domains
    fncs = exps.get('functions')

    j2c.check_initial_state_vectors(fncs, True)

    # use consistent solver settings
    slv = exps.get('solver')

    # visualization
    vis = []
    if args.visualize:
        vis += ['-gridout_i', 3,
                '-spacedt', 1]

    # ---------- 2D/3D elliptic wavefront --------------------------------------------

    domains = exps.get('functions')

    for dkey, domain in domains.items():
 
        ep_ = domain.get('EP')
        cv_ = domain.get('CV')

        if ep_ is None or cv_ is None:
            print('\nNo EP or CV defined for domain {} !\n'.format(dkey))
            continue
       
        selected = False
        if 'domain' in args:
            if dkey in args.domain:
                print('domain {} given'.format(dkey))
                selected = True

        if not cv_.get('mutable') or not selected:
            continue

        # spatial test resolution
        if 'resolution' in args:
            dx = args.resolution
            print('cmd line r: {}'.format(dx))
        else:
            dx = cv_.get('dx')
            print('json r: {}'.format(dx))

        # build mesh of given resolution
        myo_tag = 1
        bth_tag = 0
        meshname, geom = make_slab(args.lf, args.ls, args.ln, args.bf, args.bs, args.bn, dx/1000, myo_tag, bth_tag)
        meshID = os.path.basename(meshname)

        # build tissue-level configuration 
        cnf = {'slab': {'tags': [myo_tag], 'func': dkey}, 'bath': {'tags': [bth_tag], 'func': 'bath'}}

        # check configuration
        if not j2c.check_config(cnf, fncs):
            sys.exit()

        # build simulation
        cmd = tools.carp_cmd()
        cmd += ['-simID', job.ID]
        cmd += ['-tend', args.duration]

        # add solver settings
        cmd += j2c.add_slv_settings(slv)         

        # overrule source model if given as argument
        if 'sourceModel' in args:
            print(f'Setting source Model from argument: {args.sourceModel}')
            cmd += ep.model_type_opts(args.sourceModel)

        cmd += vis
        cmd += ['-meshname', meshname]
        cmd += j2c.setup_tissue(cnf, fncs, args.propagation)

        # configure stim
        stim, numstim = setup_stimulus(geom, args.d, args.stim_geometry)
        prpOpts, numstim, ek_stim = ep.setupPropagation(args.propagation, numstim, [], '')
        if len(ek_stim):
            # convert to new format
            ek_stim = ['-stim[{}].crct.type'.format(numstim-1), 8]
        cmd += prpOpts + ['-num_stim', numstim] + stim + ek_stim

        # run
        job.carp(cmd)

        if args.visualize:
            pmode = j2c.propagation_mode(args.propagation)
            mesh = os.path.join(job.ID, meshID + mesh_ext(pmode))
            data = os.path.join(job.ID, 'vm.igb')
            view = './tunecv3.mshz'
            job.meshalyzer(mesh, data, view)


if __name__ == '__main__':
    run()
