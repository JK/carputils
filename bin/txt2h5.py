#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
Create a HDF5 repository file for the ionic models in LIMPET.
"""

epilog = """
(c) Bernardo M. Rocha <bernardomartinsrocha@gmail.com> and 
    Gernot Plank <gernot.plank@medunigraz.at> 
"""


import argparse
import sys, os
import resource
from   numpy import loadtxt, shape
from   tables import *
from   pkg_resources import parse_version, get_distribution

# Python2 - Python3 compatibility
if sys.version_info.major > 2:
    xrange = range

def parser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)

    # positional arguments
    parser.add_argument('headerfile',
                        help='Text file with header details on ionic model output.')
    parser.add_argument('datafile',
                        help='Data file with traces of ionic model output.')
    parser.add_argument('hdf5file',
                        help='Name of the hdf5 file to be created.')

    # optional arguments
    parser.add_argument('--whitelist',
                        nargs='*',
                        help='List of IM parameters to be included in the h5 file. \n'
                             'No need to specify the time column!\n'
                             'Example: --whitelist IK1 INa IKs')
    return parser


def read_sv_text_header(filename):
    """
    Reads a text file header provided by an ionic model author
    and extracts information about the states variables to create
    the HDF5 repository.
    """
    time_names = ('time', 'Time', 'T', 't')
    
    toks = loadtxt(filename, dtype=str)
    toks = list(toks)

    # standardize different time spellings
    if toks[0] in time_names:
        toks[0] = 't'

    # always assume that a time column is present
    # changing bench header output is not trivial
    if not 't' in toks:
        print('Note: Assuming first data column to be the timings!')
        toks.insert(0, 't')

    return toks

# end of read_sv_text_header()


def read_sv_text(sv_list, filename):
    """
    document this function
    """
    sv_data = loadtxt(filename)
    sv_shap = shape(sv_data)
    assert (sv_shap[1] == len(sv_list)), "SV in the header file does not match the columns the data file!"

    return sv_data

# end of read_sv_text()


def createTblDesc(sv_list):
    """
    Create and return an instance of Description class which will be used to
    describe the structure of the table for the hdf5 file.
    """
    # assuming float as a default data type
    dtype = 'float'
    
    colDict = {}
    for sv in sv_list:
        if   (dtype == 'float'):
            colDict[sv] = Float32Col()
        elif (dtype == 'double'):
            colDict[sv] = Float64Col()
    
    h5desc = Description(colDict)
    return h5desc   

# end of createTblDesc


def dump_text2HDF5(names, data, h5file, whitelist):
    """
    Function to create a repository based on a header created by the author
    """
    ver_pyTables = get_distribution("tables").version
    # need to get rid of name patterns like "sv->" as pyTables don't like them
    for i, name in enumerate(names):
        if 'sv->' in name:
            names[i] = name.lstrip('sv->')
    h5Desc = createTblDesc(names)

    # -----------------------------------------
    # changes between pyTables 2.x and pyTables 3.x
    #   openFile    -> open_file
    #   createGroup -> create_group
    #   createArray -> create_array
    # -----------------------------------------

    # save the data in the repository
    if parse_version(ver_pyTables) >= parse_version('3.0.0'):
        gwfile = open_file(h5file, mode = "w", title = "IM validation traces")
        group  = gwfile.create_group("/", 'protocol_1', 'protocol 1')
    else:
        gwfile = openFile(h5file, mode = "w", title = "IM validation traces")
        group  = gwfile.createGroup("/", 'protocol_1', 'protocol 1')

    
    for i in xrange(len(names)):
        vn = names[i]

        # apply filter if specified
        if whitelist is not None:
            if not vn in whitelist:
                continue

        if parse_version(ver_pyTables) >= parse_version('3.0.0'):
            gwfile.create_array(group, vn, data[:,i], "traces")
        else:
            gwfile.createArray( group, vn, data[:,i], "traces")
    
    gwfile.close()

# end of dump_text2HDF5()


def cpu_time():
    return resource.getrusage(resource.RUSAGE_SELF)[0]


def main():
    start = cpu_time()
    args  = parser().parse_args()

    if not os.path.isfile(args.headerfile):
        print("{} is not a valid file.\n".format(args.headerfile))
        sys.exit(-1)

    # time colum must always be an output
    whitelist = args.whitelist
    if whitelist is not None:
        whitelist.insert(0, 't')

    sv_list = read_sv_text_header(args.headerfile)
    sv_data = read_sv_text(sv_list, args.datafile)
   
    dump_text2HDF5(sv_list, sv_data, args.hdf5file, whitelist)
    print(" Created {}".format(args.hdf5file))
    print(" Total CPU time {0:.1f} sec".format(cpu_time()-start))
    return
    
if __name__ == "__main__":
    main()
