#!/usr/bin/env python3

# ---( system imports )--------------------------
import os
import sys
import json
import numpy
import fnmatch
import itertools
import scipy.stats as spstats
import matplotlib.pyplot as mplplot
import matplotlib.gridspec as mplgrid
import matplotlib.lines as mpllines
from datetime import date
# ---( carputils imports )-----------------------
from carputils import tools
from carputils import tuning as tune
from carputils.forcepss import j2carp as j2c
from carputils.forcepss import tissue_limcyc as tlc


EXAMPLE_DESCRIPTIVE_NAME = 'Tissue experiment velocity and conductivity tuning'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)

CLEAN_PATTERN = r'^(\d{4}-)|(imp_)|(tunecv_)'


def argument_parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp = parser.add_argument_group('Experiment selection and general options')
    exp.add_argument('--plan', type=str, required=True, default='plan.json',
                     help='Macro parameter experiment description in .json format.')
    exp.add_argument('--protected-funcs', type=str, nargs='+', default=None,
                     help='List of functions not to process.')
    exp.add_argument('--list-funcs', action='store_true',
                     help='List functions on experiment description and exit.')
    exp.add_argument('--update', action='store_true',
                     help='Update measured velocities and conductivity settings (if --converge) '
                          'in experiment description')

    # tuning stage arguments
    tune_args = parser.add_argument_group('Conduction velocity measurement and tuning options')
    tune_args.add_argument('--tune-mode', choices=('gi', 'gm'), default=None,
                           help='Select tuning strategy, change gi, gm or beta to obtain prescribed velocities.')
    tune_args.add_argument('--converge', action='store_true', default=False,
                           help='Enforce recomputation of conductivities to match given reference '
                                'conduction velocities. This overrules settings in the experiment '
                                'description to create a new set of conductivity values.')
    tune_args.add_argument('--ctol', type=float, default=1e-4,
                           help='Sets conduction velocity error limit for conduction velocity tuning.')
    tune_args.add_argument('--resolution', type=float, default=None,
                           help='mesh resolution(s) in [um], if given, overrules any resolution settings '
                                'given in the --plan file.')
    tune_args.add_argument('--tag', type=str, default='tune_cv', help='Use tag for named output')
    
    # CV restitution settings
    cv_res = parser.add_argument_group('Conduction velocity restitution options')
    cv_res.add_argument('--restitute-pcl', type=float, nargs='+', default=None,
                        help='List of pacing cycle length for measuring CV restitution')
    cv_res.add_argument('--plot-cv-restitution',  action='store_true', help='Plot CV restitution curve')

    # Spatial convergence testing
    dx_conv = parser.add_argument_group('Conduction velocity dependence on spatial resolution testing')
    dx_conv.add_argument('--dx-list', type=float, nargs='+', default=None,
                         help='List of spatial mesh resolutions in [um] to measure CV for '
                              'for all functional regions given in the --plan file.')
    dx_conv.add_argument('--plot-cv-dx',  action='store_true', help='Plot CV dependency on spatial resolution dx.')

    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


def get_matching_names(pattern, name_list):
    matching_names = list()
    for name in name_list:
        if fnmatch.fnmatch(name, pattern):
            matching_names.append(name)
    return matching_names


def get_funcs_ignored(protected_funcs, func_names, verbose=False):
    # find ignored functions
    funcs_ignored = set()
    if protected_funcs is not None:
        for protected_func in protected_funcs:
            if protected_func.startswith('~'):
                matches = get_matching_names(protected_func[1:], func_names)
                funcs_ignored = funcs_ignored.difference(matches)
            else:
                matches = get_matching_names(protected_func, func_names)
                funcs_ignored = funcs_ignored.union(matches)
    if not len(funcs_ignored) > 0:
        funcs_ignored = None
    elif verbose:
        header = 'Ignored functions:'
        print('\n' + header + '\n' + '=' * len(header))
        print('\n'.join(funcs_ignored)+'\n')

    return funcs_ignored


def converge_CVs(job, exp, args):
    if args.resolution is None and args.dx_list is None:
        raise RuntimeError('Either "--resolution" or "--dx-list" must be provided !')

    solver = exp[j2c.SOLVER]
    fixge = solver[j2c.SOLVER_FIXGE]

    cvg = args.converge
    ctol = args.ctol
    if args.tune_mode is not None:
        fixge = args.tune_mode == 'gi'
        solver[j2c.SOLVER_FIXGE] = fixge

    dxs = None
    if args.resolution is not None:
        dxs = [args.resolution]
    if args.dx_list is not None:
        dxs = args.dx_list

    if dxs is None:
        raise RuntimeError('Error, empty dx list !')

    cv_tune = None
    cv_tune_dxs = {}
    for dx in dxs:

        solver[j2c.SOLVER_DX] = dx

        pcls = args.restitute_pcl
        np = args.np
        visual = args.visualize
        dry = args.dry
        tag = args.tag
        carp_opts = args.CARP_opts
        ID = args.ID

        funcs = exp.get(j2c.FUNCTIONS)
        if funcs is None:
            print('No functions defined in the experiment!')
            sys.exit(0)

        func_names = list(funcs.keys())
        funcs_ignored = get_funcs_ignored(args.protected_funcs, func_names, verbose=True)

        # store all limit cycle initial state vectors in one place
        tmode = 'gi' if fixge else 'gm'
        lim_cyc_dir = j2c.get_init_dir(exp.get('tuning_base_dir', './init'), args.update)
        cv_tune = tlc.converge_CVs(job, exp, cvg, ctol, tmode, dx, pcls, lim_cyc_dir,
                                   funcs_ignored, tag, np, visual, ID, carp_opts, dry)
        cv_tune_dxs[dx] = cv_tune

        if not pcls:
            with open('cv_tune_all_dx_{}.json'.format(dx), 'w') as fp:
                json.dump(cv_tune, fp, indent=4)
        else:
            with open('cv_restitution_all_dx_{}.json'.format(dx), 'w') as fp:
                json.dump(cv_tune, fp, indent=4)

    with open('cv_tune_all_dxs.json', 'w') as fp:
        json.dump(cv_tune_dxs, fp, indent=4)

    if args.dx_list is not None:
        return cv_tune_dxs
    else:
        return cv_tune


def extract_CV_dx(cv_tune, extract_all=False):
    dx = list()
    cv = dict()
    for r, rkey in cv_tune.items():
        dx.append(float(r))
        for fnc, res in rkey.items():
            key = '{}'.format(fnc)
            if key not in cv:
                cv[key] = {'f': list(), 't': list(), 'n': list()}
            cv_f = res.get('f').get('results').get('vel')
            if extract_all or cv_f > 0.0 or len(cv[key]['f']) == 0 or cv[key]['f'][-1] > 0.0:
                cv[key]['f'].append(cv_f)
            cv_t = res.get('t').get('results').get('vel')
            if extract_all or cv_t > 0.0 or len(cv[key]['t']) == 0 or cv[key]['t'][-1] > 0.0:
                cv[key]['t'].append(cv_t)
            cv_n = res.get('n').get('results').get('vel')
            if extract_all or cv_n > 0.0 or len(cv[key]['n']) == 0 or cv[key]['n'][-1] > 0.0:
                cv[key]['n'].append(cv_n)

    return cv, dx


def print_CV_dx_table(cv, dx):

    # print header
    sep_str = '-'*80
    print('\n'*2 + sep_str)
    print('Spatial discretization effect on conduction velocity')
    print(sep_str + '\n'*2)

    max_conf_len = 6  # length of 'domain' string
    for conf in cv.keys():
        max_conf_len = max(max_conf_len, len(conf))

    spacing = ' ' * 3
    conf_lbl_tpl = '{{:<{}s}}  {{:^3s}}:'.format(max_conf_len)
    header = conf_lbl_tpl.format('domain', 'dir')
    for r in dx:
        header += spacing + '{:5.1f}'.format(r)
    header = header + spacing + '[um]'
    print(header)
    print('-'*(len(header)+1))

    for conf, CVs in cv.items():
        # cv along three directions of
        cv_f, cv_t, cv_n = CVs['f'], CVs['t'], CVs['n']
        exp_ID_f = conf_lbl_tpl.format(conf, '(f)')
        exp_ID_t = conf_lbl_tpl.format(conf, '(t)')
        exp_ID_n = conf_lbl_tpl.format(conf, '(n)')
        for i in range(len(dx)):
            exp_ID_f += spacing
            if i < len(cv_f):
                exp_ID_f += '{:5.3f}'.format(cv_f[i])
            else:
                exp_ID_f += '{:^5s}'.format('---')
            exp_ID_t += spacing
            if i < len(cv_t):
                exp_ID_t += '{:5.3f}'.format(cv_t[i])
            else:
                exp_ID_t += '{:^5s}'.format('---')
            exp_ID_n += spacing
            if i < len(cv_n):
                exp_ID_n += '{:5.3f}'.format(cv_n[i])
            else:
                exp_ID_n += '{:^5s}'.format('---')
        print(exp_ID_f + spacing + '[m/s]')
        print(exp_ID_t + spacing + '[m/s]')
        print(exp_ID_n + spacing + '[m/s]')

    print('\n'*2)


def plot_CV_dx(ax, cv, dx, **kwargs):
    label_fontsize = kwargs.get('label_fontsize', 10.0)
    title_fontsize = kwargs.get('title_fontsize', 12.0)
    legend_fontsize = kwargs.get('legend_fontsize', 10.0)

    prop_cycle = mplplot.rcParams['axes.prop_cycle']
    colors = itertools.cycle(prop_cycle.by_key()['color'])

    ax.set_title('CV(dx)', fontsize=title_fontsize)
    ax.set_xlabel('dx [um]', fontsize=label_fontsize)
    ax.set_ylabel('CV [m/s]', fontsize=label_fontsize)

    for delta in dx:
        ax.axvline(delta, color='#AAAAAA', alpha=0.25, linestyle='-.')

    yticks = [list(), list()]
    block_data, block_slopes, block_max_dx = list(), list(), 0.0
    for conf, CVs in cv.items():
        cv_f, cv_t, cv_n = CVs['f'], CVs['t'], CVs['n']
        ax.axhline(cv_f[0], color='#AAAAAA', alpha=0.25, linestyle='-')
        ax.axhline(cv_t[0], color='#AAAAAA', alpha=0.25, linestyle='--')
        ax.axhline(cv_n[0], color='#AAAAAA', alpha=0.25, linestyle=':')
        yticks[0] += [cv_f[0], cv_t[0], cv_n[0]]
        yticks[1] += map(lambda v: '{:1.3f}'.format(v), [cv_f[0], cv_t[0], cv_n[0]])

        col = next(colors)
        # plot CV along fiber direction
        if len(dx) >= len(cv_f):
            ax.plot(dx[:len(cv_f)], cv_f, color=col, linestyle='-', label=conf)
            if len(dx) > len(cv_f):
                rval = spstats.linregress(dx[:len(cv_f)], cv_f)
                block_slopes.append(rval.slope)
                block_max_dx = max(block_max_dx, dx[len(cv_f)-1])
                block_data += [(dx[i], cv) for i, cv in enumerate(cv_f)]

        # plot CV along traverse direction
        if len(dx) >= len(cv_t):
            ax.plot(dx[:len(cv_t)], cv_t, color=col, linestyle='--')
            if len(dx) > len(cv_t):
                rval = spstats.linregress(dx[:len(cv_t)], cv_t)
                block_slopes.append(rval.slope)
                block_max_dx = max(block_max_dx, dx[len(cv_t)-1])
                block_data += [(dx[i], cv) for i, cv in enumerate(cv_t)]

        # plot CV along normal direction
        if len(dx) >= len(cv_n):
            ax.plot(dx[:len(cv_n)], cv_n, color=col, linestyle=':')
            if len(dx) > len(cv_n):
                rval = spstats.linregress(dx[:len(cv_n)], cv_n)
                block_slopes.append(rval.slope)
                block_max_dx = max(block_max_dx, dx[len(cv_n)-1])
                block_data += [(dx[i], cv) for i, cv in enumerate(cv_n)]

    ax.set_xticks(dx)
    ax.set_yticks(yticks[0], labels=yticks[1])

    # block block area
    block_data = numpy.array(block_data, dtype=float)
    block_slope = numpy.average(block_slopes)
    block_offset = -block_slope*block_max_dx
    block_deltax, block_deltay = 0.0, 0.0
    for bdpt in block_data:
        deltax = bdpt[0]-(bdpt[1]-block_offset)/block_slope
        block_deltax = max(block_deltax, deltax)
        deltay = bdpt[1]-(block_slope*bdpt[0]+block_offset)
        block_deltay = max(block_deltay, deltay)
    block_xspace = numpy.linspace(dx[0], block_max_dx, num=10)
    block_yspace = block_slope*block_xspace+block_offset+3.0*block_deltay
    ax.fill_between(block_xspace, block_yspace, y2=0.0, color='#FF0000', alpha=0.05)
    ax.plot(block_xspace, block_yspace, color='#FF0000', alpha=0.1)
    ax.plot([dx[0], dx[0], block_max_dx, block_max_dx], [block_yspace[0], 0.0, 0.0, block_yspace[-1]],
            color='#FF0000', alpha=0.1)

    dir_lines = list()
    dir_lines.append(ax.plot([], [], color='#000000', linestyle='-')[0])
    dir_lines.append(ax.plot([], [], color='#000000', linestyle='--')[0])
    dir_lines.append(ax.plot([], [], color='#000000', linestyle=':')[0])
    legend1 = mplplot.legend(dir_lines, ["fiber", "sheet", "normal"], loc=4, fontsize=legend_fontsize)
    ax.add_artist(legend1)

    ax.legend(fontsize=legend_fontsize, loc=1)

    return ax


@tools.carpexample(argument_parser, jobID, clean_pattern=CLEAN_PATTERN)
def run(args, job):
    # read experiment description
    exp = j2c.load_simulation_plan(args.plan, verbose=True)
    if exp is None or j2c.FUNCTIONS not in exp:
        print('No functions defined in the experiment!')
        sys.exit(0)

    if j2c.SOLVER not in exp:
        exp[j2c.SOLVER] = j2c.DEFAULT_SOLVER.copy()

    funcs = exp.get(j2c.FUNCTIONS)
    func_names = list(funcs.keys())
    if args.list_funcs:
        yesno = lambda b: 'yes' if b else 'no'
        header = 'Functions in "{}":'.format(args.plan)
        print('\n'+header+'\n'+'='*len(header)+'\n')
        max_len = max([len(name)+5 for name in func_names])
        fmt_base = '   {{: <{}}} CV: {{: >3}}; EP: {{: >3}}'.format(max_len)
        for name, func in funcs.items():
            has_CV = j2c.FUNCTION_CV in func
            has_EP = j2c.FUNCTION_EP in func and func[j2c.FUNCTION_EP] is not None
            if has_EP:
                init_EP = func[j2c.FUNCTION_EP].get(j2c.FUNCTION_EP_INIT)
                init_exists = init_EP is not None and os.path.exists(init_EP)
                fmt = fmt_base + ', init: "{}" (exists: {})'
                print(fmt.format('"{}":'.format(name), yesno(has_CV), yesno(has_EP), init_EP, yesno(init_exists)))
            else:
                fmt = fmt_base
                print(fmt.format('"{}":'.format(name), yesno(has_CV), yesno(has_EP)))
        print()
        sys.exit(0)

    # carry out tissue initialization experiment
    # - measure CV
    # - tune CV
    # - measure CV restitution
    cv_tune = converge_CVs(job, exp, args)
    #print(json.dumps(cv_tune, indent=2))

    # tlc.print_restitute_results(cv_tune)

    # print results and update experiment settings
    if not args.dry:

        new_exp = None
        if args.restitute_pcl:
            # summarize CV restitution results
            tlc.print_restitute_results(cv_tune)
            # update tissue settings
            new_exp = tlc.update_exp_CVres(exp, cv_tune)

            if args.plot_cv_restitution:
                CV_res_files, CV_res_labels = tlc.get_cv_res_files(cv_tune)
                tune.plotCVRestitution(CV_res_files, CV_res_labels)

        elif args.dx_list:
            # Nothing to update here, only visualize CV(dx) results
            cv, dx = extract_CV_dx(cv_tune)
            print_CV_dx_table(cv, dx)

            if args.plot_cv_dx:
                fig = mplplot.figure(figsize=(6.0, 5.0))
                gs = mplgrid.GridSpec(1, 1)
                ax = fig.add_subplot(gs[0, 0])
                plot_CV_dx(ax, cv, dx)
                mplplot.show()

        else:
            # get solver options
            solver = exp[j2c.SOLVER]
            # summarize tuning results for all domains
            tlc.print_tune_cv_results(cv_tune, funcs, solver)
            # update tissue settings
            new_exp = tlc.update_exp_CV(exp, cv_tune)

        # update experiment description
        if new_exp is not None:
            tlc.update_exp_desc(args.update, args.plan, new_exp)


if __name__ == '__main__':
    run()
