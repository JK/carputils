#!/usr/bin/env python3

# ---( system imports )--------------------------
import os
import sys
import numpy
from datetime import date
# ---( carputils imports )-----------------------
import carputils
from carputils import tools
from carputils import settings
from carputils.forcepss import j2carp as j2c

EXAMPLE_DESCRIPTIVE_NAME = 'Translate json dictionaries into carp* command line'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>' \
                 'Karli Gillette <karli.gillette@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)


def argument_parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp = parser.add_argument_group('Experiment selection and general options')
    exp.add_argument('--plan', type=str, default=None,
                     help='Describe simulation plan in json format including configuration, experimental settings, '
                          'electrodes and protocols. Other settings provided through --config, --experiment, '
                          '--electrode* and --protocol* overrides the settings in plan file.')
    exp.add_argument('--purkinje-plan', type=str, default=None,
                     help='Describe purkinje plan in json format. ')
    exp.add_argument('--config', type=str, default=None,
                     help='Describe tissue model in terms of labels and function association in json format.')
    exp.add_argument('--functions', type=str, default=None,
                     help='Describe tissue functions associated with the config in json format.')
    exp.add_argument('--experiment', type=str, default=None,
                     help='Macro parameter experiment description in .json format.')
    exp.add_argument('--electrodes', type=str, default=None,
                     help='List of electrode descriptions in json format. '
                          'This is optional, electrodes may be listed also '
                          'in the "electrodes" section of the experiment description.')
    exp.add_argument('--electrode', type=str, nargs='+', default=None,
                     help='List of electrode names to be selected from electrode definitions.')
    exp.add_argument('--protocols', type=str, default=None,
                     help='List of protocol descriptions in json format. '
                          'This is optional, protocols may be listed also '
                          'in the "protocols" section of the experiment description.')
    exp.add_argument('--protocol', type=str, default='prepace',
                     help='Pick a specific pre-pacing protocol from protocol list'
                          'provided through --protocols, or within a "protocols" section'
                          'in --experiment.')
    exp.add_argument('--rmode', choices=('ek', 'rd', 're'), default='re',
                     help='Pick run mode')
    exp.add_argument('--cohort', type=str, default=None,
                     help='Directory storing cohort of all cases belonging to a given study')
    exp.add_argument('--case', type=str, default=None,
                     help='Pick a specific case from the cohort directory.')
    exp.add_argument('--mesh', type=str, default=None,
                     help='Path and basename of mesh to be used (/path_to_mesh_dir/mesh_base_name).')
    exp.add_argument('--network', type=str, default=None,
                     help='Path and basename of netweork to be used (/path_to_network_dir/network_name).pkje')
    exp.add_argument('--ucc', type=str, default=None,
                     help='Path to the universal ventricular coordinates file.')
    exp.add_argument('--leadfield-dir', type=str,
                     help='Specify an existing leadfield dir to look for leadfields. This is optional '
                          'and preference is given to leadfields which are specified within the plan.')
    exp.add_argument('--compute-lf-only', action='store_true',
                     help='Only compute the LFs and skip the actual simulation.')

    samp = parser.add_argument_group('Sampling setup options')
    samp.add_argument('--num-param-samples', default=2, type=int,
                      help='Number of parameter samples to take if protocol necessitates sampling. Default: 2')
    samp.add_argument('--sampling-method', default='lhs', choices=j2c.SAMPLING_METHODS, type=str,
                      help='Sampling schematic to use. Install SAlib and UncertainSCI using pip3 to gain access to '
                           'lhs, saltelli, and uq. Otherwise a sampling file in csv format is required.')
    samp.add_argument('--uq-pce-order', default=5, type=int,
                      help='PCE order for the uncertainity quantification. Must select sampling-method to be uq.')

    _, unknown_args = parser.parse_known_args()

    if len(unknown_args) > 0:
        print('\nA parser argument was provided that does not exist. Unknown arguments:')
        print('\n'.join(unknown_args))
        sys.exit()

    return parser


# ----- J2C -------------------------------------------------------

# get mesh
def get_mesh(mesh, cohort, case):
    mesh_name = None
    if mesh:
        mesh_name = mesh
    elif cohort and case:
        mesh_name = '{}/{}/{}'.format(cohort, case, case)
    else:
        print('\n\nNo mesh specified, use either mesh or cohort+case arguments.\n')
    return mesh_name


# setup mesh-related command options
def setup_mesh(mesh_name, split_file=None):
    cmd = ['-meshname', mesh_name]
    # use split information if available
    if split_file is not None:
        if os.path.exists(split_file):
            cmd += ['-use_splitting', 1,
                    '-splitname', split_file]
        else:
            print('Splitting selected, but split file "{}" does not exist!'.format(split_file))
            print('Proceed without splitting.')
    return cmd


def active_electrodes(elec_lst, elec):
    act_elecs = {}
    if elec_lst and elec:
        act_elecs = {e: elec_lst[e] for e in elec if e in elec_lst}
    else:
        print('No electrode definitions provided.')
    return act_elecs


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    args.ID = '{}_j2c'.format(today.isoformat())
    return args.ID


def leadfield_lookup(job, args, lfsetup, base_opts, run_num):

    """
    Check whether the leadfields for a given configuration exists and if not, compute them.

    Parameters:
    -----------
        job : module
            job module object returned from carputils.
        args : parser
            Argument parser object returned from carputils.
        lfsetup : lfsetup plan object
            Obect is created during sample.set_configuration or using plan[j2c.LFSETUP]
        base_opts : list
            array of carp commands
        k : run_num
            The current run number

    Returns:
    --------
        Lf_obj object and the new updated lfsetup when computed.

    """

    lf_recover_opts = []
    if lfsetup is not None:
        if settings.is_open_carp:
            print('Leadfield computation is not supported in openCARP..')
        else:
            lf_str = 'lf_run_{:06d}'.format(run_num)
            #Set the leadfield directory to either be within the current simulation or within the leadfield dir.
            if args.dry:
                output_dir = job.ID
            else:
                if args.leadfield_dir is None:
                    output_dir = os.path.join(job.ID, 'leadfields', lf_str )
                else:
                    output_dir = os.path.join(args.leadfield_dir, lf_str)
            #Set the configuration!
            lf_obj = j2c.LeadfieldComputationOptions(job, lfsetup, output_dir, dry=args.dry)

            if lf_obj.leadfield_dir is None:
                print('The leadfields do not exist !')
                lf_obj.leadfield_dir = output_dir
                leadfield_existance = lf_obj.leadfield_check(base_opts)
                if leadfield_existance is None:

                    ground_vtx_file = os.path.join(os.path.abspath(job.ID), 'ground.vtx')
                    compute_opts = lf_obj.computation_opts(base_opts, ground_vtx_file)
                    job.carp(compute_opts)
                    if args.dry:
                        lf_filename = os.path.join(output_dir, lf_str + '_lfcompute.par')
                        j2c.write_carp_par_file(lf_filename, compute_opts )

                    lf_obj.update_log(lf_obj.log_options(base_opts))

                else:
                    print('Leadfields exist, but are located in another folder. Updating the configuration!')

                lf_recover_opts = lf_obj.recover_opts()
                lfsetup = lf_obj.update_config()
            else:
                print('The leadfields for CARPentry have already been computed!')
                print('Leadfield Dir: {}'.format(lf_obj.leadfield_dir))
                lf_recover_opts = lf_obj.recover_opts()
    else:
        print('The lfsetup is None. Assuming that no lfsetup is desired..')

    return lf_recover_opts, lfsetup


@tools.carpexample(argument_parser, jobID)
def run(args, job):

    if settings.is_open_carp and args.rmode != 'rd':
        print('openCARP has no Eikonal support. Please use rd for the args.rmode. ')
        sys.exit()

    plan = j2c.load_simulation_plan(args.plan, confs_file=args.config, funcs_file=args.functions,
                                    elecs_file=args.electrode, simulations_file=args.protocols,
                                    update_confs=True, update_funcs=True, update_elecs=True,
                                    update_simulations=True)

    sampling_filename = os.path.join(job.ID, 'sampling.csv')
    sample = j2c.SweepSetup(plan, None, num_samples=args.num_param_samples,
                            sampling_method=args.sampling_method, pce_order=args.uq_pce_order,
                            ext_values_output=sampling_filename, write_files=True)

    # in case we have not any electrodes selected we assume all provide definitions are active

    # pick prepacing protocol from list
    #pp_ptcl = prtcls.get(args.protocol)

    # use basename of mesh as ID
    meshname = get_mesh(args.mesh, args.cohort, args.case)
    # get model format
    model_name, model_format, ucc_file = meshname, None, args.ucc
    if not model_name.endswith('.elem') and not model_name.endswith('.belem'):
        if os.path.exists(model_name+'.elem'):
            model_name += '.elem'
        elif os.path.exists(model_name+'.belem'):
            model_name += '.belem'
    if model_name.endswith('.elem') and os.path.exists(model_name):
        model_format = 'carp_txt'
    elif model_name.endswith('.belem') and os.path.exists(model_name):
        model_format = 'carp_bin'
    if model_format is None:
        print('Error, mesh "{}" not found!'.format(model_name))
        sys.exit(0)

    for k, param_map in enumerate(sample.param_map):
        run_suffix = 'run_{:06d}'.format(k)
        if args.dry:
            output_dir = args.ID
        else:
            output_dir = os.path.join(job.ID, run_suffix)

        if not os.path.exists(output_dir):
            os.system('mkdir -p {}'.format(output_dir))

        stimuli, elecs, cnf, fncs, slv, lfsetup, plan = sample.set_configuration(param_map)
        #j2c.print_settings_config(plan)

        act_elecs = active_electrodes(elecs, args.electrode)
        if not act_elecs:
            act_elecs = elecs

        cmd = tools.carp_cmd()
        cmd += ['-simID', output_dir]

        cmd += j2c.setup_timings(plan)
        # create split file
        split_file, split_active = j2c.generate_split_file(job, plan, meshname, force_recreation=False)
        
        # setup mesh
        cmd += setup_mesh(meshname, split_file)
        # set up tissue properties
        cmd += j2c.setup_tissue(cnf, fncs, args.rmode)
        # set up solver settings
        cmd += j2c.add_slv_settings(slv)
        # before running, check all specified initial states are there
        if not j2c.check_initial_state_vectors(fncs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, using default initial states!\n\n')
            sys.exit()

        #Compute the leadfields with the base_opts at this stage!
        lf_recover_opts, lfsetup = leadfield_lookup(job, args, lfsetup, cmd, k)
        plan[j2c.LFSETUP] = lfsetup

        if not args.compute_lf_only:

            cmd += lf_recover_opts
            # add lat detector
            lats_idx = 0
            cmd += ['-num_LATs', lats_idx+1]
            cmd += j2c.ek_lat_detect(lats_idx, threshold=-20, verbose=True)

            # add sentinel detector
            #cmd += j2c.sentinel(stimuli, lats_idx, 0, 200)

            # add stimulus
            stim_cmd, stim_idx = j2c.stim_sec_to_carp(job, stimuli, lfsetup, args.rmode, output_dir,
                                                      dry=args.dry, model_name=model_name, model_format=model_format,
                                                      ucc_file=ucc_file)

            cmd += ['-num_stim', stim_idx] + stim_cmd

            # add electrodes
            #elec_cmd, elec_idx = j2c.elec_sec_to_carp(job, elecs, stim_idx, act_elecs)
            #cmd += ['-num_stim', elec_idx] + stim_cmd + elec_cmd

            job.carp(cmd)

            carp_filename = os.path.join(output_dir, 'parameters.{}.par'.format(run_suffix))
            j2c.write_carp_par_file(carp_filename, cmd)

        plan_filename = os.path.join(output_dir, 'plan.{}.json'.format(run_suffix))
        j2c.save_simulation_plan(plan, plan_filename)


if __name__ == '__main__':
    run()
