#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
Apply a transformation matrix (stored to a yaml file) onto point data.
"""

epilog = """
(c) Anton J Prassl <anton.prassl@medunigraz.at> and
    Gernot Plank <gernot.plank@medunigraz.at>
"""

import os
import sys
import argparse
import numpy as np
import shutil
import yaml

import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite


def myparser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     epilog=epilog)

    parser.add_argument('--trfm',
                        type=valid_file,
                        default=None,
                        help='affine transformation file')

    single = parser.add_argument_group('Single File Options')
    single.add_argument('--src-pts',
                        type=valid_file,
                        default=None,
                        help='Source points data to be transformed.')

    single.add_argument('--trfm-pts',
                        type=str,
                        default=None,
                        help='Output transformed points data to file (default: <src-pts>.trfm.pts).')

    multi = parser.add_argument_group('Entire Directory Options')
    multi.add_argument('--src-dir',
                        type=valid_dir,
                        default=None,
                        help='Choose entire directory from which source points data will be transformed.')

    multi.add_argument('--trfm-dir',
                        type=str,
                        default=None,
                        help='Output transformed points data into directory (default: current working directory).')

    return parser


def valid_file(filename: str):
    try:
        assert os.path.isfile(filename)
    except AssertionError:
        sys.exit('Could not find {}. Aborting...'.format(filename))
    return filename


def valid_dir(directory: str):
    try:
        assert os.path.exists(directory)
    except AssertionError:
        sys.exit('Could not find {}. Aborting...'.format(directory))
    return directory

def read_trfm(trfmf):
    # read transformation and convert to 4x4 matrix
    print('Reading affine transformation from {}.'.format(trfmf))

    trfm = None

    if trfmf is None:
        return trfm

    with open(trfmf, 'r') as fp:
        trfm_str = yaml.safe_load(fp)
        trfm = np.fromstring(trfm_str['affine transform'], sep=' ', dtype=float)
        trfm = trfm.reshape((4, 4))

    return trfm


def apply_trfm(pts, trfm):
    pts_trfm = np.zeros(np.shape(pts))
    for i, pt in enumerate(pts):
        pt_ext = np.append(pt, 1.0)
        pt_trfm = trfm.dot(pt_ext)[:3]
        pts_trfm[i][:] = pt_trfm

    return pts_trfm


def main(opts):

    # read affine transform file
    trfm = read_trfm(opts.trfm)

    # collect list of point files to be transformed
    src_ptsf_lst = list()
    trfm_ptsf_lst = list()

    # handling a src directory of point files
    if opts.src_dir:
        # collect all point files
        for root, dirs, files in os.walk(opts.src_dir):
            for filename in files:
                if not filename.endswith('.pts'):
                    continue
                src_ptsf = os.path.join(os.path.join(root, filename))
                src_ptsf_lst.append(src_ptsf)

                # assemble output file
                trfm_ptsf = os.path.join(root, filename.replace('.pts', '.trfm.pts'))
                trfm_ptsf = trfm_ptsf.replace(opts.src_dir, '').lstrip(os.path.sep)
                if opts.trfm_dir is None:
                    # create export structure relative to current working directory
                    trfm_ptsf = os.path.join(os.getcwd(), trfm_ptsf)
                else:
                    # create export structure relative to given output directory
                    trfm_ptsf = os.path.join(os.path.abspath(opts.trfm_dir), trfm_ptsf)
                trfm_ptsf_lst.append(trfm_ptsf)
    # handling single points file
    elif opts.src_pts:
        src_ptsf_lst.append(opts.src_pts)
        if opts.trfm_pts:
            trfm_ptsf_lst.append(opts.trfm_pts)
        else:
            trfm_ptsf_lst.append(opts.src_pts.replace('.pts', '.trfm.pts'))
    else:
        print('No src point file or directory specified! Exiting...')
        return

    #
    # --- process collected files ---------------------------------------------
    # + apply transformation
    # + export files
    for idx, src_ptsf in enumerate(src_ptsf_lst):
        # get related output filename
        trfm_ptsf = trfm_ptsf_lst[idx]

        # read source points
        print('\n\nReading source points file {}.'.format(src_ptsf))
        src_pts = mshread.read_points(src_ptsf)

        # apply transform
        print('Apply transform {} to {}.'.format(opts.trfm, src_ptsf))
        src_pts_trfm = apply_trfm(src_pts, trfm)

        # write transformed points to file
        print('Writing transformed data points to {}'.format(trfm_ptsf))
        # src_pts_trfm_flat = src_pts_trfm.flatten()

        # create output directory if necessary
        if not os.path.exists(os.path.dirname(trfm_ptsf)):
            os.makedirs(os.path.dirname(trfm_ptsf), exist_ok=True)

        # writing carp_txt as mshwrite cannot export .belem files!
        mshwrite.write_points(trfm_ptsf.replace('.pts', '.pts'), src_pts_trfm)
        if os.path.exists(src_ptsf.replace('.pts', '.elem')):
            shutil.copy(src_ptsf.replace('.pts', '.elem'), trfm_ptsf.replace('.pts', '.elem'))
        if os.path.exists(src_ptsf.replace('.pts', '.lon')):
            shutil.copy(src_ptsf.replace('.pts', '.lon'), trfm_ptsf.replace('.pts', '.lon'))
    print('Done.')


if __name__ == '__main__':
    # build parser
    args = myparser().parse_args()

    main(args)
