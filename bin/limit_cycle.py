#!/usr/bin/env python3
import json
# ---( system imports )--------------------------
import os
import sys
from datetime import date
# ---( carputils imports )-----------------------
from carputils import tools

from carputils.forcepss import j2carp as j2c
from carputils.forcepss import prepace as prp


EXAMPLE_DESCRIPTIVE_NAME = 'Prepacing and validation of tissue/organ models'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Matthias Gsell <matthias.gsell@medunigraz.az>'
EXAMPLE_DIR = os.path.dirname(__file__)

CLEAN_PATTERN = r'^(\d{4}-)|(imp_)|(tunecv_)'


def argument_parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp = parser.add_argument_group('Experiment selection and general options')
    exp.add_argument('--plan', type=str, required=True, default='plan.json',
                     help='Simulation plan description in .json format.')
    exp.add_argument('--configurations', type=str, default=None,
                     help='Describe tissue model in terms of labels association.')
    exp.add_argument('--functions', type=str, default=None,
                     help='Describe tissue model in terms of functions association.')
    exp.add_argument('--electrodes', type=str, default=None,
                     help='List of electrode descriptions in json format. This is optional, electrodes '
                          'may be listed also in the "electrodes" section of the experiment description.')
    exp.add_argument('--protocols', type=str, required=True, default=None,
                     help='List of protocol descriptions in json format. This is optional, protocols may '
                          'be listed also in the "protocols" section of the experiment description.')

    exp.add_argument('--cohort', type=str, default=None,
                     help='Directory storing cohort of all cases belonging to a given study.')
    exp.add_argument('--caseID', type=str, default=None,
                     help='Pick a specific case from the cohort directory.')
    exp.add_argument('--mesh', type=str, default=None,
                     help='Path and basename of mesh to be prepaced (/path_to_mesh_dir/mesh_base_name).')
    exp.add_argument('--etagsname', type=str, default=None,
                     help='Path to an alternative element tags file (*.tag, *.btag).')
    exp.add_argument('--create-split', action='store_true', default=False,
                     help='Force re-creation of the splitting file.')

    # selection of split file and tags file at the cmd line currently not supported.
    exp.add_argument('--stage', choices=('gen-lat', 'lim-cyc', 'validate'), default='lim-cyc',
                     help='Select stage: tune for velocities, limit cycle or validate prepaced state.')
    exp.add_argument('--protocol', type=str, default=None,
                     help='Pick a specific pre-pacing protocol from protocol list '
                          'provided through --protocols, or within a "protocols" section '
                          'in --experiment.')
    exp.add_argument('--update', action='store_true', help='Update LAT vector in experiment description')

    # generation of lat args
    gl_args = parser.add_argument_group('LAT-generation specific options')
    gl_args.add_argument('--gen-lat', choices=('ek', 'rd'), default='ek',
                         help='Generate an activation sequence to create a LAT vector. '
                              'Choices are: (ek): use eikonal method to create an LAT vector; '
                              '(rd): use reaction-diffusion simulation to create an LAT vector.')
    gl_args.add_argument('--lat-name', type=str, default=None, help='Provide file name for storing lat vector.')

    # prepacing stage args
    pp_args = parser.add_argument_group('Pre-pacing specific options')
    pp_args.add_argument('--lim-cyc', type=str, default='off', dest='prepace',
                         help='Select pre-pacing mode to find tissue/organ scale limit cycle. '
                              'Choices are: (off): use single cell initial state only; '
                              '(lat): use lat-defined activation sequence; (lat-n): use lat-defined '
                              'activation sequence + n (#cycles given by the protocol) full activation sequences '
                              'where n can be between 0 (use cycle number from protocol definition) up to 3 '
                              'to account for diffusion effects; (full): brute force, run full pre-pacing at '
                              'tissue scale, without any lat-based pre-pacing.')
    pp_args.add_argument('--lat', type=str, default=None,
                         help='Provide lat vector for pre-pacing.'
                              'Required for pre-pacing modes lat and lat+n, not needed for off and full.')

    pp_args.add_argument('--cycles', type=int, default=None,
                         help='Provide number of cycles to pre-pace.'
                              'If given, this overrules the cycle settings in the plan/protocol file.')

    val_args = parser.add_argument_group('Validation specific options')
    val_args.add_argument('--ref', choices=('off', 'lat', 'lat-n', 'full'), default='off',
                          help='Pick a reference to compare currently selected prepace mode.')

    return parser


# return mesh extension as function of run
def mesh_ext(rmode):
    return '_ek' if rmode == 'ek' else '_i'


# pick run mode 'rd' or 're'
def restart(job, meshname, cnf, fncs, elecs, pp_prtcl, args, mode, rmode, restart_chkpt, visual):

    split_file, use_split = j2c.generate_split_file(job, cnf, meshname,
                                                    force_recreation=args.create_split)
    # set up overall command line
    cmd = prp.setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode,
                            tags_file=args.tags_file, use_split=use_split,
                            split_file=split_file)
    simID = 'validate_pp_{}'.format(mode)

    # determine checkpoint timing
    pp_bcl = pp_prtcl[j2c.PROTOCOLS_BCL]
    t_chk = 0
    if mode == 'full':
        pp_cyc = pp_prtcl[j2c.PROTOCOLS_CYCLES]
        if args.cycles is not None:
            pp_cyc = args.cycles
        t_chk = pp_bcl * pp_cyc

    # determine checkpoint to restart from
    cmd += ['-start_statef', restart_chkpt]
    cmd += ['-simID', simID, '-experiment', 0]

    # simulate one bcl
    cmd += ['-tend', t_chk + pp_bcl]
    if visual:
        cmd += ['-gridout_i', 3, '-spacedt', 1]

    return cmd, simID


# create mesh ID
def mesh_ID(meshname):
    return os.path.basename(meshname)


# ensure consistent prepacing checkpoint filenames for restarting
def chkpt_file_ID(prefix, ID, gen_mode, t_chk, bcl):
    return '{}_{}_pp_{}_bcl_{:.1f}_tstamp_{:.1f}'.format(prefix, ID, gen_mode, t_chk, bcl)


# ensure consistent lat filenames
def lat_file_ID(prefix, ID, gen_mode, ext):
    return '{}_{}_{}.{}'.format(prefix, ID, gen_mode, ext)


def get_mesh(mesh, cohort, caseID):
    if mesh:
        return mesh
    elif cohort and caseID:
        return '{}/{}/{}'.format(cohort, caseID, caseID)
    else:
        print('\n\nNo mesh specified, use either mesh or cohort+caseID arguments.\n')
        sys.exit()


def stage_gen_lat_from_args(job, plan, args):
    confs = plan.get(j2c.CONFIGURATIONS)
    funcs = plan.get(j2c.FUNCTIONS)
    elecs = plan.get(j2c.ELECTRODES)
    solver = plan.get(j2c.SOLVER)
    prtcls = plan.get(j2c.PROTOCOLS)

    tuning_dir = plan.get(j2c.j2cKey.TUNING_DIR, j2c.DEFAULT_TUNING_DIR)
    init_prfx = j2c.get_init_dir(tuning_dir, args.update)
    pp_prtcl = prtcls.get(args.protocol)
    mesh = get_mesh(args.mesh, args.cohort, args.caseID)
    protocol = args.protocol
    gen_lat = args.gen_lat
    visual = args.visualize
    split_file, use_split = j2c.generate_split_file(job, plan, mesh, force_recreation=args.create_split)

    # generate a new LAT vector if required
    if args.lat_name:
        lat_file = os.path.join(init_prfx, args.lat_name)
    else:
        lat_pfix = '{}_act_seq'.format(protocol)
        lat_file = os.path.join(init_prfx, lat_file_ID(lat_pfix, mesh_ID(mesh), gen_lat, 'dat'))
    print('Generating {}-based lat vector file {}'.format(gen_lat, lat_file))
    cmd_lat, tmp_lat, simID = prp.generate_lat(mesh, confs, funcs, elecs, solver, pp_prtcl, args.gen_lat,
                                               lat_file, visual, use_split=use_split, split_file=split_file,
                                               tags_file=args.etagsname)
    # generate lat vector
    job.carp(cmd_lat)

    # check whether lats file has been properly created
    print('\nCopying LAT vector')
    job.cp(tmp_lat, lat_file)

    # update LAT vector in experiment description
    if not args.dry:
        # update path to lat file in experiment protocols section
        prtcl = prtcls.get(args.protocol)
        if prtcl is not None:
            prtcl[j2c.PROTOCOLS_LATFILE] = lat_file
            j2c.update_exp_desc(args.update, args.plan, plan)

        if args.protocols:
            dct = j2c.load_simulation_plan(args.protocols, verbose=True)
            prtcls = dct.get(j2c.PROTOCOLS)
            p = prtcls.get(args.protocol)
            if p is not None:
                p[j2c.PROTOCOLS_LATFILE] = lat_file
                j2c.update_exp_desc(args.update, args.protocols, dct)

    if visual:
        lat_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(args.gen_lat))
        job.meshalyzer(lat_mesh, lat_file)


def stage_prepace_from_args(job, plan, args):
    confs = plan.get(j2c.CONFIGURATIONS)
    funcs = plan.get(j2c.FUNCTIONS)
    elecs = plan.get(j2c.ELECTRODES)
    solver = plan.get(j2c.SOLVER, j2c.DEFAULT_SOLVER)
    prtcls = plan.get(j2c.PROTOCOLS)

    tuning_dir = plan.get(j2c.j2cKey.TUNING_DIR, j2c.DEFAULT_TUNING_DIR)
    init_prfx = j2c.get_init_dir(tuning_dir, args.update)
    pp_prtcl = prtcls.get(args.protocol)
    if args.cycles is not None:
        pp_prtcl['cycles'] = args.cycles
    mesh = get_mesh(args.mesh, args.cohort, args.caseID)
    protocol = args.protocol
    gen_lat = args.gen_lat
    prepace = args.prepace
    lat = args.lat
    visual = args.visualize

    # prepacing using lat, make sure we have a proper lat vector
    pp_lat_file = pp_prtcl.get(j2c.PROTOCOLS_LATFILE)
    lat_file = None
    if prepace.startswith('lat'):
        # we need an activation lat vector
        if lat is not None:
            # command line provided gets precedence
            lat_file = lat
        elif pp_lat_file is not None:
            # experiment description provides lat
            # check whether given lat file exists
            if os.path.exists(pp_lat_file):
                lat_file = pp_lat_file
            else:
                print('\nPrepacing lat file {} does not exist.'.format(pp_lat_file))

        if lat_file is None:
            # we don't have a lat file, need to generate one
            print('\n\n')
            print('LAT-based prepacing requires LAT vector which is not provided.')
            print(' 1) Provide lat file using --lat')
            print(' 2) Or generate lat file first using --gen-lat ek or rd.\n')
            sys.exit()

    # before running, check all specified initial states are there
    if not j2c.check_initial_state_vectors(funcs, True):
        # some initial state vectors are missing
        print('\nMissing initial state vectors, exiting!\n\n')
        sys.exit()

    split_file, use_split = j2c.generate_split_file(job, plan, mesh, force_recreation=args.create_split)

    # ready to run prepacing to create initial checkpoint for restarting
    print('Run prepacing in mode {}'.format(prepace))
    propagation = pp_prtcl.get('propagation')
    cmd_pp, chkptf, t_chkpts, simID = prp.prepace(mesh, confs, funcs, elecs, pp_prtcl, prepace,
                                                  lat_file, propagation, visual, tags_file=args.etagsname,
                                                  use_split=use_split, split_file=split_file)
    if propagation != 'ek':
        cmd_pp += j2c.add_slv_settings(solver)
    # run
    job.carp(cmd_pp)
    # copy checkpoints to init directory for restarting
    for t_chk in t_chkpts:
        print('\nCopying prepacing checkpoint file')
        chkpt_file = os.path.join(simID, chkptf+'.{:.1f}'.format(t_chk)+'.roe')
        chkpt_file_name = chkpt_file_ID(protocol, mesh_ID(mesh), prepace, pp_prtcl.get(j2c.PROTOCOLS_BCL), t_chk)
        rstrt_file = os.path.join(init_prfx, chkpt_file_name+'.roe')
        job.cp(chkpt_file, rstrt_file)

    # update path to restart file in experiment protocols section
    p = plan.get(j2c.PROTOCOLS)
    if p is not None and p.get(args.protocol):
        p[j2c.PROTOCOLS_RESTART] = rstrt_file

    if args.protocols:
        dct = j2c.load_simulation_plan(args.protocols, verbose=True)
        prtcls = dct.get(j2c.PROTOCOLS)
        p = prtcls.get(args.protocol)
        if p is not None:
            p[j2c.PROTOCOLS_RESTART] = rstrt_file

    if not args.dry and p is not None:
        if p.get(args.protocol):
            j2c.update_exp_desc(args.update, args.plan, plan)
        if args.protocols:
            j2c.update_exp_desc(args.update, args.protocols, dct)

    # summarize prepacing
    prp.print_prepace_summary(p, plan, args.prepace, args.lat, rstrt_file)
    if args.visualize:
        mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
        data = os.path.join(simID, 'vm.igb')
        job.meshalyzer(mesh, data)

    if not args.dry:
        # return handle to pre-paced checkpoint for restarting
        return rstrt_file


def check_input(plan, prtcl, pp_arg):
    # check whether configuration matches up with functional description
    confs = plan.get(j2c.CONFIGURATIONS)
    funcs = plan.get(j2c.FUNCTIONS)
    if not j2c.check_config(confs, funcs):
        return False

    # check if requested prepacing protocol is in list
    prtcls = plan.get(j2c.PROTOCOLS)
    if not prtcls.get(prtcl):
        print('\n\nRequested protocol {} not provided.'.format(prtcl))
        return False

    # check validity of prepace argument
    if pp_arg == 'off' or pp_arg.startswith('lat') or pp_arg == 'full':
        # separate check on lat-
        if pp_arg.startswith('lat-'):
            rep = pp_arg.split('lat-')[1]
            if rep.isdigit() and not 0 <= int(rep) <= 3:
                print('Cycle parameter n provided through --prepace lat-n must be in range 0-3.')
                return False
    else:
        print('Argument --prepace {} is out of range.'.format(pp_arg))
        return False
        
    return True


def jobID(args):
    return '{}_tissue_tuning'.format(date.today().isoformat())


@tools.carpexample(argument_parser, jobID, clean_pattern=CLEAN_PATTERN)
def run(args, job):
    # read tissue/organ configuration
    plan = j2c.load_simulation_plan(args.plan, confs_file=args.configurations, funcs_file=args.functions,
                                    elecs_file=args.electrodes, protocols_file=args.protocols,
                                    verbose=True)

    if args.protocol is None and (prtcls := plan.get(j2c.PROTOCOLS)) is not None:
        print('\nPROTOCOLS:\n----------')
        print('\n'.join(prtcls.keys()))
        sys.exit(0)

    if not check_input(plan, args.protocol, args.prepace):
        sys.exit(1)

    # ---------- GEN-LAT --------------------------------------------
    if args.stage == 'gen-lat':
        stage_gen_lat_from_args(job, plan, args)
    # ---------- PREPACE --------------------------------------------
    elif args.stage == 'lim-cyc':
        rstrt_chkpt = stage_prepace_from_args(job, plan, args)
    # ---------- VALIDATE -------------------------------------------
    elif args.stage == 'validate':
        print('\n\nValidate stage not developed yet.')
        sys.exit(0)

        # read in all electrode and relative timings used for prepacing
        elecs = plan[j2c.ELECTRODES]
        prtcls = plan[j2c.PROTOCOLS]
        pp_prtcl = prtcls[j2c.PROTOCOLS_PREPACE]
        pp_lat_file = pp_prtcl[j2c.PROTOCOLS_LATFILE]
        propagation = pp_prtcl.get(j2c.PROTOCOLS_PROPAGATION)
        mesh = plan['mesh']

        # pick prepacing checkpoint to restart from
        tuning_dir = plan.get(j2c.j2cKey.TUNING_DIR, j2c.DEFAULT_TUNING_DIR)
        chkpt_file_base = chkpt_file_ID('prepaced', mesh_ID(mesh), args.prepace, pp_prtcl.get(j2c.PROTOCOLS_BCL))
        chkpt_file = os.path.join(tuning_dir, chkpt_file_base)
        cmd_chk, simID = restart(job, mesh, cnf, fncs, elecs, pp_prtcl, args.prepace,
                                 propagation, chkpt_file, args.visualize)

        slv = plan.get(j2c.SOLVER)
        cmd_chk += j2c.add_slv_settings(slv)

        # before running, check all specified initial states are there
        # as we are restarting, we should remove initial state vectors anyways as this is encoded in the checktpoint
        if not j2c.check_initial_state_vectors(fncs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, exiting!\n\n')
            sys.exit()

        job.carp(cmd_chk)

        if args.visualize:
            rst_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
            rst_data = os.path.join(simID, 'vm.igb')
            job.meshalyzer(rst_mesh, rst_data)


if __name__ == '__main__':
    run()
