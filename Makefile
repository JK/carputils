default: all

all: cext2 cext3 models

cext2: build_cext.py
ifeq ($(shell which python2),)
	@echo "* could not find python2. Skipping build of cext2."
else
	@echo "* building carputils C extensions for Python2 ..." && python2 build_cext.py build_ext --inplace
endif
	@echo ""

cext3: build_cext.py
ifeq ($(shell which python3),)
	@echo "* could not find python3. Skipping build of cext3."
else
	@echo "* building carputils C extensions for Python3 ..." && python3 build_cext.py build_ext --inplace
endif
	@echo ""

clean:
	@echo "* cleaning up .."
	@(rm -rf build)
	@(rm -f carputils/cext/*.so)

doxygen:
	doxygen doxygen_doc/carputils.doxygen

status st:
	git status

update up:
	git pull

models: OPENCARP_SRC = $(shell ./bin/cusummary | grep "OPENCARP_SRC" | sed 's/OPENCARP_SRC: //' | sed 's/^[ \t]*//')
# Models are only generated if the path to the directory containing openCARP source files is provided in the settings file
models:
	@([ -f $(OPENCARP_SRC)/physics/ionics.h ] && echo "* generating model.ionic, model.activetension and model.mechanics" && cd carputils/model && python3 ./generate.py $(OPENCARP_SRC); cd ../..)
	@([ -f $(OPENCARP_SRC)/physics/ionics.h ] || echo "Skipping model generation: in order to generate models, please provide the path to the directory containing openCARP source files in carputils settings file.")
	
