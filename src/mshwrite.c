/*
 * author: Matthias A.F. Gsell
 * email: gsell.matthias@gmail.com
 * date: 03-01-2020
 */

// Disable deprecated API versions
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define ENDIANESS 0
#else
#define ENDIANESS 1
#endif

#define HEADER_SIZE 1024

PyDoc_STRVAR(doc_write_points,
    "write_points(filename:string, points:NumpyArray[float,?x3]) -> None\n\n"
    "Function to write openCARP points to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the points file (*.pts)\n"
    "  points   : numpy float-array of shape (N x 3)\n"
    "             array holding the point data\n"
);

// write points to a file
static PyObject *write_points(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *pnt_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &pnt_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(pnt_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(pnt_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(pnt_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(pnt_array);
  if (shape[1] != 3) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *pnt_data = (double*)PyArray_DATA(pnt_array);
  double *pnt = NULL;

  // write number of points
  fprintf(fp, "%ld\n", shape[0]);
  // write data
  for (int i=0; i<shape[0]; i++) {
    pnt = &pnt_data[3*i];
    fprintf(fp, "%lf %lf %lf\n", pnt[0], pnt[1], pnt[2]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_points_bin,
    "write_points_bin(filename:string, points:NumpyArray[float,?x3]) -> None\n\n"
    "Function to write openCARP points to a binary file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the points file (*.pts)\n"
    "  points   : numpy float-array of shape (N x 3)\n"
    "             array holding the point data\n"
);

// write points to a file
static PyObject *write_points_bin(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *pnt_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &pnt_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(pnt_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(pnt_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(pnt_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(pnt_array);
  if (shape[1] != 3) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "wb");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *pnt_data = (double*)PyArray_DATA(pnt_array);

  // convert to float data
  float *pnt_data_flt = (float*)malloc(shape[0]*shape[1]*sizeof(float));
  for (long int i=0; i<shape[0]*shape[1]; i++)
    pnt_data_flt[i] = (float)pnt_data[i];

  char header[HEADER_SIZE];  
  const int checksum = 666;
  memset(header, 0, sizeof(char)*HEADER_SIZE);
  sprintf(header, "%ld %d %d", shape[0], ENDIANESS, checksum);
  fwrite(header, sizeof(char), HEADER_SIZE, fp);
  fwrite(pnt_data_flt, sizeof(float), shape[0]*shape[1], fp);

  // free converted data
  free(pnt_data_flt);

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_UVCs,
    "write_UVCs(filename:string, UVCs:NumpyArray[float,?x4]) -> None\n\n"
    "Function to write openCARP UVCs to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the UVC file (*.uvc)\n"
    "  UVCs     : numpy float-array of shape (N x 4)\n"
    "             array holding the UVC data\n"
);

// write UVCs to a file
static PyObject *write_UVCs(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *uvc_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &uvc_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(uvc_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(uvc_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(uvc_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(uvc_array);
  if (shape[1] != 4) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 4) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *uvc_data = (double*)PyArray_DATA(uvc_array);
  double *uvc = NULL;

  // write number of UVCs
  fprintf(fp, "%ld\n", shape[0]);
  // write data
  for (int i=0; i<shape[0]; i++) {
    uvc = &uvc_data[4*i];
    fprintf(fp, "%lf %lf %lf %lf\n", uvc[0], uvc[1], uvc[2], uvc[3]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_elements,
    "write_elements(filename:string, type:NumpyArray[int32,N], tag:NumpyArray[int32,N], "    
    "offset:NumpyArray[int32,N+1], index:NumpyArray[int32:M], type) -> None\n\n"
    "Function to write openCARP elements to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the element file (*.elem)\n"
    "  type     : numpy int32-array of size N\n"
    "             array holding the type of each element\n"
    "  tag      : numpy int32-array of size N\n"
    "             array holding tag information for each element\n"
    "  offset   : numpy int32-array of size N+1\n"
    "             array holding offset indices for each element,\n"
    "             the vertex indices of the i-th element are\n"
    "             `index[offset[i]] ... index[offset[i+1]-1]`\n"
    "  index    : numpy float-array of size M\n"
    "             array holding the vertex indices for each element\n"
);

// write elementss to a file
static PyObject *write_elements(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *type_array = NULL;
  PyArrayObject *tag_array = NULL;
  PyArrayObject *offset_array = NULL;
  PyArrayObject *index_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!O!O!O!", &fname, &PyArray_Type, &type_array, &PyArray_Type, &tag_array,
                        &PyArray_Type, &offset_array, &PyArray_Type, &index_array))
    return NULL;

  // check if arrays are contiguous
  const int iscont[4] = {PyArray_ISCONTIGUOUS(type_array),
                         PyArray_ISCONTIGUOUS(tag_array),
                         PyArray_ISCONTIGUOUS(offset_array),
                         PyArray_ISCONTIGUOUS(index_array)};
  if (!iscont[0]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous type array expected!");
    return NULL;
  }
  if (!iscont[1]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous tag array expected!");
    return NULL;
  }
  if (!iscont[2]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous offset array expected!");
    return NULL;
  }
  if (!iscont[3]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous index array expected!");
    return NULL;
  }
  // get array data types
  const int dtype[4] = {PyArray_TYPE(type_array),
                        PyArray_TYPE(tag_array),
                        PyArray_TYPE(offset_array),
                        PyArray_TYPE(index_array)};
  if (dtype[0] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer type array expected!");
    return NULL;
  }
  if (dtype[1] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer tag array expected!");
    return NULL;
  }
  if (dtype[2] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer offset array expected!");
    return NULL;
  }
  if (dtype[3] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer index array expected!");
    return NULL;
  }
  // get array dimensions
  const int ndim[4] = {PyArray_NDIM(type_array),
                       PyArray_NDIM(tag_array),
                       PyArray_NDIM(offset_array),
                       PyArray_NDIM(index_array)};
  if (ndim[0] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional type array expected!");
    return NULL;
  }
  if (ndim[1] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional tag array expected!");
    return NULL;
  }
  if (ndim[2] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional offset array expected!");
    return NULL;
  }
  if (ndim[3] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional index array expected!");
    return NULL;
  }
  // get array shapes
  const npy_intp shape[4] = {PyArray_DIM(type_array, 0),
                             PyArray_DIM(tag_array, 0),
                             PyArray_DIM(offset_array, 0),
                             PyArray_DIM(index_array, 0)};
  if ((shape[1] != shape[0]) || (shape[2] != (shape[0]+1)))
    PyErr_SetString(PyExc_ValueError, "Error, inconsistent array shapes #1!");

  const int num_elm = shape[0];

  // get array data
  int *type_data = (int*)PyArray_DATA(type_array);
  int *tag_data = (int*)PyArray_DATA(tag_array);
  int *offset_data = (int*)PyArray_DATA(offset_array);
  int *index_data = (int*)PyArray_DATA(index_array);

  if (offset_data[num_elm] != shape[3])
    PyErr_SetString(PyExc_ValueError, "Error, inconsistent array shapes #2!");

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  fprintf(fp, "%d\n", num_elm);
  for (int i=0; i<num_elm; i++) {    
    int type = type_data[i];
    int *idx = &index_data[offset_data[i]];
    int tag = tag_data[i];
    if (type == 0)
      fprintf(fp, "Tt %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], tag);
    else if (type == 1)
      fprintf(fp, "Py %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], tag);
    else if (type == 2)
      fprintf(fp, "Oc %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], tag);
    else if (type == 3)
      fprintf(fp, "Pr %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], tag);
    else if (type == 4)
      fprintf(fp, "Pr %d %d %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], idx[6], idx[7], tag);
    else if (type == 5)
      fprintf(fp, "Tr %d %d %d %d\n", idx[0], idx[1], idx[2], tag);
    else if (type == 6)
      fprintf(fp, "Qd %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], tag);
    else {
      // close file
      fclose(fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, element %d, unknwon element type '%d'!", i+1, type);
      return NULL;
    }
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_elements_bin,
    "write_elements_bin(filename:string, type:NumpyArray[int32,N], tag:NumpyArray[int32,N], "    
    "offset:NumpyArray[int32,N+1], index:NumpyArray[int32:M], type) -> None\n\n"
    "Function to write openCARP elements to a binary file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the element file (*.belem)\n"
    "  type     : numpy int32-array of size N\n"
    "             array holding the type of each element\n"
    "  tag      : numpy int32-array of size N\n"
    "             array holding tag information for each element\n"
    "  offset   : numpy int32-array of size N+1\n"
    "             array holding offset indices for each element,\n"
    "             the vertex indices of the i-th element are\n"
    "             `index[offset[i]] ... index[offset[i+1]-1]`\n"
    "  index    : numpy int32-array of size M\n"
    "             array holding the vertex indices for each element\n"
);

// write elementss to a file
static PyObject *write_elements_bin(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *type_array = NULL;
  PyArrayObject *tag_array = NULL;
  PyArrayObject *offset_array = NULL;
  PyArrayObject *index_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!O!O!O!", &fname, &PyArray_Type, &type_array, &PyArray_Type, &tag_array,
                        &PyArray_Type, &offset_array, &PyArray_Type, &index_array))
    return NULL;

  // check if arrays are contiguous
  const int iscont[4] = {PyArray_ISCONTIGUOUS(type_array),
                         PyArray_ISCONTIGUOUS(tag_array),
                         PyArray_ISCONTIGUOUS(offset_array),
                         PyArray_ISCONTIGUOUS(index_array)};
  if (!iscont[0]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous type array expected!");
    return NULL;
  }
  if (!iscont[1]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous tag array expected!");
    return NULL;
  }
  if (!iscont[2]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous offset array expected!");
    return NULL;
  }
  if (!iscont[3]) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous index array expected!");
    return NULL;
  }
  // get array data types
  const int dtype[4] = {PyArray_TYPE(type_array),
                        PyArray_TYPE(tag_array),
                        PyArray_TYPE(offset_array),
                        PyArray_TYPE(index_array)};
  if (dtype[0] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer type array expected!");
    return NULL;
  }
  if (dtype[1] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer tag array expected!");
    return NULL;
  }
  if (dtype[2] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer offset array expected!");
    return NULL;
  }
  if (dtype[3] != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer index array expected!");
    return NULL;
  }
  // get array dimensions
  const int ndim[4] = {PyArray_NDIM(type_array),
                       PyArray_NDIM(tag_array),
                       PyArray_NDIM(offset_array),
                       PyArray_NDIM(index_array)};
  if (ndim[0] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional type array expected!");
    return NULL;
  }
  if (ndim[1] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional tag array expected!");
    return NULL;
  }
  if (ndim[2] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional offset array expected!");
    return NULL;
  }
  if (ndim[3] != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional index array expected!");
    return NULL;
  }
  // get array shapes
  const npy_intp shape[4] = {PyArray_DIM(type_array, 0),
                             PyArray_DIM(tag_array, 0),
                             PyArray_DIM(offset_array, 0),
                             PyArray_DIM(index_array, 0)};
  if ((shape[1] != shape[0]) || (shape[2] != (shape[0]+1)))
    PyErr_SetString(PyExc_ValueError, "Error, inconsistent array shapes #1!");

  const int num_elm = shape[0];

  // get array data
  int *type_data = (int*)PyArray_DATA(type_array);
  int *tag_data = (int*)PyArray_DATA(tag_array);
  int *offset_data = (int*)PyArray_DATA(offset_array);
  int *index_data = (int*)PyArray_DATA(index_array);

  if (offset_data[num_elm] != shape[3])
    PyErr_SetString(PyExc_ValueError, "Error, inconsistent array shapes #2!");

  // open file
  FILE *fp = fopen(fname, "wb");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // write header
  char header[HEADER_SIZE];  
  const int checksum = 666;
  memset(header, 0, sizeof(char)*HEADER_SIZE);
  sprintf(header, "%d %d %d", num_elm, ENDIANESS, checksum);
  fwrite(header, sizeof(char), HEADER_SIZE, fp);

  int elm[10]; // 0..type, 1-N..indices, (N+1)..tag
  for (int i=0; i<num_elm; i++) {    
    int type = type_data[i];
    int *idx = &index_data[offset_data[i]];
    int tag = tag_data[i];
    if (type == 0) {
      elm[0] = 0; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = idx[3]; 
      elm[5] = tag;
      fwrite(elm, sizeof(int), 6, fp);
    }
    else if (type == 1) {
      elm[0] = 3; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = idx[3]; 
      elm[5] = idx[4]; 
      elm[6] = tag;
      fwrite(elm, sizeof(int), 7, fp);
    }
    else if (type == 2) {
      // close file
      fclose(fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, element %d, octaeder elements not supported!", i+1);
      return NULL;
    }
    else if (type == 3) {
      elm[0] = 4; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = idx[3]; 
      elm[5] = idx[4]; 
      elm[6] = idx[5]; 
      elm[7] = tag;
      fwrite(elm, sizeof(int), 8, fp);
    }
    else if (type == 4) {
      elm[0] = 1; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = idx[3]; 
      elm[5] = idx[4]; 
      elm[6] = idx[5]; 
      elm[7] = idx[6]; 
      elm[8] = idx[7]; 
      elm[9] = tag;
      fwrite(elm, sizeof(int), 10, fp);
    }
    else if (type == 5) {
      elm[0] = 6; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = tag;
      fwrite(elm, sizeof(int), 5, fp);
    }
    else if (type == 6) {
      elm[0] = 5; 
      elm[1] = idx[0]; 
      elm[2] = idx[1]; 
      elm[3] = idx[2]; 
      elm[4] = idx[3]; 
      elm[5] = tag;
      fwrite(elm, sizeof(int), 6, fp);
    }
    else {
      // close file
      fclose(fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, element %d, unknwon element type '%d'!", i+1, type);
      return NULL;
    }
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_fibers,
    "write_fibers(filename:string, fibers:NumpyArray[float,?x{3,6}]) -> None\n\n"
    "Function to write openCARP fibers to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the fibers file (*.lon)\n"
    "  fibers   : numpy float-array of shape (N x 3) or (N x 6)\n"
    "             array holding the fiber data\n"
);

// write fibers to a file
static PyObject *write_fibers(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *lon_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &lon_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(lon_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(lon_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(lon_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(lon_array);
  if ((shape[1] != 3) && (shape[1] != 6)) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) or (n x 6) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *lon_data = (double*)PyArray_DATA(lon_array);
  double *lon = NULL;

  // write fiber data
  if (shape[1] == 3) { // dimension 1
    // write fiber dimension
    fputs("1\n", fp);
    // write data
    for (int i=0; i<shape[0]; i++) {
      lon = &lon_data[3*i];
      fprintf(fp, "%lf %lf %lf\n", lon[0], lon[1], lon[2]);
    }
  }
  else {  // dimension 2
    // write fiber dimension
    fputs("2\n", fp);
    // write data
    for (int i=0; i<shape[0]; i++) {
      lon = &lon_data[6*i];
      fprintf(fp, "%lf %lf %lf %lf %lf %lf\n", lon[0], lon[1], lon[2], lon[3], lon[4], lon[5]);
    }
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_fibers_bin,
    "write_fibers_bin(filename:string, fibers:NumpyArray[float,?x{3,6}]) -> None\n\n"
    "Function to write openCARP fibers to a binary file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the fibers file (*.blon)\n"
    "  fibers   : numpy float-array of shape (N x 3) or (N x 6)\n"
    "             array holding the fiber data\n"
);

// write fibers to a binary file
static PyObject *write_fibers_bin(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *lon_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &lon_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(lon_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(lon_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(lon_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(lon_array);
  if ((shape[1] != 3) && (shape[1] != 6)) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) or (n x 6) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "wb");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *lon_data = (double*)PyArray_DATA(lon_array);

  // convert to float data
  float *lon_data_flt = (float*)malloc(shape[0]*shape[1]*sizeof(float));
  for (long int i=0; i<shape[0]*shape[1]; i++)
    lon_data_flt[i] = (float)lon_data[i];

  char header[HEADER_SIZE];
  const int checksum = 666;
  memset(header, 0, sizeof(char)*HEADER_SIZE);
  const int num_fib = (shape[1] / 3);
  sprintf(header, "%d %ld %d %d", num_fib, shape[0], ENDIANESS, checksum);
  fwrite(header, sizeof(char), HEADER_SIZE, fp);
  fwrite(lon_data, sizeof(float), shape[0]*shape[1], fp);

  // free converted data
  free(lon_data_flt);

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_scalar_data,
    "write_scalar_data(filename:string, data:NumpyArray[float,?]) -> None\n\n"
    "Function to write openCARP scalar data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  data     : numpy float-array\n"
    "             array holding the scalar data\n"
);

// write scalar data to a file
static PyObject *write_scalar_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *scl_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &scl_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(scl_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(scl_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(scl_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(scl_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *scl_data = (double*)PyArray_DATA(scl_array);
  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%lf\n", scl_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_scalar_data_int,
    "write_scalar_data_int(filename:string, data:NumpyArray[int32,?]) -> None\n\n"
    "Function to write openCARP scalar integer data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  data     : numpy int32-array\n"
    "             array holding the scalar data\n"
);

// write scalar integer data to a file
static PyObject *write_scalar_data_int(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *scl_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &scl_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(scl_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(scl_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(scl_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(scl_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  int *scl_data = (int*)PyArray_DATA(scl_array);
  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%d\n", scl_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_vector_data,
    "write_vector_data(filename:string, data:NumpyArray[float,?x3]) -> None\n\n"
    "Function to write openCARP vector data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.vec)\n"
    "  data     : numpy float-array of shape (N x 3)\n"
    "             array holding the vector data\n"
);

// write vector data to a file
static PyObject *write_vector_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *vec_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &vec_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(vec_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(vec_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(vec_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(vec_array);
  if (shape[1] != 3) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *vec_data = (double*)PyArray_DATA(vec_array);
  double *vec = NULL;

  // write vector data
  for (int i=0; i<shape[0]; i++) {
    vec = &vec_data[3*i];
    fprintf(fp, "%lf %lf %lf\n", vec[0], vec[1], vec[2]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_vertex_data,
    "write_vertex_data_int(filename:string, data:NumpyArray[int32,?], *, domain:string) -> None\n\n"
    "Function to write openCARP vertex data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.vtx)\n"
    "  data     : numpy int32-array\n"
    "             array holding the scalar data\n"
    "  domain   : string, optional, default='intra'\n"
    "             domain specification (usually 'intra' or 'extra')"
);

// write vertex data to a file
static PyObject *write_vertex_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *vtx_array = NULL;
  char *domain = "intra";
  static char *kwlist[] = {"", "", "domain", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "sO!|s", kwlist, &fname, &PyArray_Type, &vtx_array, &domain))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(vtx_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(vtx_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(vtx_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(vtx_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  int *vtx_data = (int*)PyArray_DATA(vtx_array);

  // write number of vertices and domain
  fprintf(fp, "%ld\n%s\n", shape[0], domain);

  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%d\n", vtx_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_element_btags,
    "write_element_btags(filename:string, data:NumpyArray[int32,?]) -> None\n\n"
    "Function to write openCARP binary element tag data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the btag file (*btag)\n"
    "  data     : numpy int32-array\n"
    "             array holding the tag data\n"
);

// write binary element tags to a file
static PyObject *write_element_btags(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *tag_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &tag_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(tag_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(tag_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(tag_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(tag_array);

  // open file
  FILE *fp = fopen(fname, "wb");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  int *tag_data = (int*)PyArray_DATA(tag_array);

  // write binary tag data
  char header[HEADER_SIZE];
  memset(header, 0, sizeof(char)*HEADER_SIZE);
  sprintf(header, "%ld %d", shape[0], ENDIANESS);
  fwrite(header, sizeof(char), HEADER_SIZE, fp);
  fwrite(tag_data, sizeof(int), shape[0], fp);

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

// define methods
static PyMethodDef MshWriteMethods[] = {
    {"write_points", (PyCFunction)write_points, METH_VARARGS|METH_KEYWORDS, doc_write_points},
    {"write_points_bin", (PyCFunction)write_points_bin, METH_VARARGS|METH_KEYWORDS, doc_write_points_bin},
    {"write_UVCs", (PyCFunction)write_UVCs, METH_VARARGS|METH_KEYWORDS, doc_write_UVCs},
    {"write_elements", (PyCFunction)write_elements, METH_VARARGS|METH_KEYWORDS, doc_write_elements},
    {"write_elements_bin", (PyCFunction)write_elements_bin, METH_VARARGS|METH_KEYWORDS, doc_write_elements_bin},
    {"write_fibers", (PyCFunction)write_fibers, METH_VARARGS|METH_KEYWORDS, doc_write_fibers},
    {"write_fibers_bin", (PyCFunction)write_fibers_bin, METH_VARARGS|METH_KEYWORDS, doc_write_fibers_bin},
    {"write_scalar_data", (PyCFunction)write_scalar_data, METH_VARARGS|METH_KEYWORDS, doc_write_scalar_data},
    {"write_scalar_data_int", (PyCFunction)write_scalar_data_int, METH_VARARGS|METH_KEYWORDS, doc_write_scalar_data_int},
    {"write_vector_data", (PyCFunction)write_vector_data, METH_VARARGS|METH_KEYWORDS, doc_write_vector_data},
    {"write_vertex_data", (PyCFunction)write_vertex_data, METH_VARARGS|METH_KEYWORDS, doc_write_vertex_data},
    {"write_element_btags", (PyCFunction)write_element_btags, METH_VARARGS|METH_KEYWORDS, doc_write_element_btags},
    {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(doc_mod_mshwrite, "Module providing several functions to write openCARP mesh related data to files.\n");

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef MshWriteModule = {
    PyModuleDef_HEAD_INIT,
    "mshwrite",       /* name of module */
    doc_mod_mshwrite, /* module documentation, may be NULL */
    -1,               /* size of per-interpreter state of the module,
                         or -1 if the module keeps state in global variables. */
    MshWriteMethods
};

PyMODINIT_FUNC PyInit_mshwrite(void)
{
    // initialize module
    PyObject *mod = PyModule_Create(&MshWriteModule);
    // import numpy module
    import_array();

    return mod;
}

#else

PyMODINIT_FUNC initmshwrite(void)
{
    // initialize module
    PyObject *mod = Py_InitModule3("mshwrite", MshWriteMethods, doc_mod_mshwrite);
    // import numpy module
    import_array();

    (void) mod;
}

#endif
