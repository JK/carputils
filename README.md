# carputils

carputils is a Python framework for generating and running openCARP
examples. carputils also provides functions for automatically comparing
simulation results against known solutions (regression testing).

## Installing

See [INSTALL.md](INSTALL.md)

## Contributing
You are here to help? Awesome, feel welcome and read [CONTRIBUTING.md](
https://opencarp.org/community/contribute) to learn how to ask questions
and how to work on something and our process for submitting merge requests to
the openCARP project (including carputils).
All members of our community are expected to follow our code of conduct:
[CODE_OF_CONDUCT.md](https://opencarp.org/community/code-of-conduct).
Please make sure you are welcoming and friendly in all of our spaces.

## Authors
See our list of contributors in
[CONTRIBUTORS.md](https://opencarp.org/community/contributors).

## Dependencies
carputils was built upon the open cardiac electrophysiology simulator
[openCARP](http://www.openCARP.org). The simulator calls as well as input
and output files are tailored for openCARP.

## License
carputils is released under the [Apache Software License](LICENSE.md),
Version 2.0 ("Apache 2.0").
