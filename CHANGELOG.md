# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/). carputils itself does not feature tagged
releases but a specific carputils version is included in each [openCARP releasae](https://opencarp.org/download/releases).

### Added
- Added `ginkgo` flavor
- Merged ForCEPSS, a framework for cardiac electrophysiology simulations standardization, into master (2023-12-05)
- CI pipeline for testing carputils installation on various OS (2022-02-16)
- New script for creating a metadata file for experiments (bin/generateMetadata)
- `--release-bundle` option for `run.py` to ask for the publicarion of the experiment on opencarp.org (2021-08-31)
- `--push-bundle` option for `run.py` to upload a bundle on a git repository (2021-07-21)
- Added HoreKa cluster to the list of machines (2021-11-10)
- Added `OPENCARP_SRC` parameter in `settings.yaml`, to indicate the path to openCARP source files. 
- `--bundle_output` option for `run.py` to include the outputs of the simulation in a bundle (2021-07-21)
- `--bundle` option for `run.py` to generate standalone bundle of experiment (2021-06-21)
- Makefile target `models` to auto-generate model.ionic, model.activetension and model.mechanics (2021-06-09)
- Added new experiment 02_tissue/21_induction_protocols. (2021-01-21)
- model/induceReentry.py

### Changed
- Modify the behavior of `--push-bundle` option to allow to erase previous bundles pushed to a repository. (2022-03-29)
- Change the restrictions on dependencies versions to improve compatibility and sustainability (2022-02-16)
- Use `REGRESSION_REF` from carputils `settings.yaml` for autotester (2021-11-29)
- `CONTRIBUTORS.md` is now `CONTRIBUTORS.yml`, which will also be used to generate the metadata for archives of the releases. (2020-11-10)
- New default values for `settings.yaml`, so that regression tests are easier to run.
- New order for `settings.yaml` discovery: working directory and user-local configuration are prioritized over carputils installation folder.
- The 'tuneCV' and 'restituteCV' scripts are now installed when installing carputils.

### Removed

### Fixed
- Description of IonicModel IDs (2024-01-11)
- `restituteCV` was not working because surf argument had no default. Set to 3D now (args.self = False)
- The dependency to the `pathlib` library was causing issues, the functions using it were replaced by an alternative
  which doesn't depend on `pathlib` anymore. (2021-09-01) 
