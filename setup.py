#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from setuptools import setup, find_packages, Extension
from setuptools.command.develop import develop
from setuptools.command.install import install
import os, sys, subprocess

import carputils


if sys.version_info.major > 2:
    PYTHON_CMD = 'python3'
    REQUIREMENTS_FILE = 'requirements.py3.txt'
else:
    PYTHON_CMD = 'python2'
    REQUIREMENTS_FILE = 'requirements.py2.txt'


class GetNumpyInclude(object):
    """Defer numpy.get_include() until numpy is installed."""

    def __str__(self):
        import numpy
        return numpy.get_include()

compile_args = ['-std=c99',
                '-Wno-unused-result',
                '-Wno-incompatible-pointer-types']

link_args = []

# Extension modules
mshread_ext = Extension('mshread', sources=['src/mshread.c'],
                        include_dirs=[GetNumpyInclude()],
                        extra_compile_args=compile_args,
                        extra_link_args=link_args)

mshwrite_ext = Extension('mshwrite', sources=['src/mshwrite.c'],
                         include_dirs=[GetNumpyInclude()],
                         extra_compile_args=compile_args,
                         extra_link_args=link_args)

mshutils_ext = Extension('mshutils', sources=['src/mshutils.c'],
                         include_dirs=[GetNumpyInclude()],
                         extra_compile_args=compile_args,
                         extra_link_args=link_args)

mshdata_ext = Extension('mshdata', sources=['src/mshdata.c'],
                         include_dirs=[GetNumpyInclude()],
                         extra_compile_args=compile_args,
                         extra_link_args=link_args)


# Read README.md
with open('README.md', 'r') as f:
    long_description = f.read()


# Read requirements.txt
with open(REQUIREMENTS_FILE, 'r') as f:
    lines = f.readlines()
install_requires = [line.strip() for line in lines if line]


# Add data for package
package_data={
        'carputils': ['resources/*',
                      'resources/options/*',
                      'resources/petsc_block_options/*',
                      'resources/petsc_options/*',
                      'resources/ginkgo_options/*',
                      'resources/trace/*',
                      '../bin/*']
}


# Add scripts to be installed
scripts=['bin/cusettings',
         'bin/cusummary',
         'bin/cuclean',
         'bin/carphelp',
         'bin/carptests',
         'bin/update-carp',
         'bin/tuneCV',
         'bin/restituteCV'
]


# Run post-install cusettings to generate settings.yaml
def generate_settings_yaml_file():
    print('Running cusettings...')
    try:
        cmd = [PYTHON_CMD, './bin/cusettings', os.path.expanduser('~/.config/carputils/settings.yaml')]
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError as e:
        print(e.output)

class PostDevelopCommand(develop):
    """Post-installation for development mode."""
    def run(self):
        develop.run(self)
        generate_settings_yaml_file()

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        install.run(self)
        generate_settings_yaml_file()

cmdclass = {
    'develop': PostDevelopCommand,
    'install': PostInstallCommand,
}


setup(name='carputils',
#      version=carputils.__version__,
      description='carputils is a Python framework for generating and running openCARP examples.',
      long_description=long_description,
      url='https://git.opencarp.org/openCARP/carputils',
      license='Apache 2.0',
      packages=find_packages(),
      package_data=package_data,
      scripts=scripts,
      cmdclass=cmdclass,
      zip_safe=False,
      setup_requires=['numpy>=1.14.5'], # for systems without python packages yet
      install_requires=install_requires,
      ext_package='carputils.cext',
      ext_modules=[mshread_ext, mshwrite_ext, mshutils_ext, mshdata_ext]
)
